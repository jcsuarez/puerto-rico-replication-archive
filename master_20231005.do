********************************************************************************
********************************************************************************
********************************************************************************
*  Master Script for 2023 September Submission of Suarez Serrato (AER, 2023)   *
*  Author: Eddie Yu															   *
*  Directions:																   *
*    - 1. Uncomment `0. Install Packages' for installation of packages.		   *
*    - 2. Comment out `2. Initial Build'. This uses raw NETS, QCEW data		   *
*		  non-existent to this particular replication archive.    			   *
*	 - 3. Run this script.													   *
********************************************************************************
********************************************************************************
********************************************************************************

global dropbox "/Users/jisangyu/JC's Data Emporium Dropbox/Eddie Yu/Puerto Rico/Replication2024"

********************************************************************************
********************************************************************************
* 0. Install Packages
********************************************************************************
********************************************************************************

// ssc install listtex, replace all 
// ssc install binscatter, replace all
// ssc install estout, replace all 
// ssc install cdfplot, replace all
// ssc install winsor, replace all
// ssc install reghdfe, replace all
// ssc install tuples, replace all
// ssc install coefplot, replace all 
// ssc install spmap, replace all 
// ssc install maptile, replace all 
// * Install ftools (remove program if it existed previously)
// cap ado uninstall ftools
// net install ftools, from("https://raw.githubusercontent.com/sergiocorreia/ftools/master/src/")
// * Install reghdfe
// cap ado uninstall reghdfe
// net install reghdfe, from("https://raw.githubusercontent.com/sergiocorreia/reghdfe/master/src/")
// * Install ivreg2, the core package
// cap ado uninstall ivreg2
// ssc install ivreg2
// * Finally, install this package
// cap ado uninstall ivreghdfe
// net install ivreghdfe, from(https://raw.githubusercontent.com/sergiocorreia/ivreghdfe/master/src/)
// ssc install reghdfe, replace all
// ssc install reg2hdfe, replace all 
// ssc install moremata, replace all
// ssc install tmpdir, replace all
// ssc install ranktest, replace all
// ssc install dataout, replace
// maptile_install using "http://files.michaelstepner.com/geo_county1990.zip"
// maptile_install using "http://files.michaelstepner.com/geo_state.zip"

********************************************************************************
********************************************************************************
* 1. Set Global Dropbox Paths
********************************************************************************
********************************************************************************

global output 		"$dropbox/Programs/output"
global data 		"$dropbox/Data"
global analysis 	"$dropbox/Programs"
global linkdata		"$dropbox/Programs/output/base_1995"
global xwalk		"$dropbox/Data/Cross Walk"
global qcewdata		"$dropbox/Data/QCEW"
global bea			"$dropbox/Data/BEA"
global consp		"$dropbox/Data/conspuma"
global additional 	"$dropbox/Data/data_additional"
global output_NETS 	"$dropbox/Programs/output/NETS_20171128"
global output_NETS2 "$dropbox/Programs/output/NETS_20171129_v2"
global output_NETS_control "$dropbox/Programs/output/NETS_control"
global WRDS			"$dropbox/Data/WRDS"
global ASM			"$dropbox/Data/ASM"
global SOI			"$dropbox/Data/SOI Data"
global irs 			"$dropbox/Data/IRS"
global ster			"$output/ster"

** Weight/Interaction Specifications
global pr_wgt 		""     											// Option: "[aw = wgt]"
global pra_control 	"" 										// Option: "i.year|pr_link_all" 
global pra_control2 ""												// Option: "pr_link_all"      
global pra_control_rob "" 											// Option: "cen_pr_link_a" 
global pra_control_inc ""											// Option: "_IyeaXpr_b*"


*** Start
cd "$analysis"
clear
set more off 
snapshot erase _all
estimates clear
	
*initializing ado files to format additional stats
sysdir set PERSONAL "$analysis/ado" // ensures adopath to refer to maptile geopgraphy
do "$analysis/jc_add_stats.ado"
do "$analysis/jc_stat_out.ado"

********************************************************************************
********************************************************************************
* 2. Initial Build (run before replication)
********************************************************************************
********************************************************************************

********** NETS Pr link build: Run on External Drive 
do "$analysis/0-build/nets_pr_extract.do"						// Extract firms with an establishment in PR, runs on external harddrive
do "$analysis/0-build/nets_pr_extract_panel_v2.do"				// Extract firms with an establishment in PR in panel format, runs on external harddrive
do "$analysis/0-build/nets_pr_extract_naic3.do"					// Extract firms with an establishment in PR, runs on external harddrive and includes all NAICS3 codes (and md, fd, and emp)
do "$analysis/0-build/nets_pr_extract_naic3_1993.do"			// Same as above, but additionally produces "pr_link_est_county_n3_93.dta", "pr_link_est_countyXindustry_n3_93.dta"
do "$analysis/0-build/nets_pr_extract_naic3_93_95firms.do"		// Same as above, but additionally produces "pr_link_est_county_n3_93_95f.dta", "pr_link_est_countyXindustry_n3_93_95f.dta"
do "$analysis/0-build/nets_pr_extract_emp.do"					// Extract firms with an establishment in PR, also saves employment

********** Build Data for NETS Controls: Run on External Drive 
do "$analysis/0-build/Nets_control_panel/get_pr_exposed_characteristics_region2.do"	// selects the 682 firms we care about and then from those selects
											// the actual state and insustries we care about (census region and 3 digit NAICS)
																						
do "$analysis/0-build/Nets_control_panel/get_HQs_controls_region2.do" 		// This code goes over all the estabs in the NETS and selects: 
										//	1- Those that are headquarters only (hqduns == duns definition)
										//	2- Those that are around before 1995 (in 1995 and 1990)
										//	3- Drops Industries and firms that are suspect (gov related duns and NAICS)
										//	4- Keeps Those in 3d NAICS-census region combos that match PR exposed firms 
										//	5- Saves the HQDuns for these firms 
																						
do "$analysis/0-build/Nets_control_panel/get_HQs_size_region2.do"		// This code goes over all the estabs in the NETS and selects: 
										// 1- All estabs linked to control firms (with HQs in list) 
										// 2- Gathers just employment and establishments in 1995
										// 3- collapses at hq level 
										// 4- merges all files and collapses again 
										// 5- keeps only those firms in the relevant size categories (emp and est quitiles)
										// 6- This reduces the number of potential HQ firms. 

do "$analysis/0-build/Nets_control_panel/get_control_panel_region2.do" 		// This code goes over all the estabs in the NETS and selects: 
										//	1- All estabs linked to control firms (with HQs in list by industry and size) 
										//	2- Gathers employment and establishment counts by year 
										//	3- Collapses at the hq level 
										//	4- merges all files and collapses again 
********** NETS FAKE Pr link build: Run on External Drive -- this seems irrelevant with any files
// do "$analysis/0-build/placebo_nets_shock.do" 					// Generates placebo shock of similar firms that are not exposed to S 936 


********************************************************************************
********************************************************************************
* 3. Build Data for Analysis
********************************************************************************
********************************************************************************

do "$analysis/0-build/percent_pharma.do" 	 					// creates county level pharma exposure variables (124 positive values)  
do "$analysis/0-build/county_level_data_all.do"   				// Import additional county-level data from various sources, saves "county_level_data_all.dta"
do "$analysis/0-build/county_manufacturing_emp_annual.do"		// Impots annual QCEW data, saves "county_manu_emp_ests.dta"
do "$analysis/0-build/compustat_merge_manual.do" 				// Merge existing NETS data with COMPUSTAT data
do "$analysis/0-build/compustat_ETR_calc.do" 					// Include ETR variables in compustat data
do "$analysis/0-build/compustat_ETR_calc_segments.do" 			// Merge compustat segments data with ETR calculation variables
do "$analysis/0-build/compustat_ETR_CAR_estimate.do" 			// Computes Cumulative Abnormal Returns (CARs) estimates from ETR variables
do "$analysis/0-build/construct_SOI_NAICS_xwalk.do" 			// create crosswalk between SOI industry codes and NAICS
do "$analysis/0-build/build_NETS.do" 							// creates Replication Data/interim/NETS_emp.dta
do "$analysis/0-build/qcew_extract.do"							// Extract QCEW files, produces "extract_qcew_1990_2012.dta"
do "$analysis/0-build/qcew_extract_naic3.do"					// same as above, but extract_naic3 keeps everything at 3 digit NAIC level and produces "extract_qcew_1990_2012_naic3.dta"
do "$analysis/0-build/clean_ASM.do" 							// Imports and cleans ASM data
do "$analysis/0-build/nafta_merge.do" 	 						// saves import_hakobyanlaren_nafta.dta with nafta exposure 
do "$analysis/0-build/CBP_firm_size_extract.do" 	 			// extracts firm size distribution 
do "$analysis/0-build/pr_link_est_county_addvars_d.do"			// Merge NETS and county-level data using the version that carries employment link measures as well, old version -_d
do "$analysis/0-build/county_pop_ests_annual.do" 				// Assembles annual estimates for working age population at the pid level, produces a single dataset with 3 vars: pid, year, and working age pop 15-64 "$additional/county population estimates/county_pop_ests.dta"
do "$analysis/0-build/welfare_import.do" 						// welfare transfers
do "$analysis/0-build/tradeable_naics.do" 						// cleans and import raw data from Mian and Sufi

********************************************************************************
********************************************************************************
* 4. SOI imputation to NETS & Compustat, PR Tax Haven Clean
********************************************************************************
********************************************************************************

do "$analysis/0-build/SOI_import.do" 							// Import some basic IRS SOI stats about US possessions tax credit
do "$analysis/0-build/pr_link_compustat_sample.do" 				// PR link made with only compustat sample and some relevant scalars
do "$analysis/1-descriptive/Industy_employment_2018_10_03.do"	// uses "extract_qcew_1990_2012_naic3.dta" and "$output_NETS/pr_extract_est.dta" to produce "$output_NETS/industry_cat_emp_sums_v2"

	/* We mostly use Narrow Comparison : drop bottom 60% of 1995 PPE Deciles */
do "$analysis/0-build/clean_compu_for_analysis_sizecutoff.do" 		/* drop if filter; creates compu_NETS_ETR_sizecutoff.dta */
																// also creates ET_post*, PR_post* interaction variables for long-difference
do "$analysis/0-build/clean_compu_for_analysis.do" /* drop if filter; creates compu_NETS_ETR.dta */
do "$analysis/0-build/impute_ETR_SOI.do" // use soi-naics cross walk to impute PR calibration numbers, produced SOI_imputed_ETR.dta
do "$analysis/0-build/merge_SOI_to_compu.do" // merge SOI_imputed_ETR.dta to compu_NETS_ETR_sizecutoff.dta, produces onestep_compu_SOI_clean.dta 
do "$analysis/0-build/merge_SOI_to_NETS.do" // merge SOI_imputed_ETR.dta to NETS, produces "NETS_emp_SOI_imputed.dta"
// then constructs DFL weights, post1 & post2 (long diff) produces "NETS_emp_SOI_DFL.dta"
do "$analysis/0-build/pr_treat_taxhaven_clean.do" // creates pr treatment variables data & tax haven usage data


********************************************************************************
********************************************************************************
* 5-1. Build replication .dta files for Main Figures & Tables 
*      (separate data file per figure or table)
********************************************************************************
********************************************************************************

do "$analysis/clean/figure1_clean.do" // prepares figure1_ab, figure1_c dta
do "$analysis/clean/figure2_clean.do"
do "$analysis/clean/figure3_clean.do"
do "$analysis/clean/figure4_clean.do"
do "$analysis/clean/figure5_clean.do"
do "$analysis/clean/figure6_clean.do"
do "$analysis/clean/figure7_clean_baseline.do"		// figure 7 uses 12 dtas, baseline also covers state trends and FE			
do "$analysis/clean/figure7_clean_conspuma.do"			
do "$analysis/clean/figure7_clean_czone.do"			
do "$analysis/clean/figure7_clean_PC.do"			
do "$analysis/clean/figure7_clean_empinst.do"			
do "$analysis/clean/figure7_clean_MD.do"			
do "$analysis/clean/figure7_clean_FD.do"			
do "$analysis/clean/figure7_clean_compu.do"
do "$analysis/clean/figure7_clean_syFE.do"			
do "$analysis/clean/figure7_clean_size.do"			
do "$analysis/clean/figure7_clean_93f.do"			
do "$analysis/clean/figure7_clean_9395f.do"
do "$analysis/clean/figure8_clean.do"
do "$analysis/clean/figure9_clean.do"
do "$analysis/clean/figure10_clean.do"
do "$analysis/clean/figure11_clean.do"


do "$analysis/clean/table1_clean.do"
do "$analysis/clean/table2_clean.do" // outputs table2_data_A, table2_data_B, table2_data_C
do "$analysis/clean/table3_clean.do" // outputs table3_data_A, table3_data_B, table2data_C
do "$analysis/clean/table4_clean_IRSBEAcounty.do"	// long difference IRS and BEA
do "$analysis/clean/table4_clean_QCEWcounty.do"		// long difference QCEW county
do "$analysis/clean/table4_clean_QCEWconspuma.do"	// long difference QCEW conspuma
do "$analysis/clean/table4_clean_QCEWczone.do"		// long difference QCEW czone
do "$analysis/clean/table5_clean.do"


********************************************************************************
********************************************************************************
* 5-2. Build replication .dta files for Appendix Figures & Tables 
*      (separate data file per figure or table)
********************************************************************************
********************************************************************************

/* figureA1 requires no data */
do "$analysis/clean/figureA2_clean.do"
do "$analysis/clean/figureA3_clean.do"
do "$analysis/clean/figureA4_clean.do"
/* figure A5 uses data created from figure2_clean.do (figure2_pr_link.dta, figure2_pr_link_ind.dta) */
do "$analysis/clean/figureA6_clean.do"
do "$analysis/clean/figureA7_clean.do"
do "$analysis/clean/figureA8_clean.do"
do "$analysis/clean/figureA9_clean.do" // uses compu_NETS_ETR_sizecutoff.dta
do "$analysis/clean/figureA10_clean.do" // uses compu_NETS_ETR_sizecutoff.dta
do "$analysis/clean/figureA11_clean.do" // uses compu_NETS_ETR_sizecutoff.dta
do "$analysis/clean/figureA12_clean.do" // uses compu_NETS_ETR_sizecutoff.dta
do "$analysis/clean/figureA13_clean.do"
/* figureA14 requires no data; just depreciateion calculation */
do "$analysis/clean/figureA15_clean.do"
do "$analysis/clean/figureA16_clean.do"
do "$analysis/clean/figureA17_clean.do"
do "$analysis/clean/figureA18_clean.do"
do "$analysis/clean/figureA19_clean.do" // creates two dataset, figureA19_data_A, figureA19_data_B.dta
/* figure A20 uses figure7_data_syFE.dta, created from figure7_clean.do */
/* figure A21 uses figure7_data_size.dta, created from figure7_clean.do */
/* figure A22 uses figure10_data.dta, created from figure10_clean.do */
/* figure A23 uses figure11_data.dta, created from figure11_clean.do */
do "$analysis/clean/figureA24_A25_clean.do" // creates figureA24_A25_data.dta
do "$analysis/clean/figureA26_clean.do" // creates figureA26_data_A, figureA26_data_C.dta


/* table A1 uses compu_NETS_ETR_sizecutoff.dta, no need for new data cleaning */
do "$analysis/clean/tableA2_clean.do"
do "$analysis/clean/tableA3_clean.do"
do "$analysis/clean/tableA4_clean.do"
/* Tables A5 - A8 just runs regressions from ster files, requires no data */
/* Table A9 uses figure7_data_*.dtas, no new data cleaning required */
do "$analysis/clean/tableA10_clean.do" /* creates tableA10_data to be used with figure7_data_*.dtas */
/* Table A11 uses figure7_data_empinst, no need for new data cleaning */
/* Table A12 uses figure7_data_MD, no need for new data cleaning */
/* Table A13 uses figure7_data_FD, no need for new data cleaning */
/* Table A14 uses figure7_data_compu, no need for new data cleaning */
/* Table A15 uses figure7_data_size, no need for new data cleaning */
/* Table A16 uses figure7_data_93f, no need for new data cleaning */
/* Table A17 uses figure7_data_9395f, no need for new data cleaning */
do "$analysis/clean/tableA18_clean.do" /* creates tableA18_data.dta */
do "$analysis/clean/tableA19_clean.do" /* creates tableA19_data.dta */
/* Table A20 uses figure7_data_9395f, no need for new data cleaning */
/* Table A21 uses figure8_data_A, no need for new data cleaning */
/* Table A22 uses figure8_data_B, no need for new data cleaning */
do "$analysis/clean/tableA23_A24_clean.do" /* creates tableA23_A24_data.dta */
/* Table A25 is already created from figure10.do */
/* Table A26 uses figure11_data, no need for new data cleaning */
/* Table A27 uses figure11_data, no need for new data cleaning */
/* Table A28 uses table5_data, no need for new data cleaning */
do "$analysis/clean/tableA29_clean.do" /* creates tableA29_data.dta */
do "$analysis/clean/tableA30_clean.do" /* creates tableA30_data.dta */
do "$analysis/clean/tableA31_clean.do" /* creates tableA31_data.dta */
/* Table A32 uses tableA31_data, no need for new data cleaning */
do "$analysis/clean/tableA33_A34_clean.do" /* creates tableA33_A34_data.dta */




********************************************************************************
********************************************************************************
* 6-1. Produce output Main Figures, Tables (11 Main Figures, 5 Main Tables)
********************************************************************************
********************************************************************************

do "$analysis/run/figure1.do" // produces figure 1 Panel A, B, C
do "$analysis/run/figure2.do"
do "$analysis/run/figure3.do" 
do "$analysis/run/figure4.do" 
do "$analysis/run/figure5.do" 
do "$analysis/run/figure6.do" 
do "$analysis/run/figure7.do" 
do "$analysis/run/figure8.do" 
do "$analysis/run/figure9.do" 
do "$analysis/run/figure10.do" // also produces tableA26.tex, figureA16.pdf
do "$analysis/run/figure11.do" // produces figure 11 Panel A-F.pdf


do "$analysis/run/table1.do" 
		*****************************************************
		** Compustat-related Regressions (Table 2, A5, A6)
		do "$analysis/run/Compu_Investment.do" 
		do "$analysis/run/Compu_ETR.do" 
		do "$analysis/run/Compu_IV.do" 
		*****************************************************
		
do "$analysis/run/table2.do"
		*****************************************************
		** NETS-related Regressions (Table 3, A7, A8)
		do "$analysis/run/NETS_Employment.do" 
		do "$analysis/run/NETS_ETR.do" 
		do "$analysis/run/NETS_IV.do" 
		*****************************************************
		
do "$analysis/run/table3.do"
do "$analysis/run/table4.do"
do "$analysis/run/table5.do"

********************************************************************************
********************************************************************************
* 6-2. Produce output Main Figures, Tables (26 Appendix Figures, 34 Appendix Tables)
********************************************************************************
********************************************************************************

do "$analysis/run/figureA1.do"
do "$analysis/run/figureA2.do"
do "$analysis/run/figureA3.do"
do "$analysis/run/figureA4.do" // Figure A4 Panel A is the same as Figure 2 Panel A; only outputs Figure A4 Panel B
do "$analysis/run/figureA5.do"
do "$analysis/run/figureA6.do"
do "$analysis/run/figureA7.do"
do "$analysis/run/figureA8.do"
do "$analysis/run/figureA9.do"
do "$analysis/run/figureA10.do"
do "$analysis/run/figureA11.do"
do "$analysis/run/figureA12.do"
do "$analysis/run/figureA13.do"
do "$analysis/run/figureA14.do"
do "$analysis/run/figureA15.do"
do "$analysis/run/figureA16.do"
do "$analysis/run/figureA17.do"
do "$analysis/run/figureA18.do"
do "$analysis/run/figureA19.do" 
do "$analysis/run/figureA20.do"
do "$analysis/run/figureA21.do"
/* figure A22 is created from figure10.do*/
do "$analysis/run/figureA23.do"
do "$analysis/run/figureA24_A25.do" // creates both figureA24.pdf, figureA25.pdf
do "$analysis/run/figureA26.do" 

do "$analysis/run/tableA1.do"
do "$analysis/run/tableA2.do"
do "$analysis/run/tableA3.do"
do "$analysis/run/tableA4.do"
do "$analysis/run/tableA5.do"
do "$analysis/run/tableA6.do"
do "$analysis/run/tableA7.do"
do "$analysis/run/tableA8.do"
do "$analysis/run/tableA9.do"
do "$analysis/run/tableA10.do"
do "$analysis/run/tableA11.do" /* this takes a long time (around 20 minutes) */
do "$analysis/run/tableA12.do"
do "$analysis/run/tableA13.do"
do "$analysis/run/tableA14.do"
do "$analysis/run/tableA15.do"
do "$analysis/run/tableA16.do"
do "$analysis/run/tableA17.do"
do "$analysis/run/tableA18.do"
do "$analysis/run/tableA19.do"
do "$analysis/run/tableA20.do"
do "$analysis/run/tableA21.do"
do "$analysis/run/tableA22.do"
do "$analysis/run/tableA23_A24.do"
/* table A25 is created from figure10.do*/
do "$analysis/run/tableA26.do"
do "$analysis/run/tableA27.do"
do "$analysis/run/tableA28.do"
do "$analysis/run/tableA29.do"
do "$analysis/run/tableA30.do" /* this takes around 16 minutes */
do "$analysis/run/tableA31.do"
do "$analysis/run/tableA32.do"
do "$analysis/run/tableA33_A34.do"
