// This file recreates Figure 6
// Author: eddie yu
// Date: 2023-10-16

/* Data sources:
NETS and QCEW
*/

clear all
set more off
snapshot erase _all 

*******************************************************
* Starting with the data for panel A
*******************************************************
use "$data/Replication Data/figure6_data_A", clear
 
// Second Define Command for ES 
capture program drop ES_graph
program define ES_graph
syntax,  level(real) [  yti(string) note(string) outname(string) ylab(string) ]

qui { 
mat b = e(b)
mat V = e(V) 
mat es_b = 0 
mat es_ci_l = 0 
mat es_ci_h = 0 
mat time = 0 

// for 95% CI 
if `level' == 99 { 
	local z= 2.6  
}
if `level' == 95 { 
	local z= 1.96  
}
// for 90% CI 
if `level' == 90 { 
	local z= 1.645 
} 
	
forval i = 1/23 { 
	mat time = (time,`i'-6+1995)
	mat es_b = (es_b, b[1,`i'])
	mat es_ci_l = (es_ci_l, b[1,`i']-`z'*sqrt(V[`i',`i'])   )
	mat es_ci_h = (es_ci_h, b[1,`i']+`z'*sqrt(V[`i',`i'])   )		
	}
mat es_b =  (es_b[1,2..6],0,es_b[1,7...])'
mat es_ci_l = (es_ci_l[1,2..6],0,es_ci_l[1,7...])'  
mat es_ci_h = (es_ci_h[1,2..6],0,es_ci_h[1,7...])' 
mat time = time[1,2...]' 

capture: drop es_b es_ci* time 
svmat es_b 
svmat time
svmat es_ci_l
svmat es_ci_h 
}

twoway (scatter es_b time, yline(0 , lcolor(black)) xline(1995, lcolor(black) lpattern(dash))) ///
    (rcap es_ci_h es_ci_l time,  lcolor(navy) lpattern(dash)) ///
	, plotregion(fcolor(white) lcolor(white)) ///
	graphregion(fcolor(white) lcolor(white)) ///
	xtitle("Year") xlab(1990(5)2012) ///
	legend(pos(6) r(1) label(1 "Estimate") label(2 "`level'% CI")) ///
	ytitle("`yti'") ///
	note("`note'") ylab("`ylab'")
graph export "`outname'.pdf", replace	

end 

// Graphs
xi i.year|pr_link_i , noomit
drop _IyeaXp*1995

eststo emp_2:  reghdfe emp_growth _IyeaX* [aw=wgt]  , a(i.pid i.year#i.industr ) cl(fips_state indust ) nocons
	estadd local hasiy "Yes"
	estadd local hascty "Yes"
	
ES_graph , level(95) yti("Effect S936 Exposure (IQR)") ///
note("Notes: (Emp{sub:ict}-Emp{sub:ic1995})/Emp{sub:ic1995}={&alpha}{sub:c}+{&gamma}{sub:it}+{&beta}{sub:t}S936 Exposure{sub:c}+{&epsilon}{sub:ict}. SE's clustered by State and Industry.") ///
outname("$output/Graphs/figure6_A") ylab("-.12(.02).04")

*******************************************************
* Starting with the data for panel B
*******************************************************
use "$data/Replication Data/figure6_data_B", clear
xi i.year|pr_link_i, noomit
drop _IyeaXp*1995

destring industry_code, replace

est clear 
eststo emp_2:  reghdfe emp_growth _IyeaX* [aw=base_emp] , a(Y=i.year#i.industr A=i.pid) cl(fips_state indust)
gen year_hat = Y - A + _b[_cons]					

* saving yearly PR interaction terms	
mat b = e(b)
svmat b

* Generating a single variable dependent on year that has the PR interaction effect
egen year_beta = rowfirst(b1)
forval i = 1/5  {
qui sum b`i'
local b_mean = r(mean)
replace b`i' = `b_mean' if b`i' ==. 
local year  = `i' +  1989
replace year_beta = b`i' if year == `year'
}
forval i = 6/22 {
qui sum b`i'
local b_mean = r(mean)
replace b`i' = `b_mean' if b`i' ==. 
local year  = `i' +  1990
replace year_beta = b`i' if year == `year'
}

capture: drop b1-b22
replace year_beta = 0 if year_beta == .

* Multiplying interactions by different levels of PR link to find effects
sum pr_link_i [fw=base_emp], d
 local pr_link_mean = r(mean)
 local pr_link_25 = r(p25)
 local pr_link_5 = r(p5)
 local pr_link_75 = r(p75)
 local pr_link_95 = r(p95)
 
gen  var_pr_link_mean = `pr_link_mean' * year_beta	
gen  var_pr_link_25 = `pr_link_25' * year_beta	
gen  var_pr_link_5 = `pr_link_5' * year_beta	
gen  var_pr_link_75 = `pr_link_75' * year_beta
gen  var_pr_link_95 = `pr_link_95' * year_beta		

* collapsing data to graph
collapse (mean) year_hat var_* [fw=base_emp], by(year)	
	
* rescalling	
replace year_hat = year_hat * 100
foreach Q of var  var_* {
replace `Q' = `Q' * 100 + year_hat
}
 
* graphing
 twoway (scatteri -10 1990.5   -10 1991.25 , recast(area) color(gs12) ) ///
     (scatteri 30 1990.5   30 1991.25 , recast(area) color(gs12) ) ///
     (scatteri -10 2001   -10 2002 , recast(area) color(gs12) ) ///
     (scatteri 30 2001   30 2002 , recast(area) color(gs12) ) ///
     (scatteri -10 2007.75   -10 2009.75 , recast(area) color(gs12) ) ///
     (scatteri 30 2007.75   30 2009.75 , recast(area) color(gs12) ) /// 
 (scatter var_pr_link_mean year, msymbol(0) lcolor(navy)  mcolor(navy) c(direct) yline(0 , lcolor(black)) xline(1995, lcolor(black) lpattern(dash)) msize(small) lpattern(solid)) ///
    (scatter var_pr_link_5 year, msymbol(D) lcolor(orange)  mcolor(orange) c(direct) lpattern(dash) msize(small)) ///
    (scatter var_pr_link_25 year, msymbol(T) lcolor(red) mcolor(red) c(direct) lpattern(longdash) msize(small)) ///
    (scatter var_pr_link_75 year, msymbol(S) lcolor(green) mcolor(green) c(direct) lpattern(shortdash) msize(small)) ///
    (scatter var_pr_link_95 year, msymbol(+) lcolor(black) mcolor(black) c(direct) lpattern(dash_dot) msize(small)) ///
	, plotregion(fcolor(white) lcolor(white)) ///
	graphregion(fcolor(white) lcolor(white)) ///
	xtitle("Year") xlab(1990(5)2012) ///
	legend(size(small) pos(6) r(3) on order (7 8 9 10 11 1) label(7 "Mean S936 Exposure") label(9 "25th Percentile Exposure")  label(10 "75th Percentile Exposure") ///
	 label(11 "95th Percentile Exposure") label(8 "5th Percentile Exposure") label(1 "Recession")) ///
	yti("Percentage Change in Employment")
graph export "$output/Graphs/figure6_B.pdf", replace
