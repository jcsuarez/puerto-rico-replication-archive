***************************************************************
// Filename: test_sun_shapiro_longdiff.do
// Author: eddie yu
// Date: 2023-10-16
// Task: Sun and Shapiro Test
//
***************************************************************

clear all
snapshot erase _all
est clear

cd "$output/Work2024"

use "$data/Replication Data/figure6_data_A.dta", clear

* pre/post for Long Difference
gen PR = (pr_link_i > 0)
capture: drop pre post
gen pre = (year<1995)
gen post = (year>1995)

foreach var in pre post {
	gen PR_`var' = (`var'==1)*PR
}


summ pr_link_i if base_emp > 0, d 
scalar iqr = r(p75)-r(p25) 

* Create Decile bins of PR Exposure
xtile bin_pr_link = pr_link_i, nq(10)

* create bin1, bin2, ..., bin10 where bin1 is totally unaffected group
* Check if bin1's PR exposure = 0
tab bin_pr_link, summ(pr_link_i) // 40% of sample has PR exposure = 0

* create sample of regression to run DiD w/ TWFE
forval v = 3/10 {
	gen reg_sample`v' = 0
	replace reg_sample`v' = 1 if bin_pr_link == `v' | bin_pr_link == 1
	tab bin_pr_link if reg_sample`v' == 1 // check if we have totally unaffected & each group
}



forval i = 3/10 {
	preserve
// 	local i = 3
	keep if reg_sample`i'==1 
	tab bin_pr_link
	di `avg_pr_link'
	eststo emp_`i':  reghdfe emp_growth PR_pre PR_post [aw=wgt], a(i.pid i.year#i.industr ) cl(fips_state indust ) nocons
	lincom PR_post
		estadd local beta_pr = string(r(estimate), "%8.3fc")
		estadd local se_pr   = "(" + string(r(se), "%8.3fc") + ")"
		estadd local p_pr    = "[" + string(r(p), "%8.3fc") + "]"
		estadd local ci_pr   = "[" + string(r(lb), "%8.3fc") + "," + string(r(ub), "%8.3fc") + "]"
		estadd local lb_pr   = string(r(lb), "%8.3fc")
		estadd local ub_pr   = string(r(ub), "%8.3fc")
	summ pr_link_i if pr_link_i > 0, d
		estadd local avg_pr_link = string(r(mean), "%8.3fc")
		estadd local beta_pr_scaled = string( _b[PR_post] / r(mean), "%8.3fc")
		estadd local iqr_adj_pr_link = string(r(mean)/iqr, "%8.3fc")
		estadd local beta_pr_scaled_iqr = string( _b[PR_post] / r(mean)/iqr, "%8.3fc")
	
// 	est save ster/emp_reg_long_`i', replace
	restore
}


esttab emp_* using sun_shapiro_pr_bin_pooled.tex, drop(*) ///
	stats(beta_pr se_pr p_pr ci_pr avg_pr_link beta_pr_scaled iqr_adj_pr_link beta_pr_scaled_iqr N, /// 
	labels("Pooled Estimate" "s.e." "p-val" "95\% CI" ///
			  "Mean PR Exposure" "Estimate / Mean PR Exposure" "Mean PR Exposure (IQR adjusted)" "Estimate / IQR adjusted PR Exposure")) /// 
	mtitles("Bin3" "Bin4" "Bin5" "Bin6" "Bin7" "Bin8" "Bin9" "Bin10") ///
	nostar nonotes nonum nogap replace label nocons collabels(none) varwidth(40) 
	