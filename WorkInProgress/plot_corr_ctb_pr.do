***************************************************************
// Filename: es_ctb_controls.do
// Author: eddie yu
// Date: 2023-10-16
// Task: CTB Controls event study plots
//
***************************************************************

clear all
snapshot erase _all
est clear

cd "$output/Work2024"

// Explore CTB vs PR variation
// ssc install schemepack, replace
set scheme tab1	 


use "$data/Replication Data/figure6_data_A.dta", clear

merge m:1 pid using "$data/ITAX/ctb_pr_shocks.dta", nogen

gen ctb_treat_ind = ctb_treat_emp>0.05915
keep if year==2006

sum pr_link_ind, det

// Plot scatter: FOR JC
preserve
	collapse (sum) wgt (mean) ctb_treat_emp pr_link_ind, by(pid)
	drop if pr_link_ind>5
 
	graph twoway (scatter ctb_treat_emp pr_link_ind [aw=wgt],  mcolor(%30) ) ///
				 (lfitci ctb_treat_emp pr_link_ind [aw=wgt] ), ///
			xti("S936 Exposure") yti("CTB Exposure")

	correlate ctb_treat_emp pr_link_ind [aw=wgt] 			 
restore 

graph export "$output/Work2024/corr_ctb_pr.png", replace

graph twoway (kdensity pr_link_ind if ctb_treat_ind==0 & pr_link_ind<5 [aw=wgt] ) ///
			 (kdensity pr_link_ind if ctb_treat_ind==1 & pr_link_ind<5 [aw=wgt] ), ///
			 xti("S936 Exposure") yti("Kernel Density") ///
			legend(order(1 2) label(1 "Low CTB Counties") label(2 "High CTB Counties"))


graph export "$output/Work2024/kde_ctb_pr.png", replace			

// plot 10-year growth: FOR JC
keep if emp_growth<2	// drop outliers that throw off scale
drop if emp_growth==-1
graph twoway (scatter emp_growth pr_link_ind if ctb_treat_ind==0 [aw=wgt],  mcolor(%20) ) ///
			 (lfitci emp_growth pr_link_ind if ctb_treat_ind==0 [aw=wgt]) ///
			 (scatter emp_growth pr_link_ind if ctb_treat_ind==1 [aw=wgt],  mcolor(%20) ) ///		 
		     (lfitci emp_growth pr_link_ind if ctb_treat_ind==1 [aw=wgt]), ///
			 legend(order(1 4 3 5) label(1 "Low CTB Labor Markets") label(4 "High CTB Labor Markets") ///
				label(3 "Low CTB: Linear Fit") label(5 "High CTB: Linear Fit")) ///
			 xtitle("S936 Exposure") ytitle("Employment Growth, 1996 - 2006")
			 
			 
graph export "$output/Work2024/transparency_ctb_pr.png", replace	
