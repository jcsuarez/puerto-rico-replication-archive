***************************************************************
// Filename: es_ctb_controls.do
// Author: eddie yu
// Date: 2023-10-16
// Task: CTB Controls event study plots
//
***************************************************************

clear all
snapshot erase _all
est clear

cd "$output/Work2024"

use "$data/Replication Data/figure6_data_A.dta", clear

merge m:1 pid using "$data/ITAX/ctb_pr_shocks.dta", nogen

// Graphs
gen ctb_treat_ind = ctb_treat_emp>0.05915
xi i.year|cen_pr_link  i.year*ctb_treat_ind i.year|ctb_treat_emp, noomit


forval i=1990(1)2014 { 
	capture: label var _IyeaXpr_`i' "PR `i'"
} 

forval i=1990(1)2014 { 
	capture: label var _IyeaXctb_`i' "CTB `i'"
} 

forval i=1990(1)2014 { 
	capture: label var _IyeaXctba`i' "CTB Emp `i'"
} 


// drop _IyeaXp*1995

drop _IyeaXce*1995
drop _IyeaXctb_1996 // discrete CTB
drop _IyeaXctba1996 // continuous CTB



snapshot save


* reg specs
global specname1 "Low CTB Exposure"
global specname2 "High CTB Exposure"

global spec1 "if ctb_treat_ind==0 [aw=wgt]  , a(i.pid i.year#i.industr ) cl(fips_state indust ) nocons"
global spec2 "if ctb_treat_ind==1 [aw=wgt]  , a(i.pid i.year#i.industr ) cl(fips_state indust ) nocons"

global c1 "navy"
global c2 "dkorange"



*** Store estimates ***
local y "emp_growth"
local cnt = 0
foreach spec in spec1 spec2  { // spec3 spec4 spec5 spec6 spec7
	local cnt = `cnt' + 1
	reghdfe `y' _IyeaXce* $`spec'
	estimates save `spec'_base, replace
	
preserve
	estimates use `spec'_base

	** figure years for the graph
	gen fig_year = 1989 in 1
	foreach i of numlist 1/23 {
		local j = `i' + 1989
		replace fig_year = `j' in `i'
	}
	
	** annual betas for the preperiod
	gen pr_beta = 0
	gen pr_beta_lb = 0
	gen pr_beta_ub = 0
	foreach i of numlist 1/5 {
		local j = `i' + 1989
		qui lincom _b[_IyeaXcen_`j'] , level(95)
		replace pr_beta = r(estimate) in `i'
		replace pr_beta_lb = r(lb) in `i'
		replace pr_beta_ub = r(ub) in `i'
	}
	* zero effect in 1995
	qui lincom  0
	replace pr_beta = r(estimate) in 5

	** annual betas for the postperiod
	foreach i of numlist 7/23 {
		local j = `i' + 1989
		qui lincom _b[_IyeaXcen_`j'] , level(95)
		replace pr_beta = r(estimate) in `i'
		replace pr_beta_lb = r(lb) in `i'
		replace pr_beta_ub = r(ub) in `i'
	}
	
	gen specname = "`spec'"
	keep if _n < 24
	keep fig_year pr_beta* specname
	tempfile `spec'_graph
	save ``spec'_graph', replace	
restore
}


*** Graph Overlay ***
{
di `cnt'
preserve
	use `spec1_graph', replace
	local j = 0
	forvalues i = 2/`cnt' {
		local j = `j' + 0.15
		append using `spec`i'_graph'
		replace fig_year = fig_year+`j' if specname=="spec`i'" & fig_year!=1995
	}
		
	graph twoway ///
	 (scatter pr_beta fig_year if specn=="spec1", lco($c1) mco($c1) msize(small)) ///
	 (rcap pr_beta_lb pr_beta_ub fig_year if specn=="spec1", lco($c1) lpa(solid)) ///
	 (scatter pr_beta fig_year if specn=="spec2", lco($c2) mco($c2) msize(small)) ///
	 (rcap pr_beta_lb pr_beta_ub fig_year if specn=="spec2", lco($c2) lpa(solid)) ///
	 , yline(0 , lcolor(gs12)) xline(1995, lcolor(black) lpattern(solid))  ///
		plotregion(fcolor(white) lcolor(white)) ///
		graphregion(fcolor(white) lcolor(white))  ///
		 xtitle("Year") xlab(1990(5)2012) bgcolor(white) ///
		legend(size(small) pos(6)  r(2) on order (1 3 5) ///
		label(1 "$specname1")  ///
		label(3 "$specname2")) ///
		ti("Effect of S936 Exposure on Employment Growth", margin(medium))
	

restore	
}

graph export "$output/Work2024/ctb_heterogeneity.png", replace


est clear
snapshot restore 1

* reg specs
global specname1 "Main Event Study"
global specname2 "Discrete CTB Control"
global specname3 "Continuous CTB Control"

global spec1 "[aw=wgt]  , a(i.pid i.year#i.industr) cl(fips_state indust ) nocons"
global spec2 "[aw=wgt]  , a(i.pid i.year#i.industr i.year#i.ctb_treat_ind) cl(fips_state indust ) nocons"
global spec3 "_IyeaXctba* [aw=wgt]  , a(i.pid i.year#i.industr) cl(fips_state indust ) nocons"

global c1 "navy"
global c2 "dkorange"
global c3 "midgreen"

*** Store estimates ***
local y "emp_growth"
local cnt = 0
foreach spec in spec1 spec2 spec3 { //  spec4 spec5 spec6 spec7
	local cnt = `cnt' + 1
	reghdfe `y' _IyeaXce* $`spec'
	estimates save `spec'_base, replace
	
preserve
	estimates use `spec'_base

	** figure years for the graph
	gen fig_year = 1989 in 1
	foreach i of numlist 1/23 {
		local j = `i' + 1989
		replace fig_year = `j' in `i'
	}
	
	** annual betas for the preperiod
	gen pr_beta = 0
	gen pr_beta_lb = 0
	gen pr_beta_ub = 0
	foreach i of numlist 1/5 {
		local j = `i' + 1989
		qui lincom _b[_IyeaXcen_`j'] , level(95)
		replace pr_beta = r(estimate) in `i'
		replace pr_beta_lb = r(lb) in `i'
		replace pr_beta_ub = r(ub) in `i'
	}
	* zero effect in 1995
	qui lincom  0
	replace pr_beta = r(estimate) in 5

	** annual betas for the postperiod
	foreach i of numlist 7/23 {
		local j = `i' + 1989
		qui lincom _b[_IyeaXcen_`j'] , level(95)
		replace pr_beta = r(estimate) in `i'
		replace pr_beta_lb = r(lb) in `i'
		replace pr_beta_ub = r(ub) in `i'
	}
	
	gen specname = "`spec'"
	keep if _n < 24
	keep fig_year pr_beta* specname
	tempfile `spec'_graph
	save ``spec'_graph', replace	
restore
}


*** Graph Overlay ***
{
di `cnt'
preserve
	use `spec1_graph', replace
	local j = 0
	forvalues i = 2/`cnt' {
		local j = `j' + 0.15
		append using `spec`i'_graph'
		replace fig_year = fig_year+`j' if specname=="spec`i'" & fig_year!=1995
	}
		
	graph twoway ///
	 (scatter pr_beta fig_year if specn=="spec1", lco($c1) mco($c1) msize(small)) ///
	 (rcap pr_beta_lb pr_beta_ub fig_year if specn=="spec1", lco($c1) lpa(solid)) ///
	 (scatter pr_beta fig_year if specn=="spec2", lco($c2) mco($c2) msize(small)) ///
	 (rcap pr_beta_lb pr_beta_ub fig_year if specn=="spec2", lco($c2) lpa(solid)) ///
	 (scatter pr_beta fig_year if specn=="spec3", lco($c3) mco($c3) msize(small)) ///
	 (rcap pr_beta_lb pr_beta_ub fig_year if specn=="spec3", lco($c3) lpa(solid)) ///
	 , yline(0 , lcolor(gs12)) xline(1995, lcolor(black) lpattern(solid))  ///
		plotregion(fcolor(white) lcolor(white)) ///
		graphregion(fcolor(white) lcolor(white))  ///
		 xtitle("Year") xlab(1990(5)2012) bgcolor(white) ///
		legend(size(small) pos(6)  r(2) on order (1 3 5) ///
		label(1 "$specname1")  ///
		label(3 "$specname2")  ///
		label(5 "$specname3")) ///
		ti("Controlling for CTB", margin(medium))
	

restore	
}


graph export "$output/Work2024/ctb_controls.png", replace
