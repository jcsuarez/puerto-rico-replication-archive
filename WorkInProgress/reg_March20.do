


// Explore CTB vs PR variation
// ssc install schemepack, replace
set scheme tab1	 

// global data "/Users/jisangyu/JC's Data Emporium Dropbox/Eddie Yu/Puerto Rico/Replication2024/Data"
global data "/Users/kevinroberts/Dropbox (Personal)/Puerto Rico/Replication2024/Data"

use "$data/Replication Data/figure6_data_A.dta", clear

merge m:1 pid using "$data/ITAX/ctb_pr_shocks.dta", nogen

gen ctb_treat_ind = ctb_treat_emp>0.05915
keep if year==2006

sum pr_link_ind, det

// Plot scatter: FOR JC
preserve
	collapse (sum) wgt (mean) ctb_treat_emp pr_link_ind, by(pid)
	drop if pr_link_ind>5
 
	graph twoway (scatter ctb_treat_emp pr_link_ind [aw=wgt],  mcolor(%30) ) ///
				 (lfitci ctb_treat_emp pr_link_ind [aw=wgt] )

	correlate ctb_treat_emp pr_link_ind [aw=wgt] 			 
restore 


graph twoway (kdensity pr_link_ind if ctb_treat_ind==0 & pr_link_ind<5 [aw=wgt] ) ///
			 (kdensity pr_link_ind if ctb_treat_ind==1 & pr_link_ind<5 [aw=wgt] ), ///
			legend(order(1 2) label(1 "Low CTB Counties") label(2 "High CTB Counties"))


// plot 10-year growth: FOR JC
keep if emp_growth<2	// drop outliers that throw off scale
drop if emp_growth==-1
graph twoway (scatter emp_growth pr_link_ind if ctb_treat_ind==0 [aw=wgt],  mcolor(%20) ) ///
			 (lfitci emp_growth pr_link_ind if ctb_treat_ind==0 [aw=wgt]) ///
			 (scatter emp_growth pr_link_ind if ctb_treat_ind==1 [aw=wgt],  mcolor(%20) ) ///		 
		     (lfitci emp_growth pr_link_ind if ctb_treat_ind==1 [aw=wgt]), ///
			 legend(order(1 4 3 5) label(1 "Low CTB Labor Markets") label(4 "High CTB Labor Markets") ///
				label(3 "Low CTB: Linear Fit") label(5 "High CTB: Linear Fit")) ///
			 xtitle("S936 Exposure") ytitle("Employment Growth, 1996 - 2006")

// Re-do Figure 6 with CTB controls
use "$data/Replication Data/figure6_data_A.dta", clear

merge m:1 pid using "$data/ITAX/ctb_pr_shocks.dta", nogen

// Stuff added
* DPAD and ETI
* IPW: 



// Second Define Command for ES 
capture program drop ES_graph
program define ES_graph
syntax,  level(real) [  yti(string) note(string) outname(string) ylab(string) ]

qui { 
mat b = e(b)
mat V = e(V) 
mat es_b = 0 
mat es_ci_l = 0 
mat es_ci_h = 0 
mat time = 0 

// for 95% CI 
if `level' == 99 { 
	local z= 2.6  
}
if `level' == 95 { 
	local z= 1.96  
}
// for 90% CI 
if `level' == 90 { 
	local z= 1.645 
} 
	
forval i = 1/23 { 
	mat time = (time,`i'-6+1995)
	mat es_b = (es_b, b[1,`i'])
	mat es_ci_l = (es_ci_l, b[1,`i']-`z'*sqrt(V[`i',`i'])   )
	mat es_ci_h = (es_ci_h, b[1,`i']+`z'*sqrt(V[`i',`i'])   )		
	}
mat es_b =  (es_b[1,2..6],0,es_b[1,7...])'
mat es_ci_l = (es_ci_l[1,2..6],0,es_ci_l[1,7...])'  
mat es_ci_h = (es_ci_h[1,2..6],0,es_ci_h[1,7...])' 
mat time = time[1,2...]' 

capture: drop es_b es_ci* time 
svmat es_b 
svmat time
svmat es_ci_l
svmat es_ci_h 
}

twoway (scatter es_b time, yline(0 , lcolor(black)) xline(1995, lcolor(black) lpattern(dash))) ///
    (rcap es_ci_h es_ci_l time,  lcolor(navy) lpattern(dash)) ///
	, plotregion(fcolor(white) lcolor(white)) ///
	graphregion(fcolor(white) lcolor(white)) ///
	xtitle("Year") xlab(1990(5)2012) ///
	legend(pos(6) r(1) label(1 "Estimate") label(2 "`level'% CI")) ///
	ytitle("`yti'") ///
	note("`note'") ylab("`ylab'")
graph export "`outname'.pdf", replace	

end 

// Graphs
gen ctb_treat_ind = ctb_treat_emp>0.05915
xi i.year|pr_link_ind  i.year*ctb_treat_ind i.year|ctb_treat_emp, noomit

drop _IyeaXp*1995

// drop _IyeaXce*1995
drop _IyeaXctb_1996 // discrete CTB
drop _IyeaXctba1996 // continuous CTB

xtile ctb_bin20 = ctb_treat_emp, n(20)
xtile ctb_bin50 = ctb_treat_emp, n(50)


// Compare high vs. low: seems like PR effects are larger among CTB-exposed counties, but significant in both groups

// Create overlay: CTB heterogeneity
eststo emp_1:  reghdfe emp_growth _IyeaXcen* if ctb_treat_ind==0 [aw=wgt]  , a(i.pid i.year#i.industr ) cl(fips_state indust ) nocons
eststo emp_2:  reghdfe emp_growth _IyeaXcen* if ctb_treat_ind==1 [aw=wgt]  , a(i.pid i.year#i.industr ) cl(fips_state indust ) nocons




// Main event study
eststo emp_3:  reghdfe emp_growth _IyeaXp* [aw=wgt]  , a(i.pid i.year#i.industr) cl(fips_state indust ) nocons

// Main event study: Discrete CTB control
eststo emp_4:  reghdfe emp_growth _IyeaXp* [aw=wgt]  , a(i.pid i.year#i.industr i.year#i.ctb_treat_ind) cl(fips_state indust ) nocons

// Main event study: Continuous CTB control
eststo emp_5:  reghdfe emp_growth _IyeaXp*  _IyeaXctba* [aw=wgt]  , a(i.pid i.year#i.industr) cl(fips_state indust ) nocons

// Nonparametric CTB controls
eststo emp_6:  reghdfe emp_growth _IyeaXp* [aw=wgt]  , a(i.pid i.year#i.industr i.year#i.ctb_bin20) cl(fips_state indust ) nocons

eststo emp_7:  reghdfe emp_growth _IyeaXp* [aw=wgt]  , a(i.pid i.year#i.industr i.year#i.ctb_bin50) cl(fips_state indust ) nocons


