 // Define Command for MYQR
capture program drop myqr
program define myqr , eclass 
syntax,  levelb(real) [  spec(string)outname(string) ylab(string) ]

	bsqreg emp_growth pr_link_ind `spec' [iw=base_emp], q(`levelb')

	tempname coef 
	mat `coef' = e(b) 
	ereturn clear 
	scalar `coef' = el(`coef',1,1)
	ereturn scalar pr_link_ind = `coef'
end
