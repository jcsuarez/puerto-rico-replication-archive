// This file creates the data sets for table 5, transfers
// Author: Dan Garrett
// Date: 5-9-2020

/* Data sources:
-NETS, QCEW, BEA
*/

* starting point is technically 2-main_analysis/transfers_county_regression_20180430

clear all
set more off 
snapshot erase _all

*******************************************************
* Baseline dataset
*******************************************************
// A.0. Get regional crosswalk read 
use "$xwalk/cw_cty_czone.dta", clear
rename cty_fips pid 
duplicates drop 
tempfile cross
save "`cross'"

// A.0. Get link data 
use "$output_NETS/pr_link_est_countyXindustry.dta", clear 
gen pr_emp = pr_link*total if industry_cd < 13
bys fips: egen tot_emp = total(total) 
bys fips: egen tot_pr = total(pr_emp)

gen pr_l = tot_pr/tot_emp 
keep pr_l fips
rename  fips pid
rename pr_l pr_link_ind
duplicates drop 
tempfile pr 
save "`pr'" 

use "$output_NETS/pr_link_est_county.dta", clear 
rename  fips pid
tempfile pr2 
save "`pr2'" 

// A.1. Getting the outcome and other county data
use "$data/BEA_CA35/tansfers_clean", replace

rename pid fips_county

merge m:1 fips_county using "$output_NETS/pr_link_est_county_addvars_emp.dta"

keep if _merge == 3
drop _merge

rename fips_county pid

// A.6. Merge in pr link 
merge m:1 pid using "`pr'"
keep if _merg == 3
drop _merge 

merge m:1 pid using "`pr2'"
drop if year == . 
drop if pr_link == . 
drop _merge 
rename pr_link pr_link_all

sum pr_link_al, d 
drop if year == . 

tsset pid year
tsfill, full

// Generating the transfer measure (not per capita in this file)
drop if czone == .

snapshot save

**** taking out veteran transfers
replace trans_tot = trans_tot - trans_vet

foreach Q of var trans_tot trans_unemp trans_SSA trans_medicare  trans_public_med trans_income trans_educ trans_vet  {
gen `Q'_p = `Q' / tot_pop
}

// A.4.  Growth rates. Base year: 1995
gen temp = tot_pop if year == 1995
bysort pid: egen base_pop = max(temp) // this is now total population
drop temp

foreach Q of var trans_tot trans_unemp trans_SSA trans_medicare  trans_public_med trans_income trans_educ  {
gen temp = `Q'_p if year == 1995
bysort pid: egen base_`Q'_p = max(temp) // this is now working age population instead of employment
drop temp
}


foreach Q of var trans_tot trans_unemp trans_SSA trans_medicare  trans_public_med trans_income trans_educ  {
gen `Q'_growth =  (`Q'_p - base_`Q'_p)/base_`Q'_p // (`Q'_p - base_`Q'_p)*1000  
replace `Q'_growth = 0 if `Q'_growth == .
}
foreach Q of var trans_tot trans_unemp trans_SSA trans_medicare  trans_public_med trans_income trans_educ  {
gen `Q'_growth_levels =  (`Q'_p - base_`Q'_p)*1000  
replace `Q'_growth = 0 if `Q'_growth == .
}

// A.5. Define PR Growth Data
** For now, keep only balanced industries . Matt: can you fix it so we don't have to do this? 
bys pid: gen count = _N
keep if count == 23 
drop count 

sum base_pop, d
gen wgt=base_pop/(r(N) * r(mean))

sum pr_link_i $pr_wgt if base_pop > 0, d 
replace pr_link_i = pr_link_i/(r(p75)-r(p25))

sum pr_link_al  $pr_wgt if base_pop > 0, d 
replace pr_link_al = pr_link_al/(r(p75)-r(p25))

// B. Graphs with All PR Link 
cd "$output" 
set more off 

*random cleaning:
rename timepwt48 tpwt48
winsor wgt, p(.01) gen(wgt1)

gen total_transfers_p =  trans_tot_p + trans_vet_p
foreach Q of var trans_tot trans_unemp trans_SSA trans_medicare  trans_public_med trans_income trans_educ  {
gen `Q'_weight =  `Q'_p / total_transfers_p
replace `Q'_p = `Q'_p * 1000
}

* keeping variables and saving 
keep trans_tot trans_unemp trans_income trans_educ trans_SSA trans_medicare  trans_public_med *_growth* *_weight *_p pr_link_i wgt year statefips total_transfers_p
keep if inrange(year,2004,2008) | inrange(year,1990,1995)
save "$data/Replication Data/table5_data", replace

