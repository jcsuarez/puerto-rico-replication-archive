***************************************************************
//Filename: figure4_clean.do
//Author: eddie yu
//Date: 2023-09-20
//Task: Given NETS_emp_SOI_DFL.dta,
//		this script produces dataset figure4_data
***************************************************************


/* Data sources:
-Compustat
-NETS to determine PR firms
*/

clear all
set more off 
snapshot erase _all

************************************
* Grabbing and organizing the data
************************************
use "$data/Replication Data/interim/NETS_emp_SOI_DFL", clear

* actual dataset for regressions
snapshot save

keep hqduns emp_growth year n2 PR wgt 
save "$data/Replication Data/figure4_data", replace
