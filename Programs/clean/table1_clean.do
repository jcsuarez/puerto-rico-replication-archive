// This file creates the data sets for Table 1, list of large nets firms
// Author: Dan Garrett
// Date: 4-20-2020

/* Data sources:
NETS
*/

* starting point for all panels is 1-descriptives/jobs_ts_d

clear all
set more off 
snapshot erase _all

************************************
* Grabbing and organizing the data
************************************
*** get list of hqduns that actually work by dropping gov't etc... 
use "$output_NETS/pr_extract_est.dta", clear

gen naic4 = floor(naics95/100)
gen naic3 = floor(naics95/1000)
gen naic2 = floor(naics95/10000)

********************
*getting rid of governmental and social non-profit orgs
********************
drop if naic2 == 92 | naic3 == 813
* dropping USPS and the federal government
drop if hqduns95 == 3261245 | hqduns95 == 161906193

* dropping firms with HQ in PR
drop if pr_based == 1 

* dropping county industries with no link
drop if pr_link == 0
bysort hqduns: egen temp_max = max(emp95)
gen temp_keep = (emp95 == temp_max)

keep if temp_keep == 1

* making a list of hqduns to use from the panel data
keep hqduns naic3
bysort hqduns: egen m_naic3 = mode(naic3), minmode
drop naic3
duplicates drop 
tempfile tokeep 
save "`tokeep'"

** old code 
use "$output_NETS/pr_extract_panel.dta", clear 

** Keep only firms at start of the sample 
gen temp = emp_PR if year == 1990
bysort hqd: egen base_emp_PR = min(temp)
drop temp 
keep if base > 0  // This line drops 4,991 observations

** MErge in to keep 
merge m:1 hqduns95 using "`tokeep'"
keep if _merge == 3 // This drops 23 observations from the master, 214 from using

egen firm = group(hqd) 
sum firm, d

rename hqduns95 hqduns
drop _merge 
gen ind = m_naic3

* dan changed to 1995 on 10-24-2018

bys year: egen tot_US = sum (emp_US)
bys year: egen tot_PR = sum (emp_PR)
bys year: egen tot = sum (emp_total)
gen temp_1 = tot * (year == 1995)
gen temp_2 = tot * (year == 2006)

egen tot_1995 = max(temp_1)
egen tot_2006 = max(temp_2)

gen tot_change = tot_2006 - tot_1995
gen tot_pchange = (tot_2006 - tot_1995) / tot_1995

keep if year == 1995
local minPR "1500"
keep if emp_PR >= `minPR'

/*
gsort - emp_PR
gen id = _n
drop if id > 20
*/
tempfile topten
save "`topten'", replace
* merging in names and such 
use "$output_NETS2/pr_extract_panel_v2_company.dta", clear
merge m:1 hqd using "`topten'"
keep if _merge == 3
drop _merge

gen emp06 = emp_total *(year == 2006)
bys hqduns: egen emp2006 = max(emp06)
gen emp90 = emp_total *(year == 1990)
bys hqduns: egen emp1990 = max(emp90)
gen emp95 = emp_total *(year == 1995)
bys hqduns: egen emp1995 = max(emp95)

gen emp_change = emp2006 - emp1995
gen emp_pchange = (emp2006 - emp1995) / emp1995 

keep hqd company emp_US emp_PR emp_change emp_pchange year tot_US ind tot_PR tot_change tot_pchange
keep if year == 1995
duplicates drop
drop if emp_PR < 1500

gsort -emp_PR
keep company emp_US emp_PR ind emp_change emp_pchange tot_US tot_PR tot_change tot_pchange
replace company = subinstr(company, "&", "\&",.)


* keeping variables and saving 
save "$data/Replication Data/table1_data", replace

