*******************************************************************************
//Filename: figureA7_clean.do
//Author: eddie yu
//Date: 2023-10-16
//Task: Given state_taxes_analysis, misallocation_paneldata, 
//      pr_link_est_county_addvars, import_hakobyanlaren_nafta and 
// 		figure6_data_A, we create data for figure A7
*******************************************************************************

** Create balance table data from state_taxes_analysis, misallocation_paneldata,
**  pr_link_est_county_addvars, import_hakobyanlaren_nafta
clear all
set more off 

// Get the data!
use "$data/data_additional/St Tax Structure 05_10_2017/state_taxes_analysis.dta", clear
keep if year == 1990 
keep fips salestax propertytax corporate_rate GDP rev_totaltaxes rec_val rdcred rdcred trainingsubsidy jobcreationcred corpinctax totalincentives investment_credit 
rename fips statefips
tempfile incentives 
save "`incentives'"


use "$data/data_additional/Misallocation 5_10_2017/misallocation_paneldata.dta", clear
keep if year == 1990 
keep income_rate_avg fipstate
rename fips statefips
tempfile incentives2 
save "`incentives2'"

  
*use "$output/pr_link_est_county_addvars.dta", clear 
use "$output_NETS/pr_link_est_county_addvars.dta", clear 
merge m:1 statefips using "`incentives'"
drop if _merge == 2 
drop _merge 

merge m:1 statefips using "`incentives2'"
drop if _merge == 2 
drop _merge 

** Add nafta variables 
merge 1:1 fips_county using "$output/import_hakobyanlaren_nafta.dta"
drop if _merge == 2 
drop _merge 

gen ln_pop = ln(population ) 
gen ln_GDP = ln(GDP) 
gen ln_rev = ln(rev_to)
gen rev_gdp = rev_tot/GDP 

gen zero_pr = (pr_link == 0) 

** keeping only used variables plus pid and statefips
local spec1 " realmw rgtowork income_rate_avg salestax propertytax corpinctax rec_val rev_gdp d_tradeusch_pw l_sh_routine33a locdt_noag " 
local spec2 "ln_pop labor_participation_rate pct_retail pct_agriculture pct_manufacturing_durable pct_manufacturing_nondurable capital_stock  pct_college  pct_less_HS pct_white  pct_black "

keep fips_county statefips `spec1' `spec2' pop pr_link

tempfile balance_panel
save `balance_panel', replace

*** Load Fig 10 Data, keep only pr_link
use "$data/Replication Data/figure6_data_A", clear

keep pr_link pid wgt 

duplicates drop 
drop if pid ==. 

collapse (first) pr_link (sum) wgt , by(pid)

rename pid fips_county 

*** merge figure4_ab_data
merge 1:1 fips_county using `balance_panel'

keep if _merge == 3 

save "$data/Replication Data/figureA7_data", replace
