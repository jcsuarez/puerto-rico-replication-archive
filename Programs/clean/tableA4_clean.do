***************************************************************
//Filename: tableA4_clean.do
//Author: eddie yu
//Date: 2023-10-16
//Task: This script cleans from interim/compu_NETS_ETR_sizecutoff.dta to clean
//		relevant variables for Table A4
//		All tables here refer to PR_v49.pdf / Overleaf version
**************************************************************

clear all
set more off 
snapshot erase _all

* baseline data 
use "$data/Replication Data/interim/compu_NETS_ETR_sizecutoff.dta", clear

keep gvkey capex_base_1 year n2 PR DFL_ppe _webal n3 PR_post IK_1 ppe95_bins10 ///
exp_sec exp_ind PR_post1 PR_post2 PR_pre
save "$data/Replication Data/tableA4_data", replace

