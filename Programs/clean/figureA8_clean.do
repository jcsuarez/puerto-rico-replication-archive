***************************************************************
//Filename: figureA8_clean.do
//Author: eddie yu
//Date: 2023-09-20
//Task: This script creates data for figure A8 given pr_treatment
***************************************************************

clear all
set more off
snapshot erase _all 

use "$data/Replication Data/interim/pr_treatment", clear


* interaction
xi i.year|PR , noomit
drop _IyeaXPR_1995 

forval i=1990(1)2014 { 
	capture: label var _IyeaXPR_`i' " "
} 
forval i=1991(2)2014 { 
	capture: label var _IyeaXPR_`i' "`i'"
}

tab idbflag PR if year==1995
tab fic


**** Control versions ****
gen lninv = log(capx)
gen lnat = log(at)
gen ppe_scaled = ppe*1000
sum ppe_scaled ppent
gen lnppe = log(ppe)
gen lnppe2 = log(ppe_scaled)
gen lnppent = log(ppent)
gen lnintan = log(intan)
gen tangibility = ppent/at

* create 1995 bins for ppent, gpop (gross profit / total assets)
xtset gvkey year
xtile gp95_binDFL = gp if year == 1995 , n(5) 
xfill gp95_binDFL, i(gvkey)

keep gvkey year pharma any_rd ppe95_* _IyeaX* PR capex_base

save "$data/Replication Data/figureA8_data", replace
