***************************************************************
//Filename: tableA3_clean.do
//Author: eddie yu
//Date: 2023-10-16
//Task: This script cleans from interim/compu_NETS_ETR_sizecutoff.dta to clean
//		relevant variables for Table A3
//		All tables here refer to PR_v49.pdf / Overleaf version
**************************************************************

clear all
set more off 
snapshot erase _all

* baseline data 
use "$data/Replication Data/interim/compu_NETS_ETR_sizecutoff.dta", clear

keep gvkey capex_base_p year n2 PR DFL_ppe _webal n3 PR_post IKIK ppe95_bins10 ///
exp_sec exp_ind PR_post1 PR_post2 PR_pre
save "$data/Replication Data/tableA3_data", replace
