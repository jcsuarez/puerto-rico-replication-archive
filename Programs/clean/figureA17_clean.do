***************************************************************
//Filename: figureA17_clean.do
//Author: eddie yu
//Date: 2023-10-16
//Task: This script cleans from interim/NETS_emp.dta to clean
//		relevant variables for Figure A17
**************************************************************

clear all
set more off
snapshot erase _all 

*******************************************************
* Starting with the data
*******************************************************
use "$data/Replication Data/interim/NETS_emp", clear

keep hqd est_growth PR year major_* wgt
save "$data/Replication Data/figureA17_data", replace
