*******************************************************************************
//Filename: figure2_clean.do
//Author: eddie yu
//Date: 2023-10-16
//Task: Given pr_link_est_county_d.dta, pr_link_est_countyXindustry_d.dta,
//		we produce fips (county) - pr_link / pr_link_ind variable
*******************************************************************************

/* Data sources:
Panel A: NETS establishment data
Panel B: NETS establishment data
*/

* starting point for both panels is descriptives/maps_d

clear all
set more off 

*******************************************************************************
*******************************************************************************
// Get link data 
use "$output_NETS/pr_link_est_county_d.dta", clear 
rename fips95 pid
gen fips_state = floor(pid/1000)
drop if fips_state == 72
** fixing miami-dade county
replace pid = 12025 if pid == 12086

replace pr = pr*100
drop total

rename pid county 

save "$data/Replication Data/figure2_pr_link", replace


*******************************************************************************
*******************************************************************************

// Get ind link data 
use "$output_NETS/pr_link_est_countyXindustry_d.dta", replace 
ren fips95 pid
gen fips_state = floor(pid/1000)
drop if fips_state == 72
** fixing miami-dade county
replace pid = 12025 if pid == 12086

gen pr_emp = pr_link*total if industry_cd < 13
bys pid: egen tot_emp = total(total) 
bys pid: egen tot_pr = total(pr_emp)
gen pr_link_ind = tot_pr/tot_emp 
replace pr_link_ind = pr_link_ind*100

keep pid pr_link_ind fips_state
rename pid county 
duplicates drop
save "$data/Replication Data/figure2_pr_link_ind", replace
