// This file creates the data sets for figure A4
// Author: eddie yu
// Date: 2023-10-16

/* Data sources:
Panel A: NETS establishment data
Panel B: NETS establishment data
*/

* starting point for both panels is descriptives/maps_d

clear all
set more off 

// Get the data!
use "$output_NETS/pr_link_est_county_d.dta", clear 
gen fips_state = floor(fips95/1000)
drop if fips_state == 72

merge 1:1 fips95 using "$output_NETS/pr_link_est_county_emp.dta"
gen county = fips95
** fixing miami-dade county
replace county = 12025 if county == 12086

gen pr_emp_link = pr_emp95 / emp95
replace pr_emp_link = pr_emp_link*100
winsor pr_emp_link, gen(w_pr_emp_link) p(.05)
drop total emp95 pr_emp95 _merge county pr_emp_link

rename w_pr_emp_link pr_emp_link 

save "$data/Replication Data/figureA4_data", replace
