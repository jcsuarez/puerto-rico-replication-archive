***************************************************************
//Filename: tableA10_clean.do
//Author: eddie yu
//Date: 2023-09-20
//Task: Given county Xwalk, PR Link data (NETS), County Population data,
//		creates tableA10_data.dta
***************************************************************


/* Data sources:
-NETS, QCEW, census estimates
*/

* starting point is technically 2-main_analysis/es_county_emp_inst

clear all
set more off 
snapshot erase _all

*******************************************************
* Baseline dataset
*******************************************************
// A.0. Get regional crosswalk read 
use "$xwalk/cw_cty_czone.dta", clear
rename cty_fips pid 
duplicates drop 
tempfile cross
save "`cross'"

// A.0. Get link data 
use "$output_NETS/pr_link_est_countyXindustry.dta", clear 
gen pr_emp = pr_link*total if industry_cd < 13
bys fips: egen tot_emp = total(total) 
bys fips: egen tot_pr = total(pr_emp)

gen pr_l = tot_pr/tot_emp 
keep pr_l fips
rename  fips pid
rename pr_l pr_link_ind
duplicates drop 
tempfile pr 
save "`pr'" 

use "$output_NETS/pr_link_est_county.dta", clear 
rename  fips pid
tempfile pr2 
save "`pr2'" 

// A.1. Getting the outcome and other county data
use "$additional/county population estimates/county_pop_ests.dta", replace
merge 1:1 pid year using "$additional/county population estimates/county_manu_emp_ests.dta"
drop _merge

replace manu_emp = 0 if manu_emp == .
drop if working_pop == .

rename pid fips_county

merge m:1 fips_county using "$output_NETS/pr_link_est_county_addvars_emp.dta"

keep if _merge == 3
drop _merge

rename fips_county pid

// A.6. Merge in pr link 
merge m:1 pid using "`pr'"
keep if _merg == 3
drop _merge 

merge m:1 pid using "`pr2'"
drop if year == . 
drop if pr_link == . 
drop _merge 
rename pr_link pr_link_all

sum pr_link_al, d 
drop if year == . 

tsset pid year
tsfill, full

// Generating the ADH measure
gen ADH = manu_emp / working_pop
drop if czone == .

// A.4.  Growth rates. Base year: 1995
gen temp = working_pop if year == 1995
bysort pid: egen base_pop = max(temp) // this is now working age population instead of employment
drop temp
gen temp = ADH if year == 1995
bysort pid: egen base_ADH = max(temp) // this is now working age population instead of employment
drop temp

*gen ADH_growth = 10 * (ADH - base_ADH)/base_ADH // MULTIPLIED BY 10 TO LINE UP WITH ADH
*replace ADH_growth = 0 if ADH_growth == . 


gen ADH_growth =  (ADH - base_ADH)/base_ADH // NOT MULTIPLIED BY 10 TO LINE UP WITH JC
replace ADH_growth = 0 if ADH_growth == . 

// A.5. Define PR Growth Data
bys pid: gen count = _N
keep if count == 23 
drop count 

sum base_pop, d
gen wgt=base_pop/(r(N) * r(mean))

sum pr_link_i if base_pop > 0, d 
replace pr_link_i = pr_link_i/(r(p75)-r(p25))

sum pr_link_al if base_pop > 0, d 
replace pr_link_al = pr_link_al/(r(p75)-r(p25))

*random cleaning:
rename timepwt48 tpwt48

* keeping variables and saving 
keep ADH_growth year pr_link_i* wgt statefips
save "$data/Replication Data/tableA10_data", replace
