***************************************************************
//Filename: figureA13_clean.do
// This file creates the data sets for figure A13 from ASM
// Author: eddie yu
// Date: 2023-10-16
***************************************************************

/* Data sources:
-ASM, NETS to make PR percentages
*/

* starting point for all panels is descriptives/balance_table

clear all
set more off 
snapshot erase _all

************************************
* Grabbing and organizing the data
************************************
///////////////////// Get data from PR Link 
*** Open PR link data by industry and create measure by state-3 digit naics 
use "$output_NETS/pr_link_est_countyXindustry_d.dta", clear 
** For Manuf Industries 
gen pr_emp = pr_link*total if industry_cd < 13
bys fips: egen tot_emp = total(total) 
bys fips: egen tot_pr = total(pr_emp)
gen pr_link_all = tot_pr/tot_emp 
drop pr_emp

** By Industry 
gen pr_emp = pr_link*total 
gen pr_link_ind = pr_emp/tot_emp

keep fips95 industry_cd tot_emp pr_link*
gen fips_state = round(fips95/1000,1)

preserve
	collapse (mean) pr_link_ind (mean) pr_link_all [fw=tot_emp] , by(indus)
	tempfile pr2
	save "`pr2'"
restore

collapse (mean) pr_link_ind (mean) pr_link_all [fw=tot_emp] , by(fips_state indus)
tempfile pr 
save "`pr'"




///////////////////// Get data from ASM
use "$ASM/combined_2017_21_15.dta", clear

// Label industries		
gen naics_2 = naics
tostring naics_2, replace 
gen industry_cd = .
replace industry_cd = 1 if naics_2 == "311"
replace industry_cd = 2 if naics_2 == "314"
replace industry_cd = 3 if naics_2 == "315"
replace industry_cd = 4 if naics_2 == "325"	// note: include 3254
replace industry_cd = 5 if naics_2 == "3254"
replace industry_cd = 6 if naics_2 == "326"
replace industry_cd = 7 if naics_2 == "316"
replace industry_cd = 8 if naics_2 == "332"
replace industry_cd = 9 if naics_2 == "333"
replace industry_cd = 10 if naics_2 == "335"
replace industry_cd = 12 if industry_cd == .

label define l_naic3 0 "Total, all industries" 1 "Food Mfg" 2 "Textile mill products" 3 "Apparel" 4 "Chemicals" 5 "Pharmaceuticals" ///
 6 "Rubber and Plastic" 7 "Leather" 8 "Fabricated metal" 9 "Machinery" 10 "Electrical equip" ///
 11 "Instruments" 12 "Other mfg" 13 "Finance, insurance, real estate" 14 "Services" ///
 15 "Wholesale and retail" 16 "Other non-mfg", replace
label values industry_cd l_naic3

drop if industry_cd == 0

duplicates tag industry_cd fips_state year, gen(dup_flag)
tab dup_flag if year==1995
tab dup_flag if year==1997
sort industry_cd fips_state year


// Collapse at 2-digit industry by state level 
collapse (sum) numemp (sum) annpayT (sum) avgprodwrkrs ///
		 (sum) prodwrkrwageT (sum) prodwrkrshrT (sum) totcostmatT ///
		 (sum) totvalshipT (sum) valaddT (sum) capexT, by(year fips_state industry_cd)

// time summary
tab year, sum(numemp)
tab year, sum(capexT)	
		 
/////// Merge data 
merge m:1 industry_cd fips_state using "`pr'"
keep if _merge == 3 
drop _merge 
egen pid = group(industry_cd fips)
tsset pid year 


*gen earnings = annpayT/numemp
gen earnings = prodwrkrwageT/avgprodwrkrs


////// Make percentage changes 
foreach var of varlist numemp annpayT avgprodwrkrs prodwrkrwageT prodwrkrshrT totcostmatT totvalshipT capexT valaddT earnings {
	gen temp = `var' if year == 1995
	bysort pid: egen base_`var' = max(temp)
	drop temp
	gen `var'_growth = (`var' - base_`var')/base_`var'
	}
	
		
foreach var of varlist pr_link* {
	sum `var', d
	replace `var' = `var'/r(sd)
	}

// There are large outliers in the pr link. Here choose to winsorize the large values
winsor pr_link_i, p(.01) g(w_pr_link_i) high
replace pr_link_i=w_pr_link_i

winsor base_numemp, p(.01) g(w_base_numemp)
gen oth_man = (industry == 12)
gen oth_pr = oth_man*pr_link_i
gen mai_pr = (1-oth_man)*pr_link_i

gen pr_link_post = (year>=1997)*pr_link_i
gen post_oth_pr = (year>=1997)*oth_pr
gen post_mai_pr = (year>=1997)*mai_pr

keep if base_capexT>1000 

* Test balance
sum pr_link_i, detail

gen temp1 = numemp if year==1995
egen emp95 = max(temp1), by(fips_state industry_cd)

* keeping variables and saving 
keep pr_link_i year industry_cd capexT capexT_g emp95 fips pid
save "$data/Replication Data/figureA13_data", replace

