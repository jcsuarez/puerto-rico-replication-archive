***************************************************************
//Filename: figure3_clean.do
//Author: eddie yu
//Date: 2023-09-20
//Task: Given size-cutoff data of paper/compu_NETS_ETR_sizecutoff.dta,
//		this script produces dataset figure3_data, 
***************************************************************


/* Data sources:
-Compustat
-NETS to determine PR firms
*/

clear all
set more off 
snapshot erase _all

************************************
* Grabbing and organizing the data
************************************
use "$data/Replication Data/interim/compu_NETS_ETR_sizecutoff.dta", clear 

* actual dataset for regressions
snapshot save

keep gvkey capex_base   year n2 PR  DFL_ppe ///
ppe95_bins10 _webal exp_sec exp_ind
save "$data/Replication Data/figure3_data", replace

// snapshot restore 1
// keep gvkey capex_base capex_base_p capex_base_1  year n2 PR IHS_rev log_tot_emp ///
// IK IK_1 IKIK DFL_ppe ppe95_bins10 _webal exp_sec exp_ind
// save "$data/Replication Data/R&R2023/paper/figureA6_data", replace
//
// snapshot restore 1
// keep gvkey capex_base_p capex_base_1  year n2 PR  DFL_ppe ///
// ppe95_bins10 _webal exp_sec exp_ind
// save "$data/Replication Data/R&R2023/paper/figureA7_data", replace
//
// snapshot restore 1
// keep gvkey capex_base_1  year n2 PR  DFL_ppe ///
// ppe95_bins10 _webal exp_sec exp_ind
// save "$data/Replication Data/R&R2023/paper/figureA8_data", replace
//
// snapshot restore 1
// keep gvkey capex_base capex_base_p capex_base_1  year n2 PR DFL_ppe n3 ///
// ppe95_bins10 _webal exp_sec exp_ind
// save "$data/Replication Data/R&R2023/paper/figureA9_data", replace

