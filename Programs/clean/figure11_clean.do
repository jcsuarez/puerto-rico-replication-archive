// This file creates the data sets for figure 11, conspuma long differences
// Author: eddie yu
// Date: 2023-10-16

/* Data sources:
-NETS, ACS
*/

* starting point is 2-main_analysis/conspuma_analysis_d_2018_03_01

clear all
set more off 
snapshot erase _all


************************************
* Grabbing and organizing the data
************************************
// A.0. Get regional crosswalk read 
use "$xwalk/consp2cty9-22-13.dta", clear
gen pid = 1000*state+county
keep conspuma pid 
duplicates drop 
tempfile cross
save "`cross'"

// A.1. Get link data for selected industries 
use "$output_NETS/pr_link_est_countyXindustry_d.dta", clear 
gen pr_emp = pr_link*total if industry_cd < 13
bys fips: egen tot_emp = total(total) 
bys fips: egen tot_pr = total(pr_emp)

gen pr_l = tot_pr/tot_emp 
keep pr_l fips tot_emp
rename  fips pid
rename pr_l pr_link_ind
duplicates drop 

merge 1:1 pid using "`cross'"
keep if _merge == 3
drop _merge 

collapse (mean) pr_link_i [fw=tot_emp], by(conspuma)
tempfile pr 
save "`pr'" 

// A.1. Get link data for all industries 
use "$output_NETS/pr_link_est_county_d.dta", clear 
rename  fips pid

merge 1:1 pid using "`cross'"
keep if _merge == 3
drop _merge 

collapse (mean) pr_link  [fw=tot], by(conspuma)

tempfile pr2 
save "`pr2'"

// Get wage changes by skill level 
use "$consp/conspuma-fed-data-102111.dta", clear 
keep conspuma year dSmlwage dUmlwage dSadjlwage dUadjlwage
tempfile wageskill
save "`wageskill'"

// A.2 Merge into analysis data 
use "$consp/conspuma-dec-092313.dta", clear 
merge 1:1 conspuma year using "`wageskill'"
keep if _merge == 3
drop _merge 

merge m:1 conspuma using "`pr'"
keep if _merge == 3
drop _merge 

merge m:1 conspuma using "`pr2'"
keep if _merge == 3
drop _merge 

* generating total employment weights for defining IQR
sum epop, d
gen wgt = epop / (r(N) * r(mean))

sum pr_link_i $pr_wgt if epop > 0, d 
replace pr_link_i = pr_link_i/(r(p75)-r(p25))

sum pr_link $pr_wgt if epop > 0, d 
replace pr_link = pr_link/(r(p75)-r(p25))

rename pr_link pr_link_all
rename pr_link_i pr_link

* keeping variables and saving 
keep *wage *rent *value* epop year state_fips pr_link
save "$data/Replication Data/figure11_data", replace
