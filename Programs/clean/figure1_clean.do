// This file creates the data sets for figure 1. 
// Author: eddie yu
// Date: 2023-10-16

/* Data sources:
Panel A: IRS SOI public data
Panel B: IRS SOI public data
Panel C: IRS SOI public data combined with sector level scalars from NETS
*/

* starting point for panels A and B is SOI_import in the build folder to do the data cleaning
use "$SOI/aggregate_SOI", clear
replace variable = subinstr(variable,"_t_","",.)
replace variable = substr(variable,1,length(variable)-1) if substr(variable,length(variable)-2,length(variable)) == "_"


keep if year <=2006

drop if variable == "manufact"
drop if variable == "tot_"
replace variable = "man_trans_" if inlist(variable,"man_motor_vehicles_")
replace variable = "man_paper_" if inlist(variable,"man_printing_")
replace variable = "man_food_bev_" if inlist(variable,"man_food_","man_beverage_","man_tobacco_")
replace variable = "man_equipment_" if inlist(variable,"man_instruments_","man_elec_components_","man_machines_","man_comps_")
replace variable = "man_textile_" if inlist(variable,"man_textile_","man_leather_","man_apparel_")
replace variable = "other" if substr(variable,1,4) != "man_" 

collapse (sum) Us , by(year variable) 

*** Find largest ones 
preserve 
collapse (sum) Us , by(variable) 
gsort -Us
gen variable2 = variable
replace variable2 = "man_misc_" if _n >=10
gen group = _n 
replace group = 4 if group >= 10 
drop Us 
tempfile group 
save "`group'"
restore

merge m:1 variable using "`group'"
drop _merge 

replace variable2 = "man_equipment_" if variable2 == "man_mach_et_al_"
replace variable2 = "man_food_bev_" if variable2 == "man_foodbevtob_"
replace variable2 = "man_misc_" if variable2 == "man_comps_elec_"

collapse (sum) Us , by(year variable2 group)  //(first) variable2
sort year group

save "$data/Replication Data/figure1_ab_data", replace

***** FIGURE 1.C: FOR DIFFERENT DO FILE *****
use "$data/Replication Data/interim/SOI_imputed_ETR.dta", clear

* Industry shares of gross profits (in 1995)
egen gp_all = sum(gp_agg), by(year)
gen gp_share = gp_agg/gp_all

* 936 Shares
// egen credit_all = sum(Us_Possessions_Tax_Credit), by(year)
// gen cred_share= Us_Possessions_Tax_Credit/credit_all
// gen temp1 = cred_share*(year==1995)
// egen share936_95 = max(temp1), by(variable)


** Exposed Sectors: NAICS codes 22, 31-33, and 48-89
drop if variable == "wholesale_t_" | variable == "retail_t_" ///
	| variable == "ag_t_" | variable == "mining_t_" | variable == "construction_t_"

** Exposed Industries: NAICS codes 321-324, 327, 331, and 336-337.	
drop if variable == "man_wood_" | variable == "man_paper_" ///
	| variable == "man_printing_" | variable == "man_petrol_" ///
	| variable == "man_minerals_" | variable == "man_primary_metals_" ///
	| variable == "man_trans_" | variable == "man_furniture_"
	
* Average across industries
// keep if indshr>0.1
collapse (mean) p_data p_new p_new_fixp [aw=gp_share], by(year) 
*collapse (mean) p_data p_new p_new_fixp , by(year) 
keep if year > 1994 & year < 2008
sort year
sum p_data if year==1995
local p1 = r(mean)
sum p_new if year==1995
local p2 = r(mean)
sum p_new_fixp if year==1995
local p3 = r(mean)
	
replace p_new = p_new - `p2' + `p1'
replace p_new_ = p_new_ - `p3' + `p1'

save "$data/Replication Data/figure1_c_data", replace
