***************************************************************
//Filename: figureA10_clean.do
//Author: eddie yu
//Date: 2023-10-16
//Task: This script creates data for figure A10
***************************************************************

clear all
set more off 
snapshot erase _all

* baseline data 
use "$data/Replication Data/interim/compu_NETS_ETR_sizecutoff.dta", clear

keep gvkey capex_base_1  year n2 PR  DFL_ppe ///
ppe95_bins10 _webal exp_sec exp_ind
save "$data/Replication Data/figureA10_data", replace
