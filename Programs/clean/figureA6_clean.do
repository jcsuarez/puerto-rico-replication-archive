// This file creates the data sets for figure A6 Panel A~D
// Author: eddie yu
// Date: 2023-10-16

/* Data sources:
All panels: NETS establishment data
*/

* starting point for all panels is descriptives/maps_d

clear all
set more off 

// Get the data!
// Prep cross walk for conspuma
use "$xwalk/consp2cty9-22-13.dta", clear
gen pid = 1000*state+county
keep conspuma pid 
duplicates drop 
tempfile cross
save "`cross'"

// Prep cross walk for CZone
use "$xwalk/cw_cty_czone.dta", clear
rename cty_fips pid 
drop cty_l 
duplicates drop 
tempfile cross2
save "`cross2'"

// Get link data 
use "$output_NETS/pr_link_est_county_d.dta", clear 
rename fips95 pid
gen fips_state = floor(pid/1000)
drop if fips_state == 72
** fixing miami-dade county
replace pid = 12025 if pid == 12086

// Merge Cross walks 
merge 1:1 pid using "`cross'"
keep if _merge == 3
drop _merge 

merge 1:1 pid using "`cross2'"
keep if _merge == 3
drop _merge 

// Get Means at State, CZone, and Conspuma 
drop total 
replace pr = pr*100

* State 
preserve 
collapse (mean) pr [fw=cty_pop], by(fips_state) 
rename pr pr_link_state
tempfile link_state
save "`link_state'"
restore 

* Czone 
preserve 
collapse (mean) pr [fw=cty_pop], by(czone) 
rename pr pr_link_czone
tempfile link_czone
save "`link_czone'"
restore 

* Conspuma 
preserve 
collapse (mean) pr [fw=cty_pop], by(conspuma) 
rename pr pr_link_conspuma
tempfile link_conspuma
save "`link_conspuma'"
restore 


merge m:1 fips_state using "`link_state'"
drop _merge 
merge m:1 czone using "`link_czone'"
drop _merge 
merge m:1 conspuma using "`link_conspuma'"
drop _merge 

gen county = pid
drop pid
** fixing miami-dade county
replace county = 12025 if county == 12086

save "$data/Replication Data/figureA6_data", replace
