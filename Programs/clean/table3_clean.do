***************************************************************
//Filename: table3_clean.do
//Author: eddie yu
//Date: 2023-10-16
//Task: This script cleans from interim/NETS_emp_SOI_DFL.dta to clean
//		relevant variables for Table 3
//		All tables here refer to PR_v49.pdf
**************************************************************

clear all
set more off 
snapshot erase _all

* baseline data 
use "$data/Replication Data/interim/NETS_emp_SOI_DFL.dta", clear
snapshot save

keep hqduns year naic2 PR emp_growth ET_* pre post* n2 n3 wgt DFL2 major_sec major_ind
save "$data/Replication Data/table3_data_A", replace

snapshot restore 1
keep hqduns year naic2 PR emp_growth ET_* pre post* n2 n3 wgt DFL2 major_sec major_ind tax_impute tax_impute2
save "$data/Replication Data/table3_data_B", replace

snapshot restore 1
keep hqduns year naic2 PR emp_growth ET_* pre post* n2 n3 wgt DFL2 major_sec major_ind tax_impute tax_impute2 
save "$data/Replication Data/table3_data_C", replace


// snapshot restore 1
// keep gvkey capex_base_p year n2 PR DFL_ppe _webal n3 PR_post IKIK ppe95_bins10 ///
// exp_sec exp_ind PR_post1 PR_post2 PR_pre
// save "$data/Replication Data/R&R2023/paper/tableA4_data", replace
//
// snapshot restore 1
// keep gvkey capex_base_1 year n2 PR DFL_ppe _webal n3 PR_post IK_1 ppe95_bins10 ///
// exp_sec exp_ind PR_post1 PR_post2 PR_pre
// save "$data/Replication Data/R&R2023/paper/tableA5_data", replace
