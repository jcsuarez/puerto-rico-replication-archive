***************************************************************
//Filename: tableA29_clean.do
//Author: eddie yu
//Date: 2023-09-20
//Task: Given ETR_compustat.dta, creates tableA29_data.dta
***************************************************************

/* Data sources:
-NETS, compustat
*/

* starting point is technically 3-wrds_segments/PR_presence_probit_JC

clear
capture log close
set more off
set matsize 8000

use "$WRDS/ETR_compustat.dta", replace
replace PR = 0 if PR == .
gen nonPR = 1-PR

// doing a cross section in 1995 of active firms with positive assets
keep if year == 1995
keep if at > 0 & at != .

// using 4d naics controls
tostring naics, replace
gen n4 = substr(naics,1,4)
gen n3 = substr(naics,1,3)
destring n3, replace
gen n2 = substr(naics,1,2)
destring n2, replace

gen pharma = (n4 == "3254")

replace ads = 0 if ads == .
replace rd  = 0 if rd  == .
replace dltt= 0 if dltt== . 

// winsorizing rd/assets and dltt/assets
replace rd = rd / at
qui sum rd, d
replace rd = r(p99) if rd >r(p99)
replace ads = ads / at
qui sum ads, d
replace ads = r(p99) if ads >r(p99)
replace dltt = dltt / at
qui sum dltt, d
replace dltt = r(p99) if dltt >r(p99)

gen any_ads = ads > 0
gen any_rd  = rd  > 0

gen gpop = gp / (at)
gen PRexp = emp_PR / (emp_PR + emp_US)
gen lnat = ln(at)

probit PR i.n3 lnat pharma rd any_rd gpop any_ads etr,  vce(cl n2)
gen count = 1 if e(sample)
keep if count == 1
drop count
gen prof = (ni > 0) * (ni != .)

foreach i of var etr-nol btd etr_change gpop PRexp at lnat dltt ni {
qui sum `i', d
gen mean`i' = r(mean)
gen sd`i' = r(sd)
replace `i' = (`i' - mean`i') / sd`i'
drop mean`i' sd`i'
}

* keeping variables and saving 
keep n3 lnat pharma etr rd any_rd gpop dltt any_ads ads PR n2
save "$data/Replication Data/tableA29_data", replace

