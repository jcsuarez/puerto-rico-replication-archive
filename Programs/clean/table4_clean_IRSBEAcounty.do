// This file creates the data sets for table 4, County (IRS, and BEA)
// Author: Dan Garrett
// Date: 5-9-2020

/* Data sources:
-NETS, QCEW, IRS, BEA
*/

* starting point is technically 2-main_analysis/es_county_d_IRS_and_BEA for the counties

clear all
set more off 
snapshot erase _all

*******************************************************
* Baseline dataset
*******************************************************
// A.0. Get link data 
use "$output_NETS/pr_link_est_countyXindustry_d.dta", clear 
gen pr_emp = pr_link*total if industry_cd < 13
bys fips: egen tot_emp = total(total) 
bys fips: egen tot_pr = total(pr_emp)

gen pr_l = tot_pr/tot_emp 
keep pr_l fips
rename  fips pid
rename pr_l pr_link_ind
duplicates drop 
tempfile pr 
save "`pr'" 

use "$output_NETS/pr_link_est_county_d.dta", clear 
rename  fips pid
tempfile pr2 
save "`pr2'" 

// A.0. Get IRS data 
use "$irs/irs_income.dta", clear
keep RETURNS GROSSINCOME fips* year 
rename RETURNS emp 
rename GROSSINCOME inc 
keep if year >= 1990
keep if year < 2013

order fips_state fips_county year 
duplicates drop 

// A.3. Fill in gaps
gen pid = 1000*fips_state+fips_county

tsset pid year
tsfill, full

// A.4.  Growth rates. Base year: 1995

foreach var of varlist inc emp { 
	gen temp = `var' if year == 1995
	bysort pid: egen base_`var' = max(temp)
	drop temp

	gen `var'_growth = (`var' - base_`var')/base_`var'
	replace `var'_growth = 0 if `var'_growth == . 
}
	
tempfile irs_dat
save "`irs_dat'", replace
	
	// A.1. Get BEA data 
use "$bea/personalincome_employment_allyears.dta", clear
keep Employment PersonalIncome2 state_fips county_fips year 
rename Emp emp 
rename Per inc 
keep if year >= 1990
keep if year < 2013

rename state_fips fips_state 
rename county_fips fips_county
order fips_state fips_county year 

// A.3. Fill in gaps
gen pid = 1000*fips_state+fips_county

tsset pid year
tsfill, full

// A.4.  Growth rates. Base year: 1995

rename inc inc_BEA
rename emp emp_BEA

foreach var of varlist inc_BEA emp_BEA { 
	gen temp = `var' if year == 1995
	bysort pid: egen base_`var' = max(temp)
	drop temp

	gen `var'_growth = (`var' - base_`var')/base_`var'
	replace `var'_growth = 0 if `var'_growth == . 
}

merge 1:1 pid year using "`irs_dat'"	
drop _merge
	
// A.6. Merge in pr link 
merge m:1 pid using "`pr'"
keep if _merg == 3
drop _merge 

merge m:1 pid using "`pr2'"
drop if year == . 
drop if pr_link == . 
drop _merge 
rename pr_link pr_link_all


** For now, keep only balanced industries . Matt: can you fix it so we don't have to do this? 
drop if base_emp ==0 

sum base_emp if year == 1995, d
gen wgt=base_emp/(r(N) * r(mean))
winsor wgt, p(.01) gen(wgt_w)

sum pr_link_i $pr_wgt if base_emp > 0, d 
replace pr_link_i = pr_link_i/(r(p75)-r(p25))

sum pr_link_al  $pr_wgt if base_emp > 0, d 
replace pr_link_al = pr_link_al/(r(p75)-r(p25))

* keeping variables and saving 
keep *_growth year pr_link_i* wgt fips_state base_emp
keep if inrange(year,1990,1995) | inrange(year,2004,2008) 
save "$data/Replication Data/table4_data_IRSBEA", replace
