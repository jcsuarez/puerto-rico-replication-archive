***************************************************************
//Filename: tableA31_clean.do
//Author: eddie yu
//Date: 2023-09-20
//Task: Given ETR_event_CARs_PR.dta,
//		creates tableA31_data.dta
***************************************************************

/* Data sources:
-NETS, compustat, CRSP
*/

* starting point is technically 4-wrd_value/event_study_ETR_PR_20170115

clear
capture log close
set more off
set matsize 8000

 
use "$WRDS/ETR_event_CARs_PR.dta", clear
keep if PR == 1

set more off
estimates drop _all

gen any_rd = (rd>0) & rd !=. 
gen any_ads = (ads>0) & ads !=. 
gen ln_at = logAT

foreach i of var CAR* {
replace `i' = `i'* 100
}

* for now, only base CAPM model
rename naics naicsh_long
tostring naicsh_long,replace
gen naicsh = substr(naicsh_long,1,2)
egen naics=group(naicsh)


gen gpop = gp / (at)
gen PRexp = emp_PR / (emp_PR + emp_US)
drop etr_1 etr_2

gen day1993 = (eve1 == 1)
gen day1995 = 1 - day1993

* keeping variables and saving 
keep etr-nol btd etr_change any_rd any_ads gpop PRexp CAR_1_3_3 CAR_2_3_3 naics day1993 day1995
save "$data/Replication Data/tableA31_data", replace
