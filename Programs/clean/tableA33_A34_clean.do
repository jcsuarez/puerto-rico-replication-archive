***************************************************************
//Filename: tableA33_A34_clean.do
//Author: eddie yu
//Date: 2023-09-20
//Task: Given following data, creates tableA33_A34_data.dta
//		- state tax analysis data (from Tax Structure paper)
//		- Misallocation Panel data 
//		- County Crosswalk
//		- PR Link X Industry (NETS), county,
//		- QCEW
//		- County Level data
//		- CBP Firm Size Distribution
***************************************************************


/* Data sources:
-NETS, QCEW
*/

* starting point is technically 2-main_analysis/robust_inter_figure // again, I don't know why we didn't use more recent versions but I cant identify differences

clear all
set more off 
snapshot erase _all

*******************************************************
* Baseline dataset
*******************************************************
use "$data/data_additional/St Tax Structure 05_10_2017/state_taxes_analysis.dta", clear
keep if year == 1990 
keep fips salestax propertytax corporate_rate GDP rev_totaltaxes rec_val rdcred rdcred trainingsubsidy jobcreationcred corpinctax totalincentives investment_credit 
rename fips statefips
tempfile incentives 
save "`incentives'"


use "$data/data_additional/Misallocation 5_10_2017/misallocation_paneldata.dta", clear
keep if year == 1990 
keep income_rate_avg fipstate
rename fips statefips
tempfile incentives2 
save "`incentives2'"

// A.0. Get regional crosswalk read 
use "$xwalk/cw_cty_czone.dta", clear
rename cty_fips pid 
duplicates drop 
tempfile cross
save "`cross'"

// A.0. Get link data 
use "$output_NETS/pr_link_est_countyXindustry_naic3.dta", clear 
gen pr_emp = pr_link*total if naic3 < 340 & naic3 > 309
bys fips: egen tot_emp = total(total) 
bys fips: egen tot_pr = total(pr_emp)

gen pr_l = tot_pr/tot_emp 
keep pr_l fips
rename  fips pid
rename pr_l pr_link_ind
duplicates drop 
tempfile pr 
save "`pr'" 

use "$output_NETS/pr_link_est_county_naic3.dta", clear 
rename  fips pid
tempfile pr2 
save "`pr2'" 

// A.1. Get QCEW data 
use "$qcewdata/extract_qcew_1990_2012_naic3.dta", clear
keep if strlen(industry_code) == 3
drop if industry_code == "101" | industry_code == "102"

order fips_state fips_county year industry_code
drop if fips_county == 0 & fips_state != 72
drop if fips_state == 72 & fips_county != 0
keep if own_code == 5		// 0-total covered; 1 Federal; 2 State; 3 Local; 4 Intl Govt; 5 Private.
keep fips_state fips_county year industry_code annual_avg_estabs annual_avg_emplvl total_annual_wages
drop if fips_state == 78

/*   */
collapse (sum) annual_avg_estabs (sum) annual_avg_emplvl (sum) total_annual_wages, by(fips_state fips_county year industry_code)
// A.3. Fill in gaps
gen pid = 1000*fips_state+fips_county

// A.6. Merge in pr link 
merge m:1 pid using "`pr'"
keep if _merg == 3
drop _merge 

merge m:1 pid using "`pr2'"
drop if year == . 
drop if pr_link == . 
drop _merge 
rename pr_link pr_link_all

*replace pr_link_all = pr_link_ind

sum pr_link_al, d 
drop if year == . 

egen pid2 = group(pid indus)

tsset pid2 year
tsfill, full

foreach var of varlist fips_state  {
bysort pid2: egen temp = max(`var')
replace `var' = temp
drop temp
}

foreach var of varlist annual_avg_estabs annual_avg_emplvl total_annual_wages {
replace `var' = 0 if `var' == .
}

// A.4.  Growth rates. Base year: 1995
gen temp = annual_avg_emplvl if year == 1995
bysort pid2: egen base_emp = max(temp)
drop temp

gen emp_growth = (annual_avg_emplvl - base_emp)/base
replace emp_growth = 0 if emp_growth == . 


// Income 
gen inc = total_annual_wages/annual_avg_emplvl
gen temp = inc if year == 1995
bysort pid2: egen base_inc = max(temp)
drop temp

gen inc_growth = (inc - base_inc)/base_inc
replace inc_growth = 0 if inc_growth == . 

// Estabs 
gen estab = annual_avg_estabs
gen temp = estab if year == 1995
bysort pid2: egen base_estab = max(temp)
drop temp

gen estab_growth = (estab - base_estab)/base_estab
replace estab_growth = 0 if estab_growth == . 


// A.5. Define PR Growth Data
bys pid2: gen count = _N
keep if count == 23 
drop count 

sum base_emp, d
gen wgt=base_emp/(r(N) * r(mean))
winsor wgt, p(.01) gen(wgt_w)

sum pr_link_i $pr_wgt if base_emp > 0, d 
replace pr_link_i = pr_link_i/(r(p75)-r(p25))

sum pr_link_al $pr_wgt if base_emp > 0, d 
replace pr_link_al = pr_link_al/(r(p75)-r(p25))


** Keeping only relevant years to make this not crash
keep if inlist(year,1995,2004,2005,2006,2007,2008)

// Add demographics and other shocks 
rename fips_county temp_fips_county
gen fips_county = pid 
merge m:1 fips_county using "$additional/county_level_data_all.dta"
drop if _merge == 2 
drop _merge 

merge m:1 statefips  using "`incentives'"
drop if _merge == 2
drop _merge 

merge m:1 statefips using "`incentives2'"
drop if _merge == 2
drop _merge 


** Add nafta variables 
merge m:1 fips_county using "$output/import_hakobyanlaren_nafta.dta"
drop if _merge == 2 
drop _merge 

tsset pid2 year 
egen ind_st = group(ind fips_state)


**** Merge in share of firm size dist 
merge m:1 pid year using "$output/CBP_firm_size_dist"
drop if _merge == 2
drop _merge 

bys statefips  : egen state_pop = total(population )
replace rev_totaltaxes = rev_tot/state_pop
replace capital_stock = capital_stock/state_pop

keep if inrange(year,2004,2008)

rename total total_orig

rename totalincentives total
rename jobcreationcred job
rename trainingsubsidy train
rename realmw mw
rename rgtowork rtw
rename rdcred rd
rename investment_credit ic
rename corporate_rate corp
rename income_rate_avg ptax
rename propertytax prop
rename salestax sales
rename d_tradeusch_pw trade
rename rev_totaltaxes rev
rename l_sh_routine33a routine
rename locdt_noag nafta 
rename capital_stock capital

gen samp1 = 1

local vars "total job train mw rtw rd ic corp ptax prop sales trade rev routine nafta" // capital

foreach Q of var `vars' { 
replace samp1 = 0 if `Q' == .
sum `Q' [aw=wgt] if inrange(year,2004,2008) & samp1 == 1, d
gen double temp_mu = r(mean) 
gen double temp_z = r(sd)
gen double std_`Q' = (`Q' - temp_mu) / temp_z 

capture: drop temp_*
}

*** need to solidify sample before standardizing 
sum pr_link_ind $pr_wgt if inrange(year,2004,2008) & samp1 == 1, d
gen cen_pr_link_i = pr_link_i -r(mean) 

sum pr_link_all $pr_wgt if inrange(year,2004,2008) & samp1 == 1, d
gen cen_pr_link_a = pr_link_a -r(mean) 


*** winsorizing
foreach var of varlist std_* { 
	winsor `var', g(w_`var') p(.01)
	replace `var' = w_`var'
}

destring industry_code, replace

* keeping variables and saving 
keep emp_growth cen_pr_link_i wgt year fips_state industr std_*
save "$data/Replication Data/tableA33_A34_data", replace

