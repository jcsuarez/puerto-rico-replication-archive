// This file creates the data sets for figure A3, all panels A, B, C
// Author: eddie yu
// Date: 2023-10-16

/* Data sources:
QCEW public files
*/

* starting point for all panels is 1-descriptives/descriptive

clear all
set more off 
snapshot erase _all

************************************
* Grabbing and organizing the data
************************************
// 1. Load Data
use "$qcewdata/extract_qcew_1990_2012.dta", clear

order fips_state fips_county year industry_code

drop if fips_county == 0 & fips_state != 72
drop if fips_state == 72 & fips_county != 0

keep if own_code == 5		// 0-total covered; 1 Federal; 2 State; 3 Local; 4 Intl Govt; 5 Private.

keep fips_state fips_county year industry_code annual_avg_estabs annual_avg_emplvl total_annual_wages

drop if fips_state == 78

// 2. Label industries		

gen industry_cd = .
replace industry_cd = 0 if industry_code == "10"	// "Total, all industries"
replace industry_cd = 1 if industry_code == "311"
replace industry_cd = 2 if industry_code == "314"
replace industry_cd = 3 if industry_code == "315"
replace industry_cd = 4 if industry_code == "325"	// note: include 3254
replace industry_cd = 5 if industry_code == "3254"
replace industry_cd = 6 if industry_code == "326"
replace industry_cd = 7 if industry_code == "316"
replace industry_cd = 8 if industry_code == "332"
replace industry_cd = 9 if industry_code == "333"
replace industry_cd = 10 if industry_code == "335"
*replace industry_cd = 11 if naic3 == 
replace industry_cd = 12 if industry_code == "31-33"
replace industry_cd = 13 if industry_code == "52" | industry_code == "53"
replace industry_cd = 14 if inlist(industry_code,"54","55","61","62","71","72","81")
replace industry_cd = 15 if inlist(industry_code,"42","44-45")
*replace industry_cd = 16 if industry_code == .


label define l_naic3 0 "Total, all industries" 1 "Food Mfg" 2 "Textile mill products" 3 "Apparel" 4 "Chemicals" 5 "Pharmaceuticals" ///
 6 "Rubber and Plastic" 7 "Leather" 8 "Fabricated metal" 9 "Machinery" 10 "Electrical equip" ///
 11 "Instruments" 12 "Other mfg" 13 "Finance, insurance, real estate" 14 "Services" ///
 15 "Wholesale and retail" 16 "Other non-mfg", replace

label values industry_cd l_naic3


drop industry_code
collapse (sum) annual_avg_estabs (sum) annual_avg_emplvl (sum) total_annual_wages, by(fips_state fips_county year industry_cd)


// 3. Fill in gaps
egen panelvar = group(fips_state fips_county industry_cd)

tsset panelvar year
tsfill, full

foreach var of varlist fips_state fips_county industry_cd {
bysort panelvar: egen temp = max(`var')
replace `var' = temp
drop temp
}

foreach var of varlist annual_avg_estabs annual_avg_emplvl total_annual_wages {
replace `var' = 0 if `var' == .
}


* keeping variables and saving 
keep if fips_state == 72 & fips_county == 0
save "$data/Replication Data/figureA3_data", replace
