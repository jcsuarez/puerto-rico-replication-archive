***************************************************************
//Filename: tableA19_clean.do
//Author: eddie yu
//Date: 2023-09-20
//Task: Given county Xwalk, PR link (NETS), county population estimates,
//		creates tableA19_data.dta
***************************************************************
/* Data sources:
-NETS, QCEW, ADH
*/

* starting point is technically 2-main_analysis/ADH_county_regression_levels

clear all
set more off 
snapshot erase _all
estimates clear

*******************************************************
* Baseline dataset
*******************************************************
// A.0. Get regional crosswalk read 
use "$xwalk/cw_cty_czone.dta", clear
rename cty_fips pid 
duplicates drop 
tempfile cross
save "`cross'"

// A.0. Get link data 
use "$output_NETS/pr_link_est_countyXindustry_md.dta", clear 
gen pr_emp = pr_link*total if industry_cd < 13
bys fips: egen tot_emp = total(total) 
bys fips: egen tot_pr = total(pr_emp)

gen pr_l = tot_pr/tot_emp 
keep pr_l fips
rename  fips pid
rename pr_l pr_link_ind
duplicates drop 
tempfile pr 
save "`pr'" 

use "$output_NETS/pr_link_est_county_md.dta", clear 
rename  fips pid
tempfile pr2 
save "`pr2'" 

// A.1. Getting the outcome and other county data
use "$additional/county population estimates/county_pop_ests.dta", replace
merge 1:1 pid year using "$additional/county population estimates/county_manu_emp_ests.dta"
drop _merge

replace manu_emp = 0 if manu_emp == .
drop if working_pop == .

rename pid fips_county

merge m:1 fips_county using "$output_NETS/pr_link_est_county_addvars_emp.dta"

keep if _merge == 3
drop _merge

rename fips_county pid

// A.6. Merge in pr link 
merge m:1 pid using "`pr'"
keep if _merg == 3
drop _merge 

merge m:1 pid using "`pr2'"
drop if year == . 
drop if pr_link == . 
drop _merge 
rename pr_link pr_link_all

sum pr_link_al, d 
drop if year == . 

tsset pid year
tsfill, full

// Generating the ADH measure
gen ADH = manu_emp / working_pop
drop if czone == .

// A.4.  Growth rates. Base year: 1995
gen temp = working_pop if year == 1995
bysort pid: egen base_pop = max(temp) // this is now working age population instead of employment
drop temp
gen temp = ADH if year == 1995
bysort pid: egen base_ADH = max(temp) // this is now working age population instead of employment
drop temp

gen ADH_growth = (ADH - base_ADH)*100 // Difference of the levels file!
replace ADH_growth = 0 if ADH_growth == . 

// A.5. Define PR Growth Data
** For now, keep only balanced industries . Matt: can you fix it so we don't have to do this? 
bys pid: gen count = _N
keep if count == 23 
drop count 

sum base_pop, d
gen wgt=base_pop/(r(N) * r(mean))

sum pr_link_i $pr_wgt if base_pop > 0, d 
replace pr_link_i = pr_link_i/(r(p75)-r(p25))

sum pr_link_al $pr_wgt if base_pop > 0, d 
replace pr_link_al = pr_link_al/(r(p75)-r(p25))


// B. Graphs with All PR Link 
cd "$output" 
set more off 
xi i.year|pr_link_i $pra_control, noomit
drop _IyeaXp*1995


* keeping variables and saving 
keep ADH_growth year pr_link_i wgt statefips pid base_pop
save "$data/Replication Data/tableA19_data", replace
