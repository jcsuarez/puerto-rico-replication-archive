***************************************************************
//Filename: table2_clean.do
//Author: eddie yu
//Date: 2023-09-20
//Task: This script cleans from interim/compu_NETS_ETR_sizecutoff.dta to clean
//		relevant variables for Table 2
//		All tables here refer to PR_v49.pdf
**************************************************************

clear all
set more off 
snapshot erase _all

* baseline data 
use "$data/Replication Data/interim/compu_NETS_ETR_sizecutoff.dta", clear
snapshot save


keep gvkey capex capex_base year n2 PR fs_capx ppe95_bins10 ///
ppent ppe tot_ias capxs emps nis DFL_ppe _webal n3 PR_* IK exp_sec exp_ind
save "$data/Replication Data/table2_data_A", replace

use "$data/Replication Data/interim/onestep_compu_SOI_clean", clear
keep year gvkey ppe95_bins10 n2 PR PR_* DFL_ppe _webal exp_sec exp_ind tax_* ET_*
save "$data/Replication Data/table2_data_B", replace

use "$data/Replication Data/interim/onestep_compu_SOI_clean", clear
keep year gvkey ppe95_bins10 n2 PR PR_* pre DFL_ppe _webal exp_sec exp_ind tax_* ET_* capex_base post*
save "$data/Replication Data/table2_data_C", replace
