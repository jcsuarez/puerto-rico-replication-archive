// This file creates the data sets for all QCEW per capita
// Author: Dan Garrett
// Date: 4-29-2020

/* Data sources:
-NETS and QCEW
*/

* starting point is technically 2-main_analysis/es_county_aw_percapita

clear all
set more off 
snapshot erase _all

*******************************************************
* Baseline dataset
*******************************************************
// A.0. Get link data 
use "$output_NETS/pr_link_est_countyXindustry_d.dta", clear 
gen pr_emp = pr_link*total if industry_cd < 13
bys fips: egen tot_emp = total(total) 
bys fips: egen tot_pr = total(pr_emp)

gen pr_l = tot_pr/tot_emp 
keep pr_l fips
rename  fips pid
rename pr_l pr_link_ind
duplicates drop 
tempfile pr 
save "`pr'" 

use "$output_NETS/pr_link_est_county_d.dta", clear 
rename  fips pid
tempfile pr2 
save "`pr2'" 

// A.1. Get QCEW data 
use "$qcewdata/extract_qcew_1990_2012_naic3.dta", clear
keep if strlen(industry_code) == 3
drop if industry_code == "101" | industry_code == "102"

order fips_state fips_county year industry_code
drop if fips_county == 0 & fips_state != 72
drop if fips_state == 72 & fips_county != 0
keep if own_code == 5		// 0-total covered; 1 Federal; 2 State; 3 Local; 4 Intl Govt; 5 Private.
keep fips_state fips_county year industry_code annual_avg_estabs annual_avg_emplvl total_annual_wages
drop if fips_state == 78

collapse (sum) annual_avg_estabs (sum) annual_avg_emplvl (sum) total_annual_wages, by(fips_state fips_county year industry_code)

// A.3. Fill in gaps
gen pid = 1000*fips_state+fips_county

// A.6. Merge in pr link 
merge m:1 pid using "`pr'"
keep if _merg == 3
drop _merge 

merge m:1 pid using "`pr2'"
drop if year == . 
drop if pr_link == . 
drop _merge 
rename pr_link pr_link_all

*replace pr_link_all = pr_link_ind

sum pr_link_al, d 

drop if year == . 
merge m:1 pid year using "$additional/county population estimates/county_pop_ests.dta"
keep if _merge == 3
drop _merge

*collapse (rawsum) annual_avg_estabs (rawsum) annual_avg_emplvl (rawsum) total_annual_wages (mean) pr_link_ind (mean) pr_link_all [fw=total] , by( fips_state pid year industry_code) // Dan: this isn't actually collapsing anything

*** SAVING DATA TO BE USED WITH CONSPUMA AND CZONES
snapshot save 

egen pid2 = group(pid indus)

tsset pid2 year
tsfill, full

foreach var of varlist fips_state  {
bysort pid2: egen temp = max(`var')
replace `var' = temp
drop temp
}

foreach var of varlist annual_avg_estabs annual_avg_emplvl total_annual_wages {
replace `var' = 0 if `var' == .
replace `var' = `var' / tot_pop
}

// A.4.  Growth rates. Base year: 1995
gen temp = annual_avg_emplvl if year == 1995
bysort pid2: egen base_emp1 = max(temp)
drop temp

gen temp = annual_avg_emplvl * tot_pop if year == 1995
bysort pid2: egen base_pop = max(temp)
drop temp

gen emp_growth = (annual_avg_emplvl - base_emp1)/base_emp1
replace emp_growth = 0 if emp_growth == . 
// Income 
gen inc = total_annual_wages
* /annual_avg_emplvl
gen temp = inc if year == 1995
bysort pid2: egen base_inc = max(temp)
drop temp

gen inc_growth = (inc - base_inc)/base_inc
replace inc_growth = 0 if inc_growth == . 


* balancing industries
bys pid2: gen count = _N
keep if count == 23 
drop count 

drop if base_emp ==0 
drop if base_pop ==0 

sum base_pop if year == 1995, d
gen wgt=base_pop/(r(N) * r(mean))
winsor wgt, p(.01) gen(wgt_w)

sum pr_link_i $pr_wgt if base_emp > 0, d 
replace pr_link_i = pr_link_i/(r(p75)-r(p25))

drop pr_link_al 

tsset pid2 year 
egen ind_st = group(ind fips_state)
destring industry_code, replace
 
* keeping variables and saving 
keep emp_growth year pr_link_* wgt industr fips_state pid pid2 wgt_w base_emp annual_avg_estabs annual_avg_emplvl total_annual_wages
save "$data/Replication Data/figure7_data_PC", replace

