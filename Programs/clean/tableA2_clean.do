***************************************************************
//Filename: tableA2_clean.do
//Author: eddie yu
//Date: 2023-09-20
//Task: Given pr_link_est_county_addvars_emp.dta &
//		pr_link_est_county_addvars_emp.dta, creates tableA2_data.dta
***************************************************************

/* Data sources:
-QCEW and NETS
*/

* starting point is 1-descriptive/top_exposure_tables

clear all
set more off 
snapshot erase _all

************************************
* Grabbing and organizing the data
************************************
* establishment version
use "$output_NETS/pr_link_est_county_addvars_emp.dta", clear

keep pr_link QName population
drop if QName == ""
drop if pr_link == .

local pop_thresh = 100000
drop if population < `pop_thresh'

sort pr_link
gen id = _n

keep if (id < 11 |  id > 447)

tempfile temp1
save "`temp1'"

* employment version
use "$output_NETS/pr_link_est_county_addvars_emp.dta", clear
keep emp95 pr_emp95  QName population
gen pr_link_emp = pr_emp95 / emp95

drop if QName == ""
drop if pr_link == .

local pop_thresh = 100000
drop if population < `pop_thresh'

sort pr_link_emp
gen id = _n

keep if (id < 11 |  id > 447)

rename QName QName2

merge 1:1 id using "`temp1'"
order id QName pr_link QName2 pr_link_emp
keep id QName pr_link QName2 pr_link_emp
format pr_link* %4.3fc

* actual dataset for regressions
keep  id QName pr_link QName2 pr_link_emp
save "$data/Replication Data/tableA2_data", replace

