***************************************************************
//Filename: figureA2_clean.do
//Author: eddie yu
//Date: 2023-09-20
//Task: With newly pulled Compustat Historical Segments data pulled 2023-07-25
//		(WRDS/compu_segments_072523.dta) and conducting compustat_merge_manual.do,
//		and conducting compustat_ETR_calc_segments.do, we created ETR_compustat_Segments_0923.dta
//		Now we cleanse this Compustat Segments data to prepare for final analysis
***************************************************************

clear all
set more off 
snapshot erase _all

************************************
* Grabbing and organizing the data
************************************
*** other Compustat and ETR data
use "$WRDS/ETR_compustat_Segments.dta", replace
replace PR = 0 if PR == .
gen nonPR = 1-PR

foreach j in 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20 21 22 23{
gen PR_phaseout`j' = (PR==1) * (year == 1989 + `j')
}
gen PR_post = 0
foreach j in 8 9 10 11 12 13 14 15 16 17 18 19 20 21 22 23{
replace PR_post = PR_post + PR_phaseout`j'
}
* fun descriptive regression without defining counterfactual group:
//reg fs_capx PR PR_phaseout* i.year, r 
//reg fs_emp PR PR_phaseout* i.year, r 
 
** fixing naics to be constant at 1995 or first listing
preserve
collapse (firstnm) naicsh, by(gvkey)
rename naicsh f_naics
tempfile naics
save "`naics'",replace
restore

merge m:1 gvkey using "`naics'"
gen temp = naicsh if year == 1995
destring temp, replace
bys gvkey: egen naics = min(temp)
tostring naics, replace
replace naicsh = naics
replace naicsh = f_naics if naicsh == "."
drop naics temp f_naics _merge
 
* Defining the regression sample: 
*tostring naics, gen(naicsh)

*** cleaning up NAICS categories to work with SOI data
gen naics3 = substr(naicsh,1,3)
gen naics2 = substr(naics3,1,2)
replace naics3 = "11" if naics2 == "11"
replace naics3 = "21" if naics2 == "21"
replace naics3 = "23" if naics2 == "23"
replace naics3 = "42" if naics2 == "42"
replace naics3 = "44" if naics2 == "44"
replace naics3 = "45" if naics2 == "45" 
replace naics3 = "48" if naics2 == "48"
replace naics3 = "49" if naics2 == "49"
replace naics3 = "51" if naics2 == "51"
replace naics3 = "53" if naics2 == "53"
replace naics3 = "54" if naics2 == "54"
replace naics3 = "61" if naics2 == "61"
replace naics3 = "62" if naics2 == "62"
replace naics3 = "71" if naics2 == "71"
replace naics3 = "999" if naics2 == ""
drop if naics3 == "33" // dropping weird obs
*xtile bin_rev = revts , n(20)
*snapshot save

*** Some varaibles generated by JC
bys cusip : gen balance = _N 
gen is_bal = (balance == 23) 
gen post = (year>1995)
/*
gen post_d = (year>1998)
gen post_p = (year>2006)
gen post_PR_JC = post*PR 
gen post_PR_JC2 = (post_d-post)*PR 
gen post_PR_JC3 = (post_p-post_d)*PR 
*/

winsor fs_capx , g(w_fs_capx) p(.01)
replace fs_capx = w_fs
winsor rev , g(w_rev) p(.01)
gen ln_rev = ln(1+w_rev)
gen zero_r = (w_rev ==0 )
gen log_tot_emp = ln(1+tot_emp)
gen zero_capx= (fs_capx == 0) 

** Generate mean version 
gen mean_fs = (fs_emp+ fs_cap)/2
replace mean_fs = fs_cap if mean_fs==. & fs_cap!=.
replace mean_fs = fs_emp if mean_fs==. & fs_emp!=.
gen pos_mean = (mean_fs > 0 )  if mean_fs !=. 

** Generate filled in predicted employment 
destring naics3, gen(temp_n3)
reg fs_emp log_tot_emp ln_rev i.zero_cap c.fs_capx i.year  i.temp_n3
predict h_fs_emp , xb
replace h_fs_emp = fs_emp if fs_emp!=.

** Generate IHS of certain variables 
gen IHS_emp = ln(h_fs_emp +sqrt(1+h_fs_emp^2))
gen IHS_capx = ln(fs_capx +sqrt(1+fs_capx^2))
gen IHS_rev = ln(rev+sqrt(1+rev^2))
gen IHS_mean = ln(mean_fs+sqrt(1+mean_fs^2))

** Make start and end year markers
bys gvk: egen min_year = min(year)
bys gvk: egen max_year = max(year)

** Get mean PR by industry 
preserve 
keep if year < 1997
collapse (mean) PR , by(naics3)
rename PR n3_PR 
gen n3 = naics3
*tempfile ind_pr
save "$output_NETS/ind_pr", replace 
restore 

merge m:1 naics3 using "$output_NETS/ind_pr"
drop _merge
*merge 1:1 gvkey year using "`domestic_IK'"


* making investment be between 0 and 1 (1st and 99th percentiles)
gen pre_ppent = ppent if year < 1996
gen pre_cap = capx if year < 1996
bys gvkey: egen ppent_base = mean(pre_ppent)
bys gvkey: egen capx_base = mean(pre_cap)
gen capex_base = ((capx / ppent_base) -  (capx_base / ppent_base))
gen IK = (capx / ppent_base)

gen capex_base_p = ((capx / ppent_base) -  (capx_base / ppent_base)) /  (capx_base / ppent_base) 
gen IKIK = (capx / ppent_base) / (capx_base / ppent_base)

** pulling from July 11 Ftax file
sum fs_rev , d
gen ftax_t = txfed / pi
gen ftax = txfed /  (pi * (1 - r(mean)))

*winsorizing
foreach Q of var capex_base capex_base_p IK IKIK ftax {
//domestic_IK domestic_IK_base
sum `Q', d

gen `Q'_1 = `Q'
/*
replace `Q'_1 = r(p1) if `Q' < r(p1) & `Q' != .
replace `Q'_1 = r(p99) if `Q' > r(p99) & `Q' != .
*/
egen temp1 = pctile(`Q'), p(1) 
egen temp2 = pctile(`Q'), p(99)
replace `Q'_1 = temp1 if `Q'_1 < temp1 & `Q' != .
replace `Q'_1 = temp2 if `Q'_1 > temp2 & `Q' != .

capture: drop temp1 temp2

sum `Q', d
replace `Q' = r(p5) if `Q' < r(p5) & `Q' != .
replace `Q' = r(p95) if `Q' > r(p95) & `Q' != .

}


***** Run regressions on following outcomes: 
* Run only on data until 2006 
keep if year < 2007 
keep if year > 1990
* Run only on firms wth obs before and after 1996 
keep if min_year < 1996 & max_year > 1996
* Run only on firms wth at least 7 obs overall
bys cusip : gen balance2 = _N 
keep if balance2 > 6

gen ds_capx = 1 - fs_capx	
gen US = (fic=="USA")


* some DFL weights (original version to produce PR_v49)
xtile ppe95 = ppent if year == 1995 , n(50) 
destring n3, replace
destring naics2, gen(n2_num)

logit PR i.n3 i.ppe95 if year == 1995

capture: drop phat min_phat w w_phat
predict phat, pr 
winsor phat, p(.01) g(w_phat) 
replace phat = w_phat
bys gvkey: egen min_phat = min(phat) 
* ATE
* gen DFL = (PR/min_phat+(1-PR)/(1-min_phat))
* ATOT
gen DFL = (PR+(1-PR)*min_phat/(1-min_phat))

tostring n3, replace
encode naics2, generate(n2)

************************************************************************
* for ETR analysis
* merging with extra tax variables
merge 1:1 gvkey year using "$WRDS/compustat_extra_tax_vars"
keep if _merge == 3
drop _merge
merge 1:1 gvkey year using "$WRDS/compustat_EBIT"
keep if _merge == 3
drop _merge

* total tax defintion
sort gvkey year
gen tottax = 100 * txt / pi

gen pre_ftax = ftax if year < 1996
gen pre_tottax = tottax if year < 1996
bys gvkey: egen ftax_base = mean(pre_ftax)
bys gvkey: egen tottax_base = mean(pre_tottax)
gen ftax_d = ftax - ftax_base
gen tottax_d = tottax - tottax_base

************************************************************************

**** Exposed industry/sector indicators
gen exp_sec = inlist(n2,3,5,6,7,11,12)
gen exp_ind = ( ~inlist(n3,"324","323","337","322","327","336","331","321"))*exp_sec	


*** NEW Specification: DFL with PPE 95 ***
* create ppe95 bins
drop ppe95
gen ppe95 = ppent if year == 1995
xfill ppe95, i(gvkey)
xtile ppe95_bins10 = ppe95, n(10)
xtile ppe95_bins5 = ppe95, n(5)
xtile ppe95_bins3 = ppe95, n(3)	

save "$data/Replication Data/figureA2_data", replace
