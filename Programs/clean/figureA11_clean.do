***************************************************************
//Filename: figureA11_clean.do
//Author: eddie yu
//Date: 2023-09-20
//Task: Given size-cutoff data of interim/compu_NETS_ETR_sizecutoff.dta,
//		this script produces dataset figureA11_data
***************************************************************


/* Data sources:
-Compustat
-NETS to determine PR firms
*/

clear all
set more off 
snapshot erase _all

************************************
* Grabbing and organizing the data
************************************
use "$data/Replication Data/interim/compu_NETS_ETR_sizecutoff.dta", clear 

keep gvkey capex_base capex_base_p capex_base_1  year n2 PR IHS_rev log_tot_emp ///
IK IK_1 IKIK DFL_ppe ppe95_bins10 _webal exp_sec exp_ind
save "$data/Replication Data/figureA11_data", replace
