***************************************************************
// Filename: figureA24_A25_clean.do
// This file creates the data sets for figure A18 & A19, even study with 2 lines 
// Author: eddie yu
// Date: 2023-10-16
***************************************************************

/* Data sources:
-NETS, compustat, CRSP
*/

* the version that went in the paper was not the most up-to-date, but going with it since it was in the submitted paper and there aren't any visual differences I can see in newer versions.  
* starting point is technically 4-wrd_value/event_study_combined_manual

clear
capture log close
set more off
set matsize 8000

*stocks1990-1996 is the old file with only exact name matches
use "$WRDS/stocks1991-1996", replace

* rescalling the outcome
replace ret = ret * 100

{
***** Organizing data
* making a list of even dates (checked manually for correctness)
local event_dates "12100 13068" // 13083"
*12100 is Feb 16, 1993, the Tuesday after Clinton and Pryor talked
*13068 is 10-12-1995
*13083 is 10-27-1995 (note the overlap)
}


*generating numbers of trading days instead of actual dates
sort permno date
by permno: gen datenum=_n 
foreach event_date in `event_dates' {
by permno: gen target`event_date'=datenum if date==`event_date'
egen td`event_date'=min(target`event_date'), by(permno)
drop target`event_date'
gen dif`event_date'=datenum-td`event_date'
}
* Defining the estimation and event windows (counting windows to make sure they
* 											 actually are trading on those days)
local window = 100
local min_obs = 70
local prewindow = 5

foreach event_date in `event_dates' {
by permno: gen event`event_date'=1 if dif`event_date'>=0 & dif`event_date'<=15 & ret != .
replace event`event_date'=0 if event`event_date' == .

*including some pre-period
by permno: gen pre_event`event_date'=1 if dif`event_date'>=-`prewindow' & dif`event_date'<=15 & ret != .
replace pre_event`event_date'=0 if pre_event`event_date' == .

* estimation windows will also be used to cap the event windows so there is only 1 dummy
* variable when outputting tables.
forvalues i=0(1)5 {
local j = `i' * 3

by permno: gen estimation`event_date'_`i'=1 if dif`event_date'>= -`window' & dif`event_date'<=`j' & ret != .
replace estimation`event_date'_`i'=0 if estimation`event_date'_`i' == .
* cutting out estimation on firms with limited pre-estimation periods
by permno: egen sample_size = sum(estimation`event_date'_`i')
replace estimation`event_date'_`i'=0 if sample_size < `min_obs' 

* Getting a non-event sample for estimating counterfactuals and CARs
by permno: gen pre_est`event_date'_`i'=1 if dif`event_date'>= -`window' & dif`event_date'< 0 & ret != .
replace pre_est`event_date'_`i'=0 if pre_est`event_date'_`i' == .

*dropping those firms without enough observations for estimation in the pre-sample
replace pre_est`event_date'_`i'=0 if sample_size < `min_obs' 
drop sample_size

* generating interactions for graphical explanation of event studies
gen event_int_`event_date'_`i' =  pre_event`event_date' * (dif`event_date' + `prewindow' + 1) * estimation`event_date'_`i'
}
}
expand 2 if estimation12100_5 == 1, gen(eve1)
expand 2 if estimation13068_5 == 1, gen(eve2)

gen pooled_eve = eve1 + eve2
keep if pooled_eve == 1

*Sometimes dividends are given in two lines, dropping those (does not affect return, price, etc.)
duplicates drop permno date, force

tempfile events
save "`events'", replace

*** Pulling in the FF 3 factors, momentum, and risk free rate.
{
* These data also include market returns but they are less precise than those from CRSP

import delimited "$WRDS/F-F_Research_Data_Factors_daily.txt",  clear delim(" ", collapse) stringc(1)
*turning dates into a form that matches what we already have
rename date date_1
gen date = date(date_1, "YMD")
format date %tdNN/DD/CCYY
drop date_1

tempfile factors
save "`factors'", replace

**** merging factors with stock returns
use "`events'", clear

merge m:1 date using "`factors'"
keep if _merge == 3
drop _merge
save "`events'", replace

* now momentum
import delimited "$WRDS/F-F_Momentum_Factor_daily.txt",  clear delim(" ", collapse) stringc(1)
*turning dates into a form that matches what we already have
rename date date_1
gen date = date(date_1, "YMD")
format date %tdNN/DD/CCYY
drop date_1

tempfile factors
save "`factors'", replace

**** merging factors with stock returns
use "`events'", clear

merge m:1 date using "`factors'"
keep if _merge == 3
drop _merge
rename mom MOM

save "`events'", replace
}

* extended cleaning
xtset permno datenum

*creating values from previous trading day for value weighting
*NOTE: Some prices are coded as negatives (not -99 that means missing) so I use the absolute value
gen val = abs(l.prc * l.shrout)
egen id=group(permno eve1)

* keeping variables and saving 
keep event_int_* estimation12100_5 estimation13068_5 id mktrf smb hml MOM ret
keep if estimation12100_5==1 | estimation13068_5 ==1
save "$data/Replication Data/figureA24_A25_data", replace

