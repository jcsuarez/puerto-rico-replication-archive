// This file creates the data sets for all QCEW heterogeneous by size
// Author: Dan Garrett
// Date: 4-29-2020

/* Data sources:
-NETS and QCEW
*/

* starting point is technically 2-main_analysis/es_county_large_firms_20190617

clear all
set more off 
snapshot erase _all

*******************************************************
* Baseline dataset
*******************************************************
// A.0. Get regional crosswalk read 
use "$xwalk/cw_cty_czone.dta", clear
rename cty_fips pid 
duplicates drop 
tempfile cross
save "`cross'"

// A.0. Get link data 
use "$output_NETS/pr_link_est_countyXindustry_naic3.dta", clear 
gen pr_emp = pr_link*total if inrange(naic3,300,340)
gen pr_emp_s1 = pr_link_s1*total if inrange(naic3,300,340)
gen pr_emp_s2 = pr_link_s2*total if inrange(naic3,300,340)
gen pr_emp_s3 = pr_link_s3*total if inrange(naic3,300,340)
gen pr_emp_s4 = pr_link_s4*total if inrange(naic3,300,340)
bys fips: egen tot_emp = total(total) 
bys fips: egen tot_pr    = total(pr_emp)
bys fips: egen tot_pr_s1 = total(pr_emp_s1)
bys fips: egen tot_pr_s2 = total(pr_emp_s2)
bys fips: egen tot_pr_s3 = total(pr_emp_s3)
bys fips: egen tot_pr_s4 = total(pr_emp_s4)

gen pr_l = tot_pr/tot_emp 
gen pr_l_s1 = tot_pr_s1/tot_emp 
gen pr_l_s2 = tot_pr_s2/tot_emp 
gen pr_l_s3 = tot_pr_s3/tot_emp 
gen pr_l_s4 = tot_pr_s4/tot_emp 
keep pr_l pr_l_* fips
rename  fips pid
rename pr_l    pr_link_ind
rename pr_l_s1 pr_link_s1_ind
rename pr_l_s2 pr_link_s2_ind
rename pr_l_s3 pr_link_s3_ind
rename pr_l_s4 pr_link_s4_ind
duplicates drop 
tempfile pr 
save "`pr'" 

use "$output_NETS/pr_link_est_county_naic3.dta", clear 
rename  fips pid
tempfile pr2 
save "`pr2'" 

// A.1. Get QCEW data 
use "$qcewdata/extract_qcew_1990_2012_naic3.dta", clear
keep if strlen(industry_code) == 3
drop if industry_code == "101" | industry_code == "102"

order fips_state fips_county year industry_code
drop if fips_county == 0 & fips_state != 72
drop if fips_state == 72 & fips_county != 0
keep if own_code == 5		// 0-total covered; 1 Federal; 2 State; 3 Local; 4 Intl Govt; 5 Private.
keep fips_state fips_county year industry_code annual_avg_estabs annual_avg_emplvl total_annual_wages
drop if fips_state == 78
collapse (sum) annual_avg_estabs (sum) annual_avg_emplvl (sum) total_annual_wages, by(fips_state fips_county year industry_code)

// A.3. Fill in gaps
gen pid = 1000*fips_state+fips_county

// A.6. Merge in pr link 
merge m:1 pid using "`pr'"
keep if _merg == 3
drop _merge 

merge m:1 pid using "`pr2'"
drop if year == . 
drop if pr_link == . 
drop _merge 
rename pr_link pr_link_all

*replace pr_link_all = pr_link_ind

sum pr_link_al, d 

drop if year == . 

*collapse (rawsum) annual_avg_estabs (rawsum) annual_avg_emplvl (rawsum) total_annual_wages (mean) pr_link_ind (mean) pr_link_all [fw=total] , by( fips_state pid year industry_code) // Dan: this isn't actually collapsing anything

egen pid2 = group(pid indus)

tsset pid2 year
tsfill, full

foreach var of varlist fips_state  {
bysort pid2: egen temp = max(`var')
replace `var' = temp
drop temp
}

foreach var of varlist annual_avg_estabs annual_avg_emplvl total_annual_wages {
replace `var' = 0 if `var' == .
}

// A.4.  Growth rates. Base year: 1995
gen temp = annual_avg_emplvl if year == 1995
bysort pid2: egen base_emp = max(temp)
drop temp

gen emp_growth = (annual_avg_emplvl - base_emp)/base
replace emp_growth = 0 if emp_growth == . 

// Income 
gen inc = total_annual_wages
* /annual_avg_emplvl
gen temp = inc if year == 1995
bysort pid2: egen base_inc = max(temp)
drop temp

gen inc_growth = (inc - base_inc)/base_inc
replace inc_growth = 0 if inc_growth == . 

// Estabs 
gen estab = annual_avg_estabs
gen temp = estab if year == 1995
bysort pid2: egen base_estab = max(temp)
drop temp

gen estab_growth = (estab - base_estab)/base_estab
replace estab_growth = 0 if estab_growth == . 

** For now, keep only balanced industries . Matt: can you fix it so we don't have to do this? 
bys pid2: gen count = _N
keep if count == 23 
drop count 

drop if base_emp ==0 

sum base_emp if year == 1995, d
gen wgt=base_emp/(r(N) * r(mean))
winsor wgt, p(.01) gen(wgt_w)

sum pr_link_ind $pr_wgt if base_emp > 0, d 
replace pr_link_i = pr_link_i/(r(p75)-r(p25))
sum pr_link_ind $pr_wgt if base_emp > 0, d 

scalar sd_main = r(sd)

** v1 normalizes each to the baseline case
** v2 normalizes each individually
** v3 normalizes to the same SD
sum pr_link_s1_ind $pr_wgt if base_emp > 0, d 
*replace pr_link_s1_ind = pr_link_s1_ind/(r(p75)-r(p25))
replace pr_link_s1_ind = pr_link_s1_ind * sd_main / r(sd)

sum pr_link_s2_ind $pr_wgt if base_emp > 0, d 
*replace pr_link_s2_ind = pr_link_s2_ind/(r(p75)-r(p25))
replace pr_link_s2_ind = pr_link_s2_ind * sd_main / r(sd)

sum pr_link_s3_ind $pr_wgt if base_emp > 0, d 
*replace pr_link_s3_ind = pr_link_s3_ind/(r(p75)-r(p25))
replace pr_link_s3_ind = pr_link_s3_ind * sd_main / r(sd)

sum pr_link_s4_ind $pr_wgt if base_emp > 0, d 
*replace pr_link_s4_ind = pr_link_s4_ind/(r(p75)-r(p25))
replace pr_link_s4_ind = pr_link_s4_ind * sd_main / r(sd)

tsset pid2 year 
egen ind_st = group(ind fips_state)
destring industry_code, replace
 
capture: drop pr_link_al 

* keeping variables and saving 
keep emp_growth year pr_link_* wgt industr fips_state pid pid2 wgt_w base_emp annual_avg_estabs annual_avg_emplvl total_annual_wages
save "$data/Replication Data/figure7_data_size", replace

