***************************************************************
//Filename: tableA33_A34.do
//Author: eddie yu
//Date: 2023-09-20
//Task: Given tableA33_A34_data.dta, creates Table A33 & Table A34
***************************************************************

/* Data sources:
QCEW, NETS, many existing papers
*/

clear all
set more off
snapshot erase _all 
estimates clear

*******************************************************
* starting with data
*******************************************************
use "$data/Replication Data/tableA33_A34_data", replace

eststo rob_0: reghdfe emp_growth cen_pr_link_i  std_* [aw=wgt] if inrange(year,2004,2008),  a( i.year  i.industr) cl(fips_state  i.industr) 
	estadd local hasy "Yes"
	estadd local hasind "Yes"
	estadd local hass "Yes"		
	estadd local hasc "Yes"		
foreach var of varlist std_* { 
		eststo rob_`var': reghdfe emp_growth cen_pr_link_i  std_* c.cen_pr_link_i#c.`var' [aw=wgt] if inrange(year,2004,2008) ,  a( i.year i.industr) cl(fips_state  i.industr) 
	estadd local hasy "Yes"
	estadd local hasind "Yes"
	estadd local hass "Yes"		
	estadd local hasc "Yes"		
	} 
eststo rob_all: reghdfe emp_growth cen_pr_link_i std_*  c.cen_pr_link_i#(c.std_*) [aw=wgt] if inrange(year,2004,2008) ,  a( i.year  i.industr) cl(fips_state industr) 
	estadd local hasy "Yes"
	estadd local hasind "Yes"
	estadd local hass "Yes"		
	estadd local hasc "Yes"			

*** split tables
esttab rob_0 rob_std_total rob_std_job rob_std_train ///
rob_std_mw rob_std_rtw rob_std_rd rob_std_ic rob_std_corp ///
 using "$output/Tables/tableA33.tex", keep(cen_pr_link_i) stats() ///
cells(b(fmt(3)) se(par) p) mlab(none) label  ///
noobs nogap nomtitle tex replace coll(none) nocons ///
postfoot("\multicolumn{10}{l}{Interaction of Exposure with:} \\") coeflabel(cen_pr_link_i "Exposure to Section 936 ")

esttab rob_0 rob_std_total rob_std_job rob_std_train ///
rob_std_mw rob_std_rtw rob_std_rd rob_std_ic rob_std_corp ///
  using "$output/Tables/tableA33.tex", drop(std* cen_pr_link_i $pra_control_rob _cons)  ///
cells(b(fmt(3)) se(par) p) mlab(none) coll(none) nonum s() noobs scalars(  "hasy Year Fixed Effects" "hasind Industry Fixed Effects" ///
 "hasc Includes all Controls") /// 
coeflabel(cen_pr_link_i "Exposure to Section 936 "  /// 
c.cen_pr_link_i#c.std_total "Total Incentives (Bartik)"  /// 
c.cen_pr_link_i#c.std_job "Job Creation Incentive" /// 
c.cen_pr_link_i#c.std_train "Job Training Subsidy" /// 
c.cen_pr_link_i#c.std_mw "Real Minimum Wage" /// 
c.cen_pr_link_i#c.std_rtw "Right to Work" /// 
c.cen_pr_link_i#c.std_rd "RD Tax Credit" /// 
c.cen_pr_link_i#c.std_ic "Investment Tax Credit" /// 
c.cen_pr_link_i#c.std_corp "Corporate Income Tax" /// 
c.cen_pr_link_i#c.std_ptax "Personal Income Tax" /// 
c.cen_pr_link_i#c.std_prop "Property Tax"  /// 
c.cen_pr_link_i#c.std_sales "Sales Tax" /// 
c.cen_pr_link_i#c.std_capital "Capital Stock" /// 
c.cen_pr_link_i#c.std_trade "Trade Exposure (China)" /// 
c.cen_pr_link_i#c.std_rev "State Revenue Per Capita" /// 
c.cen_pr_link_i#c.std_routine "Share Routine Workers " ///
c.cen_pr_link_i#c.std_nafta "Trade Exposure (Nafta)" /// 
c.cen_pr_link_i#c.pr_link_ind "\hspace{1em}Exposure to Section 936 ") ///
label /// 
 prefoot("\hline") ///
postfoot( ///
"  \hline\hline" ///
"\end{tabular} }" ) preh(" ") append sty(tex)


//
esttab  rob_0 rob_std_ptax rob_std_prop rob_std_sales /// 
 rob_std_trade rob_std_rev rob_std_routine rob_std_nafta rob_all ///
 using "$output/Tables/tableA34.tex", coll(none) keep(cen_pr_link_i) stats() ///
cells(b(fmt(3)) se(par) p) label ///
noobs nogap nomtitle tex replace nocons ///
postfoot("\multicolumn{10}{l}{Interaction of Exposure with:} \\") coeflabel(cen_pr_link_i "Exposure to Section 936 ")

esttab  rob_0 rob_std_ptax rob_std_prop rob_std_sales /// 
 rob_std_trade rob_std_rev rob_std_routine rob_std_nafta rob_all ///
  using "$output/Tables/tableA34.tex", drop(std* cen_pr_link_i $pra_control_rob _cons)  ///
cells(b(fmt(3)) se(par) p) mlab(none) coll(none) s() nonum noobs nogap scalars(  "hasy Year Fixed Effects" "hasind Industry Fixed Effects" ///
"hasc Includes all Controls") /// 
coeflabel(cen_pr_link_i "Exposure to Section 936 "  /// 
c.cen_pr_link_i#c.std_total "Total Incentives (Bartik)"  /// 
c.cen_pr_link_i#c.std_job "Job Creation Incentive" /// 
c.cen_pr_link_i#c.std_train "Job Training Subsidy" /// 
c.cen_pr_link_i#c.std_mw "Real Minimum Wage" /// 
c.cen_pr_link_i#c.std_rtw "Right to Work" /// 
c.cen_pr_link_i#c.std_rd "RD Tax Credit" /// 
c.cen_pr_link_i#c.std_ic "Investment Tax Credit" /// 
c.cen_pr_link_i#c.std_corp "Corporate Income Tax" /// 
c.cen_pr_link_i#c.std_ptax "Personal Income Tax" /// 
c.cen_pr_link_i#c.std_prop "Property Tax"  /// 
c.cen_pr_link_i#c.std_sales "Sales Tax" /// 
c.cen_pr_link_i#c.std_capital "Capital Stock" /// 
c.cen_pr_link_i#c.std_trade "Trade Exposure (China)" /// 
c.cen_pr_link_i#c.std_rev "State Revenue Per Capita" /// 
c.cen_pr_link_i#c.std_routine "Share Routine Workers " ///
c.cen_pr_link_i#c.std_nafta "Trade Exposure (Nafta)" /// 
c.cen_pr_link_i#c.pr_link_ind "\hspace{1em}Exposure to Section 936 ") ///
label /// 
 prefoot("\hline") ///
postfoot( ///
"  \hline\hline" ///
"\end{tabular} }" ) preh(" ")  append sty(tex)
