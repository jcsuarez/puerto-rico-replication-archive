***************************************************************
//Filename: figureA11.do
//Author: eddie yu
//Date: 2023-09-20
//Task: This script creates Figure A11 Panel A, B, C
***************************************************************

/* Data sources:
various sources including NETS and compustat
*/

clear all
set more off
snapshot erase _all 

*******************************************************
* Panels A and B
*******************************************************
use "$data/Replication Data/figureA11_data", replace

*** labels for some graphs
local fs_capx_name "Foreign Share Investment"
local capex_base_name "Investment/Physical Capital"
local capex_base_p_name "% Change in Investment/Physical Capital"
local capex_base_1_name "Investment/Physical Capital"
local ftax_name "Federal Tax Rate"
 
gen capex_baseline = IK - capex_base
gen capex_baseline_1 = IK_1 - capex_base_1
gen capex_baseline_p = IKIK - capex_base_p

local capex_base_sum "capex_baseline"
local capex_base_p_sum "capex_baseline_p"
local capex_base_1_sum "capex_baseline_1" 

* combining years and N2
egen yearg = group(year n2)

*figures 5, 6, A5, and A6
xi i.year|PR , noomit
drop _IyeaXPR_1995 

forval i=1990(1)2014 { 
	capture: label var _IyeaXPR_`i' " "
} 
forval i=1990(2)2014 { 
	capture: label var _IyeaXPR_`i' "`i'"
} 

* Spec
global level "gvkey"
egen FE = group(year ppe95_bins10)
global spec1 "[aw=DFL_ppe], cl(gvkey) a($level ppe95_bins10##year n2##ppe95_bins10##year)"

global specname1 "Firm FE + Size-by-Year FE"
global c1 "navy"


est clear
local indexlist "A B C"
foreach index of local indexlist {	//
	preserve
	if "`index'"=="A" {
		local y "capex_base"
	}
	else if "`index'" == "B" {
		local y "capex_base_p"
	}
	else if "`index'" == "C" {
		local y "capex_base_1"
	}

	*running the regression
	reghdfe `y' _IyeaX* PR $spec1

	** figure years for the graph
	gen fig_year = 1989 in 1
	foreach i of numlist 1/23 {
		local j = `i' + 1989
		replace fig_year = `j' in `i'
	}
	
	** annual betas for the preperiod
	gen pr_beta = 0
	gen pr_beta_lb = 0
	gen pr_beta_ub = 0
	foreach i of numlist 1/5 {
		local j = `i' + 1989
		qui lincom _b[_IyeaXPR_`j'] , level(95)
		replace pr_beta = r(estimate) in `i'
		replace pr_beta_lb = r(lb) in `i'
		replace pr_beta_ub = r(ub) in `i'
	}
	* zero effect in 1995
	qui lincom  0
	replace pr_beta = r(estimate) in 5

	** annual betas for the postperiod
	foreach i of numlist 7/23 {
		local j = `i' + 1989
		qui lincom _b[_IyeaXPR_`j'] , level(95)
		replace pr_beta = r(estimate) in `i'
		replace pr_beta_lb = r(lb) in `i'
		replace pr_beta_ub = r(ub) in `i'
	}


* outputting the graph
 graph twoway (scatter pr_beta fig_year, lcolor(navy) mcolor(navy) yline(0 , lcolor(maroon)) xline(1995, lcolor(black) lpattern(solid)) msize(small)) ///
    (rcap pr_beta_lb pr_beta_ub fig_year, lcolor(navy) lpattern(solid)) ///
	, plotregion(fcolor(white) lcolor(white)) ///
	graphregion(fcolor(white) lcolor(white))  ///
	xtitle("Year") xlab(1990(5)2012) bgcolor(white) ///
	legend(size(small) pos(6) on order (1 2) label(1 "PR Presence")  ///
	label(2 "95% CI") r(1)) ///
	yti("Effect of S936 on ``y'_name'", margin(medium))
graph export "$output/Graphs/figureA11_`index'.pdf", replace

	restore
}
