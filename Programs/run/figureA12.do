***************************************************************
//Filename: figureA12.do
//Author: eddie yu
//Date: 2023-09-20
//Task: This script creates Figure A6 under different specifications
***************************************************************

/* Data sources:
various sources including NETS and compustat
*/

clear all
set more off
snapshot erase _all 

*******************************************************
* Panels A and B
*******************************************************
use "$data/Replication Data/figureA12_data", replace

*** labels for some graphs
local fs_capx_name "Foreign Share Investment"
local capex_base_name "Investment/Physical Capital"
local capex_base_p_name "% Change in Investment/Physical Capital"
local capex_base_1_name "Investment/Physical Capital"
local ftax_name "Federal Tax Rate"
 
gen capex_baseline = IK - capex_base
gen capex_baseline_1 = IK_1 - capex_base_1
gen capex_baseline_p = IKIK - capex_base_p

local capex_base_sum "capex_baseline"
local capex_base_p_sum "capex_baseline_p"
local capex_base_1_sum "capex_baseline_1" 

* combining years and N2
egen yearg = group(year n2)

*figures 5, 6, A5, and A6
xi i.year|PR , noomit
drop _IyeaXPR_1995 

forval i=1990(1)2014 { 
	capture: label var _IyeaXPR_`i' " "
} 
forval i=1990(2)2014 { 
	capture: label var _IyeaXPR_`i' "`i'"
} 

* Spec
global level "gvkey"
egen FE = group(year ppe95_bins10)
global spec1 ", cl(gvkey) a(A=$level B=ppe95_bins10##year)"

global specname1 "Firm FE + Size-by-Year FE"
global c1 "navy"


est clear
local indexlist "A B C"
foreach index of local indexlist {	//
	preserve
	if "`index'"=="A" {
		local y "capex_base"
	}
	else if "`index'" == "B" {
		local y "capex_base_p"
	}
	else if "`index'" == "C" {
		local y "capex_base_1"
	}

	*running the regression
	reghdfe `y' _IyeaX* PR $spec1
	estimates save "$output/ster/figureA12_`y'_base", replace

	*generating variables for the levels graph
	foreach i of numlist 1990/2012 {
		sum B if e(sample) & year == `i'
		scalar year_mean`i' = r(mean)

	}
	** year FE and such
	gen year_fe = 0
		foreach i of numlist 1/23 {
		local j = `i' + 1989
		replace year_fe = year_mean`j' in `i'
	}

	** figure years for the graph
	gen fig_year = 1989 in 1
		foreach i of numlist 1/23 {
		local j = `i' + 1989
		replace fig_year = `j' in `i'
	}

	** adding in 1995 baseline value:
	sum ``y'_sum' if e(sample) // ``y'_sum'
	scalar baseline = r(mean)

	** total effect for PR firms
	gen pr_beta_tot = 0
	gen beta_tot = 0

	lincom _b[_IyeaXPR_1990] + year_mean1990 +_b[_cons]  + baseline , level(90)
	replace pr_beta_tot = r(estimate) in 1
	foreach i of numlist 2/5 {
	local j = `i' + 1989
	lincom _b[_IyeaXPR_`j'] + year_mean`j' +_b[_cons]  + baseline  , level(90)
	replace pr_beta_tot = r(estimate) in `i'
	}
	lincom year_mean1995 +_b[_cons] + baseline  , level(90)
	replace pr_beta_tot = r(estimate) in 6
	foreach i of numlist 7/23 {
	local j = `i' + 1989
	lincom _b[_IyeaXPR_`j'] + year_mean`j' +_b[_cons]  + baseline , level(90)
	replace pr_beta_tot = r(estimate) in `i'
	}
	** version for non-PR firms that essentially differences out the beta and difference in FE
	lincom year_mean1990 +_b[_cons]  + baseline , level(90)
	replace beta_tot = r(estimate) in 1
	foreach i of numlist 2/5 {
	local j = `i' + 1989
	lincom year_mean`j' +_b[_cons]  + baseline , level(90)
	replace beta_tot = r(estimate) in `i'
	}
	lincom year_mean1995 +_b[_cons]  + baseline , level(90)
	replace beta_tot = r(estimate) in 6
	foreach i of numlist 6/23 {
	local j = `i' + 1989
	lincom year_mean`j' +_b[_cons] + baseline , level(90)
	replace beta_tot = r(estimate) in `i'
	}

	** outputting a type of total effect graph
	 graph twoway (scatter pr_beta_tot fig_year, lcolor(navy) mcolor(navy) c(line) xline(1995, lcolor(black) lpattern(solid))) ///
		(scatter beta_tot fig_year, lcolor(maroon) mcolor(maroon) c(line)) ///
		, plotregion(fcolor(white) lcolor(white)) ///
		graphregion(fcolor(white) lcolor(white))  ///
		xtitle("Year") xlab(1990(5)2012) bgcolor(white) ///
		legend(size(small) pos(6) on order (1 2) ///
		label(1 "Puerto Rico Presence") label(2 "Other Firms") r(1)) ///
		yti("Effect of S936 on ``y'_name'", size(small) margin(medium))
	graph export "$output/Graphs/figureA12_`index'.pdf", replace
	restore
}
