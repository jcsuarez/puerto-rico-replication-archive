***************************************************************
//Filename: figureA1.do
//Author: eddie yu
//Date: 2023-10-16
//Task: This script creates Figure A1, model graphs.
***************************************************************

clear

// Part 1 
// Graph of how profit shifting affects reported profitabilities 

local th  = .35 
local tl  = .09
local tl2 = .17 
local Kl  = 25
local a   = 12
local a2   = 3
local rho = 0.04

local r1 = -2
local r2 = 2           

twoway (function y =  `a'*x , ///
	range(`r1' `r2') lcolor(black) lpattern(dash) lwidth(thick)) ///
	(function y =  `a2'*x , ///
	range(`r1' `r2') lcolor(black) lpattern(solid) lwidth(thick)) ///
	(function y = 0 , ///
	range(`r1' `r2') lcolor(black) lpattern(dot) xline(0, lcolor(black) lpattern(dot) lwidth(thick)) ) , ///
        ytitle("Reported Profitability: f{sup:^}{sub:j}=f{sup:–}{sub:j}+a({&tau}{sup:–}-{&tau}{sub:j})", si(medlarge)) xtitle("Tax Differential: ({&tau}{sup:–}-{&tau}{sub:j})", si(medlarge)) ///
	legend(off) plotregion(fcolor(white) lcolor(white)) ///
	graphregion(fcolor(white) lcolor(white)) ///
	text(-22 -.9 "Under-report if {&tau}{sup:–}<{&tau}{sub:j}", si(medlarge)) ///
	text(-22 .9  "Over-report if {&tau}{sup:–}>{&tau}{sub:j}", si(medlarge)) ///
	text(23 1  "f{sup:^}{sub:j} for high value of a", si(medlarge))  ///
	text(21 1  "(low profit shifting cost)", si(medlarge)) ///
	text(9.25 1.45  "f{sup:^}{sub:j} for low value of a", si(medlarge))  ///
	text(7.25 1.45   "(high profit shifting cost)", si(medlarge))	///
	text(1.5 -1.47 "True profitability: f{sup:–}{sub:j} ", si(medlarge))	///
	xlab(none) ylab(none)
	

graph export "$output/Graphs/figureA1_A.pdf", replace 
	
//Part 2 
//Graph of how profit shifting affects capital investment 	

local a   = 8
local r1 = 40
local r2 = 80

local x1 = 50
local x2 = 58
local x3 = 68

local y1 = 1.2/(`x1'-30)
local y2 = 1.2/(`x2'-30)
local y3 = 1.2/(`x3'-30)

set obs 3
gen y = . 
replace y = `y1' in 1
replace y = `y2' in 2
replace y = `y3' in 3

gen x = . 
replace x = `x1' in 1
replace x = `x2' in 2
replace x = `x3' in 3

twoway (function y = `rho'/(1-`th'), ///
	range(`r1' `r2') lcolor(black) lpattern(shortdash) lwidth(thick)) ///
       (function y = (`rho'-(`Kl'/(`Kl'+x))^2*`a'*(`th'-`tl')^2/2)/(1-`th'), ///
        range(`r1' `r2') lcolor(black) lpattern(dash) lwidth(thick)) ///
       (function y = (`rho'-(`Kl'/(`Kl'+x))^2*`a'*(`th'-`tl2')^2/2)/(1-`th'), ///
        range(`r1' `r2') lcolor(black) lpattern(dash_dot) lwidth(thick)) ///
       (function y =  1.2/(x-30) , ///
        range(`r1' `r2') lcolor(black) lpattern(solid)  lwidth(thick)) ///
       (spike y x, lcolor(black) lpattern(dot) lwidth(thick)) , ///
       ytitle("Marginal Product of Capital", si(medlarge)) xtitle("Capital: K{sub:j}", si(medlarge)) ///
	legend(off) plotregion(fcolor(white) lcolor(white)) ///
	graphregion(fcolor(white) lcolor(white)) ///
	text(.11 43.75 "(1-{&tau}{sub:j})f'{sup:}(K{sub:j})", si(medlarge)) ///
	text(.0675 41 "{&rho}{sup:NPS}", si(medlarge)) ///
	text(.04 41.5 "{&rho}{sup:–}{sub:1}", si(medlarge)) ///
	text(.01125 43 "{&rho}{sup:–}{sub:0}", si(medlarge)) ///
	text(.0025 68.75 "K{sub:j}{sup:0}", si(medlarge)) ///
	text(.0025 58.75 "K{sub:j}{sup:1}", si(medlarge)) ///	
	text(.0025 51.25  "K{sub:j}{sup:NPS}", si(medlarge)) ///		
	xlab(none) ylab(none)
graph export "$output/Graphs/figureA1_B.pdf", replace 
