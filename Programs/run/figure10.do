// This file recreates Figure 10, heterogeneity, includes figure A22 and table A25 since they are all identical
// Author: eddie yu
// Date: 2023-10-16

/* Data sources:
NETS and QCEW
*/

clear all
set more off
snapshot erase _all 

// First Define Command for ES data 
capture program drop ES_graph_data
program define ES_graph_data
syntax,  level(real) yti(string) tshifter(real)


qui { 
mat b = e(b)
mat V = e(V) 
mat es_b = 0 
mat es_ci_l = 0 
mat es_ci_h = 0 
mat time = 0 

// for 95% CI 
if `level' == 99 { 
	local z= 2.6  
}
if `level' == 95 { 
	local z= 1.96  
}
// for 90% CI 
if `level' == 90 { 
	local z= 1.645 
} 


forval i = 1/23 { 
	mat time = (time,`i'-6+1995 + `tshifter')
	mat es_b = (es_b, b[1,`i'])
	mat es_ci_l = (es_ci_l, b[1,`i']-`z'*sqrt(V[`i',`i'])   )
	mat es_ci_h = (es_ci_h, b[1,`i']+`z'*sqrt(V[`i',`i'])   )		
	}
mat es_b =  (es_b[1,2..6],0,es_b[1,7...])'
mat es_ci_l = (es_ci_l[1,2..6],0,es_ci_l[1,7...])'  
mat es_ci_h = (es_ci_h[1,2..6],0,es_ci_h[1,7...])' 
mat time = time[1,2...]' 

capture: drop es_b es_ci_l es_ci_h time 
svmat es_b 
rename es_b `yti'_es_b
svmat time
rename time `yti'_time
svmat es_ci_l
rename es_ci_l `yti'_es_ci_l
svmat es_ci_h 
rename es_ci_h `yti'_es_ci_h
}

end  


*******************************************************
* Starting with the data
*******************************************************
use "$data/Replication Data/figure10_data", clear

xi i.year|pr_link_i , noomit
drop _IyeaXp*1995

* running the same regression 5 times. each time has a different variable as the notable interaction
* notable interactions will be put out in a table. All results in all 5 columns actually come from a single regression

/// Do first with all industries that had any importance to the sector 
eststo emp_1:  reghdfe emp_growth c._IyeaX*  [aw=wgt]  , a(i.pid i.year#i.industr ) cl(fips_state indust ) nocons
ES_graph_data , level(95) yti("spec1")	tshifter(-0.2)
	estadd local hasiy "Yes"
	estadd local hascty "Yes"

eststo emp_2:  reghdfe emp_growth c._IyeaX* if has_yes !=0 [aw=wgt]  , a(i.pid i.year#i.industr ) cl(fips_state indust ) nocons
ES_graph_data , level(95) yti("spec2")	tshifter(-0.1)
	estadd local hasiy "Yes"
	estadd local hascty "Yes"
	
eststo emp_3:  reghdfe emp_growth c._IyeaX* if has_no !=0 [aw=wgt]  , a(i.pid i.year#i.industr ) cl(fips_state indust ) nocons
ES_graph_data , level(95) yti("spec3")	tshifter(-0)
	estadd local hasiy "Yes"
	estadd local hascty "Yes"
	
eststo emp_4:  reghdfe emp_growth c._IyeaX* if has_oth !=0 [aw=wgt]  , a(i.pid i.year#i.industr ) cl(fips_state indust ) nocons
ES_graph_data , level(95) yti("spec4")	tshifter(0.1)
	estadd local hasiy "Yes"
	estadd local hascty "Yes"
	
eststo emp_5:  reghdfe emp_growth c._IyeaX* if has_const !=0 [aw=wgt]  , a(i.pid i.year#i.industr ) cl(fips_state indust ) nocons
ES_graph_data , level(95) yti("spec5")	tshifter(0.2)
	estadd local hasiy "Yes"
	estadd local hascty "Yes"
	
esttab emp* using "$output/Tables/tableA25.tex", drop(*) stats() ///
b(3) se par label   ///
noobs nogap nomtitle tex replace nocons nonum ///
postfoot("\multicolumn{1}{l}{\textbf{Exposure to Section 936}}") nonum posthead("")


esttab emp* using "$output/Tables/tableA25.tex",  preh("") keep(_IyeaXpr__*) ///
 b(3) se par nogap mlab("Average Effect" "Tradable" "Non-Tradable" "Other" "Construction" ) coll(none) s() noobs  ///
coeflabel(_IyeaXpr__1990 "\hspace{1em}X 1990 " /// 
_IyeaXpr__1991 "\hspace{1em}X 1991 " /// 
_IyeaXpr__1992 "\hspace{1em}X 1992 " /// 
_IyeaXpr__1993 "\hspace{1em}X 1993 " /// 
_IyeaXpr__1994 "\hspace{1em}X 1994 " /// 
_IyeaXpr__1996 "\hline \hspace{1em}X 1996 " /// 
_IyeaXpr__1997 "\hspace{1em}X 1997 " /// 
_IyeaXpr__1998 "\hspace{1em}X 1998 " /// 
_IyeaXpr__1999 "\hspace{1em}X 1999 " /// 
_IyeaXpr__2000 "\hspace{1em}X 2000 " /// 
_IyeaXpr__2001 "\hspace{1em}X 2001 " /// 
_IyeaXpr__2002 "\hspace{1em}X 2002 " /// 
_IyeaXpr__2003 "\hspace{1em}X 2003 " /// 
_IyeaXpr__2004 "\hspace{1em}X 2004 " /// 
_IyeaXpr__2005 "\hspace{1em}X 2005 " /// 
_IyeaXpr__2006 "\hspace{1em}X 2006 " /// 
_IyeaXpr__2007 "\hspace{1em}X 2007 " /// 
_IyeaXpr__2008 "\hspace{1em}X 2008 " /// 
_IyeaXpr__2009 "\hspace{1em}X 2009 " /// 
_IyeaXpr__2010 "\hspace{1em}X 2010 " /// 
_IyeaXpr__2011 "\hspace{1em}X 2011 " /// 
_IyeaXpr__2012 "\hspace{1em}X 2012 " )   scalars(  "hasiy Industry-by-Year Fixed Effects" "hascty County Fixed Effects" ) label /// 
 prefoot("\hline") ///
postfoot( ///
"  \hline\hline" ///
"\end{tabular} }" ) append sty(tex)

	
twoway 		(scatter spec1_es_b spec1_time, msymbol(0) connect(line) lcolor(navy) yline(0 , lcolor(black)) xline(1995, lcolor(black) lpattern(solid)  ))  ///
		(scatter spec2_es_b spec2_time, msymbol(D) connect(line) lcolor(orange) mcolor(orange) lpattern(dash))      ///
		(scatter spec3_es_b spec3_time, msymbol(T) connect(line) lcolor(red) mcolor(red) lpattern(longdash))     ///
		(scatter spec4_es_b spec4_time, msymbol(S) connect(line) lcolor(green) mcolor(green) lpattern(shortdash))    ///
		(scatter spec5_es_b spec5_time, msymbol(+) connect(line) lcolor(black) mcolor(black) lpattern(dash_dot))   ///
	, plotregion(fcolor(white) lcolor(white)) ///
	graphregion(fcolor(white) lcolor(white)) ///
	xtitle("Year") xlab(1990(5)2012) ///
	legend(size(small) pos(6) label(1 "All") ///
	label(2 "Tradable")   ///
	label(3 "Non-Tradable")  ///
	label(4 "Other")  ///
	label(5 "Construction")  ///
	r(2) order( - "Sector:" 1 2 3 5 4)) ///
	ytitle("Percent Employment Growth") ylab(-.15(.05).05)		
graph export "$output/Graphs/figure10.pdf", replace	


twoway 		(scatter spec1_es_b spec1_time, msymbol(0) connect(line) lcolor(navy) yline(0 , lcolor(black)) xline(1995, lcolor(black) lpattern(solid)  ))  ///
		(rcap spec1_es_ci_l spec1_es_ci_h spec1_time,  lcolor(navy) lpattern(solid))  ///
		(scatter spec2_es_b spec2_time, msymbol(D) connect(line) lcolor(orange) mcolor(orange) lpattern(dash))      ///
		(rcap spec2_es_ci_l spec2_es_ci_h spec2_time,  lcolor(orange) lpattern(dash))   /// 
		(scatter spec3_es_b spec3_time, msymbol(T) connect(line) lcolor(red) mcolor(red) lpattern(longdash))     ///
		(rcap spec3_es_ci_l spec3_es_ci_h spec3_time,  lcolor(red) lpattern(longdash))    ///
		(scatter spec4_es_b spec4_time, msymbol(S) connect(line) lcolor(green) mcolor(green) lpattern(shortdash))    ///
		(rcap spec4_es_ci_l spec4_es_ci_h spec4_time,  lcolor(green) lpattern(shortdash))    ///
		(scatter spec5_es_b spec5_time, msymbol(+) connect(line) lcolor(black) mcolor(black) lpattern(dash_dot))   ///
		(rcap spec5_es_ci_l spec5_es_ci_h spec5_time,  lcolor(black) lpattern(dash_dot))   ///
	, plotregion(fcolor(white) lcolor(white)) ///
	graphregion(fcolor(white) lcolor(white)) ///
	xtitle("Year") xlab(1990(5)2012) ///
	legend(size(small) pos(6) label(1 "All") ///
	label(3 "Tradable")   ///
	label(5 "Non-Tradable")  ///
	label(7 "Other")  ///
	label(9 "Construction")  ///
	r(2) order( - "Sector:" 1 3 5 9 7)) ///
	ytitle("Percent Employment Growth") ylab(-.2(.05).05)	
graph export "$output/Graphs/figureA22.pdf", replace		


