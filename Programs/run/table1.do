// This file recreates Table 1
// Author: Dan Garrett
// Date: 4-20-2020

/* Data sources:
NETS
*/

clear all
set more off
snapshot erase _all 

*******************************************************
* starting with the data
*******************************************************
use "$data/Replication Data/table1_data", clear

label var company "Company"
label var emp_US "US Employment"
label var emp_PR "Puerto Rico Employment"
rename company Company
rename emp_US US
rename emp_PR PR
rename emp_change Change
rename emp_pchange Percent

expand 2, gen(new)
bysort new: egen totUS = sum(US)
bysort new: egen totPR = sum(PR)
bysort new: egen totchange = sum(Change)
gen totpchange = totchange / (totUS + totPR)
bysort new: gen id = _n
replace ind = . if new == 1
replace Company = "\hline \textbf{Sub Total}" if new == 1 & id == 1
replace Company = "\hline \textbf{Total}" if new == 1 & id == 2
drop if new == 1 & id > 2
replace US = totUS if Company == "\hline \textbf{Sub Total}"
replace PR = totPR if Company == "\hline \textbf{Sub Total}"
replace US = tot_US if Company == "\hline \textbf{Total}"
replace PR = tot_PR if Company == "\hline \textbf{Total}"
replace Change = totchange if Company == "\hline \textbf{Sub Total}"
replace Percent = totpchange if Company == "\hline \textbf{Sub Total}"
replace Change = tot_change if Company == "\hline \textbf{Total}"
replace Percent = tot_pchange if Company == "\hline \textbf{Total}"
keep Company US PR ind Change Percent
form US PR %12.0gc
rename US US2
rename PR PR2
rename Change Change2

replace Percent = Percent * 100
rename Percent Percent2
gen US = string(US2, "%12.0gc")
gen PR = string(PR2, "%12.0gc")
gen Change = string(Change2, "%12.0gc")
gen Percent = string(Percent2, "%6.1fc")
replace Percent = Percent + "\%"
keep Company US PR ind Change Percent

replace Company = ltrim(Company) 
replace Company = rtrim(Company) 

* industries manually from googling (done for top 1990 firms)
gen Industry = "Chemicals"
replace Industry = "Wholesale and Retail" if Company == "BAXTER INTERNATIONAL INC"  ///
| Company == "PXC \& M HOLDINGS INC" | Company == "FLUOR CORPORATION" | Company == "KMART CORPORATION" ///
| Company == "SEARS ROEBUCK AND CO" | Company == "AVON PRODUCTS INC"
replace Industry = "Food Mfg" if Company == "UNI GROUP INC" | Company == "SARA LEE CORPORATION"  ///
| Company == "H J HEINZ COMPANY" | Company  == "RJR ACQUISITION CORP"
replace Industry = "Other Mfg" if Company == "DIGITAL EQUIPMENT CORPORATION" | Company == "JOHNSON \& JOHNSON" | Company == "AMR CORPORATION"
replace Industry = " " if ind == .
replace Industry = "Other Non-Mfg" if Company == "CBS CORPORATION"  | Company == "BERKSHIRE HATHAWAY INC"
replace Industry = "Electrical Equipment" if Company == "GENERAL ELECTRIC COMPANY"
replace Industry = "Services" if Company == "FLUOR CORPORATION"
replace Industry = "Beverages" if Company == "PEPSICO INC"

keep Company US PR Industry Change Percent

replace Company = "WESTINGHOUSE (LATER CBS)" if Company == "CBS CORPORATION" 

*tostring US PR, replace format(%12.0gc)

cd "$output/Tables"
drop Change Percent
dataout, tex replace save(table1) noauto

// NOTE: This output might need to be slightly adjusted to be in the same format as in the paper
