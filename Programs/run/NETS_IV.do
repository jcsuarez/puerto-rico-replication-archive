***************************************************************
//Filename: NETS_IV.do
//Author: eddie yu
//Date: 2023-09-20
//Task: This script conducts IV regression of Exposure to S936 on Employment,
//		using NETS data (tax_impute1, tax_impute2)
***************************************************************

clear all
set more off
snapshot erase _all 

*******************************************************
* Starting with the data
*******************************************************
use "$data/Replication Data/table3_data_C", clear



* Regression Specifications
global level "PR"
global spec1 " [aw=wgt], cluster(hqduns) nocon a($level year)"
global spec2 " [aw=wgt], cluster(hqduns) nocon a($level naic2##year)"
global spec3 "if major_sec [aw=wgt], cluster(hqduns) nocon a($level naic2##year)"
global spec4 "if major_ind [aw=wgt], cluster(hqduns) nocon a($level naic2##year)"
global spec5 "if major_ind [aw=DFL2], cluster(hqduns) nocon a($level naic2##year)"


global specname1 "Baseline"
global specname2 "NAIC2"
global specname3 "Exp. Sector"
global specname4 "Exp. Industry"
global specname5 "Exp. Industry + DFL"

global c1 "navy"
global c2 "dkorange"
global c3 "midgreen"
global c4 "maroon"
global c5 "midblue"
global c6 "red"

* winsorize
capture: drop emp_growth_w
winsor2 emp_growth, by(PR year) cuts(10 100)

* percentage
capture: drop emp_growth100
gen emp_growth100 = 100*emp_growth

* ETR tax_impute

** PR-by-year instruments
local dep1 = "(tax_impute = PR##i.year)"
local dep2 = "(tax_impute2 = PR##i.year)"



* PR-by-year instruments
local dep1 = "(tax_impute = PR##i.year)"
local dep2 = "(tax_impute2 = PR##i.year)"
local i = 0
foreach spec in spec1 spec2 spec3 spec4 spec5 {
	local i = `i' + 1
	eststo `spec'_sto1: qui ivreghdfe emp_growth100 `dep1' $`spec' 
	est save "$output/ster/NETS_IV_PRyear_taximpute1_`i'", replace
	eststo `spec'_sto2: qui ivreghdfe emp_growth100 `dep2' $`spec' 
	est save "$output/ster/NETS_IV_PRyear_taximpute2_`i'", replace
}

*** "Industry-specific rho weight" ***
esttab spec?_sto1, keep(tax_impute) cells(b p)
*** "Constant rho weight" ***
esttab spec?_sto2, keep(tax_impute2) cells(b p)


* PR-by-post instruments ( PR##i.post3 )
preserve
	*drop if year>2006
	local dep1 = "(tax_impute = PR##i.post2) PR##i.post1 PR##i.pre"
	local dep2 = "(tax_impute2 = PR##i.post2) PR##i.post1 PR##i.pre"
	local i = 0
	foreach spec in spec1 spec2 spec3 spec4 spec5 {
		local i = `i' + 1
		eststo `spec'_sto3: qui ivreghdfe emp_growth100 `dep1' $`spec' 
		est save "$output/ster/NETS_IV_PRpost_taximpute1_`i'", replace
		eststo `spec'_sto4: qui ivreghdfe emp_growth100 `dep2' $`spec' 
		est save "$output/ster/NETS_IV_PRpost_taximpute2_`i'", replace
	}

	*** "Industry-specific rho weight" ***
	esttab spec?_sto3, keep(tax_impute) cells(b p)
	*** "Constant rho weight" ***
	esttab spec?_sto4, keep(tax_impute2) cells(b p)
restore
