***************************************************************
//Filename: tableA18.do
//Author: eddie yu
//Date: 2023-09-20
//Task: Given tableA18_data.dta, creates Table A18
***************************************************************

/* Data sources:
QCEW, NETS
*/

clear all
set more off
snapshot erase _all 
estimates clear

*******************************************************
* starting with data from tableA18_data.dta
*******************************************************
use "$data/Replication Data/tableA18_data", replace

*making interaction terms
xi i.year|pr_link_i, noomit
drop _IyeaXp*1995

eststo emp_1:  reghdfe ADH_growth _IyeaX* [aw=wgt]  , a( i.year ) cl(statefips )
	estadd local hasy "Yes"
    estadd local hascty ""
	
eststo emp_2:  reghdfe ADH_growth _IyeaX* [aw=wgt]  , a(i.pid i.year ) cl(statefips )
	estadd local hasy "Yes"
	estadd local hascty "Yes"

eststo emp_4:  reghdfe ADH_growth _IyeaX* [aw=wgt]  , a(i.pid i.year#i.statefips) cl(statefips )
	estadd local hasy "Yes"
	estadd local hasicty "Yes"

winsor wgt, p(.01) gen(wgt1)

eststo emp_5:  reghdfe ADH_growth _IyeaX* [aw=wgt1] , a( i.pid i.year ) cl(statefips )
	estadd local hasy "Yes"
	estadd local hascty "Yes"
	estadd local hasww "Yes"	

eststo emp_6:  reghdfe ADH_growth _IyeaX* if base_pop > 1000 [aw=wgt] , a( i.pid i.year) cl(statefips)
	estadd local hasy "Yes"
	estadd local hascty "Yes"
	estadd local hasds "Yes"
	
esttab emp* using "$output/Tables/tableA18.tex", drop(*) stats() ///
b(3) se par label  ///
noobs nogap nomtitle tex replace nocons ///
postfoot("\multicolumn{1}{l}{\textbf{Exposure to Section 936}}") nonum posthead("")


esttab emp*   using "$output/Tables/tableA18.tex",  preh("") ///
 b(3) se par mlab(none) coll(none) s() noobs nogap  keep(_IyeaXpr__*) /// 
coeflabel(_IyeaXpr__1990 "\hspace{1em}X 1990 " /// 
_IyeaXpr__1991 "\hspace{1em}X 1991 " /// 
_IyeaXpr__1992 "\hspace{1em}X 1992 " /// 
_IyeaXpr__1993 "\hspace{1em}X 1993 " /// 
_IyeaXpr__1994 "\hspace{1em}X 1994 " /// 
_IyeaXpr__1996 "\hline \hspace{1em}X 1996 " /// 
_IyeaXpr__1997 "\hspace{1em}X 1997 " /// 
_IyeaXpr__1998 "\hspace{1em}X 1998 " /// 
_IyeaXpr__1999 "\hspace{1em}X 1999 " /// 
_IyeaXpr__2000 "\hspace{1em}X 2000 " /// 
_IyeaXpr__2001 "\hspace{1em}X 2001 " /// 
_IyeaXpr__2002 "\hspace{1em}X 2002 " /// 
_IyeaXpr__2003 "\hspace{1em}X 2003 " /// 
_IyeaXpr__2004 "\hspace{1em}X 2004 " /// 
_IyeaXpr__2005 "\hspace{1em}X 2005 " /// 
_IyeaXpr__2006 "\hspace{1em}X 2006 " /// 
_IyeaXpr__2007 "\hspace{1em}X 2007 " /// 
_IyeaXpr__2008 "\hspace{1em}X 2008 " /// 
_IyeaXpr__2009 "\hspace{1em}X 2009 " /// 
_IyeaXpr__2010 "\hspace{1em}X 2010 " /// 
_IyeaXpr__2011 "\hspace{1em}X 2011 " /// 
_IyeaXpr__2012 "\hspace{1em}X 2012 " )   scalars(  "hasy Year Fixed Effects" "hascty County Fixed Effects" ///
"hasicty Year-by-State Fixed Effects" ///
"hasww Winsorized Weights" "hasds Drops Small County-Industries ($<$1000)" ) label /// 
 prefoot("\hline") ///
postfoot( ///
"  \hline\hline" ///
"\end{tabular} }" ) append sty(tex)
