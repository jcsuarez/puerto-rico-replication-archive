***************************************************************
//Filename: tableA29.do
//Author: eddie yu
//Date: 2023-09-20
//Task: Given tableA29_data.dta, creates Table A29
***************************************************************


/* Data sources:
NETS, compustat
*/

clear all
set more off
snapshot erase _all 

*******************************************************
* Starting with the data
*******************************************************
use "$data/Replication Data/tableA29_data", clear

* defining specifications
local spec1 "i.n3 lnat pharma "
local spec2 "i.n3 lnat pharma etr "
local spec3 "i.n3 lnat pharma etr rd any_rd "
local spec4 "i.n3 lnat pharma etr rd any_rd gpop dltt"
local spec5 "i.n3 lnat pharma etr rd any_rd gpop dltt any_ads ads"

* running regressions with increasing controls
est clear
forval i = 1/5 {
eststo reg_`i':  probit PR `spec`i'',  vce(cl n2)
	estadd local ind1 ""
	estadd local ind13 "Yes"
}

*outputting results	
esttab reg_* using "$output/Tables/tableA29.tex", keep(_cons) stats() ///
b(3) se par label coeflabel(event_dummy "\hspace{1em}Event") ///
noobs nogap tex replace nocons postfoot("") nomti nodep

esttab reg_* using "$output/Tables/tableA29.tex", keep(any_rd gpop dltt etr lnat pharma any_ads) ///
cells(b(fmt(3)) se(par) p) mlab(none) coll(none) order(_cons day*)  ///
varlabel(any_ads "\hspace{1em}Any Advertising" any_rd "\hspace{1em}Any Research \& Development" gpop "\hspace{1em}Gross Profit / Operating Assets"  ///
etr "\hspace{1em}ETR" pharma "\hspace{1em}NAICS 3254 (Pharma)" lnat "\hspace{1em}ln(Assets)" dltt "\hspace{1em}Long-term Debt / Assets" ///
) ///
scalars( N "ind13 3 Digit NAICS Industry Fixed Effects" ) label /// 
preh("") prefoot("\hline")  postfoot( ///
"\hline " ///
"\end{tabular} }" ) append sty(tex) nonum nomti  nodep
