*******************************************************************************
//Filename: figureA7.do
//Author: eddie yu
//Date: 2023-10-16
//Task: Given figureA7_data, we show the covariate balance
//		of two panels
*******************************************************************************

**** Balance table and graphs 
global specname1 "Exposure to All Industries"
global specname2 "Exposure to S936 Industries"

label var realmw "Minimum Wage"
label var rgtowork "Right to Work"
label var income_rate_avg "Personal Income Tax"
label var rec_val "R&D Credit" 
label var rev_gdp "State Revenue/GDP"
label var d_tradeusch_pw "Trade Exposure (China)"
label var locdt_noag "Trade Exposure (Nafta)"
label var l_sh_routine33a "Share of Routine Labor"

label var labor_participation_rate "Labor Force Participation"
label var pct_retail "Percentage Retail"
label var pct_agriculture "Percentage Agriculture"
label var pct_manufacturing_durable "Percentage Manufacturing Durable" 
label var pct_manufacturing_nondurable "Percentage Manufacturing Non-Durable"
label var capital_stock "Capital Stock"
label var pct_college "Percentage College"
label var pct_less_HS "Percentage Less than HS"
label var pct_white "Percentage White"
label var pct_black "Percentage Black"

* state specific variables
local spec1 " realmw rgtowork income_rate_avg salestax propertytax corpinctax rec_val rev_gdp d_tradeusch_pw l_sh_routine33a locdt_noag " 
* variables with within-state variation
local spec2 "ln_pop labor_participation_rate pct_retail pct_agriculture pct_manufacturing_durable pct_manufacturing_nondurable capital_stock  pct_college  pct_less_HS pct_white  pct_black "

set more off 
* normalizing variables  to z-scores
foreach Q in pr_link `spec2' `spec1' { 
	sum `Q' [aw=pop]
	replace `Q' = (`Q'-r(mean))/r(sd)
}
foreach Q in pr_link_ `spec2' `spec1' { 
	sum `Q' [aw=pop]
	replace `Q' = (`Q'-r(mean))/r(sd)
} 

capture: gen cons = 1
local FE1 "cons"
local FE2 "statefips"

*** Make Graphs 
est clear 
forval i = 1/2 {   
		qui: eststo reg_`i': areg pr_link_   `spec`i'' [fw=pop] , a(`FE`i'') cl(statefips ) 
		qui: eststo regi_`i': areg pr_link   `spec`i'' [fw=pop] , a(`FE`i'') cl(statefips ) 
	} 	

coefplot regi_1 reg_1  , drop(_cons ln_pop) ///
xline(0) level(95) xtitle("Standardized Effect", size(medsmall)) ///
graphregion(color(white))  xlab(-1(.25)1) ciopts(recast(rcap)) ///
 legend(size(small) pos(6) r(1) order(1 "$specname1" 3 "$specname2") cols(1))
graph export "$output/Graphs/figureA7_A.pdf", replace 

coefplot regi_2 reg_2  , drop(_cons ln_pop *statefips*) ///
 xline(0) level(95) xtitle("Standardized Effect", size(medsmall)) ///
 graphregion(color(white)) xlab(-1(.25)1) ciopts(recast(rcap)) ///
 legend(size(small) pos(6) r(1) order(1 "$specname1" 3 "$specname2") cols(1))
graph export "$output/Graphs/figureA7_B.pdf", replace 

