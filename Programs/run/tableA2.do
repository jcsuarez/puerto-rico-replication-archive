***************************************************************
//Filename: tableA2.do
//Author: eddie yu
//Date: 2023-09-20
//Task: Given tableA2_data.dta, creates Table A2
***************************************************************

/* Data sources:
QCEW and NETS
*/

clear all
set more off
snapshot erase _all 

*******************************************************
* Starting with the data
*******************************************************
use "$data/Replication Data/tableA2_data", clear


label var QName "County Name" 
label var id "Place"
label var QName2 "County Name" 
label var pr_link "Section 936 Establishment Exposure"
label var pr_link_emp "Section 936 Employment Exposure"  

listtex id QName pr_link QName2 pr_link_emp using "$output/Tables/tableA2.tex" ///
, replace rstyle(tabular) headlines("\def\sym#1{\ifmmode^{#1}\else\(^{#1}\)\fi}\begin{tabular}{l*{4}{c}} \hline\hline Rank & County & S936 Est Exposure & County & S936 Emp Exposure \\ \hline ") ///
footlines(" \hline\hline\end{tabular} ")
