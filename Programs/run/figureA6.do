// This file recreates Figure A6
// Author: eddie yu
// Date: 2023-10-16

clear all
set more off

/* Data sources:
All Panels: IRS SOI public data
*/

// importing data
use "$data/Replication Data/figureA6_data", clear

** making maps
maptile pr_link , geo(county1990) stateoutline(linewidthstyle)  nquantiles(5) res(.5)
graph export "$output/Graphs/figureA6_A.png", replace 

maptile pr_link_state , geo(county1990) stateoutline(linewidthstyle)  nquantiles(5)   res(.5)
graph export "$output/Graphs/figureA6_B.png", replace 

maptile pr_link_conspuma , geo(county1990) stateoutline(linewidthstyle)  nquantiles(5)   res(.5)
graph export "$output/Graphs/figureA6_C.png", replace 

maptile pr_link_czone , geo(county1990) stateoutline(linewidthstyle)  nquantiles(5)   res(.5)
graph export "$output/Graphs/figureA6_D.png", replace 
