***************************************************************
//Filename: tableA27.do
//Author: eddie yu
//Date: 2023-09-20
//Task: Given figure11_data.dta, creates Table A27
***************************************************************

/* Data sources:
QCEW, NETS, IRS, BEA
*/

clear all
set more off
snapshot erase _all 

*******************************************************
* starting with data from figure 11
*******************************************************
use "$data/Replication Data/figure11_data", replace

* Regressions on Wage 
eststo mw_d :areg dmlwage   c.pr_link* i.year  [aw=epop] if year > 1990    , a(state_fips) cl(state_fips )
count if e(smaple) & year > 1990
estadd scalar N_uw = r(N)
estadd local hasy "Yes"
estadd local hass "Yes"

eststo mw_a :areg dmlwage   c.pr_link* i.year  [aw=epop] if year ==1990    , a(state_fips) cl(state_fips )
count if e(smaple) & year == 1990 
estadd scalar N_uw = r(N)
estadd local hasy "Yes"
estadd local hass "Yes"

* Regressions on Wage 
eststo mwS_d : areg dSmlwage   c.pr_link* i.year  [aw=epop] if year > 1990    , a(state_fips) cl(state_fips )
count if e(smaple) & year > 1990
estadd scalar N_uw = r(N)
estadd local hasy "Yes"
estadd local hass "Yes"

eststo mwS_a : areg dSmlwage   c.pr_link* i.year  [aw=epop] if year ==1990    , a(state_fips) cl(state_fips )
count if e(smaple) & year == 1990 
estadd scalar N_uw = r(N)
estadd local hasy "Yes"
estadd local hass "Yes"

* Regressions on Wage 
eststo mwU_d : areg dUmlwage   c.pr_link* i.year  [aw=epop] if year > 1990    , a(state_fips) cl(state_fips )
count if e(smaple) & year > 1990
estadd scalar N_uw = r(N)
estadd local hasy "Yes"
estadd local hass "Yes"

eststo mwU_a : areg dUmlwage   c.pr_link* i.year  [aw=epop] if year ==1990    , a(state_fips) cl(state_fips )
count if e(smaple) & year == 1990 
estadd scalar N_uw = r(N)
estadd local hasy "Yes"
estadd local hass "Yes"


* Regressions on Rent 
eststo mr_d :areg dmlrent   c.pr_link* i.year  [aw=epop] if year > 1990    , a(state_fips) cl(state_fips )
count if e(smaple) & year > 1990
estadd scalar N_uw = r(N)
estadd local hasy "Yes"
estadd local hass "Yes"

eststo mr_a :areg dmlrent   c.pr_link* i.year  [aw=epop] if year ==1990    , a(state_fips) cl(state_fips )
count if e(smaple) & year == 1990 
estadd scalar N_uw = r(N)
estadd local hasy "Yes"
estadd local hass "Yes"

* Regressions on Rent 
eststo mv_d :areg dmlvalue   c.pr_link* i.year  [aw=epop] if year > 1990   , a(state_fips) cl(state_fips )
count if e(smaple) & year > 1990
estadd scalar N_uw = r(N)
estadd local hasy "Yes"
estadd local hass "Yes"

eststo mv_a :areg dmlvalue   c.pr_link* i.year  [aw=epop] if year ==1990   , a(state_fips) cl(state_fips )
count if e(smaple) & year == 1990 
estadd scalar N_uw = r(N)
estadd local hasy "Yes"
estadd local hass "Yes"

esttab m*d using "$output/Tables/tableA27.tex", drop(*) stats() ///
b(3) se par label coeflabel(pr_link "Exposure to Section 936 ") ///
noobs nogap nomtitle tex replace nocons postfoot("")

esttab m*d using "$output/Tables/tableA27.tex", keep(pr_link)  ///
cells(b(fmt(3)) se(par) p)  coll(none) noobs scalars( "N_uw Observations" "hasy Year Fixed Effects" "hass State Fixed Effects") label /// 
coeflabel(pr_link "Exposure to Section 936 ") mlab("All"  "High Skill" "Low Skill" "Rent" "Home Value" ) ///
preh("\multicolumn{6}{l}{\textbf{Stacked Differences After 1990}} \\ & \multicolumn{3}{c}{Wages} & & \\ \cmidrule(lr){2-4} ") postfoot("\hline") append sty(tex) nonum

esttab m*a using "$output/Tables/tableA27.tex", keep(pr_link)  ///
cells(b(fmt(3)) se(par) p)  coll(none) noobs scalars( "N_uw Observations" "hasy Year Fixed Effects" "hass State Fixed Effects") label /// 
coeflabel(pr_link "Exposure to Section 936 ") mlab("All"  "High Skill" "Low Skill" "Rent" "Home Value" ) ///
preh("\multicolumn{6}{l}{\textbf{Differences Before 1990}} \\ & \multicolumn{3}{c}{Wages} & & \\ \cmidrule(lr){2-4} ") prefoot("\hline") ///
postfoot( ///
"  \\ \hline\hline" ///
"\end{tabular} }" ) append sty(tex) nonum
