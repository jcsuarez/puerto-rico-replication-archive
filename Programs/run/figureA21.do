// This file recreates Figure A21: Robustness to Focusing Expsoure on large firms
// Author: eddie yu
// Date: 2023-10-16

/* Data sources:
QCEW, NETS
*/

clear all
set more off
snapshot erase _all 

// First Define Command for ES data 
capture program drop ES_graph_data
program define ES_graph_data
syntax,  level(real) yti(string) tshifter(real)


qui { 
mat b = e(b)
mat V = e(V) 
mat es_b = 0 
mat es_ci_l = 0 
mat es_ci_h = 0 
mat time = 0 

// for 95% CI 
if `level' == 99 { 
	local z= 2.6  
}
if `level' == 95 { 
	local z= 1.96  
}
// for 90% CI 
if `level' == 90 { 
	local z= 1.645 
} 


forval i = 1/23 { 
	mat time = (time,`i'-6+1995 + `tshifter')
	mat es_b = (es_b, b[1,`i'])
	mat es_ci_l = (es_ci_l, b[1,`i']-`z'*sqrt(V[`i',`i'])   )
	mat es_ci_h = (es_ci_h, b[1,`i']+`z'*sqrt(V[`i',`i'])   )		
	}
mat es_b =  (es_b[1,2..6],0,es_b[1,7...])'
mat es_ci_l = (es_ci_l[1,2..6],0,es_ci_l[1,7...])'  
mat es_ci_h = (es_ci_h[1,2..6],0,es_ci_h[1,7...])' 
mat time = time[1,2...]' 

capture: drop es_b es_ci_l es_ci_h time 
svmat es_b 
rename es_b `yti'_es_b
svmat time
rename time `yti'_time
svmat es_ci_l
rename es_ci_l `yti'_es_ci_l
svmat es_ci_h 
rename es_ci_h `yti'_es_ci_h
}

end  
 
*******************************************************
* starting with data from figure 7
*******************************************************
use "$data/Replication Data/figure7_data_size", replace

*making interaction terms
xi i.year|pr_link_ind , noomit
drop _IyeaXp*1995
eststo emp_1:  reghdfe emp_growth _IyeaXpr__* [aw=wgt]  , a(i.pid  i.year#i.industr ) cl(fips_state indust ) nocons
	estadd local hasiy "Yes"
    estadd local hascty "Yes"
	
ES_graph_data , level(95) yti("spec1") tshifter(-.2)

xi i.year|pr_link_s1_ind , noomit
drop _IyeaXp*1995
eststo emp_2:  reghdfe emp_growth _IyeaX* [aw=wgt]  , a(i.pid i.year#i.industr ) cl(fips_state indust ) nocons
	estadd local hasiy "Yes"
	estadd local hascty "Yes"

ES_graph_data , level(95) yti("spec2")	tshifter(-0.1)

xi i.year|pr_link_s2_ind , noomit
drop _IyeaXp*1995
eststo emp_3:  reghdfe emp_growth _IyeaX* [aw=wgt]  , a( i.pid i.year#i.indust ) cl(fips_state indust ) nocons
	estadd local hasiy "Yes"
	estadd local hascty "Yes"

ES_graph_data , level(95) yti("spec3")	tshifter(0)

xi i.year|pr_link_s3_ind , noomit
drop _IyeaXp*1995
eststo emp_4:  reghdfe emp_growth _IyeaX* [aw=wgt] , a( i.pid i.year#i.indust ) cl(fips_state indust ) nocons
	estadd local hasiy "Yes"
	estadd local hascty "Yes"

ES_graph_data , level(95) yti("spec4")	tshifter(0.1)

xi i.year|pr_link_s4_ind , noomit
drop _IyeaXp*1995
eststo emp_5:  reghdfe emp_growth _IyeaX* [aw=wgt] , a( i.pid i.year#i.indust ) cl(fips_state indust ) nocons
	estadd local hasiy "Yes"
	estadd local hascty "Yes"

ES_graph_data , level(95) yti("spec5")	tshifter(0.2)

* Graph output mirroring figure 3 in http://gabriel-zucman.eu/files/teaching/FuestEtal16.pdf
 twoway (line spec1_es_b spec1_time, lcolor(navy) yline(0 , lcolor(black)) xline(1995, lcolor(black) lpattern(solid)))  ///
		(rcap spec1_es_ci_l spec1_es_ci_h spec1_time,  lcolor(navy) lpattern(dash))  ///
		(line spec2_es_b spec2_time, lcolor(orange) lpattern(solid))      ///
		(rcap spec2_es_ci_l spec2_es_ci_h spec2_time,  lcolor(orange) lpattern(dash))   /// 
		(line spec3_es_b spec3_time, lcolor(black) lpattern(solid))     ///
		(rcap spec3_es_ci_l spec3_es_ci_h spec3_time,  lcolor(black) lpattern(dash))    ///
		(line spec4_es_b spec4_time, lcolor(green) lpattern(solid))    ///
		(rcap spec4_es_ci_l spec4_es_ci_h spec4_time,  lcolor(green) lpattern(dash))    ///
		(line spec5_es_b spec5_time, lcolor(red) lpattern(solid))   ///
		(rcap spec5_es_ci_l spec5_es_ci_h spec5_time,  lcolor(red) lpattern(dash))   ///
	, plotregion(fcolor(white) lcolor(white)) ///
	graphregion(fcolor(white) lcolor(white)) ///
	xtitle("Year") xlab(1990(5)2012) ///
	legend(size(small) pos(6) label(1 "Baseline") ///
	label(3 ">20,000")   ///
	label(5 ">30,000")  ///
	label(7 ">40,000") label(9 ">50,000")  ///
	r(2) order( 1 3 5 7 9)) ///
	ytitle("Percent Employment Growth") 
graph export "$output/Graphs/figureA21.pdf", replace	
