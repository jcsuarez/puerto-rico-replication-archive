// This file recreates Figure 11, conspuma long differences residualized
// Author: eddie yu
// Date: 2023-10-16

/* Data sources:
NETS and ACS
*/

clear all
set more off
snapshot erase _all 

*******************************************************
* Starting with the data
*******************************************************
use "$data/Replication Data/figure11_data", clear

* residualizing
foreach var of varlist pr_link d*mlwage dmlrent dmlvalue { 
	capture: drop res_`var'
	reg `var' i.year i.state_fips [aw=epop]
	predict res_`var', res
	sum `var' [aw=epop]
	}
sum pr_link [aw=epop]
replace res_pr_link = res_pr_link+r(mean)
	
** Graphs 
local res_pr_link "res_pr_link" 	
	qui sum `res_pr_link', d
	replace `res_pr_link' = `res_pr_link' - r(mean)
	
	xtile q_`res_pr_link'=`res_pr_link', nq(20)
	gen after = (year>1990)
	preserve 
	collapse (mean)res_dSmlwage (mean)res_dUmlwage (mean)res_dmlwage (mean) res_dmlrent (mean) `res_pr_link'  [aw=epop] , by(q_`res_pr_link' after) 
	rename res_dSmlwage q_res_dSlwage
	rename res_dUmlwage q_res_dUlwage
	rename `res_pr_link' q_m_`res_pr_link'
	rename res_dmlwage q_res_dlwage
	rename res_dmlrent q_res_dlrent
	tempfile qmeans
	save "`qmeans'"
	restore 
	merge m:1 q_`res_pr_link' after using "`qmeans'"
	drop _merge 

	local i = cond(`res_pr_link' == res_pr_link ,1 ,2)

*** Graphs for wages 
rename res_dmlwage res_dlwage
rename res_dUmlwage res_dUlwage
rename res_dSmlwage res_dSlwage
rename res_dmlrent res_dlrent
sum res_dlwage [aw=epop] if year == 1990,d
reg res_dlwage `res_pr_link' [aw=epop] if year ==1990, cl(state_fips)
local b: di %5.4f _b[`res_pr_link']
local se: di %5.4f _se[`res_pr_link']
mat table = r(table)
local p: di %5.4f table[4,1]

sum res_dlwage [aw=epop] if year == 1990,d
twoway  scatter res_dlwage `res_pr_link' [aw=epop] if year ==1990 & inrange(res_dlwage,`r(p1)',`r(p99)')   ///
		, yline(`r(mean)',lcolor(black) lpattern(dash)) mfcolor(gs15) ///
		|| lfitci  res_dlwage res_pr_link [aw=epop]  if year == 1990, level(90) lcolor(black) est(cl(state_fips)) ///
		|| scatter q_res_dlwage q_m_`res_pr_link' if year ==1990 & inrange(res_dlwage,`r(p1)',`r(p99)') ,  ///
		xlab(-3(2)3) ylab(-.15(.05).15) mcolor(black) ///
		graphregion(fcolor(white)) ///
		xtitle("IQR Exposure to Section 936") ///
		ytitle("Residualized Wage Growth") ///
		legend(size(small) pos(6) r(2)order(1 4 3 2) label(1 "Data (weighted by population)")  label(2 "90% CI") label(3 "Linear Fit") ///
		label(4 "Binned Data by Ventiles")) note("The slope of the regression line is  `b' with standard error `se' and p-value `p'")
	
graph export "$output/Graphs/figure11_D.pdf", replace		

sum res_dlwage [aw=epop] if year > 1990,d
reg res_dlwage `res_pr_link' [aw=epop] if year > 1990 , cl(state_fips)
local b: di %5.4f _b[`res_pr_link']
local se: di %5.4f _se[`res_pr_link']
mat table = r(table)
local p: di %5.4f table[4,1]
	
sum res_dlwage [aw=epop] if year > 1990,d
twoway  scatter res_dlwage `res_pr_link' [aw=epop] if year > 1990 & inrange(res_dlwage,`r(p1)',`r(p99)')   ///
		, yline(`r(mean)',lcolor(black) lpattern(dash)) mfcolor(gs15) ///
		|| lfitci  res_dlwage res_pr_link [aw=epop]  if year > 1990, level(90) lcolor(black)  est(cl(state_fips)) ///
		|| scatter q_res_dlwage q_m_`res_pr_link' if year > 1990 & inrange(res_dlwage,`r(p1)',`r(p99)') ,  ///
		xlab(-3(2)3)  ylab(-.15(.05).15) mcolor(black) ///
		graphregion(fcolor(white)) ///
		xtitle("IQR Exposure to Section 936") ///
		ytitle("Residualized Wage Growth") ///
		legend(size(small) pos(6) r(2)order(1 4 3 2) label(1 "Data (weighted by population)")  label(2 "90% CI") label(3 "Linear Fit") ///
		label(4 "Binned Data by Ventiles"))  note("The slope of the regression line is  `b' with standard error `se' and p-value `p'")	
  
graph export "$output/Graphs/figure11_A.pdf", replace		  

  
  
** Grpahs for LS wages
sum res_dUlwage [aw=epop] if year == 1990,d
reg res_dUlwage `res_pr_link' [aw=epop] if year ==1990, cl(state_fips)
local b: di %5.4f _b[`res_pr_link']
local se: di %5.4f _se[`res_pr_link']
mat table = r(table)
local p: di %5.4f table[4,1]
 
sum res_dUlwage [aw=epop] if year == 1990,d
twoway  scatter res_dUlwage `res_pr_link' [aw=epop] if year ==1990 & inrange(res_dUlwage,`r(p1)',`r(p99)')   ///
		, yline(`r(mean)',lcolor(black) lpattern(dash)) mfcolor(gs15) ///
		|| lfitci  res_dUlwage res_pr_link [aw=epop]  if year == 1990, level(90) lcolor(black) est(cl(state_fips)) ///
		|| scatter q_res_dUlwage q_m_`res_pr_link' if year ==1990 & inrange(res_dUlwage,`r(p1)',`r(p99)') ,  ///
		xlab(-3(2)3)  ylab(-.15(.05).15) mcolor(black) ///
		graphregion(fcolor(white)) ///
		xtitle("IQR Exposure to Section 936") ///
		ytitle("Residualized Wage Growth") ///
		legend(size(small) pos(6) r(2)order(1 4 3 2) label(1 "Data (weighted by population)")  label(2 "90% CI") label(3 "Linear Fit") ///
		label(4 "Binned Data by Ventiles"))  note("The slope of the regression line is  `b' with standard error `se' and p-value `p'")
	
graph export "$output/Graphs/figure11_E.pdf", replace		

sum res_dUlwage [aw=epop] if year > 1990,d
reg res_dUlwage `res_pr_link' [aw=epop] if year >1990, cl(state_fips)
local b: di %5.4f _b[`res_pr_link']
local se: di %5.4f _se[`res_pr_link']
mat table = r(table)
local p: di %5.4f table[4,1]
		
sum res_dUlwage [aw=epop] if year > 1990,d
twoway  scatter res_dUlwage `res_pr_link' [aw=epop] if year > 1990 & inrange(res_dUlwage,`r(p1)',`r(p99)')   ///
		, yline(`r(mean)',lcolor(black) lpattern(dash)) mfcolor(gs15) ///
		|| lfitci  res_dUlwage res_pr_link [aw=epop]  if year > 1990, level(90) lcolor(black) est(cl(state_fips)) ///
		|| scatter q_res_dUlwage q_m_`res_pr_link' if year > 1990 & inrange(res_dUlwage,`r(p1)',`r(p99)') ,  ///
		xlab(-3(2)3) ylab(-.15(.05).15) mcolor(black) ///
		graphregion(fcolor(white)) ///
		xtitle("IQR Exposure to Section 936") ///
		ytitle("Residualized Wage Growth") ///
		legend(size(small) pos(6) r(2)order(1 4 3 2) label(1 "Data (weighted by population)")  label(2 "90% CI") label(3 "Linear Fit") ///
		label(4 "Binned Data by Ventiles")) 	 note("The slope of the regression line is  `b' with standard error `se' and p-value `p'")
  
graph export "$output/Graphs/figure11_B.pdf", replace		  
  
    
  
** Graphs for rents 
sum res_dlrent [aw=epop] if year == 1990,d
reg res_dlrent `res_pr_link' [aw=epop] if year ==1990, cl(state_fips)
local b: di %5.4f _b[`res_pr_link']
local se: di %5.4f _se[`res_pr_link']
mat table = r(table)
local p: di %5.4f table[4,1]

sum res_dlrent [aw=epop] if year == 1990,d
twoway  scatter res_dlrent `res_pr_link' [aw=epop] if year ==1990 & inrange(res_dlrent,`r(p1)',`r(p99)')   ///
		, yline(`r(mean)',lcolor(black) lpattern(dash)) mfcolor(gs15) ///
		|| lfitci  res_dlrent res_pr_link [aw=epop]  if year == 1990, level(90) lcolor(black) est(cl(state_fips)) ///
		|| scatter q_res_dlrent q_m_`res_pr_link' if year ==1990 & inrange(res_dlrent,`r(p1)',`r(p99)') ,  ///
		xlab(-3(2)3)  ylab(-.3(.1).3) mcolor(black) ///
		graphregion(fcolor(white)) ///
		xtitle("IQR Exposure to Section 936") ///
		ytitle("Residualized Rent Growth") ///
		legend(size(small) pos(6) r(2)order(1 4 3 2) label(1 "Data (weighted by population)")  label(2 "90% CI") label(3 "Linear Fit") ///
		label(4 "Binned Data by Ventiles"))  note("The slope of the regression line is  `b' with standard error `se' and p-value `p'")
	
graph export "$output/Graphs/figure11_F.pdf", replace		
		
sum res_dlrent [aw=epop] if year > 1990,d
reg res_dlrent `res_pr_link' [aw=epop] if year >1990, cl(state_fips)
local b: di %5.4f _b[`res_pr_link']
local se: di %5.4f _se[`res_pr_link']
mat table = r(table)
local p: di %5.4f table[4,1]

sum res_dlrent [aw=epop] if year > 1990,d
twoway  scatter res_dlrent `res_pr_link' [aw=epop] if year > 1990 & inrange(res_dlrent,`r(p1)',`r(p99)')   ///
		, yline(`r(mean)',lcolor(black) lpattern(dash)) mfcolor(gs15) ///
		|| lfitci  res_dlrent res_pr_link [aw=epop]  if year > 1990, level(90) lcolor(black) est(cl(state_fips)) ///
		|| scatter q_res_dlrent q_m_`res_pr_link' if year > 1990 & inrange(res_dlrent,`r(p1)',`r(p99)') ,  ///
		xlab(-3(2)3) ylab(-.3(.1).3) mcolor(black) ///
		graphregion(fcolor(white)) ///
		xtitle("IQR Exposure to Section 936") ///
		ytitle("Residualized Rent Growth") ///
		legend(size(small) pos(6) r(2)order(1 4 3 2) label(1 "Data (weighted by population)")  label(2 "90% CI") label(3 "Linear Fit") ///
		label(4 "Binned Data by Ventiles"))  note("The slope of the regression line is  `b' with standard error `se' and p-value `p'")	
  
graph export "$output/Graphs/figure11_C.pdf", replace	
