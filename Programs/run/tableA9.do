***************************************************************
//Filename: tableA9.do
//Author: eddie yu
//Date: 2023-09-20
//Task: Given figure7_data_*.dta, creates Table A9
***************************************************************

/* Data sources:
QCEW and NETS
*/

clear all
set more off
snapshot erase _all 
estimates clear

*******************************************************
* County data
*******************************************************
use "$data/Replication Data/figure7_data_baseline", replace

* making interaction variable
xi i.year|pr_link_ind , noomit
drop _IyeaXp*1995

* county regressions
eststo emp_1:  reghdfe emp_growth _IyeaX* [aw=wgt]  , a( i.year#i.industr ) cl(fips_state indust )
	estadd local hasiy "Yes"
    estadd local hascty ""

eststo emp_2:  reghdfe emp_growth _IyeaX* [aw=wgt]  , a(i.pid i.year#i.industr ) cl(fips_state indust ) nocons
	estadd local hasiy "Yes"
	estadd local hascty "Yes"

eststo emp_3:  reghdfe emp_growth _IyeaX* [aw=wgt]  , a( i.pid2 i.year#i.indust ) cl(fips_state indust )
	estadd local hasiy "Yes"
	estadd local hasicty "Yes"

eststo emp_4:  reghdfe emp_growth _IyeaX* [aw=wgt_w] , a( i.pid i.year#i.indust ) cl(fips_state indust )
	estadd local hasiy "Yes"
	estadd local hascty "Yes"
	estadd local hasww "Yes"	

eststo emp_5:  reghdfe emp_growth _IyeaX* if base_emp > 1000 [aw=wgt] , a( i.pid i.year#i.indust ) cl(fips_state indust )
	estadd local hasiy "Yes"
	estadd local hascty "Yes"
	estadd local hasds "Yes"

***************************************************
* Conspuma data
***************************************************
use "$data/Replication Data/figure7_data_conspuma", replace

* making interaction variable
xi i.year|pr_link_ind , noomit
drop _IyeaXp*1995

eststo emp_6:  reghdfe emp_growth _IyeaX* [aw=wgt]  , a( i.year#i.industr ) cl(fips_state indust )
	estadd local hasiy "Yes"
    estadd local hascty ""

***************************************************
* Commuting Zone data
***************************************************
use "$data/Replication Data/figure7_data_czone", replace

* making interaction variable
xi i.year|pr_link_ind , noomit
drop _IyeaXp*1995

eststo emp_7:  reghdfe emp_growth _IyeaX* [aw=wgt] , a( i.year#i.industr ) cl(pid industr)

***************************************************
* assembling the table
***************************************************

esttab emp_? using "$output/Tables/tableA9.tex", drop(*) stats() ///
b(3) se par label  ///
noobs nogap nomtitle tex replace nocons ///
postfoot("\multicolumn{1}{l}{\textbf{Exposure to Section 936}}") nonum posthead("")


esttab emp_? using "$output/Tables/tableA9.tex",  preh("") ///
 b(3) se par mlab(none) coll(none) s() noobs nogap keep(_IyeaXpr__*)  /// 
coeflabel(_IyeaXpr__1990 "\hspace{1em}X 1990 " /// 
_IyeaXpr__1991 "\hspace{1em}X 1991 " /// 
_IyeaXpr__1992 "\hspace{1em}X 1992 " /// 
_IyeaXpr__1993 "\hspace{1em}X 1993 " /// 
_IyeaXpr__1994 "\hspace{1em}X 1994 " /// 
_IyeaXpr__1996 "\hline \hspace{1em}X 1996 " /// 
_IyeaXpr__1997 "\hspace{1em}X 1997 " /// 
_IyeaXpr__1998 "\hspace{1em}X 1998 " /// 
_IyeaXpr__1999 "\hspace{1em}X 1999 " /// 
_IyeaXpr__2000 "\hspace{1em}X 2000 " /// 
_IyeaXpr__2001 "\hspace{1em}X 2001 " /// 
_IyeaXpr__2002 "\hspace{1em}X 2002 " /// 
_IyeaXpr__2003 "\hspace{1em}X 2003 " /// 
_IyeaXpr__2004 "\hspace{1em}X 2004 " /// 
_IyeaXpr__2005 "\hspace{1em}X 2005 " /// 
_IyeaXpr__2006 "\hspace{1em}X 2006 " /// 
_IyeaXpr__2007 "\hspace{1em}X 2007 " /// 
_IyeaXpr__2008 "\hspace{1em}X 2008 " /// 
_IyeaXpr__2009 "\hspace{1em}X 2009 " /// 
_IyeaXpr__2010 "\hspace{1em}X 2010 " /// 
_IyeaXpr__2011 "\hspace{1em}X 2011 " /// 
_IyeaXpr__2012 "\hspace{1em}X 2012 " )   label /// 
 prefoot("\hline") ///
postfoot( ///
"Year by Industry Fixed Effects & Y & Y & Y & Y &  Y & Y & Y  \\" ///
"Location Fixed Effects &  &  Y &   &   &  & &   \\" ///
"Industry-by-County Fixed Effects &   &  & Y &   &  & &   \\" ///
"Winsorized Weights &    &  & & Y &  & &  \\" ///
"Drops Small County-Industries ($<$1000) &  &  & &   & Y & & \\"  ///
"Conspuma Geography  &   &   &   &  & & Y & \\"  ///
"Commuting Zone Geography  &   &   &   &  & & & Y \\"  ///
"  \hline\hline" ///
"\end{tabular} }" ) append sty(tex)


