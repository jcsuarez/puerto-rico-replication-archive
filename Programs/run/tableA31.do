***************************************************************
//Filename: tableA31.do
//Author: eddie yu
//Date: 2023-09-20
//Task: Given tableA31_data.dta, creates Table A31
***************************************************************

/* Data sources:
NETS, compustat
*/

clear all
set more off
snapshot erase _all 

*******************************************************
* Starting with the data
*******************************************************
use "$data/Replication Data/tableA31_data", clear

* w defines which spec, 3 means days (0,9)
local w = 3

foreach i of var etr-nol btd etr_change any_rd any_ads gpop PRexp {
qui sum `i' if CAR_1_3_`w' != ., d
gen mean`i' = r(mean)
gen sd`i' = r(sd)
gen `i'_`w' = (`i' - mean`i') / sd`i'
drop mean`i' sd`i'
}

* j determines if CARs use CAPM (1) or FF3factor + momentum (2)
local j = 1
* de-meaning all of the control (including dummies, so interpretation will be relative to average)
bysort naics: egen meanCAR_`j'_3_`w' = mean(CAR_`j'_3_`w')
egen ag_meanCAR_`j'_3_`w' = mean(CAR_`j'_3_`w')
gen CAR_`j'_3_`w'_demean = CAR_`j'_3_`w' - meanCAR_`j'_3_`w' + ag_meanCAR_`j'_3_`w'
drop meanCAR_`j'_3_`w' ag_meanCAR_`j'_3_`w'

qui eststo reg_1_`j'_`w':  areg CAR_`j'_3_`w' rd_`w' any_rd_`w'  , a(naics)  robust
	estadd local ind1 "Yes"


qui eststo reg_2_`j'_`w':  areg CAR_`j'_3_`w' rd_`w' any_rd_`w' gpop_`w' , a(naics)  robust
	estadd local ind1 "Yes"


qui eststo reg_3_`j'_`w':  areg CAR_`j'_3_`w' rd_`w' any_rd_`w' gpop_`w' any_ads_`w' , a(naics)  robust
	estadd local ind1 "Yes"


qui eststo reg_4_`j'_`w':  areg CAR_`j'_3_`w' rd_`w' any_rd_`w' gpop_`w' any_ads_`w' etr_change_`w' , a(naics)  robust
	estadd local ind1 "Yes"


qui eststo reg_5_`j'_`w':  areg CAR_`j'_3_`w' rd_`w' any_rd_`w' gpop_`w' any_ads_`w' etr_change_`w' PRexp_`w', a(naics)  robust
	estadd local ind1 "Yes"


eststo reg_6_`j'_`w':  reg CAR_`j'_3_`w'_demean rd_`w' any_rd_`w' any_ads_`w' gpop_`w' etr_change_`w' PRexp_`w' day1993 day1995, robust nocons  // 
	estadd local ind1 "Yes"

esttab reg_*_`j'_`w' using "$output/Tables/tableA31.tex", keep(_cons) stats() ///
b(3) se par label coeflabel(event_dummy "\hspace{1em}Event") ///
noobs nogap tex replace nocons postfoot("") nomti

esttab reg_*_`j'_`w' using "$output/Tables/tableA31.tex", keep(_cons rd_`w' gpop_`w' any_ads_`w' etr_change_`w' PRexp_`w' day*) ///
cells(b(fmt(3)) se(par) p) mlab(none) coll(none) order(_cons day*)  ///
varlabel(_cons "\hspace{1em}Exposure to Section 936" day1993 "\hspace{1em}February 16, 1993" day1995 "\hspace{1em}October 12, 1995"  ///
any_ads_`w' "\hspace{1em}Any Advertising" rd_`w' "\hspace{1em}Research \& Development" gpop_`w' "\hspace{1em}Gross Profit / Operating Assets"  ///
etr_change_`w' "\hspace{1em}Change in ETR" PRexp_`w' "\hspace{1em}Relative PR Employment") ///
scalars( N "ind1 Industry Fixed Effects" ) label /// 
preh("") prefoot("\hline")  postfoot( ///
"\hline " ///
"\end{tabular} }" ) append sty(tex) nonum
