*******************************************************************************
//Filename: figure2.do
//Author: eddie yu
//Date: 2023-10-16
//Task: Distribution of Exposure to S936 at County Level in 1995, 
//		for  (1) pr_link / (2) pr_link_ind variable
*******************************************************************************

/* Data sources:
Panel A: NETS establishment shares at county level
Panel B: NETS establishment shares at county level
*/

clear all
set more off


*******************************************************************************
* pr_link variable
use "$data/Replication Data/figure2_pr_link", clear

** graphing the baseline version
maptile pr_link , geo(county1990) stateoutline(linewidthstyle)  nquantiles(5) res(.1)
graph export "$output/Graphs/figure2_A.png", replace 


*******************************************************************************

* pr_link_ind variable
use "$data/Replication Data/figure2_pr_link_ind", clear

** graphing the baseline version
maptile pr_link , geo(county1990) stateoutline(linewidthstyle)  nquantiles(5) res(.1)
graph export "$output/Graphs/figure2_B.png", replace 
