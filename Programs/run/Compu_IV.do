***************************************************************
//Filename: Compu_IV.do
//Author: eddie yu
//Date: 2023-09-20
//Task: This script conducts IV regression on Narrow-Sample, Long-Difference
//		Size-by-Year FE, Winsorized at 5%
//		Using PR#post as IV will go into main tables,
//		using PR##year as IV will go into the appendix
***************************************************************

clear all
set more off
snapshot erase _all 

*******************************************************
* Starting with the data
*******************************************************
use "$data/Replication Data/table2_data_C", clear

destring n2, replace

* Approach 0922: 7 columns
global level "gvkey"
global spec1 ", cl(gvkey) a(PR ppe95_bins10##year)"
global spec2 ", cl(gvkey) a($level ppe95_bins10##year)"
global spec3 "if exp_sec, cl(gvkey) a($level ppe95_bins10##year)"
global spec4 "if exp_ind, cl(gvkey) a($level ppe95_bins10##year)"
global spec5 ", cl(gvkey) a($level ppe95_bins10##year n2##ppe95_bins10##year)"
global spec6 "[aw=DFL_ppe], cl(gvkey) a($level ppe95_bins10##year n2##ppe95_bins10##year)"
global spec7 "[aw=_webal], cl(gvkey) a($level ppe95_bins10##year n2##ppe95_bins10##year)"


summ capex_base if year == 2006, mean
scalar avg =  r(mean)/100 // 1.025
g capex_base_scaled = capex_base * 100 / r(mean)
summ capex_base_scaled, d


* PR-by-year instruments
local dep1 = "(tax_impute = PR##i.year)"
local dep2 = "(tax_impute2 = PR##i.year)"
local i = 0
foreach spec in spec1 spec2 spec3 spec4 spec5 spec6 spec7 {
	local i = `i' + 1
	eststo `spec'_est1: qui ivreghdfe capex_base_scaled `dep1' $`spec' 
	est save "$output/ster/Compu_IV_PRyear_taximpute1_`i'", replace
	eststo `spec'_est2: qui ivreghdfe capex_base_scaled `dep2' $`spec' 
	est save "$output/ster/Compu_IV_PRyear_taximpute2_`i'", replace

}		

*** "Industry-specific rho weight" ***	
esttab spec?_est1, keep(tax_impute) cells(b p)
*** "Constant rho weight" ***	
esttab spec?_est2, keep(tax_impute2) cells(b p)	


* PR-by-post instruments
local dep1 = "(tax_impute = PR##i.post2) PR##i.post1 PR##i.pre"
local dep2 = "(tax_impute2 = PR##i.post2) PR##i.post1 PR##i.pre"
local i = 0
foreach spec in spec1 spec2 spec3 spec4 spec5 spec6 spec7 {
	local i = `i' + 1
	eststo `spec'_est1: qui ivreghdfe capex_base_scaled `dep1' $`spec' 
	est save "$output/ster/Compu_IV_PRpost_taximpute1_`i'", replace
	eststo `spec'_est2: qui ivreghdfe capex_base_scaled `dep2' $`spec' 
	est save "$output/ster/Compu_IV_PRpost_taximpute2_`i'", replace
}		

*** "Industry-specific rho weight" ***	
esttab spec?_est1, keep(tax_impute) cells(b p)
*** "Constant rho weight" ***	
esttab spec?_est2, keep(tax_impute2) cells(b p)	
