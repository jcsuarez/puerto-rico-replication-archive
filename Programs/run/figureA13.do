***************************************************************
//Filename: figureA13.do
// This file creates two versions of ASM industry-state exposure event studies
// Author: Kevin Roberts
// Date: 2023-10-16
***************************************************************

/* Data sources:
ASM and NETS
*/

clear all
set more off
snapshot erase _all 

*******************************************************
* Panels A and B
*******************************************************
use "$data/Replication Data/figureA13_data", replace

xi i.year|pr_link_i , noomit 
forval i=1987(1)2014 { 
	capture: label var _IyeaXpr__`i' " "
} 
forval i=1987(2)2014 { 
	capture: label var _IyeaXpr__`i' "`i'"
} 
*gen post = (year>=1997)
drop _IyeaXpr__1995

snapshot save

**** Panel A ****
snapshot restore 1
reghdfe capexT_g _IyeaXpr* [aw=emp95], a(i.pid i.year#i.industry_cd  ) vce(cluster fips) nocons

** figure years for the graph
capture: drop fig_year
gen fig_year = 1987 in 1
foreach i of numlist 1/28 {
local j = `i' + 1986
replace fig_year = `j' in `i'
}

** annual betas for the preperiod
capture: drop pr_beta*
gen pr_beta = 0
gen pr_beta_lb = 0
gen pr_beta_ub = 0
foreach i of numlist 1/8 {
local j = `i' + 1986
lincom _b[_IyeaXpr__`j'] , level(90)
replace pr_beta = r(estimate) in `i'
replace pr_beta_lb = r(lb) in `i'
replace pr_beta_ub = r(ub) in `i'
}
* zero effect in 1995
replace pr_beta = 0 in 9
*missing in 1996
replace pr_beta = . in 10
replace pr_beta_lb = . in 10
replace pr_beta_ub = . in 10

** annual betas for the postperiod
foreach i of numlist 11/28 {
local j = `i' + 1986
lincom _b[_IyeaXpr__`j'] , level(90)
replace pr_beta = r(estimate) in `i'
replace pr_beta_lb = r(lb) in `i'
replace pr_beta_ub = r(ub) in `i'
}

* outputting the graph
 graph twoway (scatter pr_beta fig_year, lcolor(navy) mcolor(navy) yline(0 , lcolor(maroon)) xline(1995, lcolor(black) lpattern(solid)) msize(small)) ///
    (rcap pr_beta_lb pr_beta_ub fig_year, lcolor(navy) lpattern(solid)) ///
	, plotregion(fcolor(white) lcolor(white)) ///
	graphregion(fcolor(white) lcolor(white))  ///
	xtitle("Year") xlab(1987(3)2014) bgcolor(white) ///
	legend(size(small) on order (1 2) pos(6) label(1 "Puerto Rico Presence")  ///
	label(2 "90% CI") r(1)) ///
	yti("Effect of Exposure to S936 on CAPX", margin(medium))
graph export "$output/Graphs/figureA13_A.pdf", replace 


**** Panel B ****
snapshot restore 1
reghdfe capexT_g _IyeaXpr*, a(i.industry_cd i.year  ) vce(cluster fips) nocons


** figure years for the graph
capture: drop fig_year
gen fig_year = 1987 in 1
foreach i of numlist 1/28 {
local j = `i' + 1986
replace fig_year = `j' in `i'
}

** annual betas for the preperiod
capture: drop pr_beta*
gen pr_beta = 0
gen pr_beta_lb = 0
gen pr_beta_ub = 0
foreach i of numlist 1/8 {
local j = `i' + 1986
lincom _b[_IyeaXpr__`j'] , level(90)
replace pr_beta = r(estimate) in `i'
replace pr_beta_lb = r(lb) in `i'
replace pr_beta_ub = r(ub) in `i'
}
* zero effect in 1995
replace pr_beta = 0 in 9
*missing in 1996
replace pr_beta = . in 10
replace pr_beta_lb = . in 10
replace pr_beta_ub = . in 10

** annual betas for the postperiod
foreach i of numlist 11/28 {
local j = `i' + 1986
lincom _b[_IyeaXpr__`j'] , level(90)
replace pr_beta = r(estimate) in `i'
replace pr_beta_lb = r(lb) in `i'
replace pr_beta_ub = r(ub) in `i'
}

* outputting the graph
 graph twoway (scatter pr_beta fig_year, lcolor(navy) mcolor(navy) yline(0 , lcolor(maroon)) xline(1995, lcolor(black) lpattern(solid)) msize(small)) ///
    (rcap pr_beta_lb pr_beta_ub fig_year, lcolor(navy) lpattern(solid)) ///
	, plotregion(fcolor(white) lcolor(white)) ///
	graphregion(fcolor(white) lcolor(white))  ///
	xtitle("Year") xlab(1987(3)2014) bgcolor(white) ///
	legend(size(small) on order (1 2) pos(6) label(1 "Puerto Rico Presence")  ///
	label(2 "90% CI") r(1)) ///
	yti("Effect of Exposure to S936 on CAPX", margin(medium))
graph export "$output/Graphs/figureA13_B.pdf", replace 


