***************************************************************
//Filename: figureA19.do
//Author: eddie yu
//Date: 2023-10-16
//Task: This script creates Figure A.19, Panel A, B
**************************************************************

/* Data sources:
QCEW, NETS, ADH
*/

clear all
set more off
snapshot erase _all 

*graphing program
capture program drop ES_graph
program define ES_graph
syntax,  level(real) [  yti(string) note(string) outname(string) ylab(string) ]

qui { 
mat b = e(b)
mat V = e(V) 
mat es_b = 0 
mat es_ci_l = 0 
mat es_ci_h = 0 
mat time = 0 

// for 95% CI 
if `level' == 99 { 
	local z= 2.6  
}
if `level' == 95 { 
	local z= 1.96  
}
// for 90% CI 
if `level' == 90 { 
	local z= 1.645 
} 
	
forval i = 1/23 { 
	mat time = (time,`i'-6+1995)
	mat es_b = (es_b, b[1,`i'])
	mat es_ci_l = (es_ci_l, b[1,`i']-`z'*sqrt(V[`i',`i'])   )
	mat es_ci_h = (es_ci_h, b[1,`i']+`z'*sqrt(V[`i',`i'])   )		
	}
mat es_b =  (es_b[1,2..6],0,es_b[1,7...])'
mat es_ci_l = (es_ci_l[1,2..6],0,es_ci_l[1,7...])'  
mat es_ci_h = (es_ci_h[1,2..6],0,es_ci_h[1,7...])' 
mat time = time[1,2...]' 

capture: drop es_b es_ci* time 
svmat es_b 
svmat time
svmat es_ci_l
svmat es_ci_h 
}

twoway (scatter es_b time, yline(0 , lcolor(black)) xline(1995, lcolor(black) lpattern(dash))) ///
    (rcap es_ci_h es_ci_l time,  lcolor(navy) lpattern(dash)) ///
	, plotregion(fcolor(white) lcolor(white)) ///
	graphregion(fcolor(white) lcolor(white)) ///
	xtitle("Year") xlab(1990(5)2012) ///
	legend(size(small) pos(6) r(1) label(1 "PR Presence") label(2 "`level'% CI")) ///
	ytitle("`yti'", margin(medsmall)) bgcolor(white)  ///
	note("`note'") ylab("`ylab'")
graph export "`outname'.pdf", replace	

end 

*******************************************************
* starting with data from tableA19 for panel A
*******************************************************
use "$data/Replication Data/figureA19_data_A", replace

* setting up interactions
xi i.year|pr_link_i, noomit
drop _IyeaXp*1995

* running regressions
reghdfe ADH_growth _IyeaX* [aw=wgt]  , a(i.pid i.year ) cl(statefips )

ES_graph , level(95) yti("Effect S936 Exposure (IQR)") ///
note("Notes: (Epop{sub:ict}-Epop{sub:ic1995})/Epop{sub:ic1995}={&alpha}{sub:c}+{&gamma}{sub:it}+{&beta}{sub:t}S936 Exposure{sub:c}+{&epsilon}{sub:ict}. SE's clustered by State and Industry.") ///
outname("$output/Graphs/figureA19_A") ylab("-.12(.02).04")

*******************************************************
* starting with data from tableA20 for panel B
*******************************************************
use "$data/Replication Data/figureA19_data_B", replace

* setting up interactions
xi i.year|pr_link_i, noomit
drop _IyeaXp*1995

* running regressions
reghdfe ADH_growth _IyeaX* [aw=wgt]  , a(i.pid i.year ) cl(statefips )

ES_graph , level(95) yti("Effect S936 Exposure (IQR)") ///
note("Notes: Epop{sub:ict}-Epop{sub:ic1995}={&alpha}{sub:c}+{&gamma}{sub:it}+{&beta}{sub:t}S936 Exposure{sub:c}+{&epsilon}{sub:ict}. SE's clustered by State and Industry.") ///
outname("$output/Graphs/figureA19_B") ylab("-3.5(1).5")
