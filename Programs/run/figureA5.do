*******************************************************************************
//Filename: figureA5.do
//Author: eddie yu
//Date: 2023-10-16
//Task: Distribution of Standardized Exposure to S936 at County Level in 1995, 
//		for  (1) pr_link / (2) pr_link_ind variable
*******************************************************************************

/* Data sources:
Panel A: NETS establishment shares at county level
Panel B: NETS establishment shares at county level
*/

clear all
set more off


*******************************************************************************
* pr_link variable
use "$data/Replication Data/figure2_pr_link", clear

* Residualized Link 
reg pr_link i.fips_state
predict res_pr_link, res

* winsorizing residualized links
winsor res_pr_link, gen(w_res_pr_link) p(.01)
egen std_w_res_pr_link = std(w_res_pr_link)

** graphing the residualized version
maptile std_w_res_pr_link , geo(county1990) stateoutline(linewidthstyle)  nquantiles(5)   res(.5)
graph export "$output/Graphs/figureA5_A.png", replace 


*******************************************************************************

* pr_link_ind variable
use "$data/Replication Data/figure2_pr_link_ind", clear

* Residualized Link 
reg pr_link i.fips_state
predict res_pr_link, res

* winsorizing residualized links
winsor res_pr_link, gen(w_res_pr_link) p(.01)
egen std_w_res_pr_link = std(w_res_pr_link)

** graphing the residualized version
maptile std_w_res_pr_link , geo(county1990) stateoutline(linewidthstyle)  nquantiles(5)   res(.5)
graph export "$output/Graphs/figureA5_B.png", replace 
