// This file recreates Figure 5: Employment Effects S936 by Tercile of Exposure
// Author: eddie yu
// Date: 2023-10-16

/* Data sources:
NETS and QCEW
*/

clear all
set more off
snapshot erase _all 

*******************************************************
* Starting with the data
*******************************************************
use "$data/Replication Data/figure5_data", clear

xi i.year*i.bin_PR_i, noomit
drop _IyeaX*1995* _IyeaXbin*4 // dropping year 1995 and PR_Link_i == 0 

est clear 
set more off 
eststo emp_2:  reghdfe emp_growth _IyeaX* [aw=base_emp] , a(Y=i.year#i.industr A=i.pid) cl(fips_state indust)
gen year_hat = Y - A + _b[_cons]					
	
* saving yearly PR interaction terms	
qui { 
mat b = e(b)
svmat b
}

* Generating a single variable dependent on year that has the PR interaction effect
egen year_beta1 = rowfirst(b1)
egen year_beta2 = rowfirst(b2)
egen year_beta3 = rowfirst(b3)
forval j = 1/3{
forval k = 1/5  {
local i = `j' + (`k' - 1) * 3

qui sum b`i'
local b_mean = r(mean)
replace b`i' = `b_mean' if b`i' == . 
local year  = `k' +  1989
replace year_beta`j' = b`i' if year == `year'

}
forval k = 6/22 {
local i = `j' + (`k' - 1) * 3

qui sum b`i'
local b_mean = r(mean)
replace b`i' = `b_mean' if b`i' ==. 
local year  = `k' +  1990
replace year_beta`j' = b`i' if year == `year'
}
}

drop b??
replace year_beta1 = 0 if year_beta1 == .
replace year_beta2 = 0 if year_beta2 == .
replace year_beta3 = 0 if year_beta3 == .

collapse (mean) year_hat (first) year_beta? [fw=base_emp], by(year)	

replace year_beta1 = (year_beta1 + year_hat) * 100
replace year_beta2 = (year_beta2 + year_hat) * 100
replace year_beta3 = (year_beta3 + year_hat) * 100
 
* graphing
 graph twoway (scatteri -10 1990.5   -10 1991.25 , recast(area) color(gs12) ) ///
     (scatteri 30 1990.5   30 1991.25 , recast(area) color(gs12) ) ///
     (scatteri -10 2001   -10 2002 , recast(area) color(gs12) ) ///
     (scatteri 30 2001   30 2002 , recast(area) color(gs12) ) ///
     (scatteri -10 2007.75   -10 2009.75 , recast(area) color(gs12) ) ///
     (scatteri 30 2007.75   30 2009.75 , recast(area) color(gs12) ) ///  
	(scatter year_beta1 year, lcolor(navy) mcolor(navy) c(direct) yline(0 , lcolor(black)) xline(1995, lcolor(black) lpattern(dash)) msize(small)) ///
    (scatter year_beta3 year, lcolor(orange) mcolor(orange) c(direct) lpattern(dash) msize(small)) ///
	, plotregion(fcolor(white) lcolor(white)) ///
	graphregion(fcolor(white) lcolor(white))  ///
	xtitle("Year") xlab(1990(5)2012) bgcolor(white) ///
	legend(size(small) pos(6) on order (7 8 1) label(7 "Low S936 Exposure")  ///
	label(8 "High S936 Exposure") label(1 "Recession") r(1)) ///
	yti("Percentage Change in Employment", margin(medium))
	
graph export "$output/Graphs/figure5.pdf", replace	

