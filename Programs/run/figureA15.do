***************************************************************
//Filename: figureA15.do
//Author: eddie yu
//Date: 2023-10-16
//Task: Given figureA15_data, creates Figure A15
***************************************************************


/* Data sources:
Nets
*/

* starting point for all panels is 3-wrds_segments/DFL_nets_emp_2019_06_11_dan

clear all
set more off
snapshot erase _all 

// Define Command for ES graph
capture program drop ES_graph
program define ES_graph
syntax,  level(real) [  yti(string) note(string) outname(string) ylab(string) ]

qui { 
mat b = e(b)
mat V = e(V) 
mat es_b = 0 
mat es_ci_l = 0 
mat es_ci_h = 0 
mat time = 0 

// for 95% CI 
if `level' == 99 { 
	local z= 2.6  
}
if `level' == 95 { 
	local z= 1.96  
}
// for 90% CI 
if `level' == 90 { 
	local z= 1.645 
} 
	
forval i = 1/23 { 
	mat time = (time,`i'-6+1995)
	mat es_b = (es_b, b[1,`i'])
	mat es_ci_l = (es_ci_l, b[1,`i']-`z'*sqrt(V[`i',`i'])   )
	mat es_ci_h = (es_ci_h, b[1,`i']+`z'*sqrt(V[`i',`i'])   )		
	}
mat es_b =  (es_b[1,2..6],0,es_b[1,7...])'
mat es_ci_l = (es_ci_l[1,2..6],0,es_ci_l[1,7...])'  
mat es_ci_h = (es_ci_h[1,2..6],0,es_ci_h[1,7...])' 
mat time = time[1,2...]' 

capture: drop es_b es_ci* time 
svmat es_b 
svmat time
svmat es_ci_l
svmat es_ci_h 
}
end 

*******************************************************
* employment graph at NETS firm level
*******************************************************
use "$data/Replication Data/figureA15_data", replace

* Regression Specifications
global level "PR"
global spec1 " [aw=wgt],vce(cluster hqduns) nocon a($level year)"
global spec2 " [aw=wgt],vce(cluster hqduns) nocon a($level naic2##year)"
global spec3 "if major_sec [aw=wgt],vce(cluster hqduns) nocon a($level naic2##year)"
global spec4 "if major_ind [aw=wgt],vce(cluster hqduns) nocon a($level naic2##year)"
global spec5 "if major_ind [aw=w], vce(cluster hqduns) nocon a($level naic2##year)"


global specname1 "Year FE"
global specname2 "Industry X Year FE"
global specname3 "Major Sector with FE"
global specname4 "Major Industry with FE"
global specname5 "Major Industry + DFL with FE"


global c1 "maroon"
global c2 "navy"
global c3 "orange"
global c4 "green"
global c5 "black"

global m1 "plus"
global m2 "circle"
global m3 "square"
global m4 "diamond"
global m5 "triangle"


xi i.year|PR, noomit 
drop _IyeaXP*1995

* employment
est clear
eststo emp_0:  reghdfe emp_growth _IyeaXP* $spec1  
  estadd local hasy "Yes"    

ES_graph , level(95) 

rename es_b1 es_0
rename time1 t_0
rename es_ci_l1 es_l0
rename es_ci_h1 es_h0 

eststo emp_1:  reghdfe emp_growth _IyeaXP*  $spec2 
  estadd local hasy "Yes"    

ES_graph , level(95) 

rename es_b1 es_1
rename time1 t_1
rename es_ci_l1 es_l1
rename es_ci_h1 es_h1 

eststo emp_2:  reghdfe emp_growth _IyeaXP*  $spec3
  estadd local hasy "Yes"      

ES_graph , level(95) 

rename es_b1 es_2
rename time1 t_2
rename es_ci_l1 es_l2
rename es_ci_h1 es_h2  

eststo emp_3:  reghdfe emp_growth _IyeaXP* $spec4   
  estadd local hasy "Yes"     

ES_graph , level(95) 

rename es_b1 es_3
rename time1 t_3
rename es_ci_l1 es_l3
rename es_ci_h1 es_h3  

eststo emp_4:  reghdfe emp_growth _IyeaXP* $spec5
  estadd local hasy "Yes"       
  
ES_graph , level(95) 

rename es_b1 es_4
rename time1 t_4
rename es_ci_l1 es_l4
rename es_ci_h1 es_h4  

replace t_0 = t_0 - 0.2
replace t_1 = t_1 - 0.1
replace t_3 = t_3 + 0.1
replace t_4 = t_4 + 0.2
 graph twoway (scatter es_0 t_0, lcolor($c1) connect(line) mcolor($c1) ///
				msymbol($m1) yline(0 , lcolor(red)) xline(1995, lcolor(black) lpattern(solid)) msize(small)) ///
				(rcap es_l0 es_h0 t_0, lcolor($c1) lpattern(dash)) ///
				(scatter es_1 t_1, lcolor($c2) connect(line) mcolor($c2) msymbol($m2) msize(small)) ///
				(rcap es_l1 es_h1 t_1, lcolor($c2) lpattern(dash)) ///
				(scatter es_2 t_2, lcolor($c3) connect(line) mcolor($c3) msymbol($m3) msize(small)) ///
				(rcap es_l2 es_h2 t_2, lcolor($c3) lpattern(dash)) ///
				(scatter es_3 t_3, lcolor($c4) connect(line) mcolor($c4) msymbol($m4) msize(small)) ///
				(rcap es_l3 es_h3 t_3, lcolor($c4) lpattern(dash)) ///
				(scatter es_4 t_4, lcolor($c5) connect(line) mcolor($c5) msymbol($m5) msize(small)) ///
				(rcap es_l4 es_h4 t_4, lcolor($c5) lpattern(dash)) ///
	, plotregion(fcolor(white) lcolor(white)) ///
	graphregion(fcolor(white) lcolor(white))  ///
	xtitle("Year") xlab(1990(5)2012) ylab(-.2(.1).1) bgcolor(white) ///
	legend(size(vsmall) pos(6) on order (1 2 3 4 5 6 7 8 9 10) label(1 $specname1)  ///
	label(2 "95% CI") label(3 $specname2)  ///
	label(4 "95% CI") label(5 $specname3)  ///
	label(6 "95% CI") label(7 $specname4)  ///
	label(8 "95% CI") label(9 $specname5)  ///
	label(10 "95% CI") r(5)) ///
	yti("Effect of S936 on Firm Employment", margin(medium))
graph export "$output/Graphs/figureA15.pdf", replace 
