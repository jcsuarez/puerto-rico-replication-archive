***************************************************************
//Filename: figureA17.do
//Author: eddie yu
//Date: 2023-10-16
//Task: This script creates Figure A17 under different specifications
***************************************************************

clear all
set more off
snapshot erase _all 

*******************************************************
* Starting with the data
*******************************************************
use "$data/Replication Data/figureA17_data", clear


gen est_growth100 = est_growth

*** labels for some graphs
local fs_capx_name "Foreign Share Investment"
local capex_base_name "Investment/Physical Capital"
local capex_base_p_name "% Change in Investment/Physical Capital"
local capex_base_1_name "Investment/Physical Capital"
local ftax_name "Federal Tax Rate"
local est_growth100_name "Number of Establishments"
 

* interaction variables
xi i.year|PR , noomit
drop _IyeaXPR_1995 

forval i=1990(1)2014 { 
	capture: label var _IyeaXPR_`i' " "
} 
forval i=1991(2)2014 { 
	capture: label var _IyeaXPR_`i' "`i'"
} 

* Spec
global level "PR"
global spec1 " [aw=wgt],vce(cluster hqduns) nocon a($level year)"
// global spec2 " [aw=wgt],vce(cluster hqduns) nocon a($level naic2##year)"
// global spec3 "if major_sec [aw=wgt],vce(cluster hqduns) nocon a($level naic2##year)"
// global spec2 "if major_ind [aw=wgt],vce(cluster hqduns) nocon a($level naic2##year)"
// global spec5 "if major_ind [aw=DFL2], vce(cluster hqduns) nocon a($level naic2##year)"

// global specname1 "Baseline"
// global specname2 "NAIC2"
// global specname3 "Exp. Sector"
// global specname2 "Exp. Industry"
// global specname5 "Exp. Industry + DFL"

global c1 "navy"
global c2 "dkorange"
global c3 "midgreen"
global c4 "maroon"
global c5 "midblue"

snapshot save



*** Store estimates ***
local y "est_growth100"
local cnt = 0
foreach spec in spec1  { //  spec2 spec3 spec4 spec5
	local cnt = `cnt' + 1
	reghdfe `y' _IyeaX* PR $`spec'
	estimates save `spec'_base, replace
	
preserve
	estimates use `spec'_base

	** figure years for the graph
	gen fig_year = 1990 in 1
	foreach i of numlist 1/23 {
		local j = `i' + 1989
		replace fig_year = `j' in `i'
	}
	
	** annual betas for the preperiod
	gen pr_beta = 0
	gen pr_beta_lb = 0
	gen pr_beta_ub = 0
	foreach i of numlist 1/5 {
		local j = `i' + 1989
		qui lincom _b[_IyeaXPR_`j'] , level(95)
		replace pr_beta = r(estimate) in `i'
		replace pr_beta_lb = r(lb) in `i'
		replace pr_beta_ub = r(ub) in `i'
	}
	* zero effect in 1995
	qui lincom  0
	replace pr_beta = r(estimate) in 5

	** annual betas for the postperiod
	foreach i of numlist 7/23 {
		local j = `i' + 1989
		qui lincom _b[_IyeaXPR_`j'] , level(95)
		replace pr_beta = r(estimate) in `i'
		replace pr_beta_lb = r(lb) in `i'
		replace pr_beta_ub = r(ub) in `i'
	}
	
	gen specname = "`spec'"
	keep if _n < 24
	keep fig_year pr_beta* specname
	tempfile `spec'_graph
	save ``spec'_graph', replace	
restore
}


*** Graph Overlay ***
{
preserve
	use `spec1_graph', replace
	local j = 0
	forvalues i = 2/`cnt' {
		local j = `j' + 0.1
		append using `spec`i'_graph'
		replace fig_year = fig_year+`j' if specname=="spec`i'" & fig_year!=1995
	}
		
	graph twoway ///
	 (scatter pr_beta fig_year if specn=="spec1", lco($c1) mco($c1) msize(small)) ///
	 (rcap pr_beta_lb pr_beta_ub fig_year if specn=="spec1", lco($c1) lpa(solid)) ///
		, yline(0 , lcolor(maroon) lpatter(solid)) xline(1995, lcolor(black) lpattern(solid))  ///
		plotregion(fcolor(white) lcolor(white)) ///
		graphregion(fcolor(white) lcolor(white))  ///
		 xtitle("Year") xlab(1990(5)2012) bgcolor(white) ///
		legend(size(small) pos(6)  r(1) on order (1 2) ///
		label(1 "Estimate") ///
		label(2 "Confidence Interval")) ///
		yti("Effect of S936 on ``y'_name'", margin(medium))
restore	
}

graph export "$output/Graphs/figureA17.pdf", replace 

