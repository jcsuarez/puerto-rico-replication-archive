***************************************************************
//Filename: figure3.do
//Author: eddie yu
//Date: 2023-09-20
//Task: This script creates Figure3 under different specifications
***************************************************************

clear all
set more off
snapshot erase _all 

*******************************************************
* Starting with the data
*******************************************************
use "$data/Replication Data//figure3_data", clear

*** labels for some graphs
local fs_capx_name "Foreign Share Investment"
local capex_base_name "Investment/Physical Capital"
local capex_base_p_name "% Change in Investment/Physical Capital"
local capex_base_1_name "Investment/Physical Capital"
local ftax_name "Federal Tax Rate"
 
* combining years and N2
egen yearg = group(year n2)

*figures 5, 6, A5, and A6
xi i.year|PR , noomit
drop _IyeaXPR_1995 

forval i=1990(1)2014 { 
	capture: label var _IyeaXPR_`i' " "
} 
forval i=1990(2)2014 { 
	capture: label var _IyeaXPR_`i' "`i'"
} 

* Approach 0922: 7 columns
global level "gvkey"
global spec1 ", cl(gvkey) a(ppe95_bins10##year)"
// global spec2 ", cl(gvkey) a($level ppe95_bins10##year)"
// global spec3 "if exp_sec, cl(gvkey) a($level ppe95_bins10##year)"
// global spec4 "if exp_ind, cl(gvkey) a($level ppe95_bins10##year)"
// global spec5 ", cl(gvkey) a($level ppe95_bins10##year n2##ppe95_bins10##year)"
// global spec6 "[aw=DFL_ppe], cl(gvkey) a($level ppe95_bins10##year n2##ppe95_bins10##year)"
// global spec7 "[aw=_webal], cl(gvkey) a($level ppe95_bins10##year n2##ppe95_bins10##year)"

xtset gvkey year

global estimate_name "Estimate"

// global specname1 "(1) Baseline; Size-by-Year FE"
// global specname2 "(2) Baseline + Firm FE"
// global specname3 "(3) Exposed Sector: Baseline + Firm FE"
// global specname4 "(4) Exposed Industry: Baseline + Firm FE"
// global specname5 "(5) Spec (2) + Industry-by-Size-by-Year FE)"
// global specname6 "(6) Spec (5) + DFL Weights"
// global specname7 "(7) Spec (5) + Entropy Balancing"

global c1 "navy"
global c2 "dkorange"
global c3 "midgreen"
global c4 "maroon"
global c5 "midblue"
global c6 "red"
global c7 "olive"

snapshot save


xtset gvkey year


*** Store estimates ***
local y "capex_base"
local cnt = 0
foreach spec in spec1  { // spec2 spec3 spec4 spec5 spec6 spec7
	local cnt = `cnt' + 1
	reghdfe `y' _IyeaX* PR $`spec'
	estimates save `spec'_base, replace
	
preserve
	estimates use `spec'_base

	** figure years for the graph
	gen fig_year = 1989 in 1
	foreach i of numlist 1/23 {
		local j = `i' + 1989
		replace fig_year = `j' in `i'
	}
	
	** annual betas for the preperiod
	gen pr_beta = 0
	gen pr_beta_lb = 0
	gen pr_beta_ub = 0
	foreach i of numlist 1/5 {
		local j = `i' + 1989
		qui lincom _b[_IyeaXPR_`j'] , level(95)
		replace pr_beta = r(estimate) in `i'
		replace pr_beta_lb = r(lb) in `i'
		replace pr_beta_ub = r(ub) in `i'
	}
	* zero effect in 1995
	qui lincom  0
	replace pr_beta = r(estimate) in 5

	** annual betas for the postperiod
	foreach i of numlist 7/23 {
		local j = `i' + 1989
		qui lincom _b[_IyeaXPR_`j'] , level(95)
		replace pr_beta = r(estimate) in `i'
		replace pr_beta_lb = r(lb) in `i'
		replace pr_beta_ub = r(ub) in `i'
	}
	
	gen specname = "`spec'"
	keep if _n < 24
	keep fig_year pr_beta* specname
	tempfile `spec'_graph
	save ``spec'_graph', replace	
restore
}


*** Graph Overlay ***
{
preserve
	use `spec1_graph', replace
	local j = 0
	forvalues i = 2/`cnt' {
		local j = `j' + 0.1
		append using `spec`i'_graph'
		replace fig_year = fig_year+`j' if specname=="spec`i'" & fig_year!=1995
	}
		
	graph twoway ///
	 (scatter pr_beta fig_year if spec=="spec1", lco($c1) mco($c1) msize(small)) ///
	 (rcap pr_beta_lb pr_beta_ub fig_year if specn=="spec1", lco($c1) lpa(solid)) ///
		, yline(0 , lcolor(maroon) lpatter(solid)) xline(1995, lcolor(black) lpattern(solid))  ///
		plotregion(fcolor(white) lcolor(white)) ///
		graphregion(fcolor(white) lcolor(white))  ///
		 xtitle("Year") xlab(1990(5)2012) bgcolor(white) ///
		legend(size(small) pos(6)  r(1) on order (1 2) ///
		label(1 "PR Presence") ///
		label(2 "95% CI")) ///
		yti("Effect of S936 on ``y'_name'", margin(medium))
restore	
}

graph export "$output/Graphs/figure3.pdf", replace 

