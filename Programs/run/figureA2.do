***************************************************************
//Filename: figureA2.do
//Author: eddie yu
//Date: 2023-09-20
//Task: With figureA2_data, this script plots lorenz curve
***************************************************************

clear all
set more off 
snapshot erase _all

use "$data/Replication Data/figureA2_data", clear

** Plot Lorenz Curve **
lorenz estimate ppe95 if PR==1, level(90) 
	local cut = e(p)[1,13]
	local cut_val = round(e(b)[1, 13], 0.001)

di `cut'
di `cut_val'

	
lorenz estimate ppe95 if PR==1, level(90) ///
	graph(diagonal(lpattern(solid)) legend(size(small) pos(3) ///
	on order (2) label(2 "90% Confidence Interval") r(2)) ///
	xti("Centiles of 1995 PPE") yti("Cumulative Density") /// 
	xline(`cut', lcolor(maroon) lpattern(dash) lwidth(0.1)) /// 
	yline(`cut_val', lcolor(maroon) lpattern(dash) lwidth(0.1)) ///
	xlabel(`cut', add custom labsize(big) labcolor(red)) ylabel(`cut_val', add custom labsize(big) labcolor(red)))

	graph export "$output/Graphs/figureA2.pdf", replace  
