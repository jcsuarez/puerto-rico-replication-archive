// This file recreates Figure 9, robustness checks
// Author: eddie yu
// Date: 2023-10-16

/* Data sources:
NETS and QCEW
*/

clear all
set more off
snapshot erase _all 

// Define Command for ES 
capture program drop ES_graph
program define ES_graph
syntax,  level(real) [  yti(string) note(string) outname(string) ylab(string) ]

qui { 
mat b = e(b)
mat V = e(V) 
mat es_b = 0 
mat es_ci_l = 0 
mat es_ci_h = 0 
mat time = 0 

// for 95% CI 
if `level' == 99 { 
	local z= 2.6  
}
if `level' == 95 { 
	local z= 1.96  
}
// for 90% CI 
if `level' == 90 { 
	local z= 1.645 
} 
	
forval i = 1/23 { 
	mat time = (time,`i'-6+1995)
	mat es_b = (es_b, b[1,`i'])
	mat es_ci_l = (es_ci_l, b[1,`i']-`z'*sqrt(V[`i',`i'])   )
	mat es_ci_h = (es_ci_h, b[1,`i']+`z'*sqrt(V[`i',`i'])   )		
	}
mat es_b =  (es_b[1,2..6],0,es_b[1,7...])'
mat es_ci_l = (es_ci_l[1,2..6],0,es_ci_l[1,7...])'  
mat es_ci_h = (es_ci_h[1,2..6],0,es_ci_h[1,7...])' 
mat time = time[1,2...]' 

capture: drop es_b es_ci* time 
svmat es_b 
svmat time
svmat es_ci_l
svmat es_ci_h 
}

twoway (scatter es_b time, yline(0 , lcolor(black)) xline(1995, lcolor(black) lpattern(dash))) ///
    (rcap es_ci_h es_ci_l time,  lcolor(navy) lpattern(dash)) ///
	, plotregion(fcolor(white) lcolor(white)) ///
	graphregion(fcolor(white) lcolor(white)) ///
	xtitle("Year") xlab(1990(5)2012) ///
	legend(size(small) pos(6) label(1 "Estimate") label(2 "`level'% CI")) ///
	ytitle("`yti'") ///
	note("`note'") ylab("`ylab'")
graph export "`outname'.pdf", replace	

end 

*******************************************************
* Starting with the data for panel A
*******************************************************
use "$data/Replication Data/figure9_data", clear

** Robustness tables 
est clear 
eststo rob_0: reghdfe emp_growth cen_pr_link_i [aw=wgt] if inrange(year,2004,2008) ,  a( i.year  i.industr) cl(fips_state  i.industr) 
	estadd local hasy "Yes"
	estadd local hasind "Yes"
foreach var of varlist std_* { 
	eststo rob_`var': reghdfe emp_growth cen_pr_link_i `var' [aw=wgt] if inrange(year,2004,2008) ,  a( i.year  i.industr) cl(fips_state  i.industr) 
	estadd local hasy "Yes"
	estadd local hasind "Yes"

	} 
eststo rob_all: reghdfe emp_growth cen_pr_link_i std_* [aw=wgt] if inrange(year,2004,2008) ,  a( i.year  i.industr) cl(fips_state  i.industr) 
	estadd local hasy "Yes"
	estadd local hasind "Yes"		
	
eststo rob_fsd: reghdfe emp_growth cen_pr_link_i $pra_control_rob fs_std_n2-fs_std_n6 std_* [aw=wgt] if inrange(year,2004,2008) ,  a( i.year  i.industr) cl(fips_state  i.industr) 
	estadd local hasy "Yes"
	estadd local hasind "Yes"		
	estadd local hasfsd "Yes"	

	estimates restore rob_0
	local beta = _b[cen_pr_link_i]
	local mini = round(`beta'*2, -.03)
	local mini = max(`mini', -0.15)
label var cen_pr_link_i "Robustness of Effect of Exposure to S936 on Employment Growth 2004-2008"
	coefplot (rob_0,m(circle) msiz(medlarge)) (rob_std_total,m(square_hollow) msiz(medlarge)) (rob_std_job,m(diamond) msiz(medlarge)) ///
	(rob_std_train,m(triangle_hollow) msiz(medlarge)) (rob_std_mw,m(smdiamond) msiz(medlarge)) ///
	(rob_std_rtw,m(smtriangle_hollow) msiz(medlarge)) (rob_std_rd,m(triangle) msiz(medlarge)) (rob_std_ic,m(diamond_hollow) msiz(medlarge)) ///
	(rob_std_corp,m(square) msiz(medlarge)) (rob_std_ptax,m(X) msiz(medlarge)) (rob_std_prop,m(smcircle_hollow) msiz(medlarge)) ///
	(rob_std_sales,m(smsquare_hollow) msiz(medlarge)) (rob_std_trade,m(smtriangle) msiz(medlarge)) (rob_std_rev,m(smdiamond_hollow) msiz(medlarge)) ///
	(rob_std_routine,m(smcircle) msiz(medlarge)) (rob_std_nafta,m(circle_hollow) msiz(medlarge)) ///
	(rob_all,m(smsquare) msiz(medlarge)) (rob_fsd,m(plus) msiz(medlarge)) ///
	,  keep(cen_pr_link_i)  bgcolor(white)  ///
	graphregion(color(white)) vertical ylab(`mini'(.03)0) yline(0) ciopts(recast(rcap)) yline(`beta' , lpattern(dash) lcolor(navy)) ///
	legend(size(small) pos(6) r(5) label(2 "Base") label(4 "Total Incentives") ///
	label(6 "Job Incentives") ///
	label(8 "Training Subsidy") ///
	label(10 "Min Wage") ///
	label(12 "Right to Work") ///
	label(14 "R&D Cred") ///
	label(16 "Investment Cred") ///
	label(18 "Corporate Tax") ///
	label(20 "Personal Tax") ///
	label(22 "Property Tax") ///
	label(24 "Sales Tax Rate") ///
	label(26 "Trade (China)") ///
	label(28 "Tax Revenue ") ///
	label(30 "Routine Jobs") ///	
	label(32 "Trade (Nafta)") ///	
	label(34 "All Covariates") ///		
	label(36 "All+Firm Size Dist.") cols(4)) ///
	ytitle("Effect of Exposure to S936 on Emp. Growth", margin(small)) scale(.93)
	
	graph export "$output/Graphs/figure9.pdf", replace 
