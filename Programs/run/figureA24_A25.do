***************************************************************
// Filename: figureA24_A25.do
// This file recreates Figure A24 and A25, event studies
// Author: Dan Garrett
// Date: 2023-10-16
***************************************************************

/* Data sources:
NETS, compustat, crsp
*/

clear all
set more off
snapshot erase _all 

*******************************************************
* Starting with the data
*******************************************************
use "$data/Replication Data/figureA24_A25_data", clear

set more off
local spec1 "c.mktrf"
local spec2 "`spec1' c.smb c.hml c.MOM"
local event_dates "12100 13068"

local i = 5
forvalues k=1(1)2 {
* with lags and leads
gen x`k' = 0
replace x`k' = event_int_12100_`i' + event_int_13068_`i'
eststo reg_`k': reg ret i.id i.id#(`spec`k'') ib0.x`k' if estimation12100_`i'==1 | ///
 estimation13068_`i'==1,  r

}

* Aggregating up individual estimates of AR into CAR (Matching Investment graph style)
forvalues k=1(1)2 {

* part 1
estimates restore reg_`k'
gen fig_b_`k'_pre = 0
gen fig_se_`k'_pre = 0
gen fig_b_`k' = 0
gen fig_se_`k' = 0

*counting up from t = -prewindow
local agg "1.x"
forvalues i=1(1)21 {
lincom (`agg')

local j = `i' + 1
replace fig_b_`k'_pre=r(estimate) in `j'
replace fig_se_`k'_pre=r(se) in `j'
local agg "`agg' + `j'.x"
}
*counting up from the event
local agg "6.x"
forvalues i=6(1)21 {
lincom (`agg')

local j = `i' + 1
replace fig_b_`k'=r(estimate) in `j'
replace fig_se_`k'=r(se) in `j'
local agg "`agg' + `j'.x"
}
gen fig_upper_`k'_pre=fig_b_`k'_pre+1.645*fig_se_`k'_pre
gen fig_lower_`k'_pre=fig_b_`k'_pre-1.645*fig_se_`k'_pre
gen fig_upper_`k'=fig_b_`k'+1.645*fig_se_`k'
gen fig_lower_`k'=fig_b_`k'-1.645*fig_se_`k'
}
gen fig_t = _n - 7 in 1/22
gen fig_t2 = fig_t + .2

* Actual graphs
twoway (line fig_b_1_pre fig_t if fig_t<16 & fig_t>-8, msymbol(diamond) xline(-0.0, lc(gs11)) c(l) lpattern(dash) lcolor(cranberry) lwidth(medthick)) ///
	(rcap fig_upper_1_pre fig_lower_1_pre fig_t  if fig_t<16 & fig_t>-8, lpattern(dash) lcolor(cranberry) c(l) m(i) lwidth(medthick) yline(0, lcolor(black))) ///
	(line fig_b_1 fig_t2 if fig_t<16 & fig_t>-8, msymbol(diamond) c(l) lpattern(solid) lcolor(navy) lwidth(medthick)) ///
	(rcap fig_upper_1 fig_lower_1 fig_t2  if fig_t<16 & fig_t>-8, lpattern(solid) lcolor(navy) c(l) m(i) lwidth(medthick)), /// 
	xlabel(-6 (3) 15) ///
	graphregion(fcolor(white)) ///
	xtitle("Days from Event") ///
	ytitle("CAR (Percent)") ///
	legend(size(small) pos(6) r(2) order(1 2 3 4) label(1 "Cumulative Abnormal Return with Leads")  label(2 "90% CI") ///
	label(3 "Cumulative Abnormal Return without Leads") label(4 "90% CI"))
graph export "$output/Graphs/figureA24.pdf", replace

* version with 3 factors + momentum
twoway (line fig_b_2_pre fig_t if fig_t<16 & fig_t>-8, msymbol(diamond) xline(-0.0, lc(gs11)) c(l) lpattern(dash) lcolor(cranberry) lwidth(medthick)) ///
	(rcap fig_upper_2_pre fig_lower_2_pre fig_t  if fig_t<16 & fig_t>-8, lpattern(dash) lcolor(cranberry) c(l) m(i) lwidth(medthick) yline(0, lcolor(black))) ///
	(line fig_b_2 fig_t2 if fig_t<16 & fig_t>-8, msymbol(diamond) c(l) lpattern(solid) lcolor(navy) lwidth(medthick)) ///
	(rcap fig_upper_2 fig_lower_2 fig_t2  if fig_t<16 & fig_t>-8, lpattern(solid) lcolor(navy) c(l) m(i) lwidth(medthick)), /// 
	xlabel(-6 (3) 15) ///
	graphregion(fcolor(white)) ///
	xtitle("Days from Event") ///
	ytitle("CAR (Percent)") ///
	legend(size(small) pos(6) r(2) order(1 2 3 4) label(1 "Cumulative Abnormal Return with Leads")  label(2 "90% CI") ///
	label(3 "Cumulative Abnormal Return without Leads") label(4 "90% CI")) 
graph export "$output/Graphs/figureA25.pdf", replace
