// This file recreates Figure A4 Panel B in the appendix
// Author: eddie yu
// Date: 2023-10-16


/* Data sources:
Panel B: NETS establishment data
*/

*******************************************************
* Panels B (Panel A is same as Figure 2 - Panel A)
*******************************************************


use "$data/Replication Data/figureA4_data", clear 

rename fips95 county 
* version using employment links
maptile pr_emp_link , geo(county1990) stateoutline(linewidthstyle)  nquantiles(5) res(.5)
graph export "$output/Graphs/figureA4_B.png", replace
