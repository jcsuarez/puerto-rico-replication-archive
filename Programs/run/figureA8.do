***************************************************************
//Filename: figureA8.do
//Author: eddie yu
//Date: 2023-10-16
//Task: This script creates figures that specifies IPW estimators
//		for Pharma indicator, R&D indicator, and their interaction with
//		PPE-decile bins.
***************************************************************

clear all
set more off
snapshot erase _all

use "$data/Replication Data/figureA8_data", clear

*** PR SELECTION: IPW score
global specname1 "Pharma Indicator-by-PPE Decile"
global specname2 "Any R&D Indicator-by-PPE Decile"
global specname3 "Any Pharma-by-Any R&D-by-PPE Decile"

global c1 "navy"
global c2 "maroon"
global c3 "dkorange"



global spec1 "i.pharma##i.ppe95_binDFL if year == 1995"
global spec2 "i.any_rd##i.ppe95_binDFL if year == 1995" 
global spec3 "i.any_rd##i.pharma#i.ppe95_binDFL if year == 1995"

global rspec "[pw=w_att], cl(gvkey) a(gvkey ppe95_bins10##year)"

local cnt = 0
foreach spec in spec1 spec2 spec3  { 

	preserve 
	local cnt = `cnt' + 1
	qui logit PR $`spec'
	predict phat, pr
	winsor phat, p(.01) g(w_phat) 
	replace phat = w_phat
	xfill phat, i(gvkey)
	
	* ipw
	gen double w_att = cond(PR==1,1,phat/(1-phat))
	gen double w_ate = cond(PR==1,1/phat,1/(1-phat))
	
	reghdfe capex_base _IyeaX* PR $rspec
	estimates save `spec'_base, replace
		
		estimates use `spec'_base

		** figure years for the graph
		gen fig_year = 1990 in 1
		foreach i of numlist 1/23 {
			local j = `i' + 1989
			replace fig_year = `j' in `i'
		}
		
		** annual betas for the preperiod
		gen pr_beta = 0
		gen pr_beta_lb = 0
		gen pr_beta_ub = 0
		foreach i of numlist 1/5 {
			local j = `i' + 1989
			qui lincom _b[_IyeaXPR_`j'] , level(95)
			replace pr_beta = r(estimate) in `i'
			replace pr_beta_lb = r(lb) in `i'
			replace pr_beta_ub = r(ub) in `i'
		}
		* zero effect in 1995
		qui lincom  0
		replace pr_beta = r(estimate) in 5

		** annual betas for the postperiod
		foreach i of numlist 7/23 {
			local j = `i' + 1989
			qui lincom _b[_IyeaXPR_`j'] , level(95)
			replace pr_beta = r(estimate) in `i'
			replace pr_beta_lb = r(lb) in `i'
			replace pr_beta_ub = r(ub) in `i'
		}
		
		gen specname = "`spec'"
		keep if _n < 24
		keep fig_year pr_beta* specname
		tempfile `spec'_graph
		save ``spec'_graph', replace	
	restore

}
*** Graph Overlay ***
{
di `cnt'
preserve
	use `spec1_graph', replace
	local j = 0
	forvalues i = 2/`cnt' {
		local j = `j' + 0.15
		append using `spec`i'_graph'
		replace fig_year = fig_year+`j' if specname=="spec`i'" & fig_year!=1995
	}
		
	graph twoway ///
	 (scatter pr_beta fig_year if specn=="spec1", lco($c1) mco($c1) msize(small)) ///
	 (rcap pr_beta_lb pr_beta_ub fig_year if specn=="spec1", lco($c1) lpa(solid)) ///
	 (scatter pr_beta fig_year if specn=="spec2", lco($c2) mco($c2) msize(small)) ///
	 (rcap pr_beta_lb pr_beta_ub fig_year if specn=="spec2", lco($c2) lpa(solid)) ///
	 (scatter pr_beta fig_year if specn=="spec3", lco($c3) mco($c3) msize(small)) ///
	 (rcap pr_beta_lb pr_beta_ub fig_year if specn=="spec3", lco($c3) lpa(solid)) ///
	 , yline(0 , lcolor(gs12)) xline(1995, lcolor(black) lpattern(solid))  ///
		plotregion(fcolor(white) lcolor(white)) ///
		graphregion(fcolor(white) lcolor(white))  ///
		 xtitle("Year") xlab(1990(5)2012) bgcolor(white) ///
		legend(size(small) pos(6)  r(2) on order (1 3 5) ///
		label(1 "$specname1")  ///
		label(3 "$specname2")  ///
		label(5 "$specname3")) ///
		yti("Effect of S936 Exposure on Investment", margin(medium))
	

restore	
}

	graph export "$output/Graphs/figureA8.pdf", replace 
