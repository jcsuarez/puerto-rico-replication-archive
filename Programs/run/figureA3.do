// This file recreates Figure A3
// Author: eddie yu
// Date: 2023-10-16

/* Data sources:
QCEW public files
*/

clear all
set more off
snapshot erase _all 

*******************************************************
* log US employment on log capital absorbing industries
*******************************************************
use "$data/Replication Data/figureA3_data", clear

** First, levels for Puerto Rico
graph twoway (line annual_avg_emplvl year if fips_state == 72 & industry_cd == 0 & fips_county == 0) ///
	, ytitle("Employment in Puerto Rico") plotregion(fcolor(white) lcolor(white)) ///
	graphregion(fcolor(white) lcolor(white)) 
graph export "$output/Graphs/figureA3_A.pdf", replace	
	
graph twoway (line annual_avg_emplvl year if fips_state == 72 & industry_cd == 1 & fips_county == 0) ///
	(line annual_avg_emplvl year if fips_state == 72 & industry_cd == 2 & fips_county == 0) ///
	(line annual_avg_emplvl year if fips_state == 72 & industry_cd == 3 & fips_county == 0) ///
	(line annual_avg_emplvl year if fips_state == 72 & industry_cd == 4 & fips_county == 0) ///
	(line annual_avg_emplvl year if fips_state == 72 & industry_cd == 5 & fips_county == 0) ///
	, ytitle(Employment) legend(size(small) pos(6) r(3) label(1 Food Mfg) label(2 Textile) label(3 Apparel) label(4 Chemicals) label(5 Pharmaceuticals)) ///
	 plotregion(fcolor(white) lcolor(white)) ///
	graphregion(fcolor(white) lcolor(white)) 
graph export "$output/Graphs/figureA3_B1.pdf", replace 

graph twoway (line annual_avg_emplvl year if fips_state == 72 & industry_cd == 6 & fips_county == 0) ///
	(line annual_avg_emplvl year if fips_state == 72 & industry_cd == 7 & fips_county == 0) ///
	(line annual_avg_emplvl year if fips_state == 72 & industry_cd == 8 & fips_county == 0) ///
	(line annual_avg_emplvl year if fips_state == 72 & industry_cd == 9 & fips_county == 0) ///
	(line annual_avg_emplvl year if fips_state == 72 & industry_cd == 10 & fips_county == 0) ///
	, ytitle(Employment) legend(size(small) pos(6) r(3) label(1 Rubber and Plastic) label(2 Leather) label(3 Fabricated Metal) label(4 Machinery) label(5 Electrical Equip)) ///
	 plotregion(fcolor(white) lcolor(white)) ///
	graphregion(fcolor(white) lcolor(white)) 
graph export "$output/Graphs/figureA3_B2.pdf", replace 
