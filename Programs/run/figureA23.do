***************************************************************
//Filename: figureA23.do
// This file uses figure11_data, creates Figure A23, conspuma long differences adjusted and residualized
// Author: eddie yu
// Date: 2023-10-16
***************************************************************

/* Data sources:
NETS and ACS
*/

clear all
set more off
snapshot erase _all 

*******************************************************
* Starting with the data
*******************************************************
use "$data/Replication Data/figure11_data", clear

* residualizing
foreach var of varlist d*adjlwage dadjlrent dadjlvalue pr_link { 
	capture: drop res_`var'
	reg `var' i.year i.state_fips [aw=epop]
	predict res_`var', res
	sum `var' [aw=epop]
	}
sum pr_link [aw=epop]
replace res_pr_link = res_pr_link+r(mean)
	
** Graphs 
local res_pr_link "res_pr_link" 	
	qui sum `res_pr_link', d
	replace `res_pr_link' = `res_pr_link' - r(mean)
	
	xtile q_`res_pr_link'=`res_pr_link', nq(20)
	gen after = (year>1990)
	preserve 
	collapse (mean)res_dSadjlwage (mean)res_dUadjlwage (mean)res_dadjlwage (mean) res_dadjlrent (mean) `res_pr_link'  [aw=epop] , by(q_`res_pr_link' after) 
	rename res_dSadjlwage q_res_dSadjlwage
	rename res_dUadjlwage q_res_dUadjlwage
	rename `res_pr_link' q_m_`res_pr_link'
	rename res_dadjlwage q_res_dadjlwage
	rename res_dadjlrent q_res_dadjlrent
	tempfile qmeans
	save "`qmeans'"
	restore 
	merge m:1 q_`res_pr_link' after using "`qmeans'"
	drop _merge 

	local i = cond(`res_pr_link' == res_pr_link ,1 ,2)

*** Graphs for wages 
sum res_dadjlwage [aw=epop] if year == 1990,d
reg res_dadjlwage `res_pr_link' [aw=epop] if year ==1990, cl(state_fips)
local b: di %5.4f _b[`res_pr_link']
local se: di %5.4f _se[`res_pr_link']
mat table = r(table)
local p: di %5.4f table[4,1]

sum res_dadjlwage [aw=epop] if year == 1990,d
twoway  scatter res_dadjlwage `res_pr_link' [aw=epop] if year ==1990 & inrange(res_dadjlwage,`r(p1)',`r(p99)')   ///
		, yline(`r(mean)',lcolor(black) lpattern(dash)) mfcolor(gs15) ///
		|| lfitci  res_dadjlwage res_pr_link [aw=epop]  if year == 1990, level(90) lcolor(black) est(cl(state_fips)) ///
		|| scatter q_res_dadjlwage q_m_`res_pr_link' if year ==1990 & inrange(res_dadjlwage,`r(p1)',`r(p99)') ,  ///
		xlab(-3(2)3) ylab(-.15(.05).15) mcolor(black) ///
		graphregion(fcolor(white)) ///
		xtitle("IQR Adjusted Exposure to Section 936") ///
		ytitle("Residualized Wage Growth") ///
		legend(size(small) pos(6) r(2) order(1 4 3 2) label(1 "Data (weighted by population)")  label(2 "90% CI") label(3 "Linear Fit") ///
		label(4 "Binned Data by Ventiles")) note("The slope of the regression line is  `b' with standard error `se' and p-value `p'")
	
graph export "$output/Graphs/figureA23_D.pdf", replace		

sum res_dadjlwage [aw=epop] if year > 1990,d
reg res_dadjlwage `res_pr_link' [aw=epop] if year > 1990 , cl(state_fips)
local b: di %5.4f _b[`res_pr_link']
local se: di %5.4f _se[`res_pr_link']
mat table = r(table)
local p: di %5.4f table[4,1]
	
sum res_dadjlwage [aw=epop] if year > 1990,d
twoway  scatter res_dadjlwage `res_pr_link' [aw=epop] if year > 1990 & inrange(res_dadjlwage,`r(p1)',`r(p99)')   ///
		, yline(`r(mean)',lcolor(black) lpattern(dash)) mfcolor(gs15) ///
		|| lfitci  res_dadjlwage res_pr_link [aw=epop]  if year > 1990, level(90) lcolor(black)  est(cl(state_fips)) ///
		|| scatter q_res_dadjlwage q_m_`res_pr_link' if year > 1990 & inrange(res_dadjlwage,`r(p1)',`r(p99)') ,  ///
		xlab(-3(2)3)  ylab(-.15(.05).15) mcolor(black) ///
		graphregion(fcolor(white)) ///
		xtitle("IQR Adjusted Exposure to Section 936") ///
		ytitle("Residualized Wage Growth") ///
		legend(size(small) pos(6) r(2) order(1 4 3 2) label(1 "Data (weighted by population)")  label(2 "90% CI") label(3 "Linear Fit") ///
		label(4 "Binned Data by Ventiles"))  note("The slope of the regression line is  `b' with standard error `se' and p-value `p'")	
  
graph export "$output/Graphs/figureA23_A.pdf", replace		  

  
  
** Grpahs for LS wages
sum res_dUadjlwage [aw=epop] if year == 1990,d
reg res_dUadjlwage `res_pr_link' [aw=epop] if year ==1990, cl(state_fips)
local b: di %5.4f _b[`res_pr_link']
local se: di %5.4f _se[`res_pr_link']
mat table = r(table)
local p: di %5.4f table[4,1]
 
sum res_dUadjlwage [aw=epop] if year == 1990,d
twoway  scatter res_dUadjlwage `res_pr_link' [aw=epop] if year ==1990 & inrange(res_dUadjlwage,`r(p1)',`r(p99)')   ///
		, yline(`r(mean)',lcolor(black) lpattern(dash)) mfcolor(gs15) ///
		|| lfitci  res_dUadjlwage res_pr_link [aw=epop]  if year == 1990, level(90) lcolor(black) est(cl(state_fips)) ///
		|| scatter q_res_dUadjlwage q_m_`res_pr_link' if year ==1990 & inrange(res_dUadjlwage,`r(p1)',`r(p99)') ,  ///
		xlab(-3(2)3)  ylab(-.15(.05).15) mcolor(black) ///
		graphregion(fcolor(white)) ///
		xtitle("IQR Adjusted Exposure to Section 936") ///
		ytitle("Residualized Wage Growth") ///
		legend(size(small) pos(6) r(2) order(1 4 3 2) label(1 "Data (weighted by population)")  label(2 "90% CI") label(3 "Linear Fit") ///
		label(4 "Binned Data by Ventiles"))  note("The slope of the regression line is  `b' with standard error `se' and p-value `p'")
	
graph export "$output/Graphs/figureA23_E.pdf", replace		

sum res_dUadjlwage [aw=epop] if year > 1990,d
reg res_dUadjlwage `res_pr_link' [aw=epop] if year >1990, cl(state_fips)
local b: di %5.4f _b[`res_pr_link']
local se: di %5.4f _se[`res_pr_link']
mat table = r(table)
local p: di %5.4f table[4,1]
		
sum res_dUadjlwage [aw=epop] if year > 1990,d
twoway  scatter res_dUadjlwage `res_pr_link' [aw=epop] if year > 1990 & inrange(res_dUadjlwage,`r(p1)',`r(p99)')   ///
		, yline(`r(mean)',lcolor(black) lpattern(dash)) mfcolor(gs15) ///
		|| lfitci  res_dUadjlwage res_pr_link [aw=epop]  if year > 1990, level(90) lcolor(black) est(cl(state_fips)) ///
		|| scatter q_res_dUadjlwage q_m_`res_pr_link' if year > 1990 & inrange(res_dUadjlwage,`r(p1)',`r(p99)') ,  ///
		xlab(-3(2)3) ylab(-.15(.05).15) mcolor(black) ///
		graphregion(fcolor(white)) ///
		xtitle("IQR Adjusted Exposure to Section 936") ///
		ytitle("Residualized Wage Growth") ///
		legend(size(small) pos(6) r(2) order(1 4 3 2) label(1 "Data (weighted by population)")  label(2 "90% CI") label(3 "Linear Fit") ///
		label(4 "Binned Data by Ventiles")) 	 note("The slope of the regression line is  `b' with standard error `se' and p-value `p'")
  
graph export "$output/Graphs/figureA23_B.pdf", replace		  
  
    
  
** Graphs for rents 
sum res_dadjlrent [aw=epop] if year == 1990,d
reg res_dadjlrent `res_pr_link' [aw=epop] if year ==1990, cl(state_fips)
local b: di %5.4f _b[`res_pr_link']
local se: di %5.4f _se[`res_pr_link']
mat table = r(table)
local p: di %5.4f table[4,1]

sum res_dadjlrent [aw=epop] if year == 1990,d
twoway  scatter res_dadjlrent `res_pr_link' [aw=epop] if year ==1990 & inrange(res_dadjlrent,`r(p1)',`r(p99)')   ///
		, yline(`r(mean)',lcolor(black) lpattern(dash)) mfcolor(gs15) ///
		|| lfitci  res_dadjlrent res_pr_link [aw=epop]  if year == 1990, level(90) lcolor(black) est(cl(state_fips)) ///
		|| scatter q_res_dadjlrent q_m_`res_pr_link' if year ==1990 & inrange(res_dadjlrent,`r(p1)',`r(p99)') ,  ///
		xlab(-3(2)3)  ylab(-.3(.1).3) mcolor(black) ///
		graphregion(fcolor(white)) ///
		xtitle("IQR Adjusted Exposure to Section 936") ///
		ytitle("Residualized Rent Growth") ///
		legend(size(small) pos(6) r(2) order(1 4 3 2) label(1 "Data (weighted by population)")  label(2 "90% CI") label(3 "Linear Fit") ///
		label(4 "Binned Data by Ventiles"))  note("The slope of the regression line is  `b' with standard error `se' and p-value `p'")
	
graph export "$output/Graphs/figureA23_F.pdf", replace		
		
sum res_dadjlrent [aw=epop] if year > 1990,d
reg res_dadjlrent `res_pr_link' [aw=epop] if year >1990, cl(state_fips)
local b: di %5.4f _b[`res_pr_link']
local se: di %5.4f _se[`res_pr_link']
mat table = r(table)
local p: di %5.4f table[4,1]

sum res_dadjlrent [aw=epop] if year > 1990,d
twoway  scatter res_dadjlrent `res_pr_link' [aw=epop] if year > 1990 & inrange(res_dadjlrent,`r(p1)',`r(p99)')   ///
		, yline(`r(mean)',lcolor(black) lpattern(dash)) mfcolor(gs15) ///
		|| lfitci  res_dadjlrent res_pr_link [aw=epop]  if year > 1990, level(90) lcolor(black) est(cl(state_fips)) ///
		|| scatter q_res_dadjlrent q_m_`res_pr_link' if year > 1990 & inrange(res_dadjlrent,`r(p1)',`r(p99)') ,  ///
		xlab(-3(2)3) ylab(-.3(.1).3) mcolor(black) ///
		graphregion(fcolor(white)) ///
		xtitle("IQR Adjusted Exposure to Section 936") ///
		ytitle("Residualized Rent Growth") ///
		legend(size(small) pos(6) r(2) order(1 4 3 2) label(1 "Data (weighted by population)")  label(2 "90% CI") label(3 "Linear Fit") ///
		label(4 "Binned Data by Ventiles"))  note("The slope of the regression line is  `b' with standard error `se' and p-value `p'")	
  
graph export "$output/Graphs/figureA23_C.pdf", replace	
