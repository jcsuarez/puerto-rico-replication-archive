***************************************************************
//Filename: tableA1.do
//Author: eddie yu
//Date: 2023-09-20
//Task: Given tableA1_data.dta, creates Table A1
***************************************************************

**************************************************
// Original PR_v43 version: 
// This file creates tableA2.do
// Author: Dan Garrett
// Date: 4-20-2020
**************************************************

/* Data sources:
COMPUSTAT and NETS
*/

clear all
set more off
snapshot erase _all 

*******************************************************
* Starting with the data
*******************************************************
use "$data/Replication Data/interim/compu_NETS_ETR_sizecutoff.dta", replace

keep if year == 1995

* collecting variables
local General "etr txfed MNE nol logAT"
local Sales "rd ads"
local Assets "ppe lev btd intang"
local Physical "capex"

local vars "`General' `Sales' `Physical' `Assets'"

*PR firm characteristics
estpost tabstat `vars' if PR == 1, statistics(mean sd count ) ///
	columns(statistics)
est store tempA

* non-PR firm characteristics
estpost tabstat `vars' if PR == 0, statistics(mean sd count ) ///
	columns(statistics)
est store tempC

esttab tempA tempC using "$output/Tables/tableA1.tex", replace ///
	refcat(etr "\emph{General Characteristics}" rd "\emph{Spending}" ppe "\emph{Capital and Financing}",) /// 
	cells("mean(fmt(3)) sd(fmt(3)) count(fmt(0))" ) nonum label nomtitle tex ///
	collabels("Mean" "SD" "Count" ) noobs mgroups("S936 Firms" "All Control Firms", pattern(1 1 1) ///
prefix(\multicolumn{@span}{c}{) suffix(})   ///
span erepeat(\cmidrule(lr){@span}))
