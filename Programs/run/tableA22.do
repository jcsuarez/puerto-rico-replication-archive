***************************************************************
//Filename: tableA22.do
//Author: eddie yu
//Date: 2023-09-20
//Task: Given figure8_data_B.dta, creates Table A22
***************************************************************

/* Data sources:
QCEW, NETS
*/

clear all
set more off
snapshot erase _all 

*******************************************************
* starting with data from figure 8
*******************************************************
use "$data/Replication Data/figure8_data_B", replace

*making interaction terms
xi i.year|pharm_annual_avg_emplvl i.year|pr_link_ind , noomit
drop _IyeaXp*1995

eststo emp_1:  reghdfe emp_growth _IyeaXph* _IyeaXpr*  [aw=wgt]  , a( i.year#i.industr ) cl(fips_state indust )
	estadd local hasiy "Yes"
    estadd local hascty ""

eststo emp_2:  reghdfe emp_growth _IyeaXpr* _IyeaXph* [aw=wgt]  , a(i.year#i.industr) cl(fips_state indust )
	estadd local hasiy "Yes"
	estadd local hascty "Yes"

eststo emp_4:  reghdfe emp_growth _IyeaXph* _IyeaXpr* [aw=wgt]  , a( i.pid2 i.year#i.indust ) cl(fips_state indust )
	estadd local hasiy "Yes"
	estadd local hasicty "Yes"

eststo emp_5:  reghdfe emp_growth _IyeaXph* _IyeaXpr* [aw=wgt_w] , a( i.pid i.year#i.indust ) cl(fips_state indust )
	estadd local hasiy "Yes"
	estadd local hascty "Yes"
	estadd local hasww "Yes"	

eststo emp_6:  reghdfe emp_growth _IyeaXph* _IyeaXpr* if base_emp > 1000 [aw=wgt] , a( i.pid i.year#i.indust ) cl(fips_state indust )
	estadd local hasiy "Yes"
	estadd local hascty "Yes"
	estadd local hasds "Yes"

esttab emp* using "$output/Tables/tableA22.tex", drop(*) stats() ///
b(3) se par label  ///
noobs nogap nomtitle tex replace nocons ///
postfoot("\multicolumn{1}{l}{\textbf{Exposure to Pharmaceuticals}}") posthead("") nonum


esttab emp*   using "$output/Tables/tableA22.tex",  preh("") ///
 b(3) se par mlab(none) coll(none) s() nogap noobs keep(_IyeaXph*) /// 
coeflabel(_IyeaXpha_1990 "\hspace{1em}X 1990 " /// 
_IyeaXpha_1991 "\hspace{1em}X 1991 " /// 
_IyeaXpha_1992 "\hspace{1em}X 1992 " /// 
_IyeaXpha_1993 "\hspace{1em}X 1993 " /// 
_IyeaXpha_1994 "\hspace{1em}X 1994 " /// 
_IyeaXpha_1996 "\hline \hspace{1em}X 1996 " /// 
_IyeaXpha_1997 "\hspace{1em}X 1997 " /// 
_IyeaXpha_1998 "\hspace{1em}X 1998 " /// 
_IyeaXpha_1999 "\hspace{1em}X 1999 " /// 
_IyeaXpha_2000 "\hspace{1em}X 2000 " /// 
_IyeaXpha_2001 "\hspace{1em}X 2001 " /// 
_IyeaXpha_2002 "\hspace{1em}X 2002 " /// 
_IyeaXpha_2003 "\hspace{1em}X 2003 " /// 
_IyeaXpha_2004 "\hspace{1em}X 2004 " /// 
_IyeaXpha_2005 "\hspace{1em}X 2005 " /// 
_IyeaXpha_2006 "\hspace{1em}X 2006 " /// 
_IyeaXpha_2007 "\hspace{1em}X 2007 " /// 
_IyeaXpha_2008 "\hspace{1em}X 2008 " /// 
_IyeaXpha_2009 "\hspace{1em}X 2009 " /// 
_IyeaXpha_2010 "\hspace{1em}X 2010 " /// 
_IyeaXpha_2011 "\hspace{1em}X 2011 " /// 
_IyeaXpha_2012 "\hspace{1em}X 2012 " )   scalars(  "hasiy Industry-by-Year Fixed Effects" "hascty County Fixed Effects" ///
"hasicty Industry-by-County Fixed Effects" ///
"hasww Winsorized Weights" "hasds Drops Small County-Industries ($<$1000)" ) label /// 
 prefoot("\hline") ///
postfoot( ///
"  \hline\hline" ///
"\end{tabular} }" ) append sty(tex)
