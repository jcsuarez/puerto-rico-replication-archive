*******************************************************************************
//Filename: figureA18.do
//Author: eddie yu
//Date: 2023-09-20
//Task: Given county crosswalk data (cw_cty_czone.dta),
//		pr_link_est_county_d.dta, pr_link_est_countyXindustry_d.dta,
//		this script produces fips (county) - pr_link / pr_link_ind variable
*******************************************************************************

/* Data sources:
NETS and QCEW
*/

clear all
set more off
snapshot erase _all 

*******************************************************
* Starting with the data for panel A
*******************************************************
use "$data/Replication Data/figureA18_data", clear


snapshot save

global c1 "navy"
global c2 "dkorange"

global specname1 "Exposure to All Industries"
global specname2 "Exposure to S936 Industries"

local y "emp_growth"

estimates clear
forval i = 1/2 {
	snapshot restore 1
	if `i' == 1 {
		xi i.year|pr_link_all , noomit
		drop _IyeaXp*1995
	}
	if `i' == 2  {
		xi i.year|pr_link_ind , noomit
		drop _IyeaXp*1995
	}

	
	eststo emp`i': reghdfe `y'  _IyeaX* [aw=wgt], a(i.pid i.year#i.industr ) cl(fips_state indust ) nocons
		estadd local hasiy "Yes"
		estadd local hascty "Yes"
	estimates save emp`i'_base, replace

	preserve
		estimates use emp`i'_base

		** figure years for the graph
		gen fig_year = 1989 in 1
		foreach year of numlist 1/23 {
			local j = `year' + 1989
			replace fig_year = `j' in `year'
		}
		
		** annual betas for the preperiod
		gen pr_beta = 0
		gen pr_beta_lb = 0
		gen pr_beta_ub = 0
		foreach year of numlist 1/5 {
			local j = `year' + 1989
			qui lincom _b[_IyeaXpr__`j'] , level(95)
			replace pr_beta = r(estimate) in `year'
			replace pr_beta_lb = r(lb) in `year'
			replace pr_beta_ub = r(ub) in `year'
		}
		* zero effect in 1995
		qui lincom  0
		replace pr_beta = r(estimate) in 5

		** annual betas for the postperiod
		foreach year of numlist 7/23 {
			local j = `year' + 1989
			qui lincom _b[_IyeaXpr__`j'] , level(95)
			replace pr_beta = r(estimate) in `year'
			replace pr_beta_lb = r(lb) in `year'
			replace pr_beta_ub = r(ub) in `year'
		}
		
		gen specname = "spec`i'"
		keep if _n < 23
		keep fig_year pr_beta* specname
		tempfile emp`i'_graph
		save `emp`i'_graph', replace	
	restore 
}

*** Graph Overlay ***	
{
preserve
	use `emp1_graph', replace
	local j = 0
	forvalues i = 2/2 {
		local j = `j' + 0.1
		append using `emp`i'_graph'
		replace fig_year = fig_year+`j' if specname=="spec`i'" & fig_year!=1995
	}
		
	graph twoway ///
	 (scatter pr_beta fig_year if specn=="spec1", lco($c1) mco($c1) msize(small)) ///
	 (rcap pr_beta_lb pr_beta_ub fig_year if specn=="spec1", lco($c1) lpa(solid)) ///
	 (scatter pr_beta fig_year if specn=="spec2", lco($c2) mco($c2) msize(small)) ///
	 (rcap pr_beta_lb pr_beta_ub fig_year if specn=="spec2", lco($c2) lpa(solid)) ///
		, yline(0 , lcolor(black)) xline(1995, lcolor(black) lpattern(solid))  ///
		plotregion(fcolor(white) lcolor(white)) ///
		graphregion(fcolor(white) lcolor(white))  ///
		 xtitle("Year") xlab(1990(5)2012) bgcolor(white) ///
		legend(size(small) pos(6)  r(1) on order (1 3) ///
		label(1 "$specname1")  ///
		label(3 "$specname2") )  ///
		yti("Effect of S936 on Employment Growth", margin(medium))
restore	
}


graph export "$output/Graphs/figureA18.pdf", replace 
