***************************************************************
//Filename: tableA10.do
//Author: eddie yu
//Date: 2023-09-20
//Task: Given tableA10_data.dta & figure7_data_*.dta, creates Table A10
***************************************************************

/* Data sources:
QCEW and NETS
starts in combined_appendix_tables
*/

clear all
set more off
snapshot erase _all 
estimates clear

*******************************************************
* employment instrument
*******************************************************
use "$data/Replication Data/figure7_data_empinst", replace

* making interaction variable
xi i.year|pr_link_ind_e i.year|pr_link_ind, noomit
drop _IyeaXp*1995

* county regressions
eststo emp_1:  ivreghdfe emp_growth (_IyeaXpr__* = _IyeaXpr_a*) $pra_control_inc [aw=wgt]  , a( i.year#i.industr ) cl(fips_state indust )
	estadd local hasiy "Yes"
    estadd local hascty ""
	
*******************************************************
* MD
*******************************************************
use "$data/Replication Data/figure7_data_MD", replace

* making interaction variable
xi i.year|pr_link_ind, noomit
drop _IyeaXp*1995

* county regressions
eststo emp_2:  reghdfe emp_growth _IyeaX* [aw=wgt]  , a( i.year#i.industr ) cl(fips_state indust )
	estadd local hasiy "Yes"
    estadd local hascty ""
	
*******************************************************
* FD
*******************************************************
use "$data/Replication Data/figure7_data_FD", replace

* making interaction variable
xi i.year|pr_link_ind, noomit
drop _IyeaXp*1995

* county regressions
eststo emp_3:  reghdfe emp_growth _IyeaX* [aw=wgt]  , a( i.year#i.industr ) cl(fips_state indust )
	estadd local hasiy "Yes"
    estadd local hascty ""

***************************************************
* Compustat firms
***************************************************
use "$data/Replication Data/figure7_data_compu", replace

* making interaction variable
xi i.year|pr_link_ind , noomit
drop _IyeaXp*1995

eststo emp_4:  reghdfe emp_growth _IyeaX* [aw=wgt]  , a( i.year#i.industr ) cl(fips_state indust )
	estadd local hasiy "Yes"
    estadd local hascty ""

***************************************************
* per capita
***************************************************
use "$data/Replication Data/figure7_data_PC", replace

* making interaction variable
xi i.year|pr_link_ind , noomit
drop _IyeaXp*1995

eststo emp_5:  reghdfe emp_growth _IyeaX* [aw=wgt]  , a( i.year) cl(fips_state indust )
	estadd local hasiy "Yes"
    estadd local hascty ""

***************************************************
* manufacturing employment per capita
***************************************************
use "$data/Replication Data/tableA10_data", replace

* making interaction variable
xi i.year|pr_link_ind , noomit
drop _IyeaXp*1995

eststo emp_6:  reghdfe ADH_growth _IyeaX* [aw=wgt]  , a( i.year ) cl(statefips )
	estadd local hasiy "Yes"
    estadd local hascty ""
	
***************************************************
* assembling the table
***************************************************
// annual affects table for the first specifications
esttab emp_? using "$output/Tables/tableA10.tex", drop(*) stats() ///
b(3) se par label  ///
noobs nogap nomtitle tex replace nocons ///
postfoot("\multicolumn{1}{l}{\textbf{Exposure to Section 936}}") nonum posthead("")


esttab emp_? using "$output/Tables/tableA10.tex",  preh("") /// 
b(3) se par mlab(none) coll(none) s() noobs nogap keep(_IyeaXpr__*) /// 
coeflabel(_IyeaXpr__1990 "\hspace{1em}X 1990 " /// 
_IyeaXpr__1991 "\hspace{1em}X 1991 " /// 
_IyeaXpr__1992 "\hspace{1em}X 1992 " /// 
_IyeaXpr__1993 "\hspace{1em}X 1993 " /// 
_IyeaXpr__1994 "\hspace{1em}X 1994 " /// 
_IyeaXpr__1996 "\hline \hspace{1em}X 1996 " /// 
_IyeaXpr__1997 "\hspace{1em}X 1997 " /// 
_IyeaXpr__1998 "\hspace{1em}X 1998 " /// 
_IyeaXpr__1999 "\hspace{1em}X 1999 " /// 
_IyeaXpr__2000 "\hspace{1em}X 2000 " /// 
_IyeaXpr__2001 "\hspace{1em}X 2001 " /// 
_IyeaXpr__2002 "\hspace{1em}X 2002 " /// 
_IyeaXpr__2003 "\hspace{1em}X 2003 " /// 
_IyeaXpr__2004 "\hspace{1em}X 2004 " /// 
_IyeaXpr__2005 "\hspace{1em}X 2005 " /// 
_IyeaXpr__2006 "\hspace{1em}X 2006 " /// 
_IyeaXpr__2007 "\hspace{1em}X 2007 " /// 
_IyeaXpr__2008 "\hspace{1em}X 2008 " /// 
_IyeaXpr__2009 "\hspace{1em}X 2009 " /// 
_IyeaXpr__2010 "\hspace{1em}X 2010 " /// 
_IyeaXpr__2011 "\hspace{1em}X 2011 " /// 
_IyeaXpr__2012 "\hspace{1em}X 2012 " )  label /// 
 prefoot("\hline") ///
postfoot( ///
"Year Fixed Effects & Y & Y & Y & Y & Y & Y \\" ///
"Year by Industry Fixed Effects & Y & Y & Y & Y &  &  \\" ///
"Employment Instrument &   Y &   &   &  & &   \\" ///
"Large Retailers Dropped from Links &    & Y &   &  & &   \\" ///
"No US Headquarter Dropped from Links &    &   & Y &  & &  \\" ///
"Only Compustat Sample Links &  &   &   & Y & & \\"  ///
"Employment Growth Per Capita  &   &   &   & & Y & \\"  ///
"Manufacturing Employment Per Capita  &   &   &  &  & & Y \\"  ///
"  \hline\hline" ///
"\end{tabular} }" ) append sty(tex)

