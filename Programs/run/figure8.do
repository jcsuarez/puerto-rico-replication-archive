// This file recreates Figure 8, placebo checks
// Author: eddie yu
// Date: 2023-10-16

/* Data sources:
NETS and QCEW
*/

clear all
set more off
snapshot erase _all 

// Define Command for ES 
capture program drop ES_graph
program define ES_graph
syntax,  level(real) [  yti(string) note(string) outname(string) ylab(string) ]

qui { 
mat b = e(b)
mat V = e(V) 
mat es_b = 0 
mat es_ci_l = 0 
mat es_ci_h = 0 
mat time = 0 

// for 95% CI 
if `level' == 99 { 
	local z= 2.6  
}
if `level' == 95 { 
	local z= 1.96  
}
// for 90% CI 
if `level' == 90 { 
	local z= 1.645 
} 
	
forval i = 1/23 { 
	mat time = (time,`i'-6+1995)
	mat es_b = (es_b, b[1,`i'])
	mat es_ci_l = (es_ci_l, b[1,`i']-`z'*sqrt(V[`i',`i'])   )
	mat es_ci_h = (es_ci_h, b[1,`i']+`z'*sqrt(V[`i',`i'])   )		
	}
mat es_b =  (es_b[1,2..6],0,es_b[1,7...])'
mat es_ci_l = (es_ci_l[1,2..6],0,es_ci_l[1,7...])'  
mat es_ci_h = (es_ci_h[1,2..6],0,es_ci_h[1,7...])' 
mat time = time[1,2...]' 

capture: drop es_b es_ci* time 
svmat es_b 
svmat time
svmat es_ci_l
svmat es_ci_h 
}

twoway (scatter es_b time, yline(0 , lcolor(black)) xline(1995, lcolor(black) lpattern(dash))) ///
    (rcap es_ci_h es_ci_l time,  lcolor(navy) lpattern(dash)) ///
	, plotregion(fcolor(white) lcolor(white)) ///
	graphregion(fcolor(white) lcolor(white)) ///
	xtitle("Year") xlab(1990(5)2012) ///
	legend(size(small) pos(6) r(1) label(1 "Estimate") label(2 "`level'% CI")) ///
	ytitle("`yti'") ///
	note("`note'") ylab("`ylab'")
graph export "`outname'.pdf", replace	

end 

*******************************************************
* Starting with the data for panel A
*******************************************************
use "$data/Replication Data/figure8_data_A", clear
 
// Graphs
xi i.year|pr_link_ind i.year|pr_link_ind_r , noomit
drop _IyeaXp*1995

reghdfe emp_growth _IyeaX* [aw=wgt]  , a( i.year#i.industr ) cl(fips_state indust )

ES_graph , level(95) yti("Effect S936 Exposure (IQR)") ///
note("Notes: (Emp{sub:ict}-Emp{sub:ic1995})/Emp{sub:ic1995}={&alpha}{sub:c}+{&gamma}{sub:it}+{&beta}{sub:t}S936 Exposure{sub:c}+{&epsilon}{sub:ict}. SE's clustered by State and Industry.") ///
outname("$output/Graphs/figure8_A") ylab("-.12(.02).04")

*******************************************************
* Starting with the data for panel B
*******************************************************
use "$data/Replication Data/figure8_data_B", clear

xi i.year|pharm_annual_avg_emplvl i.year|pr_link_ind, noomit
drop _IyeaXp*1995

reghdfe emp_growth _IyeaXph* _IyeaXpr*  [aw=wgt]  , a( i.year#i.industr ) cl(fips_state indust )
	
ES_graph , level(95) yti("Effect S936 Exposure (IQR)") ///
note("Notes: (Emp{sub:ict}-Emp{sub:ic1995})/Emp{sub:ic1995}={&alpha}{sub:c}+{&gamma}{sub:it}+{&beta}{sub:t}S936 Exposure{sub:c}+{&epsilon}{sub:ict}. SE's clustered by State and Industry.") ///
outname("$output/Graphs/figure8_B") ylab("-.12(.02).04")	

