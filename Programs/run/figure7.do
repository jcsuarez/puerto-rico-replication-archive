// This file recreates Figure 7
// Author: eddie yu
// Date: 2023-10-16

/* Data sources:
NETS and QCEW
*/

clear all
set more off
snapshot erase _all 

// Define Command for ES data 
capture program drop ES_graph_data
program define ES_graph_data
syntax,  level(real) yti(string) tshifter(real)


qui { 
mat b = e(b)
mat V = e(V) 
mat es_b = 0 
mat es_ci_l = 0 
mat es_ci_h = 0 
mat time = 0 

// for 95% CI 
if `level' == 99 { 
	local z= 2.6  
}
if `level' == 95 { 
	local z= 1.96  
}
// for 90% CI 
if `level' == 90 { 
	local z= 1.645 
} 


forval i = 1/23 { 
	mat time = (time,`i'-6+1995 + `tshifter')
	mat es_b = (es_b, b[1,`i'])
	mat es_ci_l = (es_ci_l, b[1,`i']-`z'*sqrt(V[`i',`i'])   )
	mat es_ci_h = (es_ci_h, b[1,`i']+`z'*sqrt(V[`i',`i'])   )		
	}
mat es_b =  (es_b[1,2..6],0,es_b[1,7...])'
mat es_ci_l = (es_ci_l[1,2..6],0,es_ci_l[1,7...])'  
mat es_ci_h = (es_ci_h[1,2..6],0,es_ci_h[1,7...])' 
mat time = time[1,2...]' 

capture: drop es_b es_ci_l es_ci_h time 
svmat es_b 
rename es_b `yti'_es_b
svmat time
rename time `yti'_time
svmat es_ci_l
rename es_ci_l `yti'_es_ci_l
svmat es_ci_h 
rename es_ci_h `yti'_es_ci_h
}

end  

*******************************************************
* importing data, running each regression, saving reg output
*******************************************************
estimates clear

* baseline
use "$data/Replication Data/figure7_data_baseline", clear

xi i.year|pr_link_ind, noomit
drop _IyeaXp*1995

eststo emp_2_baseline:  reghdfe emp_growth _IyeaX* [aw=wgt]  , a(i.pid i.year#i.industr ) cl(fips_state indust ) nocons

*conspuma
use "$data/Replication Data/figure7_data_conspuma", clear

xi i.year|pr_link_ind, noomit
drop _IyeaXp*1995

eststo emp_2_cons:  reghdfe emp_growth _IyeaX* [aw=wgt]  , a( i.year#i.industr ) cl(fips_state indust )

*czone
use "$data/Replication Data/figure7_data_czone", clear

xi i.year|pr_link_ind, noomit
drop _IyeaXp*1995

eststo emp_2_czone:  reghdfe emp_growth _IyeaX* [aw=wgt] , a( i.year#i.industr ) cl(pid indust )

*PC (per capita)
use "$data/Replication Data/figure7_data_PC", clear

xi i.year|pr_link_ind, noomit
drop _IyeaXp*1995

eststo emp_2_PC:  reghdfe emp_growth _IyeaX* [aw=wgt]  , a(i.pid i.industry i.year) cl(fips_state industry)

* employment instrument
use "$data/Replication Data/figure7_data_empinst", clear

xi i.year|pr_link_ind_e i.year|pr_link_ind, noomit
drop _IyeaXp*1995

eststo emp_2_emp: ivreghdfe emp_growth (_IyeaXpr__* = _IyeaXpr_a*) [aw=wgt]  , a(i.pid i.year#i.industr ) cl(fips_state indust )

* MD
use "$data/Replication Data/figure7_data_MD", clear

xi i.year|pr_link_ind, noomit
drop _IyeaXp*1995

eststo emp_2_MD:  reghdfe emp_growth _IyeaX* [aw=wgt]  , a(i.pid i.year#i.industr ) cl(fips_state indust ) nocons

* FD
use "$data/Replication Data/figure7_data_FD", clear

xi i.year|pr_link_ind, noomit
drop _IyeaXp*1995

eststo emp_2_FD:  reghdfe emp_growth _IyeaX* [aw=wgt]  , a(i.pid i.year#i.industr ) cl(fips_state indust ) nocons

* compustat
use "$data/Replication Data/figure7_data_compu", clear

xi i.year|pr_link_ind, noomit
drop _IyeaXp*1995

eststo emp_2_compu:  reghdfe emp_growth _IyeaX* [aw=wgt]  , a(i.pid i.year#i.industr ) cl(fips_state indust ) nocons

* State FE and trends
use "$data/Replication Data/figure7_data_syFE", clear

xi i.year|pr_link_ind, noomit
drop _IyeaXp*1995

eststo emp_2_stfe: reghdfe emp_growth _IyeaX* c.year#i.fips_sta  [aw=wgt]  , a( i.year#i.industry i.pid2 i.fips_state) cl(fips_state indust )

* large firms
use "$data/Replication Data/figure7_data_size", clear

xi i.year|pr_link_s4_ind, noomit
drop _IyeaXp*1995

eststo emp_2_size:  reghdfe emp_growth _IyeaX* [aw=wgt]  , a(i.pid i.year#i.industr ) cl(fips_state indust ) nocons

* 93f
use "$data/Replication Data/figure7_data_93f", clear

xi i.year|pr_link_ind, noomit
drop _IyeaXp*1995

eststo emp_2_93f:  reghdfe emp_growth _IyeaX* [aw=wgt]  , a(i.pid i.year#i.industr ) cl(fips_state indust ) nocons

* 93-95f
use "$data/Replication Data/figure7_data_9395f", clear

xi i.year|pr_link_ind, noomit
drop _IyeaXp*1995

eststo emp_2_9395f:  reghdfe emp_growth _IyeaX* [aw=wgt]  , a(i.pid i.year#i.industr ) cl(fips_state indust ) nocons



*******************************************************
* creating the graphs from reg outputs
*******************************************************
clear
* bringing in the reg outputs and using ES_graph_data
estimates restore emp_2_baseline
ES_graph_data , level(95) yti("spec2")	tshifter(0)
estimates restore emp_2_cons
ES_graph_data , level(95) yti("cons_spec1")	tshifter(0)
estimates restore emp_2_czone
ES_graph_data , level(95) yti("czone_spec1")	tshifter(0)
estimates restore emp_2_PC
ES_graph_data , level(95) yti("spec2_PC")	tshifter(0)
estimates restore emp_2_emp
ES_graph_data , level(95) yti("spec2_inst")	tshifter(0)
estimates restore emp_2_MD
ES_graph_data , level(95) yti("spec2_md")	tshifter(0)
estimates restore emp_2_FD
ES_graph_data , level(95) yti("spec2_fd")	tshifter(0)
estimates restore emp_2_compu
ES_graph_data , level(95) yti("spec2_compu")	tshifter(0)
estimates restore emp_2_stfe
ES_graph_data , level(95) yti("spec1")	tshifter(0)
estimates restore emp_2_size
ES_graph_data , level(95) yti("large_spec5")	tshifter(0)
estimates restore emp_2_93f
ES_graph_data , level(95) yti("y93_spec1")	tshifter(0)
estimates restore emp_2_9395f
ES_graph_data , level(95) yti("y95_spec1")	tshifter(0)

* generating the correct time things
keep if spec1_time != .
keep *_time *_es_b *es_ci*
gen year = spec1_time
capture: drop *_time
gen spec1_time = year - .2
gen spec2_time = year - .066
gen spec4_time = year + .066
gen spec5_time = year + .2

// differently combined versions

************************************************
* graph with baseline, different geographies, and per capita employment growth
************************************************
 twoway (line cons_spec1_es_b spec1_time			, lcolor(navy) yline(0 , lcolor(black)) xline(1995, lcolor(black) lpattern(solid)))  ///
		(rcap cons_spec1_es_ci_l cons_spec1_es_ci_h spec1_time	, lcolor(navy)  lpattern(solid))  ///
		(line czone_spec1_es_b spec2_time							, lcolor(orange)  lpattern(dash))      ///
		(rcap czone_spec1_es_ci_l czone_spec1_es_ci_h spec2_time	, lcolor(orange)  lpattern(dash))   /// 
		(line spec2_PC_es_b spec5_time							, lcolor(red)  lpattern(shortdash))      ///
		(rcap spec2_PC_es_ci_l spec2_PC_es_ci_h spec5_time	, lcolor(red)  lpattern(shortdash))   /// 
		(line spec2_es_b spec4_time							, lcolor(black)  lpattern(longdash))      ///
		(rcap spec2_es_ci_l spec2_es_ci_h spec4_time	, lcolor(black)  lpattern(longdash))   /// 
	, plotregion(fcolor(white) lcolor(white)) ///
	graphregion(fcolor(white) lcolor(white)) ///
	xtitle("Year") ytitle("Percent Employment Growth" , height(3) margin(medsmall))  ///
	xlab(1990(5)2012) bgcolor(white) ///
	legend(size(small) pos(6) r(2) label(1 "Consupuma") ///
	label(3 "Commuting Zone") label(5 "Per Capita") label(7 "Benchmark") order(7 1 3 5)) 

graph export "$output/Graphs/figure7_A.pdf", replace	


************************************************
* graph with manual dropping, uncertain HQ dropping, non-compu dropping, and employment instruments
************************************************
 twoway (line spec2_md_es_b spec1_time						, lcolor(navy) yline(0 , lcolor(black)) xline(1995, lcolor(black) lpattern(solid)))  ///
		(rcap spec2_md_es_ci_l spec2_md_es_ci_h spec1_time	, lcolor(navy)  lpattern(solid))  ///
		(line spec2_fd_es_b spec2_time						, lcolor(orange)  lpattern(dash))      ///
		(rcap spec2_fd_es_ci_l spec2_fd_es_ci_h spec2_time	, lcolor(orange)  lpattern(dash))   /// 
		(line spec2_compu_es_b spec5_time							, lcolor(red)  lpattern(shortdash))      ///
		(rcap spec2_compu_es_ci_l spec2_compu_es_ci_h spec5_time	, lcolor(red)  lpattern(shortdash))   /// 
		(line spec2_inst_es_b spec4_time							, lcolor(black)  lpattern(longdash))      ///
		(rcap spec2_inst_es_ci_l spec2_inst_es_ci_h spec4_time	, lcolor(black)  lpattern(longdash))   /// 
	, plotregion(fcolor(white) lcolor(white)) ///
	graphregion(fcolor(white) lcolor(white)) ///
	xtitle("Year") ytitle("Percent Employment Growth" , height(3) margin(medsmall))  ///
	xlab(1990(5)2012) bgcolor(white) ///
	legend(size(small) pos(6) r(2) label(1 "Large Retailers Dropped") ///
	label(3 "Uncertain HQ Dropped") label(5 "Non-Compustat Dropped") label(7 "Instrument for Employment Link") order(7 1 3 5)) 

graph export "$output/Graphs/figure7_B.pdf", replace


************************************************
* graph with 1993, large firms, state-by-year FE, and 1
************************************************
capture: drop _merge
merge 1:1 year using "$qcewdata/ES_graph_sy_fe.dta"

 twoway (line spec1_es_b spec1_time						, lcolor(navy) yline(0 , lcolor(black)) xline(1995, lcolor(black) lpattern(solid)))  ///
		(rcap spec1_es_ci_l spec1_es_ci_h spec1_time	, lcolor(navy)  lpattern(solid))  ///
		(line large_spec5_es_b spec2_time						, lcolor(orange)  lpattern(dash))      ///
		(rcap large_spec5_es_ci_l large_spec5_es_ci_h spec2_time	, lcolor(orange)  lpattern(dash))   /// 
		(line y95_spec1_es_b spec5_time							, lcolor(red)  lpattern(shortdash))      ///
		(rcap y95_spec1_es_ci_l y95_spec1_es_ci_h spec5_time	, lcolor(red)  lpattern(shortdash))   /// 
		(line y93_spec1_es_b spec4_time							, lcolor(black)  lpattern(longdash))      ///
		(rcap y93_spec1_es_ci_l y93_spec1_es_ci_h spec4_time	, lcolor(black)  lpattern(longdash))   /// 
	, plotregion(fcolor(white) lcolor(white)) ///
	graphregion(fcolor(white) lcolor(white)) ///
	xtitle("Year") ytitle("Percent Employment Growth" , height(3) margin(medsmall))  ///
	xlab(1990(5)2012) bgcolor(white) ///
	legend(size(small) pos(6) r(2) label(1 "State Time Trends and FE") ///
	label(3 "Firms > 50,000 Employees") label(5 "Establishment Links in 1993") label(7 "S936 Firms in 1993") order(1 3 5 7)) 

graph export "$output/Graphs/figure7_C.pdf", replace
