// This file recreates Table 4, long difference
// Author: Dan Garrett
// Date: 5-9-2020

/* Data sources:
QCEW, NETS, IRS, BEA
*/

clear all
set more off
snapshot erase _all 

*******************************************************
* IRS and BEA data
*******************************************************
use "$data/Replication Data/table4_data_IRSBEA", replace

eststo emp_2IRS:   reghdfe emp_growth  c.pr_link_i   [aw=base_emp] if inrange(year,2004,2008) , a(i.year) cl(fips_state  )
	estadd local hasy "Yes"
	estadd local hassel "Yes"	
	
eststo premp_2IRS:   reghdfe emp_growth  c.pr_link_i    [aw=base_emp] if inrange(year,1990,1995) , a(i.year) cl(fips_state  )
	estadd local hasy "Yes"
	estadd local hassel "Yes"	
	
eststo inc_2IRS:  reghdfe inc_growth c.pr_link_i  [aw=base_emp] if inrange(year,2004,2008) , a(i.year) cl(fips_state  )
	estadd local hasy "Yes"
	estadd local hassel "Yes"	
	
eststo princ_2IRS:	reghdfe inc_growth  c.pr_link_i    [aw=base_emp] if inrange(year,1990,1995) , a(i.year) cl(fips_state  )
	estadd local hasy "Yes"
	estadd local hassel "Yes"		
	
eststo emp_2:   reghdfe emp_BEA_growth  c.pr_link_i    [aw=base_emp] if inrange(year,2004,2008) , a(i.year) cl(fips_state  )
	estadd local hasy "Yes"
	estadd local hassel "Yes"	
	
eststo premp_2:   reghdfe emp_BEA_growth  c.pr_link_i    [aw=base_emp] if inrange(year,1990,1995) , a(i.year) cl(fips_state  )
	estadd local hasy "Yes"
	estadd local hassel "Yes"	
	
eststo inc_2:  reghdfe inc_BEA_growth c.pr_link_i  [aw=base_emp] if inrange(year,2004,2008) , a(i.year) cl(fips_state  )
	estadd local hasy "Yes"
	estadd local hassel "Yes"	
	
eststo princ_2:	reghdfe inc_BEA_growth  c.pr_link_i    [aw=base_emp] if inrange(year,1990,1995) , a(i.year) cl(fips_state  )
	estadd local hasy "Yes"
	estadd local hassel "Yes"		


*******************************************************
* QCEW County data
*******************************************************
use "$data/Replication Data/table4_data_county", replace

eststo county_emp:   reghdfe emp_growth  c.pr_link_i   [aw=base_emp] if inrange(year,2004,2008) , a(i.year industr) cl(fips_state  )
	estadd local hasy "Yes"
	estadd local hassel "Yes"	
	
eststo county_premp:   reghdfe emp_growth  c.pr_link_i    [aw=base_emp] if inrange(year,1990,1995) , a(i.year industr) cl(fips_state  )
	estadd local hasy "Yes"
	estadd local hassel "Yes"	
	
eststo county_inc:  reghdfe inc_growth c.pr_link_i  [aw=base_emp] if inrange(year,2004,2008) , a(i.year industr) cl(fips_state  )
	estadd local hasy "Yes"
	estadd local hassel "Yes"	
	
eststo county_princ:	reghdfe inc_growth  c.pr_link_i    [aw=base_emp] if inrange(year,1990,1995) , a(i.year industr) cl(fips_state  )
	estadd local hasy "Yes"
	estadd local hassel "Yes"	

*******************************************************
* QCEW Conspuma data
*******************************************************
use "$data/Replication Data/table4_data_conspuma", replace

eststo conspuma_emp:   reghdfe emp_growth  c.pr_link_i   [aw=base_emp] if inrange(year,2004,2008) , a(i.year industr) cl(fips_state  )
	estadd local hasy "Yes"
	estadd local hassel "Yes"	
	
eststo conspuma_premp:   reghdfe emp_growth  c.pr_link_i    [aw=base_emp] if inrange(year,1990,1995) , a(i.year industr) cl(fips_state  )
	estadd local hasy "Yes"
	estadd local hassel "Yes"	
	
eststo conspuma_inc:  reghdfe inc_growth c.pr_link_i  [aw=base_emp] if inrange(year,2004,2008) , a(i.year industr) cl(fips_state  )
	estadd local hasy "Yes"
	estadd local hassel "Yes"	
	
eststo conspuma_princ:	reghdfe inc_growth  c.pr_link_i    [aw=base_emp] if inrange(year,1990,1995) , a(i.year industr) cl(fips_state  )
	estadd local hasy "Yes"
	estadd local hassel "Yes"	

*******************************************************
* QCEW Commuting Zone data
*******************************************************
use "$data/Replication Data/table4_data_czone", replace

eststo czone_emp:   reghdfe emp_growth  c.pr_link_i   [aw=base_emp] if inrange(year,2004,2008) , a(i.year industr) cl(pid indust  )
	estadd local hasy "Yes"
	estadd local hassel "Yes"	
	
eststo czone_premp:   reghdfe emp_growth  c.pr_link_i    [aw=base_emp] if inrange(year,1990,1995) , a(i.year industr) cl(pid indust   )
	estadd local hasy "Yes"
	estadd local hassel "Yes"	
	
eststo czone_inc:  reghdfe inc_growth c.pr_link_i  [aw=base_emp] if inrange(year,2004,2008) , a(i.year industr) cl(pid indust   )
	estadd local hasy "Yes"
	estadd local hassel "Yes"	
	
eststo czone_princ:	reghdfe inc_growth  c.pr_link_i    [aw=base_emp] if inrange(year,1990,1995) , a(i.year industr) cl(pid indust   )
	estadd local hasy "Yes"
	estadd local hassel "Yes"	

*******************************************************
* the table
*******************************************************
// Outputting a single table with base panel on top, pr panel on bottom 
local list "emp_2IRS emp_2 county_emp  conspuma_emp czone_emp inc_2IRS inc_2 county_inc  conspuma_inc czone_inc"
local listpr " premp_2IRS premp_2 county_premp conspuma_premp czone_premp princ_2IRS princ_2 county_princ conspuma_princ czone_princ"

esttab `list' using "$output/Tables/table4.tex", drop(*) stats() ///
b(3) se par label  ///
noobs nogap nomtitle tex replace nocons nonum posthead(" & \multicolumn{5}{c}{Employment Growth}& \multicolumn{5}{c}{Income Growth} \\ \cmidrule(lr){2-6} \cmidrule(lr){7-11}") ///
prefoot("\textbf{Geography} & \multicolumn{3}{c}{County}& Conspuma & C-Zone & \multicolumn{3}{c}{County}& Conspuma & C-Zone \\ \cmidrule(lr){2-4} \cmidrule(lr){7-9}") ///
postfoot("\textbf{Data Source} & IRS & BEA & QCEW  & QCEW & QCEW & IRS & BEA & QCEW  & QCEW  & QCEW   \\ ") ///


esttab `list'  using "$output/Tables/table4.tex", keep(pr_link_ind )  ///
cells(b(fmt(3)) se(par) p) mlab(none) coll(none) s() noobs /// 
coeflabel(pr_link_ind "\hspace{1em}Exposure to Section 936 " pr_link_all "\hspace{1em}Exposure to Section 936 ") ///
preh("\multicolumn{1}{l}{\textbf{Years: 2004-2008}}") postfoot("\hline") append sty(tex)

esttab `listpr' using "$output/Tables/table4.tex", keep(pr_link_ind)  ///
cells(b(fmt(3)) se(par) p) mlab(none) coll(none) s()  /// 
coeflabel(pr_link_ind "\hspace{1em}Exposure to Section 936 ") ///
preh("\multicolumn{1}{l}{\textbf{Years: 1990-1995}}") noobs scalars(  "hasy Year Fixed Effects") label /// 
 prefoot("\hline") ///
postfoot( ///
" \hline\hline" ///
"\end{tabular} }" ) append sty(tex)	

