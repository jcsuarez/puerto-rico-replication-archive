***************************************************************
//Filename: tableA23_A24.do
//Author: eddie yu
//Date: 2023-09-20
//Task: Given tableA23_A24_data.dta, creates Table A23, A24
***************************************************************
/* Data sources:
QCEW, NETS
*/

clear all
set more off
snapshot erase _all 

*******************************************************
* starting with data
*******************************************************
use "$data/Replication Data/tableA23_A24_data", replace

** Robustness tables 
est clear 
eststo rob_0: reghdfe emp_growth cen_pr_link_i [aw=wgt] if inrange(year,2004,2008) ,  a( i.year  i.industr) cl(fips_state  i.industr) 
	estadd local hasy "Yes"
	estadd local hasind "Yes"
foreach var of varlist std_* { 
	eststo rob_`var': reghdfe emp_growth cen_pr_link_i `var' [aw=wgt] if inrange(year,2004,2008) ,  a( i.year  i.industr) cl(fips_state  i.industr) 
	estadd local hasy "Yes"
	estadd local hasind "Yes"

	} 
eststo rob_all: reghdfe emp_growth cen_pr_link_i std_* [aw=wgt] if inrange(year,2004,2008) ,  a( i.year  i.industr) cl(fips_state  i.industr) 
	estadd local hasy "Yes"
	estadd local hasind "Yes"		
	
eststo rob_fsd: reghdfe emp_growth cen_pr_link_i fs_std_n2-fs_std_n6 std_* [aw=wgt] if inrange(year,2004,2008) ,  a( i.year  i.industr) cl(fips_state  i.industr) 
	estadd local hasy "Yes"
	estadd local hasind "Yes"		
	estadd local hasfsd "Yes"	

** Separate into two tables 
esttab rob_0 rob_std_total rob_std_job rob_std_train ///
rob_std_mw rob_std_rtw rob_std_rd rob_std_ic rob_std_corp   using "$output/Tables/tableA23.tex",   ///
cells(b(fmt(3)) se(par) p) mlab(none) coll(none) s() noobs nogap scalars(  "hasy Year Fixed Effects" "hasind Industry Fixed Effects") /// 
drop( _cons) ///
coeflabel(cen_pr_link_i "Exposure to Section 936 "  /// 
std_total "Total Incentives (Bartik)"  /// 
std_job "Job Creation Incentive" /// 
std_train "Job Training Subsidy" /// 
std_mw "Real Minimum Wage" /// 
std_rtw "Right to Work" /// 
std_rd "RD Tax Credit" /// 
std_ic "Investment Tax Credit" /// 
std_corp "Corporate Income Tax" /// 
std_ptax "Personal Income Tax" /// 
std_prop "Property Tax"  /// 
std_sales "Sales Tax" /// 
std_capital "Capital Stock" /// 
std_trade "Trade Exposure (ADH)" /// 
std_rev "State Revenue Per Capita" /// 
std_routine "Share Routine Workers (Autor)" ///
pr_link_ind "\hspace{1em}Exposure to Section 936 ") ///
label /// 
 prefoot("\hline") ///
postfoot( ///
"  \hline\hline" ///
"\end{tabular} }" ) replace sty(tex)

esttab rob_0 rob_std_ptax rob_std_prop rob_std_sales /// 
 rob_std_trade rob_std_rev rob_std_routine rob_std_nafta  rob_all rob_fsd ///
 using "$output/Tables/tableA24.tex", drop(fs*  _cons)  ///
cells(b(fmt(3)) se(par) p) mlab(none) coll(none) s() noobs nogap scalars(  "hasy Year Fixed Effects" "hasind Industry Fixed Effects" ///
"hasfsd Firm Size Dist. Cont.") /// 
coeflabel(cen_pr_link_i "Exposure to Section 936 "  /// 
std_total "Total Incentives (Bartik)"  /// 
std_job "Job Creation Incentive" /// 
std_train "Job Training Subsidy" /// 
std_mw "Real Minimum Wage" /// 
std_rtw "Right to Work" /// 
std_rd "RD Tax Credit" /// 
std_ic "Investment Tax Credit" /// 
std_corp "Corporate Income Tax" /// 
std_ptax "Personal Income Tax" /// 
std_prop "Property Tax"  /// 
std_sales "Sales Tax" /// 
std_capital "Capital Stock" /// 
std_trade "Trade Exposure (China)" /// 
std_rev "State Revenue Per Capita" /// 
std_routine "Share Routine Workers" ///
std_nafta "Trade Exposure (Nafta)" /// 
pr_link_ind "\hspace{1em}Exposure to Section 936 ") ///
label /// 
 prefoot("\hline") ///
postfoot( ///
" \hline\hline" ///
"\end{tabular} }" ) replace sty(tex)
