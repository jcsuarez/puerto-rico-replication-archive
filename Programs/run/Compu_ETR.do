***************************************************************
//Filename: Compu_ETR.do
//Author: eddie yu
//Date: 2023-09-20
//Task: This script conducts ETR regression using Narrow-Sample, Long-Difference
//		Size-by-Year FE, Winsorized at 5% 
//		Conducts two regressions on tax_impute, and tax_impute2
//		tax_impute2 will go into main table, tax_impute will go into
//		Appendix
***************************************************************

clear all
set more off
snapshot erase _all 

*******************************************************
* Starting with the data
*******************************************************
use "$data/Replication Data/table2_data_B", clear

destring n2, replace

* Approach 0922: 7 columns
global level "gvkey"
global spec1 ", cl(gvkey) a(PR ppe95_bins10##year)"
global spec2 ", cl(gvkey) a($level ppe95_bins10##year)"
global spec3 "if exp_sec, cl(gvkey) a($level ppe95_bins10##year)"
global spec4 "if exp_ind, cl(gvkey) a($level ppe95_bins10##year)"
global spec5 ", cl(gvkey) a($level ppe95_bins10##year n2##ppe95_bins10##year)"
global spec6 "[aw=DFL_ppe], cl(gvkey) a($level ppe95_bins10##year n2##ppe95_bins10##year)"
global spec7 "[aw=_webal], cl(gvkey) a($level ppe95_bins10##year n2##ppe95_bins10##year)"



local i = 0
foreach spec in spec1 spec2 spec3 spec4 spec5 spec6 spec7 {
	local i = `i' + 1
	eststo `spec'_est1: reghdfe tax_impute ET_* $`spec' 
	est save "$output/ster/Compu_ETR_taximpute1_`i'", replace
	eststo `spec'_est2: reghdfe tax_impute2 ET_* $`spec' 
	est save "$output/ster/Compu_ETR_taximpute2_`i'", replace
}		

*** "Industry-specific rho weight" ***	
esttab spec?_est1, keep(ET_post2) cells(b p)
*** "Constant rho weight" ***	
esttab spec?_est2, keep(ET_post2) cells(b p)	

