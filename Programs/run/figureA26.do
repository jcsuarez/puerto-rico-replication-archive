***************************************************************
//Filename: figureA26.do
// This file uses figure26_data_A/C, produces 3 Panels (A, B, C):
// Heterogenous Employment Effects of Repaealing of S936
// Author: eddie yu
// Date: 2023-10-16
***************************************************************

clear all
set more off
snapshot erase _all

** Panel A : Interaction Terms
use "$data/Replication Data/figureA26_data_A", clear

est clear 
eststo rob_0: reghdfe emp_growth cen_pr_link_i $pra_control_rob std_* [aw=wgt] if inrange(year,2004,2008) & samp1 == 1 ,  a( i.year  i.industr) cl(fips_state  i.industr) 
	estadd local hasy "Yes"
	estadd local hasind "Yes"
	estadd local hass "Yes"		
	estadd local hasc "Yes"		
	estimates save "$ster/figureA26_rob_0", replace
	
foreach var of varlist std_* { 
		eststo rob_`var': reghdfe emp_growth cen_pr_link_i $pra_control_rob std_* c.cen_pr_link_i#c.`var' [aw=wgt] if inrange(year,2004,2008) & samp1 == 1 ,  a( i.year i.industr) cl(fips_state  i.industr) 
	estadd local hasy "Yes"
	estadd local hasind "Yes"
	estadd local hass "Yes"		
	estadd local hasc "Yes"		
	estimates save "$ster/figureA26_rob_`var'", replace
	} 
eststo rob_all: reghdfe emp_growth cen_pr_link_i std_* $pra_control_rob c.cen_pr_link_i#(c.std_*) [aw=wgt] if inrange(year,2004,2008) & samp1 == 1 ,  a( i.year  i.industr) cl(fips_state industr) 
	estadd local hasy "Yes"
	estadd local hasind "Yes"
	estadd local hass "Yes"		
	estadd local hasc "Yes"			
	estimates save "$ster/figureA26_rob_all", replace
	
* existing graph with individual interaction terms
	
	coefplot rob_all /// 
 	, keep(*cen_pr_link_i*) ciopts(recast(rcap)) drop(cen_pr_link_i) ///
	graphregion(color(white)) xlab(-.1(.02).1) xline(0) ///
	rename(c.cen_pr_link_i#c.std_total = "Total Incentives (Bartik)"  ///
	c.cen_pr_link_i#c.std_job ="Job Creation Incentive" /// 
	c.cen_pr_link_i#c.std_train ="Job Training Subsidy" /// 
	c.cen_pr_link_i#c.std_mw ="Real Minimum Wage" /// 
	c.cen_pr_link_i#c.std_rtw ="Right to Work" /// 
	c.cen_pr_link_i#c.std_rd ="RD Tax Credit" /// 
	c.cen_pr_link_i#c.std_ic ="Investment Tax Credit" /// 
	c.cen_pr_link_i#c.std_corp ="Corporate Income Tax" /// 
	c.cen_pr_link_i#c.std_ptax ="Personal Income Tax" /// 
	c.cen_pr_link_i#c.std_prop ="Property Tax"  /// 
	c.cen_pr_link_i#c.std_sales ="Sales Tax" /// 
	c.cen_pr_link_i#c.std_capital ="Capital Stock" /// 
	c.cen_pr_link_i#c.std_trade ="Trade Exposure (China)" /// 
	c.cen_pr_link_i#c.std_rev ="State Revenue Per Capita" /// 
	c.cen_pr_link_i#c.std_routine ="Share Routine Workers" ///
	c.cen_pr_link_i#c.std_nafta ="Trade Exposure (Nafta)" /// 
	) levels(90)
	
	graph export "$output/Graphs/figureA26_A.pdf", replace 

	
** Panel B: Effects of Interactions on Marginal Effect
	estimates restore rob_all
	local beta = _b[cen_pr_link_i]
	local mini = round(`beta'*2, -.03)
	local mini = max(`mini', -0.15)		
margins, dydx(cen_pr_link_i) at(std_ptax=1)  at(std_prop=1)  at(std_sales=1) ///
		 at(std_rev=1) at(std_train=1)  at(std_mw=1)  at(std_rtw=1) ///
		 at(std_rd=1) at(std_ic=1) at(std_nafta=1)  at(std_routine=1)  at(std_trade=1) ///
		 at(std_corp=-1) at(std_job=1) at(std_corp=0)
		
marginsplot /// 
 	, ciopts(recast(rcap)) ytitle("") recast(scatter) ///
	graphregion(color(white)) xline(`beta', lpattern(dash) lcolor(navy)) ///
	xline(0) bgcolor(white) xtitle("Marginal Effect of Exposure to S936 on Emp Growth, 2004-08") ///
	title("") nolab xdim(_atopt,lab("+1{&sigma} Personal Tax Rate" "+1{&sigma} Prop Tax Rate" ///
	"+1{&sigma} Sales Tax Rate"  ///
	"+1{&sigma} State Tax Revenue" "+1{&sigma} Job Training" "+1{&sigma} Minimum Wage" ///
	"+1{&sigma} Right to Work"  ///
	"+1{&sigma} R&D Credit" "+1{&sigma} Invest Incentive" "+1{&sigma} Nafta Exposure" "+1{&sigma} Routine Workers" ///
	"+1{&sigma} China Exposure"  ///
	"-1{&sigma} Corp Tax Rate" "+1{&sigma} Job Incentive" "ME at Average")) ///
    horiz
 
	graph export "$output/Graphs/figureA26_B.pdf", replace  	


** Panel C: Geographic Distribution of Marginal Effects
use "$data/Replication Data/figureA26_data_C", replace

maptile w_tot_eff , geo(county1990) stateoutline(linewidthstyle)  nquantiles(4)   revcolor  res(.5)
 
graph export "$output/Graphs/figureA26_C.pdf", replace 
