***************************************************************
//Filename: tableA7.do
//Author: eddie yu
//Date: 2023-10-16
//Task: This script creates Main Table A7 NETS Regression:
//		It constructs 3 sets of panels:
//		- Baseline Employment
//		- ETR (tax_impute)
//		- IV (PR#post, PR#year as instrumental variable)
***************************************************************


	******************************** Set Directory *********************************

	cd "$ster"
	estimates clear
	eststo clear

	******************************** Load Estimates ********************************
	*** Call the non-SOI tables ***
	foreach i in "Employment_" "ETR_taximpute2_" "IV_PRyear_taximpute2_" {
		forval j = 1/5 {
			estimates use "NETS_`i'`j'"
			estimates store `i'_spec`j'
		}
	}	


	cd "$analysis"
	********************************* Make Tables **********************************
	*** Appendix 1 : PR#year IV *** 
	esttab Employment_* using "$output/Tables/tableA7.tex", keep(ET_post2) /// 
		cells(b(fmt(3)) se(par) p) ///
		coeflabel(ET_post2 "\hspace{3mm} Exposure to Section \S936 \hspace{3mm} $\times$ Post") /// 
		nogap nomtitle tex wrap replace label nocons collabels(none) varwidth(40) nomtitle ///
		prefoot("") postf("\hline") posth("\hline \textbf{A. Employment Growth} \\")
	

	esttab ETR_taximpute2_* using "$output/Tables/tableA7.tex", keep(ET_post2) ///  
		cells(b(fmt(3)) se(par) p) ///
		coeflabel(ET_post2 "\hspace{3mm} Exposure to Section \S936 \hspace{3mm} $\times$ Post") /// 
		tex wrap append label mlab(none) collabels(none) nonum varwidth(40) nomtitle /// 
		prefoot("") postfoot("\hline") preh("") posthead("\textbf{B. Effective Tax Rate} \\")

	esttab IV_PRyear_taximpute2_* using "$output/Tables/tableA7.tex", keep(tax_impute2) ///  
		cells(b(fmt(3)) se(par) p) ///
		coeflabel(tax_impute2 "\hspace{3mm} Effective Tax Rate") /// 
		tex wrap append mlab(none) label  collabels(none) nonum  varwidth(40) nomtitle /// 
		prefoot("") posthead("\textbf{C. Semi-Elasticity of Employment} \\") /// 
		preh("") postfoot("\hline"  /// 
		"Year Fixed Effects                            & Y & Y & Y & Y & Y   \\    "      /// 
		"Industry $\times$ Year Fixed Effects   	   &   & Y & Y & Y & Y   \\    "      /// 
		"S936 Exposed Sector			               &   &   & Y & Y & Y   \\    "      ///
		"S936 Exposed Industry                         &   &   &   & Y & Y   \\    "      /// 
		"DFL Weights                                   &   &   &   &   & Y   \\ \hline\hline" /// 
		"\\ " /// 
		"\end{tabular} }" )
		
		
