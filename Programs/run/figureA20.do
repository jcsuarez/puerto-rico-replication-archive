***************************************************************
//Filename: figureA20.do
//Author: eddie yu
//Date: 2023-10-16
//Task: This script creates Figure A.20. It uses figure7_data_syFE, which
//		is already created from figure7_clean.do
**************************************************************

/* Data sources:
QCEW, NETS
*/

clear all
set more off
snapshot erase _all 
est clear

*graphing program
capture program drop ES_graph
program define ES_graph
syntax,  level(real) [  yti(string) note(string) outname(string) ylab(string) ]

qui { 
mat b = e(b)
mat V = e(V) 
mat es_b = 0 
mat es_ci_l = 0 
mat es_ci_h = 0 
mat time = 0 

// for 95% CI 
if `level' == 99 { 
	local z= 2.6  
}
if `level' == 95 { 
	local z= 1.96  
}
// for 90% CI 
if `level' == 90 { 
	local z= 1.645 
} 
	
forval i = 1/23 { 
	mat time = (time,`i'-6+1995)
	mat es_b = (es_b, b[1,`i'])
	mat es_ci_l = (es_ci_l, b[1,`i']-`z'*sqrt(V[`i',`i'])   )
	mat es_ci_h = (es_ci_h, b[1,`i']+`z'*sqrt(V[`i',`i'])   )		
	}
mat es_b =  (es_b[1,2..6],0,es_b[1,7...])'
mat es_ci_l = (es_ci_l[1,2..6],0,es_ci_l[1,7...])'  
mat es_ci_h = (es_ci_h[1,2..6],0,es_ci_h[1,7...])' 
mat time = time[1,2...]' 

capture: drop es_b es_ci* time 
svmat es_b 
svmat time
svmat es_ci_l
svmat es_ci_h 
}

twoway (scatter es_b time, yline(0 , lcolor(black)) xline(1995, lcolor(black) lpattern(dash))) ///
    (rcap es_ci_h es_ci_l time,  lcolor(navy) lpattern(dash)) ///
	, plotregion(fcolor(white) lcolor(white)) ///
	graphregion(fcolor(white) lcolor(white)) ///
	xtitle("Year") xlab(1990(5)2012) ///
	legend(size(small) pos(6) r(1) label(1 "Estimate") label(2 "`level'% CI")) ///
	ytitle("`yti'", margin(medsmall)) bgcolor(white)  ///
	note("`note'") ylabel("`ylab'")
graph export "`outname'.pdf", replace	

end 

*******************************************************
* starting with data from figure 7
*******************************************************
use "$data/Replication Data/figure7_data_syFE", replace

* setting up interactions
xi i.year|pr_link_i, noomit
drop _IyeaXp*1995

* running regressions
reghdfe emp_growth _IyeaX* [aw=wgt] , a(c.year#i.fips_sta i.year#i.industry i.pid2 i.fips_state) cl(fips_state indust )

ES_graph , level(95) yti("Effect S936 Exposure (IQR)") ///
note("Notes: (Emp{sub:ict}-Emp{sub:ic1995})/Emp{sub:ic1995}={&alpha}{sub:c}+{&gamma}{sub:it}+{&beta}{sub:t}S936 Exposure{sub:c}+{&epsilon}{sub:ict}. SE's clustered by State and Industry.") ///
outname("$output/Graphs/figureA20") ylab("-.12(.02).04")
