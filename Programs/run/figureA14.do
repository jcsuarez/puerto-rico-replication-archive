***************************************************************
//Filename: figureA14.do
//Author: eddie yu
//Date: 2023-09-20
//Task: This script creates Figure A14 under different specifications
***************************************************************

/* Data sources:
none
*/

clear all
set more off
snapshot erase _all 

*******************************************************
* no data needed, generating graph
*******************************************************
local nn = 16
set obs `nn'
gen year = _n-6


/* Depreciation rates come from: 
https://www.bea.gov/national/pdf/BEA_depreciation_rates.pdf
same thing: https://www.bea.gov/national/FA2004/Tablecandtext.pdf
*/ 

// Categories are all under  "General industrial, including materials handling equipment" (different measures in different categories)
local delta1 = .0247 // Office Builsingds 
local delta2 = .0686 // Machinery 
local delta3 = .1225   // Computer and electronic products 

local beta = .16

forval i = 1/3 { 
	gen K`i' = 1 
} 	


forval i = 7/`nn' { 
	forval j = 1/3 { 
		replace K`j' =  K`j'[`i'-1]*(1-`delta`j'' + (1-`beta')*`delta`j''*1/K`j'[`i'-1])  in `i'
	}
} 


twoway (scatter K1 year , lcolor(navy) mcolor(navy) connect(line) lpattern(solid) xline(0) )  ///
	   (scatter K2 year , lcolor(orange) mcolor(orange) msymbol(D) lpattern(dash) connect(line) )  ///
	   (scatter K3 year , lcolor(green) mcolor(green) msymbol(S) lpattern(dash_dot) connect(line) ) , ///
	   ylab(0(.25)1) ytitle("Capital Stock Relative to Steady State") graphregion(color(white)) ///
	xline(0) bgcolor(white) xtitle("Years from 16% Investment Decrease")  ///
	legend(size(small) pos(6) r(2) label(1 "Buildings, {&delta} = 0.025") ///
	label(2 "Machinery, {&delta} = 0.069") label(3 "Electronics, {&delta} = 0.123"))
graph export "$output/Graphs/figureA14.pdf", replace	
