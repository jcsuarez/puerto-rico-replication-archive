***************************************************************
//Filename: tableA12.do
//Author: eddie yu
//Date: 2023-09-20
//Task: Given figure7_data_MD.dta, creates Table A12
***************************************************************
/* Data sources:
QCEW, NETS
*/

clear all
set more off
snapshot erase _all 
estimates clear

*******************************************************
* starting with data from figure 7
*******************************************************
use "$data/Replication Data/figure7_data_MD", replace

*making interaction terms
xi i.year|pr_link_ind, noomit
drop _IyeaXp*1995

*regressions
eststo emp_1:  reghdfe emp_growth _IyeaXpr_* [aw=wgt]  , a( i.year#i.industr ) cl(fips_state indust )
	estadd local hasiy "Yes"
    estadd local hascty ""
	
eststo emp_2:  reghdfe emp_growth _IyeaXpr_* [aw=wgt]  , a(i.pid i.year#i.industr ) cl(fips_state indust )
	estadd local hasiy "Yes"
	estadd local hascty "Yes"

eststo emp_3:  reghdfe emp_growth _IyeaXpr_* [aw=wgt]  , a( i.pid2 i.year#i.indust ) cl(fips_state indust )
	estadd local hasiy "Yes"
	estadd local hasicty "Yes"

eststo emp_4:  reghdfe emp_growth _IyeaXpr_* [aw=wgt_w]  , a( i.pid i.year#i.indust ) cl(fips_state indust )
	estadd local hasiy "Yes"
	estadd local hascty "Yes"
	estadd local hasww "Yes"	

eststo emp_5:   reghdfe emp_growth _IyeaXpr_* if base_emp > 1000 [aw=wgt]  , a( i.pid i.year#i.indust ) cl(fips_state indust )
	estadd local hasiy "Yes"
	estadd local hascty "Yes"
	estadd local hasds "Yes"


* outputting table
esttab emp* using "$output/Tables/tableA12.tex", drop(*) stats() ///
b(3) se par label  ///
noobs nogap nomtitle tex replace nocons ///
postfoot("\multicolumn{1}{l}{\textbf{Exposure to Section 936}}") nonum posthead("")


esttab emp* using "$output/Tables/tableA12.tex",  preh("") ///
 b(3) se par mlab(none) coll(none) s() nogap noobs keep(_IyeaXpr__*) /// 
coeflabel(_IyeaXpr__1990 "\hspace{1em}X 1990 " /// 
_IyeaXpr__1991 "\hspace{1em}X 1991 " /// 
_IyeaXpr__1992 "\hspace{1em}X 1992 " /// 
_IyeaXpr__1993 "\hspace{1em}X 1993 " /// 
_IyeaXpr__1994 "\hspace{1em}X 1994 " /// 
_IyeaXpr__1996 "\hline \hspace{1em}X 1996 " /// 
_IyeaXpr__1997 "\hspace{1em}X 1997 " /// 
_IyeaXpr__1998 "\hspace{1em}X 1998 " /// 
_IyeaXpr__1999 "\hspace{1em}X 1999 " /// 
_IyeaXpr__2000 "\hspace{1em}X 2000 " /// 
_IyeaXpr__2001 "\hspace{1em}X 2001 " /// 
_IyeaXpr__2002 "\hspace{1em}X 2002 " /// 
_IyeaXpr__2003 "\hspace{1em}X 2003 " /// 
_IyeaXpr__2004 "\hspace{1em}X 2004 " /// 
_IyeaXpr__2005 "\hspace{1em}X 2005 " /// 
_IyeaXpr__2006 "\hspace{1em}X 2006 " /// 
_IyeaXpr__2007 "\hspace{1em}X 2007 " /// 
_IyeaXpr__2008 "\hspace{1em}X 2008 " /// 
_IyeaXpr__2009 "\hspace{1em}X 2009 " /// 
_IyeaXpr__2010 "\hspace{1em}X 2010 " /// 
_IyeaXpr__2011 "\hspace{1em}X 2011 " /// 
_IyeaXpr__2012 "\hspace{1em}X 2012 " )   scalars(  "hasiy Industry-by-Year Fixed Effects" "hascty County Fixed Effects" ///
"hasicty Industry-by-County Fixed Effects" ///
"hasww Winsorized Weights" "hasds Drops Small County-Industries ($<$1000)" ) label /// 
 prefoot("\hline") ///
postfoot( ///
"  \hline\hline" ///
"\end{tabular} }" ) append sty(tex)
