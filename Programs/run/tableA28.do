***************************************************************
//Filename: tableA28.do
//Author: eddie yu
//Date: 2023-09-20
//Task: Given figure11_data.dta, creates Table A28
***************************************************************

/* Data sources:
QCEW, NETS, BEA
*/

clear all
set more off
snapshot erase _all 

*******************************************************
* starting with data from table 5
*******************************************************
use "$data/Replication Data/table5_data", replace

est clear 
set more off
foreach Q of var trans_tot trans_unemp trans_income trans_educ trans_SSA trans_medicare  trans_public_med  {

* long diff table outputs

*percents
eststo premp_2_`Q'_p:   reghdfe `Q'_growth c.pr_link_i   [aw=wgt] if inrange(year,1990,1995) , a( i.year) cl(statefips)
	estadd local hasy "Yes"
	estadd local hasind "Yes"
	estadd local hassel "Yes"	

*levels
eststo premp_2_`Q'_l:   reghdfe `Q'_growth_levels c.pr_link_i   [aw=wgt] if inrange(year,1990,1995) , a( i.year) cl(statefips)
	estadd local hasy "Yes"
	estadd local hasind "Yes"
	estadd local hassel "Yes"	
* percent of transfers from category
sum `Q'_weight [aw=total_transfers_p] if e(sample), d
		estadd local trans_weight = string(r(mean)*100, "%8.1f") + "\%"
* Average transfers from category
sum `Q'_p 	   						  if e(sample),d 
		estadd local trans_mean = string(r(mean), "%8.1fc") 
	
}

* putting out one table with the after estimates for the paper
esttab premp_2_*_p using "$output/Tables/tableA28.tex", drop(*) stats() ///
b(3) se par label  ///
noobs nogap nomtitle tex replace nocons  ///
prehead("{\def\sym#1{\ifmmode^{#1}\else\(^{#1}\)\fi}\begin{tabular}{l*{7}{c}} \hline\hline &  & Unem- & Income  & Educ-  & Retire- & & Public   \\   & Total & ployment &  Replace-  & ation & ment and & Medicare & Medical   \\   & Transfers & Benefits & ment  & Benefits & Disability & Benefits & Benefits   \\  ") ///
postfoot("") ///

esttab premp_2_*_p  using "$output/Tables/tableA28.tex", keep(pr_link_ind )  ///
cells(b(fmt(3)) se(par) p) mlab(none) coll(none) nonum s() noobs /// 
coeflabel(pr_link_ind "\hspace{1em}Exposure to Section 936 " pr_link_all "\hspace{1em}Exposure to Section 936 ") ///
preh("\multicolumn{8}{l}{\textit{Panel A. Percent change in transfers per capita relative to 1995}} \\") posth("") postfoot("") append sty(tex)

esttab premp_2_*_l using "$output/Tables/tableA28.tex", keep(pr_link_ind)  ///
cells(b(fmt(3)) se(par) p) mlab(none) coll(none) nonum s()  /// 
coeflabel(pr_link_ind "\hspace{1em}Exposure to Section 936 ")  ///
preh("\multicolumn{8}{l}{\textit{Panel B. Dollar change in transfers per capita relative to 1995}} \\") noobs posth("") sfmt(3 1) ///
scalars("trans_weight Sample Proportion of Transfers" "trans_mean Sample Transfers Per Capita" "hasy \hline Year Fixed Effects") label /// 
 prefoot("\hline") ///
postfoot( ///
" \hline\hline" ///
"\end{tabular} }" ) append sty(tex)	
