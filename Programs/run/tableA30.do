***************************************************************
//Filename: tableA30.do
//Author: eddie yu
//Date: 2023-09-20
//Task: Given tableA30_data.dta, creates Table A30
***************************************************************

/* Data sources:
NETS, compustat
*/

clear all
set more off
snapshot erase _all 

*******************************************************
* Starting with the data
*******************************************************
use "$data/Replication Data/tableA30_data", clear

* defining specifications (rows)
local spec1 "c.mktrf"
local spec2 "c.mktrf c.smb c.hml c.MOM"
local spec3 "c.mktrf"
local spec4 "c.mktrf c.smb c.hml c.MOM"
local spec5 "c.mktrf"
local spec6 "c.mktrf c.smb c.hml c.MOM"

set more off
estimates drop _all

* Pooled regression for Feb 16, 1993 and Oct 12, 1995
forvalues k=1(1)6 {
forvalues i=0(1)5 {
* spec specific samples
local spec1_other "if estimation12100_`i' == 1 | estimation13068_`i' == 1"
local spec2_other "if estimation12100_`i' == 1 | estimation13068_`i' == 1"
local spec3_other "if estimation12100_`i' == 1 "
local spec4_other "if estimation12100_`i' == 1 "
local spec5_other "if estimation13068_`i' == 1 "
local spec6_other "if estimation13068_`i' == 1 "

local j = `i' * 3
qui gen event_dummy= event12100 / (1 + `j') + event13068 / (1 + `j')
qui eststo reg_`i'_`k': areg ret i.id#(`spec`k'') event_dummy `spec`k'_other', r a(id)
drop event_dummy
}

*Using the pre-period for a seventh column
local spec3_other "if eve1 == 1 "
local spec4_other "if eve1 == 1 "
local spec5_other "if eve2 == 1 "
local spec6_other "if eve2 == 1 "
qui gen event_dummy= pre_event12100 / (21) + pre_event13068 / (21)
qui eststo reg_6_`k': areg ret i.id#(`spec`k'') event_dummy `spec`k'_other', r a(id)
drop event_dummy
}

* Outputting tables mirroring Coups paper
esttab reg_?_1 using "$output/Tables/tableA30.tex", keep(_cons) stats() ///
b(3) se par label coeflabel(event_dummy "\hspace{1em}Event") ///
noobs nogap tex replace nocons postfoot("") mtit("(0,0)" "(0,3)" "(0,6)" "(0,9)" "(0,12)" "(0,15)" "(-5,15)") nonum

estout reg_?_1 using "$output/Tables/tableA30.tex", keep(event_dummy) ///
cells(b(fmt(3)) se(par) p) mlab(none) coll(none) ///
varlabel(event_dummy "\hspace{1em}Event") ///
preh("\multicolumn{8}{l}{\textbf{Pooled CAPM}}\\") prefoot("\hline")  postfoot(" \hline ")  append sty(tex)

estout reg_?_2 using "$output/Tables/tableA30.tex", keep(event_dummy) ///
cells(b(fmt(3)) se(par) p) mlab(none) coll(none) ///
varlabel(event_dummy "\hspace{1em}Event") ///
preh("\multicolumn{8}{l}{\textbf{Pooled Fama-French 3 Factors Plus Momentum}}\\") prefoot("\hline")  postfoot( ///
" \hline "  ) append sty(tex)

estout reg_?_3 using "$output/Tables/tableA30.tex", keep(event_dummy) ///
cells(b(fmt(3)) se(par) p) mlab(none) coll(none) ///
varlabel(event_dummy "\hspace{1em}Event") ///
preh("\multicolumn{8}{l}{\textbf{February 16, 1993 Event CAPM}}\\") prefoot("\hline")  postfoot(" \hline ")  append sty(tex)

estout reg_?_4 using "$output/Tables/tableA30.tex", keep(event_dummy) ///
cells(b(fmt(3)) se(par) p) mlab(none) coll(none) ///
varlabel(event_dummy "\hspace{1em}Event") ///
preh("\multicolumn{8}{l}{\textbf{February 16, 1993 Event Fama-French 3 Factors Plus Momentum}}\\") prefoot("\hline")  postfoot( ///
" \hline " ) append sty(tex)

estout reg_?_5 using "$output/Tables/tableA30.tex", keep(event_dummy) ///
cells(b(fmt(3)) se(par) p) mlab(none) coll(none) ///
varlabel(event_dummy "\hspace{1em}Event") ///
preh("\multicolumn{8}{l}{\textbf{October 12, 1995 CAPM}}\\") prefoot("\hline")  postfoot(" \hline ")  append sty(tex)

estout reg_?_6 using "$output/Tables/tableA30.tex", keep(event_dummy) ///
cells(b(fmt(3)) se(par) p) mlab(none) coll(none) stats(N, nostar /// 
labels("Observations") fmt(%9.0fc 3)) ///
varlabel(event_dummy "\hspace{1em}Event") ///
preh("\multicolumn{8}{l}{\textbf{October 12, 1995 Fama-French 3 Factors Plus Momentum}}\\") prefoot("\hline")  postfoot( ///
" \hline " ///
"\end{tabular} }" ) append sty(tex)
