***************************************************************
//Filename: NETS_ETR.do
//Author: eddie yu
//Date: 2023-09-20
//Task: This script conducts Exposure to S936 on ETR,
//		in percentage using NETS data
***************************************************************

clear all
set more off
snapshot erase _all 

*******************************************************
* Starting with the data
*******************************************************
use "$data/Replication Data/table3_data_B", clear

* Regression Specifications
global level "PR"
global spec1 " [aw=wgt],vce(cluster hqduns) nocon a($level year)"
global spec2 " [aw=wgt],vce(cluster hqduns) nocon a($level naic2##year)"
global spec3 "if major_sec [aw=wgt],vce(cluster hqduns) nocon a($level naic2##year)"
global spec4 "if major_ind [aw=wgt],vce(cluster hqduns) nocon a($level naic2##year)"
global spec5 "if major_ind [aw=DFL2], vce(cluster hqduns) nocon a($level naic2##year)"


global specname1 "Baseline"
global specname2 "NAIC2"
global specname3 "Exp. Sector"
global specname4 "Exp. Industry"
global specname5 "Exp. Industry + DFL"

global c1 "navy"
global c2 "dkorange"
global c3 "midgreen"
global c4 "maroon"
global c5 "midblue"
global c6 "red"

* winsorize
capture: drop emp_growth_w
winsor2 emp_growth, by(PR year) cuts(10 100)

* percentage
capture: drop emp_growth100
gen emp_growth100 = 100*emp_growth

local y "emp_growth100"
local i = 0
foreach spec in spec1 spec2 spec3 spec4 spec5 {
	local i = `i' + 1
	eststo `spec'_sto1: qui reghdfe tax_impute ET_* PR $`spec'
	est save "$output/ster/NETS_ETR_taximpute1_`i'", replace
	eststo `spec'_sto2: qui reghdfe tax_impute2 ET_* PR $`spec'
	est save "$output/ster/NETS_ETR_taximpute2_`i'", replace
}
*** "Industry-specific rho weight" ***
esttab spec?_sto1, keep(ET_*) cells(b p)
*** "Constant rho weight" ***
esttab spec?_sto2, keep(ET_*) cells(b p)
