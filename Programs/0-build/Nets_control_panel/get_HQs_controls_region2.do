/* 
This code goes over all the estabs in the NETS and selects: 
1- Those that are headquarters only
2- Those that are around before 1995 (in 1995 and 1990)
3- Drops Industries and firms that are suspect
4- Keeps Those in industry-state combos that match PR exposed firms 
5- Saves the HQDuns for these firms 

*/

////////////////////////////////////////////////////////////////
/////// Get list of HQs
////////////////////////////////////////////////////////////////
clear
set more off

*10000000

****  - Create list of all hqduns that have an establishment in PR
forvalues v = 1/6 {
local start = 1 + 10000000*(`v'-1)
local end = 10000000*`v'
di `start'
di `end'

** Import NETS data (~52.5 million observations total)
import delimited "$netspath/NETS2012Basic.txt", clear rowrange(`start':`end') colrange(1:154) varnames(1) delimiter(",")

*import delimited "$netspath/NETS2012Basic.txt", clear rowrange(1:1000) colrange(1:154) varnames(1) delimiter(",")

** Get location and drop firms in PR
gen fipsstate = floor(fipscounty/1000)
gen fipsstate95 = floor(fips95/1000)
drop if fipsstate95 == 72

* censusdivision
gen census_div = 1 if inlist(fipsstate95,9,23,25,33,44,50)
replace census_div = 2 if inlist(fipsstate95,34,36,42)
replace census_div = 3 if inlist(fipsstate95,18,17,26,39,55)
replace census_div = 4 if inlist(fipsstate95,19,20,27,29,31,38,46)
replace census_div = 5 if inlist(fipsstate95,10,11,12,13,24,37,45,51,54)
replace census_div = 6 if inlist(fipsstate95,1,21,28,47)
replace census_div = 7 if inlist(fipsstate95,5,22,40,48)
replace census_div = 8 if inlist(fipsstate95,4,8,16,35,30,49,32,56)
replace census_div = 9 if inlist(fipsstate95,2,6,15,41,53)
keep if census_div!=.

gen census_reg = 1 if inlist(census_div,1,2) 
replace census_reg = 2 if inlist(census_div,3,4) 
replace census_reg = 3 if inlist(census_div,5,6,7) 
replace census_reg = 4 if inlist(census_div,8,9) 

** Keep HQs
keep if dunsnumber == hqduns95 

** Drop If no emp in 1995 
drop if emp95 == . | emp95 == 0
drop if emp90 == . | emp90 == 0

** Drop specific industries and firms
gen naic4 = floor(naics95/100)
gen naic3 = floor(naics95/1000)
gen naic2 = floor(naics95/10000)
drop if naic2 == 92 | naic3 == 813
* dropping USPS and federal government
drop if hqduns95 == 3261245 
* this is navy, drop in future  hqduns95 == 161906193

** Gen state year combo to merge into list of ones we care about 
gen firm_group2 = census_reg*10000000+naic3*10000

** Keep firms in list of approved places/industies
keep hqduns95 firm_group2 
duplicates drop
merge m:1 firm_group2 using "$output_NETS_control/firm_group.dta"
keep if _merge == 3 
drop _merge 

save "$output_NETS_control/hq_co_`v'.dta", replace

}

** Append HQ files
use "$output_NETS_control/hq_co_1.dta", clear
forvalues v = 2/6 {
	append using "$output_NETS_control/hq_co_`v'.dta"
}
duplicates drop 
compress 
save "$output_NETS_control/hq_co_master.dta", replace

// List of HQ's in industry firm groups in 1995 
