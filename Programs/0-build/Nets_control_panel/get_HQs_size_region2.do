/* 
This code goes over all the estabs in the NETS and selects: 
1- All estabs linked to control firms (with HQs in list) 
2- Gathers just employment and establishments in 1995
3- collapses at hq level 
4- merges all files and collapses again 
5- keeps only those firms in the relevant size categories 
6- This reduces the number of potential HQ firms. 
*/

////////////////////////////////////////////////////////////////
/////// Get list of HQs
////////////////////////////////////////////////////////////////

clear
set more off

*10000000

****  - Create list of all hqduns that have an establishment in PR
forvalues v = 1/6 {
local start = 1 + 10000000*(`v'-1)
local end = 10000000*`v'
di `start'
di `end'

** Import NETS data (~52.5 million observations total)
import delimited "$netspath/NETS2012Basic.txt", clear rowrange(`start':`end') colrange(1:126) varnames(1) delimiter(",")

*import delimited "$netspath/NETS2012Basic.txt", clear rowrange(1:100) colrange(1:126) varnames(1) delimiter(",")

* Clean data 
keep hqduns95 emp95 
drop if emp95 == . | emp95 == 0
gen est = 1 

* Collapse and merge with list of firms 
collapse (sum) emp95 est , by(hqduns95)
merge 1:1 hqduns95 using "$output_NETS_control/hq_co_master.dta"
keep if _merge == 3
drop _merge 

duplicates drop 
compress

save "$output_NETS_control/hq_est_co_`v'.dta", replace

}

** Append HQ files
use "$output_NETS_control/hq_est_co_1.dta", clear
forvalues v = 2/6 {
	append using "$output_NETS_control/hq_est_co_`v'.dta"
}
collapse (sum) emp95 est , by(hqduns95 firm_group2)

******* Group employmnt into Quintiles based on the dist of linked firms 
gen q_emp = 1 if inrange(emp95,5,326) 
replace q_emp = 2 if inrange(emp95,326,1543) 
replace q_emp = 3 if inrange(emp95,1543,5754) 
replace q_emp = 4 if inrange(emp95,5754,18672) 
replace q_emp = 5 if emp95>18672

******** Group establishments into Quintiles based on the dist of linked firms 
gen q_est = 1 if inrange(est,2,5) 
replace q_est = 2 if inrange(est,5,20) 
replace q_est = 3 if inrange(est,20,72) 
replace q_est = 4 if inrange(est,72,206) 
replace q_est = 5 if est>206

gen firm_group = firm_group2+q_emp*100+q_est
merge m:1 firm_group using "$output_NETS_control/firm_group_size.dta"
keep if _merge == 3 
drop _merge 
drop emp95 est

duplicates drop 
compress 
save "$output_NETS_control/hq_est_co_master.dta", replace

// List of HQ's in industry firm groups in 1995 
