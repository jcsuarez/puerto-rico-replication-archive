snapshot erase _all

*** Create list of codes in NAICS with decription 
set more off 
use "$ASM/ASM Stata FIles and Code/ASM_1997_2014_edited.dta", clear
keep naics naicsdef
duplicates drop 
gen deflen = length(naicsdef)
bys naics: egen max_l = max(deflen)
keep if deflen == max_l
drop max_l deflen 

tempfile naics
save "`naics'", replace

***
clear

** NAICS 97 to SIC 87 crosswalk 
	**(3 digit categories taken from the 6 to 6 digit crosswalk)
	** https://www.census.gov/prod/ec97/97x-cs3.pdf
	** "Bridge Between NAICS and SIC: 1997 Economic Census"
local a1  "201, 202, 203, 204, 205, 206, 207, 208, 209, 289, 514, 544, 546," 				//  in 311
local a2  "208, 209, 211, 212, 213, 214, 514, 738,"										//	in 312
local a3  "221, 222, 223, 224, 225, 226, 228, 229, 239, 306, 513, 738,"					//	in 313
local a4  "227, 229, 232, 233, 237, 238, 239, 306, 315, 356, 571,"  					//	in 314
local a5  "225, 231, 232, 233, 234, 235, 236, 237, 238, 239, 306, 315, 569," 			//	in 315
local a12 "302, 311, 313, 314, 316, 317, 319, 399" 										//	in 316
local a6  "242, 243, 244, 245, 249, 251, 311, 399,"										//	in 321
local a7  "261, 262, 263, 265, 267, 349, 384," 											//	in 322
local a8  "239, 273, 275, 276, 277, 278, 279, 399, 733,"								//	in 323
local a10 "291, 295, 299, 331,"															//	in 324
local a9  "281, 282, 283, 284, 285, 286, 287, 288, 289, 308, 386, 395, 399, 738,"		//	in 325
local a11 "267, 301, 305, 306, 308, 399, 753,"											//	in 326
local a13 "281, 331, 332, 333, 334, 335, 336, 339," 									//	in 331
local a14 "331, 339, 341, 343, 344, 345, 346, 347, 348, 349, 353, 354, 356, 359, 391, 399,"			//	in 332
local a15 "249, 259, 342, 343, 344, 349, 351, 352, 353, 354, 355, 356, 357, 358, 359, 363, 369, 374, 379, 382, 384," // in 333
local a16 "342, 349, 357, 359, 365, 366, 367, 369, 381, 382, 384, 387, 391, 737, 781," 			//	in 334
local a17 "335, 354, 361, 362, 364, 369, 769," 											//	in 335
local a18 "239, 306, 308, 329, 342, 346, 349, 351, 353, 358, 359, 364, 369, 361, 362, 363, 364, 365, 366, 369, 371, 372, 373, 375,"   //in 336
local a19 "242, 243, 249, 251, 252, 253, 254, 259, 308, 342, 349, 382, 384, 395, 399, 571," 		// in 337
local a20 "239, 249, 253, 259, 305, 306, 308, 313, 317, 347, 357, 363, 382, 384, 385, 391, 393, 394, 395, 396," // in 339
local a21 "321, 322, 323, 324, 325, 326, 327, 328, 329, 571" 										 // in 327

// Goal from here: make a file with this data and drop duplicate SIC codes
// For now, putting each SIC in the first NAICS category it shows up in: (dropping duplicates in order)
	// Another option is rewriting over the NAICS so that the last duplicate is kept

mata
mata clear
A = (`a1' `a2' `a3' `a4' `a5' `a6' `a7' `a8' `a9' `a10' `a11' `a12')
B = (A , `a13' `a14' `a15' `a16' `a17' `a18' `a19' `a20' `a21')
A1 = B'
st_matrix("A1",A1)
end

svmat double A1, name(sickey4)
rename sickey41 sickey4
gen id = _n

gen naics = .
local ind1 "311"
local ind2 "312"
local ind3 "313"
local ind4 "314"
local ind5 "315"
local ind6 "321"
local ind7 "322"
local ind8 "323"
local ind9 "325"
local ind10 "324"
local ind11 "326"
local ind12 "316"
local ind13 "331"
local ind14 "332"
local ind15 "333"
local ind16 "334"
local ind17 "335"
local ind18 "336"
local ind19 "337"
local ind20 "339"
local ind21 "327"

foreach i of numlist 1/21 {
foreach val of numlist `a`i'' {
di `ind`i''
di `val'
replace naics = `ind`i'' if sic == `val' & naics == .
}
}
tostring sickey4, replace

sort id
duplicates drop sic, force
drop id

merge m:1 naics using "`naics'"
drop _merge

rename sickey4 sickeyc4

tempfile indcros
save "`indcros'"

*** This manual cross walk comes from studying this file: 
* /Census CDs/Best of 92/SICKEY92.csv
*** 
/*
replace sickeyc4 = 20 if naics == 311 // This is food in both but SIC includes beverages
replace sickeyc4 = 21 if naics == 312 // This is tobacco in both but NAICS includes beverages
replace sickeyc4 = 22 if naics == 313 // Mills and mills products are in same SIC
replace sickeyc4 = 22 if naics == 314 // Mills and mills products are in same SIC
replace sickeyc4 = 23 if naics == 315 // Apparel in both 
replace sickeyc4 = 24 if naics == 321 // lumber
replace sickeyc4 = 26 if naics == 322 // paper
replace sickeyc4 = 27 if naics == 323 // printig
replace sickeyc4 = 28 if naics == 325 // chemicals
replace sickeyc4 = 29 if naics == 324 // petroleum
replace sickeyc4 = 30 if naics == 326 // rubber+plastics
replace sickeyc4 = 31 if naics == 316 // leather
replace sickeyc4 = 33 if naics == 331 // primary metal 
replace sickeyc4 = 34 if naics == 332 // primary metal 
replace sickeyc4 = 35 if naics == 333 // equipment
replace sickeyc4 = 38 if naics == 333 // put instroments with equipment
replace sickeyc4 = 35 if naics == 334 // cumpters are in equipment in SIC
replace sickeyc4 = 36 if naics == 335 // electronic equipment 
replace sickeyc4 = 37 if naics == 336 // transportation
replace sickeyc4 = 25 if naics == 337 // furniture 
replace sickeyc4 = 39 if naics == 339 // other 
replace sickeyc4 = 32 if naics == 327 // is stone and clay in SIC but nonmetallic mineral in NAICS


* For now drom mill products and replace it in the ASM data later 
drop if naics == 314
tempfile indcros
save `indcros'
*/

****** Clean old ASM data 
use "$ASM/From Census CDs/ASM_A2.dta", clear 
 
gen sic_s = sic 
tostring sic_s, replace 
gen sic_len = length(sic_s)
 
keep if sic_len == 3
keep if geot == 2 
drop geoty sic_s sic_len tradek *20 
 
rename stc2 fips_state 
rename yrc2 year 
 
tostring  sickeyc4, replace
 
merge m:1 sickey using "`indcros'"
drop _merge
 
drop *fc1 invetn90

rename empn80 numemp
rename valuen100 totvalshipT
rename payn90 annpayT
rename workersn80 avgprodwrkrs
rename hoursn80 prodwrkrshrT
rename wagesn90 prodwrkrwageT
rename valaddn100 valaddT
rename materlsn100 totcostmatT
rename capexpn90 capexT

replace year = year+1900

tempfile old_asm 
save "`old_asm'" 

****** Clean new ASM data 
use "$ASM/ASM Stata FIles and Code/ASM_1997_2014_edited.dta", clear
** Add fips codes 
keep st state
drop if st == . 
duplicates drop 
rename st fips_state 
tempfile asm_fips
save "`asm_fips'"

use "$ASM/ASM Stata FIles and Code/ASM_1997_2014_edited.dta", clear
merge m:1 state using "`asm_fips'"
drop _merge 

* Merge mills and mill products 
* replace naics = 313 if naics == 314 
drop naicsdef 
drop corptax-st

*** Append files 
append using "`old_asm'"

save "$ASM/combined_2017_21_15", replace 

// Check Data at Naics 3-digit level 
snapshot save 

// Collapse at 3-digit industry by state level 
collapse (sum) numemp (sum) annpayT (sum) avgprodwrkrs ///
		 (sum) prodwrkrwageT (sum) prodwrkrshrT (sum) totcostmatT ///
		 (sum) totvalshipT (sum) valaddT (sum) capexT, by(year naics)
drop if year ==. 
drop if capex == . 
		 
foreach var of varlist numemp-capexT { 
	gen `var'_1987 = `var' if  year == 1987 
	bys naics: egen max_`var'_1987 = max(`var'_1987) 
	replace `var' = `var'/max_`var'_1987
	drop *1987
} 

twoway scatter capex year  if naics< 326 , xline(1996) by(naics) 
graph export "$output/check_naics_1.pdf", replace 

twoway scatter capex year  if naics> 325 , xline(1996) by(naics) 
graph export "$output/check_naics_2.pdf", replace 

snapshot restore 1

// Label industries		
gen naics_2 = naics
tostring naics_2, replace 
gen industry_cd = .
replace industry_cd = 1 if naics_2 == "311"
replace industry_cd = 2 if naics_2 == "314"
replace industry_cd = 3 if naics_2 == "315"
replace industry_cd = 4 if naics_2 == "325"	// note: include 3254
replace industry_cd = 5 if naics_2 == "3254"  //
replace industry_cd = 6 if naics_2 == "326"
replace industry_cd = 7 if naics_2 == "316"
replace industry_cd = 8 if naics_2 == "332"
replace industry_cd = 9 if naics_2 == "333"
replace industry_cd = 10 if naics_2 == "335" | naics_2 == "334"
replace industry_cd = 12 if industry_cd == .

label define l_naic3 0 "Total, all industries" 1 "Food Mfg" 2 "Textile mill products" 3 "Apparel" 4 "Chemicals" 5 "Pharmaceuticals" ///
 6 "Rubber and Plastic" 7 "Leather" 8 "Fabricated metal" 9 "Machinery" 10 "Electrical equip" ///
 11 "Instruments" 12 "Other mfg" 13 "Finance, insurance, real estate" 14 "Services" ///
 15 "Wholesale and retail" 16 "Other non-mfg", replace
label values industry_cd l_naic3

drop if industry_cd == 0

// Collapse at 2-digit industry by state level 
collapse (sum) numemp (sum) annpayT (sum) avgprodwrkrs ///
		 (sum) prodwrkrwageT (sum) prodwrkrshrT (sum) totcostmatT ///
		 (sum) totvalshipT (sum) valaddT (sum) capexT, by(year state fips_state industry_cd)

preserve 
collapse (sum) numemp (sum) annpayT (sum) avgprodwrkrs ///
		 (sum) prodwrkrwageT (sum) prodwrkrshrT (sum) totcostmatT ///
		 (sum) totvalshipT (sum) valaddT (sum) capexT, by(year industry_cd) 

foreach var of varlist numemp-capexT { 
	gen `var'_1987 = `var' if  year == 1987 
	bys industry_cd: egen max_`var'_1987 = max(`var'_1987) 
	replace `var' = `var'/max_`var'_1987
	drop *1987
} 

twoway scatter capex year  , xline(1996) by(industry_cd) 
graph export "$output/check_industry_cd.pdf", replace 


*** DAN'S VERSION OF THE GRAPHS
		 		 
 snapshot restore 1
// Collapse at 3-digit industry by state level 
* Since the merge isn't perfect, combine categories
* 311 and 312 should merge
* 313-316 should merge
* 321-323, 337
* 324-326
* 332, 333, 339

gen naics2 = naics
replace naics2 = 311 if naics2 == 312
replace naics2 = 313 if naics2 == 314 | naics2 == 315 | naics == 316
replace naics2 = 321 if naics2 == 322 | naics2 == 323 | naics == 337
replace naics2 = 324 if naics2 == 325 | naics2 == 326 
replace naics2 = 332 if naics2 == 333 | naics2 == 339


collapse (sum) numemp (sum) annpayT (sum) avgprodwrkrs ///
		 (sum) prodwrkrwageT (sum) prodwrkrshrT (sum) totcostmatT ///
		 (sum) totvalshipT (sum) valaddT (sum) capexT, by(year naics2)
drop if year ==. 
drop if capex == . 
		 
foreach var of varlist numemp-capexT { 
	gen `var'_1987 = `var' if  year == 1987 
	bys naics2: egen max_`var'_1987 = max(`var'_1987) 
	replace `var' = `var'/max_`var'_1987
	drop *1987
} 



twoway scatter numemp year , xline(1996) by(naics2) 
graph export "$output/check_industry_dans_version_emp.pdf", replace 

twoway scatter capexT year , xline(1996) by(naics2) 
graph export "$output/check_industry_dans_version_capex.pdf", replace 

