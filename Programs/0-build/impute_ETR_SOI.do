// This file imputes ETR data to SOI by computing industry-by-industry 
// PR share of total investment as a parameter to impute SOI
// Author: Lysle Boller
// Date: April 25, 2020

clear all
set more off
snapshot erase _all
set seed 12345


* Save temp crosswalks for SOI industries *
use "$output/NAICS_crosswalk_SOI", clear
keep if strlen(naics_ind)==3
rename naics_ind n3
tempfile xwalk_n3
save `xwalk_n3', replace

use "$output/NAICS_crosswalk_SOI", clear
keep if strlen(naics_ind)==2
rename naics_ind n2
tempfile xwalk_n2
save `xwalk_n2', replace

********************************************************************************
* Pull in full compustat to get gross profit-based size weights for ETR calculation *
********************************************************************************
use "$data/Replication Data/interim/compu_NETS_ETR.dta", clear
xtset gvkey year
gsort gvkey year
drop n2
gen n2 = substr(n3,1,2)

merge m:1 n3 using `xwalk_n3'
drop if _merge==2
rename _merge _merge1
rename variable variable_n3 
merge m:m n2 using `xwalk_n2', keep(3) nogen // from master is nonclassifiable ind. 

replace variable_n3 = variable if variable_n3==""
drop _merge* variable
rename variable_n3 variable
tab variable
 
drop if variable=="manufact_t_" // drop firms that don't have a NAICS-3 code


* weight by variable
local weight_var "gp"
collapse (sum) `weight_var' if year==1995, by(variable PR)
*egen sum_var = sum(`weight_var'), by(variable)
by variable: egen sum_var = sum(`weight_var')

gen indshr = `weight_var' / sum_var


* Get Aggregate GP
egen gp_agg = sum(gp), by(variable)


* Clean PR shares
drop if missing(variable)
gen temp1 = indshr*(PR)
egen indshr_PR = max(temp1), by(variable)
duplicates drop variable, force
replace indshr = indshr_PR 

keep variable gp_agg indshr
order variable gp_agg indshr
tempfile pr_indshr
save "`pr_indshr'"


***********
* Impute Effective Tax Rates for SOI Industries
***********

* Import SOI data
use "$SOI/aggregate_SOI", clear
drop if variable == "manufact_t_"
drop if variable == "tot_"

sort variable year
keep if year > 1994 & year < 2008

// Merge in Compustat industry shares
merge m:1 variable using "`pr_indshr'", nogen

* Observed effective tax rates
gen p_data = (Income_Tax_After_Credits + Us_Possessions_Tax_Credit) / (Income_Subject_To_Tax)
replace p_data = (Income_Tax_After_Credits) / (Income_Subject_To_Tax) if p_data == .

* Exposed firms: industry-specific shares
gen p_new = ((Income_Tax_After_Credits + Us_Possessions_Tax_Credit) * indshr - Us_Possessions_Tax_Credit) / (Income_Subject_To_Tax * indshr)
replace p_new = p_data if p_new == .

* Exposed firms: Constant shares
gen p_new_fixp = ((Income_Tax_After_Credits + Us_Possessions_Tax_Credit)*.21 - Us_Possessions_Tax_Credit) / (Income_Subject_To_Tax*.21)
replace p_new_fixp = p_data if p_new_fixp == .

* Export file
save "$data/Replication Data/interim/SOI_imputed_ETR.dta", replace
