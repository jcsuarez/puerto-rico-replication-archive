***************************************************************
//Filename: compustat_merge_manual.do
//Author: eddie yu
//Date: 2023-09-20
//Task: With newly pulled Compustat Historical Segments data pulled 2023-07-25
//		(WRDS/compu_segments_072523.dta), we construct the following dataset in $WRDS:
//        - compustat_merge_manual.dta
//	
***************************************************************
clear
capture log close
set more off
capture close
snapshot erase _all
/*s
The basic goals of this exercise:

1. Use the existing NETS data and merge with the COMPUSTAT data by name (and year)
2. Merge NETS/C data with Historical Segments from C

3. Make ticker and date files for event studies through wrds (COMMENTED OUT)
*/

** edits since July 2018 Submission:
* added domestic capex variable, no observations dropped or added
qui{
*********************************
* Importing compustat Data
*********************************
import delimited "$WRDS/compu_base.csv", encoding(ISO-8859-1) clear
gen comp_id = _n
rename conm company
rename fyear year

* Again, trying to clean up the merge
bys company: egen m_year = max(year)
	* THIS CUTOFF throws off the ETR merge and is irrelevant at the current stage
	*keep if m_year > 1996

replace company = upper(company)
replace company = subinstr(company, " AND ", " & ",.)
replace company = subinstr(company, "  ", " ",.)
replace company = subinstr(company, "..", ".",.)
replace company = subinstr(company, "'", "",.)
replace company = subinstr(company, "INCORPORATED", "INC",.)
replace company = subinstr(company, "CORPORATION", "CORP",.)
replace company = subinstr(company, "COMPANY", "CO",.)
replace company = subinstr(company, "-CL A", "CORP",.)
replace company = subinstr(company, "CORP CORP", "CORP",.)
replace company = subinstr(company, "LIMITED", "LTD",.)
replace company = subinstr(company, "COMPANIES", "COS",.)
* trying to clean up the fuzzy merge with another required variable
gen first_l = substr(company, 1, 3)

** COMPUSTAT seems to break companies up more finely than NETS with Verizon broken in 12 groups. 
** The following makes those groups have the same name, but this induces duplicates
** Therefore, this code is commented out for now.

* dropping duplicates
drop if gvkey == 11814 // early costco
drop if gvkey == 12293 // early infinity
drop if gvkey == 61738 // second itt corp
drop if gvkey == 10142 // summit bancorp

save "$WRDS/compustat.dta", replace


**********************
* Merging Needed NETS Data
**********************
use "$output_NETS2/pr_extract_panel_v2_companybasic.dta", replace
*style: A G P & COMPANY INC

**** Defining some cuttoffs for establishments to make the merge go way better
**** Using max number of establishments (max within business across years)
bys company: egen max_est = max(num_est_total) 
qui sum max_est, d
//               r(p1) =  2
//               r(p5) =  2
//              r(p10) =  2
//              r(p25) =  3
//              r(p50) =  26
//              r(p75) =  240
*local cutoff = r(p50)
*keep if max_est > `cutoff'

gen check1 = 0
gen check2 = 0
foreach j in 16 17 18 19 20 21 22 23 24 25 26 27 28 29 30 31 32 33 34 35 36 37 38 39 40 41 42 43 44 45 46 47 48 49 50 {
replace check2 = 1 if  substr(company,-`j',1) != " "
replace company = substr(company, 1, length(company) - `j' +1) if substr(company,-`j',1) != " " & check1 == 0 
replace check1 = check2
}
drop check1 check2
replace company = upper(company)
replace company = subinstr(company, " AND ", " & ",.)
replace company = subinstr(company, "  ", " ",.)
replace company = subinstr(company, "..", ".",.)
replace company = subinstr(company, "'", "",.)
replace company = subinstr(company, "INCORPORATED", "INC",.)
replace company = subinstr(company, "CORPORATION", "CORP",.)
replace company = subinstr(company, "COMPANY", "CO",.)
replace company = subinstr(company, "-CL A", "CORP",.)
replace company = subinstr(company, "CORP CORP", "CORP",.)
replace company = subinstr(company, "LIMITED", "LTD",.)
replace company = subinstr(company, "COMPANIES", "COS",.)
replace company = ltrim(company)
replace company = rtrim(company)

* Dropping some unlisted businesses and other businesses that are not unique and not in COMPUSTAT
drop if company == "TYCO ELECTRONICS CORP"
drop if company == "DLISTED"

*gen gvkey = . 

* trying to clean up the fuzzy merge with an other required variable
gen first_l = substr(company, 1, 3)

gen nets_id = _n
tempfile nets
save "`nets'", replace

******************************
* Doing the merge
******************************
 
 * doing a perfect merge
 merge 1:1 company year using "$WRDS/compustat.dta"

 * dropping those company years that didn't successfully merge to merge again on gvkey
 drop if _merge == 2
 *** MANUALLY GATHERED GVKEYS
replace gvkey = 	1474	if company  == 	"AMERICAN HERITAGE LF INV CORP"
replace gvkey = 	1478	if company  == 	"WYETH LLC"
replace gvkey = 	1487	if company  == 	"AMERICAN INTL GROUP INC"
replace gvkey = 	1526	if company  == 	"AMERICAN NATIONAL INSURANCE CO"
replace gvkey = 	1858	if company  == 	"CEVA LOGISTICS US HOLDINGS INC"
replace gvkey = 	1891	if company  == 	"AUTOMATIC DATA PROCESSING INC"
replace gvkey = 	1920	if company  == 	"AVON PRODUCTS INC"
replace gvkey = 	1945	if company  == 	"BOC GROUP INC"
replace gvkey = 	2176	if company  == 	"BERKSHIRE HATHAWAY INC"
replace gvkey = 	2410	if company  == 	"BP AMERICA INC"
replace gvkey = 	2439	if company  == 	"BROWNING-FERRIS INDUSTRIES INC"
replace gvkey = 	2797	if company  == 	"CARTER-WALLACE INC"
replace gvkey = 	2940	if company  == 	"MAGELLAN HEALTH SERVICES INC"
replace gvkey = 	3016	if company  == 	"CHRISTIANA COS INC"
replace gvkey = 	3121	if company  == 	"CLOROX CO"
replace gvkey = 	3130	if company  == 	"EL PASO CGP CO LLC"
replace gvkey = 	3221	if company  == 	"AON CORP"
replace gvkey = 	3275	if company  == 	"COMMUNICATIONS SYSTEMS INC"
replace gvkey = 	3955	if company  == 	"DIGITAL EQUIPMENT CORP"
replace gvkey = 	3980	if company  == 	"WALT DISNEY CO"
replace gvkey = 	4060	if company  == 	"DOW CHEMICAL CO"
replace gvkey = 	4066	if company  == 	"OMNICOM GROUP INC"
replace gvkey = 	4199	if company  == 	"EATON CORP"
replace gvkey = 	4218	if company  == 	"EDISON BROTHERS STORES INC"
replace gvkey = 	4600	if company  == 	"FEDERAL-MOGUL CORP"
replace gvkey = 	4679	if company  == 	"FIRST AMERICAN CORP"
replace gvkey = 	5180	if company  == 	"GLAXOSMITHKLINE HOLDINGS"
replace gvkey = 	5439	if company  == 	"HALLIBURTON ENERGY SERVICES"
replace gvkey = 	5723	if company  == 	"STARWOOD HOTELS & RESORTS"
replace gvkey = 	5764	if company  == 	"HUBBELL INC"
replace gvkey = 	5878	if company  == 	"ILLINOIS TOOL WORKS INC"
replace gvkey = 	5959	if company  == 	"INGERSOLL-RAND CO"
replace gvkey = 	6127	if company  == 	"ENRON CREDITORS RECOVERY CORP"
replace gvkey = 	6136	if company  == 	"INTERPUBLIC GROUP OF"
replace gvkey = 	6196	if company  == 	"ANIXTER INTERNATIONAL INC"
replace gvkey = 	6376	if company  == 	"KELLY SERVICES INC"
replace gvkey = 	6730	if company  == 	"ELI LILLY & CO"
replace gvkey = 	6972	if company  == 	"MAKITA USA INC"
replace gvkey = 	7065	if company  == 	"MARSH & MCLENNAN COS INC"
replace gvkey = 	7241	if company  == 	"CVS CAREMARK CORP"
replace gvkey = 	7257	if company  == 	"MERCK & CO INC"
replace gvkey = 	7471	if company  == 	"MITSUI & CO USA INC"
replace gvkey = 	7637	if company  == 	"MYLAN INC"
replace gvkey = 	7652	if company  == 	"NEC USA INC"
replace gvkey = 	7679	if company  == 	"NALCO CO"
replace gvkey = 	8334	if company  == 	"PAR TECHNOLOGY CORP"
replace gvkey = 	8424	if company  == 	"PEERLESS TUBE CO"
replace gvkey = 	8431	if company  == 	"AMERICAN FINANCIAL CORP"
replace gvkey = 	8488	if company  == 	"APPLIED BIOSYSTEMS LLC"
replace gvkey = 	8549	if company  == 	"CONOCOPHILLIPS CO"
replace gvkey = 	8579	if company  == 	"PIER 1 IMPORTS INC"
replace gvkey = 	8681	if company  == 	"NORTH HILLS SIGNAL PROC CORP"
replace gvkey = 	8734	if company  == 	"PRESIDENTIAL REALTY CORP"
replace gvkey = 	8898	if company  == 	"RAYMOND JAMES FINANCIAL INC"
replace gvkey = 	9023	if company  == 	"REGIS CORP"
replace gvkey = 	9069	if company  == 	"AQUA ALLIANCE INC"
replace gvkey = 	9203	if company  == 	"ROCKWELL AUTOMATION INC"
replace gvkey = 	9332	if company  == 	"SPS TECHNOLOGIES LLC"
replace gvkey = 	9602	if company  == 	"SENSORMATIC ELECTRONICS LLC"
replace gvkey = 	9715	if company  == 	"SYMMETRICOM INC"
replace gvkey = 	10000	if company  == 	"STANDARD MOTOR PRODUCTS INC"
replace gvkey = 	10006	if company  == 	"PITTWAY CORP"
replace gvkey = 	10097	if company  == 	"STORAGE TECHNOLOGY CORP"
replace gvkey = 	10195	if company  == 	"SUPERIOR INDUSTRIES INTL INC"
replace gvkey = 	10286	if company  == 	"TII NETWORK TECHNOLOGIES INC"
replace gvkey = 	10484	if company  == 	"CONTINENTAL ARLN HOLDINGS INC"
replace gvkey = 	10787	if company  == 	"TYCO INTERNATIONAL (US) INC"
replace gvkey = 	10846	if company  == 	"UNILEVER UNITED STATES INC"
replace gvkey = 	10984	if company  == 	"SPRINT NEXTEL CORP"
replace gvkey = 	11032	if company  == 	"UNIVERSAL HEALTH SERVICES INC"
replace gvkey = 	11213	if company  == 	"VOLT INFORMATION SCIENCES INC"
replace gvkey = 	11376	if company  == 	"WEST PHARMACEUTICAL SVCS INC"
replace gvkey = 	11600	if company  == 	"WORTHINGTON INDUSTRIES INC"
replace gvkey = 	12053	if company  == 	"EMC CORP"
replace gvkey = 	12151	if company  == 	"COTY INC"
replace gvkey = 	12206	if company  == 	"DIRECTV"
replace gvkey = 	12484	if company  == 	"ADELPHIA COMMUNICATIONS CORP"
replace gvkey = 	12633	if company  == 	"CORRECTIONS CORP AMERICA"
replace gvkey = 	13070	if company  == 	"SYSTEM SOFTWARE ASSOCIATES DEL"
replace gvkey = 	13155	if company  == 	"MARGO CARIBE INC"
replace gvkey = 	13339	if company  == 	"ONE PRICE CLOTHING STORES INC"
replace gvkey = 	13552	if company  == 	"INTERSTATE GENERAL CO LP"
replace gvkey = 	13554	if company  == 	"TIMBERLAND CO"
replace gvkey = 	13590	if company  == 	"CHILDRENS PLACE RET STORES INC"
replace gvkey = 	13665	if company  == 	"HAMILTON BANCORP INC"
replace gvkey = 	13866	if company  == 	"MAYORS JEWELERS INC"
replace gvkey = 	13988	if company  == 	"CHARLES SCHWAB CORP"
replace gvkey = 	14261	if company  == 	"SMITHKLINE BEECHAM AMERICAS"
replace gvkey = 	14620	if company  == 	"ELECTROLUX NORTH AMERICA INC"
replace gvkey = 	14960	if company  == 	"LABORATORY CORP AMER HOLDINGS"
replace gvkey = 	15133	if company  == 	"APOGENT TECHNOLOGIES INC"
replace gvkey = 	15261	if company  == 	"DORAL FINANCIAL CORP"
replace gvkey = 	15334	if company  == 	"AKZO NOBEL INC"
replace gvkey = 	15576	if company  == 	"DEUTSCHE BNK AMRCAS HOLDG CORP"
replace gvkey = 	15639	if company  == 	"SEDGWICK GROUP INC"
replace gvkey = 	16603	if company  == 	"NESTLE HOLDINGS INC"
replace gvkey = 	17874	if company  == 	"T-MOBILE USA INC"
replace gvkey = 	19043	if company  == 	"MITSUBISHI ELC US HOLDINGS INC"
replace gvkey = 	20126	if company  == 	"ADVANCED DRAINAGE SYSTEMS INC"
replace gvkey = 	20196	if company  == 	"LUXOTTICA USA INC"
replace gvkey = 	20496	if company  == 	"ATENTO USA INC"
replace gvkey = 	21320	if company  == 	"CLANCY SYSTEMS INTL INC"
replace gvkey = 	24379	if company  == 	"MGIC INVESTMENT CORP"
replace gvkey = 	24708	if company  == 	"ATLANTIC TELE-NETWORK INC"
replace gvkey = 	25000	if company  == 	"POLYMEDICA CORP"
replace gvkey = 	25399	if company  == 	"HAMPSHIRE GROUP LTD"
replace gvkey = 	25648	if company  == 	"ROCHE HOLDINGS INC"
replace gvkey = 	25761	if company  == 	"TOMMY HILFIGER USA INC"
replace gvkey = 	25972	if company  == 	"ALLIED HEALTHCARE INTL INC"
replace gvkey = 	27937	if company  == 	"PACIFIC SUNWEAR CALIFORNIA INC"
replace gvkey = 	28398	if company  == 	"SUNGLASS HUT INTERNATIONAL"
replace gvkey = 	28838	if company  == 	"CREDIT SUISSE HOLDINGS USA INC"
replace gvkey = 	28930	if company  == 	"MARRIOTT INTERNATIONAL INC"
replace gvkey = 	29789	if company  == 	"ARCADIS US INC"
replace gvkey = 	29804	if company  == 	"INVESCO LTD"
replace gvkey = 	29878	if company  == 	"DRYPERS CORP"
replace gvkey = 	30059	if company  == 	"AMERICAN EAGLE OUTFITTERS INC"
replace gvkey = 	30490	if company  == 	"APARTMENT INVESTMENT & MGT CO"
replace gvkey = 	61056	if company  == 	"RIVER HAWK AVIATION INC"
replace gvkey = 	61322	if company  == 	"EQUUS GAMING CO LP"
replace gvkey = 	61445	if company  == 	"INTIMATE FASHIONS INC"
replace gvkey = 	61483	if company  == 	"DAVITA INC"
replace gvkey = 	61552	if company  == 	"LEXMARK INTERNATIONAL INC"
replace gvkey = 	61574	if company  == 	"WATERS CORP"
replace gvkey = 	62396	if company  == 	"IDT CORP"
replace gvkey = 	63252	if company  == 	"REMEDYTEMP INC"
replace gvkey = 	63643	if company  == 	"ABERCROMBIE & FITCH CO"
replace gvkey = 	63661	if company  == 	"INTELLIGROUP INC"
replace gvkey = 	63766	if company  == 	"SABRE HOLDINGS CORP"
replace gvkey = 	64028	if company  == 	"NU SKIN ENTERPRISES INC"
replace gvkey = 	64321	if company  == 	"METRO INFORMATION SERVICES"
replace gvkey = 	65148	if company  == 	"TRAILER BRIDGE INC"
replace gvkey = 	65527	if company  == 	"HYPERCOM CORP"
replace gvkey = 	65665	if company  == 	"OAO TECHNOLOGY SOLUTIONS INC"
replace gvkey = 	65812	if company  == 	"ECHO THERAPEUTICS INC"
replace gvkey = 	66061	if company  == 	"DOLLAR THRIFTY AUTO GROUP INC"
replace gvkey = 	66062	if company  == 	"PASSPORT BRANDS INC"
replace gvkey = 	100080	if company  == 	"BAYER CORP"
replace gvkey = 	100862	if company  == 	"ADECCO USA INC"
replace gvkey = 	101204	if company  == 	"SANOFI-AVENTIS US INC"
replace gvkey = 	101352	if company  == 	"ALCATEL-LUCENT USA INC"
replace gvkey = 	111940	if company  == 	"FIRST BANCORP"
replace gvkey = 	113373	if company  == 	"STONEPATH GROUP INC"
replace gvkey = 	113490	if company  == 	"CROWN CASTLE INTL CORP"
replace gvkey = 	122575	if company  == 	"LIBERTY MUTUAL INSURANCE CO"
replace gvkey = 	124319	if company  == 	"FOUNDRY NETWORKS INC"
replace gvkey = 	140722	if company  == 	"REGUS CORP"
replace gvkey = 	142466	if company  == 	"APS HEALTHCARE INC"
replace gvkey = 	143357	if company  == 	"ACCENTURE INC"
replace gvkey = 	144496	if company  == 	"UBS AMERICAS INC"
replace gvkey = 	147988	if company  == 	"AECOM INC"
replace gvkey = 	148213	if company  == 	"CONSOLIDATED CONTAINER CO"
replace gvkey = 	148372	if company  == 	"G+G RETAIL INC"
replace gvkey = 	176082	if company  == 	"PHARMA-BIO SERV INC"
replace gvkey = 	178519	if company  == 	"LPL INVESTMENT HOLDINGS INC"
replace gvkey = 	186858	if company  == 	"ARAMARK HOLDINGS CORP"
replace gvkey = 	198058	if company  == 	"MICHAEL KORS (USA) INC"
replace gvkey = 	212782	if company  == 	"FRESENIUS MEDICAL CARE"

snapshot save 

keep if _merge == 1
drop _merge
***** DROPPING NETS OBS THAT DO NO MATCH WITH COMPUSTAT (INCLUDING YEARS MISMATCH)
drop if gvkey == .
drop datadate indfmt consol popsrc datafmt tic cusip acctchg acctstd curcd fyr act invt ni exchg costat fic addzip county naics comp_id m_year
merge 1:1 gvkey year using "$WRDS/compustat.dta"

gen new = 1
keep if _merge == 3
drop _merge
tempfile flat_merge
save "`flat_merge'", replace
 
snapshot restore 1 
keep if _merge == 3
drop _merge
append using "`flat_merge'"

*7888 matched firm years, merging back in the rest of the compustat database 
* (checked that this does not overwrite anything)
merge 1:1 gvkey year using "$WRDS/compustat.dta"
replace new = 0 if new == .
drop _merge

***** CHECKING HERE FOR LOSING OBSERVATIONS THAT SHOULD BE USED IN CRSP EVENT STUDY
/*
snapshot save
drop if max_est == .
duplicates drop hqduns, force
 ** counts 429 firms, more than the working version as of 1-9-2018
	* why? (1) not restricted to 1996 and (2) not restricted to having segments data (3) counting non-prePR
* SOLUTION: save the CRSP input file before merging with the historical segments
* NOTE: turns out only to add 5 obsevervations to the total CRSP pull
*/
**

bys gvkey: egen max_est_PR_pre = max((year<1997)*num_est_PR) 
gen prePR = 1 if max_est_PR_pre > 0 & max_est_PR_pre != .
replace prePR = 0 if max_est_PR_pre == 0
	
save "`flat_merge'", replace
rename datadate datadate1
gen datadate = date(datadate1, "MDY")
drop datadate1

save "$WRDS/compustat_full_base.dta", replace

* Exporting sample for WRDS event study download
set more off
keep if prePR == 1
duplicates drop hqduns, force
keep tic cusip

duplicates drop

export delimited cusip using "$WRDS/PRevent_manual.txt" ,  delimiter(" ") replace novar
}
*******************************************************
* Merging with Historical Segments + creating vars
*******************************************************
/* * save EIN data to be merged
use "$WRDS/compu_ein.dta", clear
collapse (firstnm) ein, by(gvkey)
destring gvkey, replace
tempfile ein
save "`ein'", replace */

use "$WRDS/compu_segments.dta", replace
// use "$WRDS/compu_segments.dta", replace

gen year = year(datadate)
destring gvkey, replace
rename datadate datadate1
 
tempfile hist_segs
save "`hist_segs'", replace

use "`flat_merge'", replace
drop if gvkey == .
merge 1:m gvkey year using "`hist_segs'"

drop if _merge != 3

drop if geotp=="1"
drop if geotp==""

* generating a dummy variable for those with establishments in PR
gen PR = 0
replace PR = 1 if num_est_PR != .

* Collapsing geographic information
collapse (sum) capxs emps ias nis revts sales salexg (firstnm) cik cusip company ///
 tic naicsh PR prePR new emp_US sales_US num_est_US num_est_PR emp_PR sales_PR ///
 emp_total sales_total, by(gvkey year geotp)

gen domestic = (geotp=="2")
gen foreign = (geotp=="3")

* Employment
gen for_emp = emps if foreign==1
bysort gvkey year: egen cfor_emp = min(for_emp)
replace cfor_emp=0 if cfor_emp==.
bysort gvkey year: egen tot_emp = sum(emps)
gen fs_emp = cfor_emp/tot_emp

* Net Income
gen for_ni = nis if foreign==1
bysort gvkey year: egen cfor_ni = min(for_ni)
replace cfor_ni=0 if cfor_ni==.
bysort gvkey year: egen tot_ni = sum(nis)
gen fs_ni = cfor_ni/tot_ni

* revenue
gen for_rev = revts if foreign==1
bysort gvkey year: egen cfor_rev = min(for_rev)
replace cfor_rev=0 if cfor_rev==.
bysort gvkey year: egen tot_rev = sum(revts)
gen fs_rev = cfor_rev/tot_rev
// replace fs_rev = . if fs_rev < 0 // doesn't make any change

* Capex
gen for_capx = capxs if foreign==1
gen dom_capx = capxs if foreign==0
bysort gvkey year: egen cfor_capx = min(for_capx)
replace cfor_capx=0 if cfor_capx ==.
bysort gvkey year: egen tot_capx = sum(capxs)
gen fs_capx = cfor_capx/tot_capx 

* Total Assets
gen for_ias = ias if foreign==1
gen dom_ias = ias if foreign==0
bysort gvkey year: egen cfor_ias = min(for_ias)
replace cfor_ias=0 if cfor_ias ==.
bysort gvkey year: egen tot_ias = sum(ias)
gen fs_ias = cfor_ias/tot_ias 

* Sales
gen for_sales = sales if foreign==1
gen dom_sales = sales if foreign==0
bysort gvkey year: egen cfor_sales = min(for_sales)
replace cfor_sales=0 if cfor_sales ==.
bysort gvkey year: egen tot_sales = sum(sales)
gen fs_sales = cfor_sales/tot_sales 

* Collapsing to firm-years
collapse (sum) capxs emps ias nis revts sales salexg dom_capx (firstnm) cik cusip ///
company naicsh tic PR prePR new fs_emp fs_ni fs_capx fs_rev fs_ias fs_sales emp_US ///
sales_US num_est_US num_est_PR emp_PR sales_PR emp_total sales_total tot_emp tot_ni ///
cfor_capx tot_capx tot_ias, by(gvkey year)

// foreach var in emp_US emp_PR sales_US sales_PR nis revts dom_capx fs_emp fs_ni fs_capx fs_rev {
// 	tab year, summ(`var')
// }

	/*
bys gvkey: egen max_est_PR_pre = max((year<1997)*num_est_PR) 
gen prePR = 1 if max_est_PR_pre > 0 & max_est_PR_pre != .
replace prePR = 0 if max_est_PR_pre == 0	
	*/
	
save "$WRDS/compustat_merge_manual.dta", replace
