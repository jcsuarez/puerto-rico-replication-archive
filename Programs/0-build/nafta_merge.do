cd "$additional/nafta/dataverse_files"
use "locvul_mexrca_fixed.dta", clear

cd "$xwalk"
merge 1:m conspuma using consp2cty9-22-13.dta
drop if _merge != 3								// Dan: changed to "drop" since keeping only those that didn't make sense 2-19-2018
drop _merge

cd "$output"
gen fips_county =  state_fips*1000+county_fips 
keep fips_county locdt_noag
duplicates drop 
save import_hakobyanlaren_nafta.dta, replace
