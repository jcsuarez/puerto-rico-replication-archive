clear
set more off
************ 2-19-2018 SAVE PERCENT PHARMA
* Extract QCEW files for PR project

clear all
set more off

local i = 1
tempfile inter
foreach year of numlist 1995 {
di `year'

import delimited "$data/QCEW/raw_annual/`year'.annual.singlefile.csv", clear rowrange(1) colrange (1:12)

keep if strlen(industry_code) == 4

destring(area_fips), replace force

gen fips_state = floor(area_fips/1000)
gen fips_county = area_fips - fips_state*1000
drop if area_fips == .

drop disclosure_code

if `i' == 1 save `inter', replace
	
if `i' > 1 {
	append using `inter'
	save  `inter', replace
}
	
	local i = `i'+1

}

destring industry_code, replace
drop if industry_code < 1111

foreach Q of var annual_avg_estabs annual_avg_emplvl total_annual_wages {
gen pharm_`Q' = `Q' * (industry_code == 3254)  
bys area_fips: egen tot_`Q' = sum(`Q')
replace pharm_`Q' = pharm_`Q' / tot_`Q' 
drop tot_`Q'
}

drop if fips_county == 0 // drops states

collapse (sum) pharm_*, by(area_fips)

rename area_fips fips
drop if fips == .
save "$data/QCEW/extract_qcew_pharm.dta", replace
