***************************************************************
//Filename: table2_clean.do
//Author: eddie yu
//Date: 2023-09-20
//Task: This script takes pr_extract_est.dta, etc. and creates Replication Data/NETS_emp.dta
**************************************************************

/* Data sources:
-NETS
*/

clear all
set more off 
snapshot erase _all

************************************
* Grabbing and organizing the data
************************************
*** get list of hqduns that actually work by dropping gov't etc... 
use "$output_NETS/pr_extract_est.dta", clear

gen naic4 = floor(naics95/100)
gen naic3 = floor(naics95/1000)
gen naic2 = floor(naics95/10000)

********************
*getting rid of governmental and social non-profit orgs
********************
drop if naic2 == 92 | naic3 == 813
* dropping USPS and the federal government
drop if hqduns95 == 3261245 | hqduns95 == 161906193

* dropping firms with HQ in PR
drop if pr_based == 1 

* dropping county industries with no link
drop if pr_link == 0
bysort hqduns: egen temp_max = max(emp95)
gen temp_keep = (emp95 == temp_max)

keep if temp_keep == 1

* making a list of hqduns to use from the panel data
keep hqduns naic3
bysort hqduns: egen m_naic3 = mode(naic3), minmode
drop naic3
duplicates drop 
tempfile tokeep 
save "`tokeep'"

** old code 
use "$output_NETS/pr_extract_panel.dta", clear 

** Keep only firms at start of the sample 
gen temp = emp_PR if year == 1990
bysort hqd: egen base_emp_PR = min(temp)
drop temp 
keep if base > 0  // This line drops 4,991 observations

** MErge in to keep 
merge m:1 hqduns95 using "`tokeep'"
keep if _merge == 3 // This drops 23 observations from the master, 214 from using

egen firm = group(hqd) 
sum firm, d
// snapshot save

keep hqd year emp_US num_est_US m_naic3
gen PR = 1 

tempfile PRdata
save "`PRdata'" 

/// Get control data 
* Get control Data 
use "$output_NETS_control/hq_est_co_firm_master.dta", clear
drop if emp95 < 6
drop if num_est95 < 3

*sample 1000, count by(firm_group)
keep if fipsstate95 != . 
drop if fipsstate95 > 56
rename fipsstate95 hqfips
 
gen firm_group2 = floor(firm_group/10000)
gen census_div = floor(firm_group2/1000)
gen naic3 = firm_group2-1000*census_div
drop firm_group2
gen PR = 0 

rename naic3 m_naic3
drop hqfips census_div  PR firm_group 

foreach y in 90 91 92 93 94 95 96 97 98 99 00 01 02 03 04 05 06 07 08 09 10 11 12 {
	rename emp`y' emp_`y'
	rename num_est`y' num_`y'
}

reshape long emp_ num_ , i(hqduns m_nai) j(year) string
 
destring year , replace 
replace year = year+1900 if year > 50 
replace year = year+2000 if year < 50 
sort year
rename emp_ emp_US
rename num_ num_est_US
gen PR = 0 

append using "`PRdata'"

************************************************
* major sectors and industries (from SOI, matching compustat regressions)
************************************************

tostring m_naic3, gen(n3)
gen naic2 = substr(n3,1,2)
gen major_sec = inlist(naic2,"22","31","32","33","48","49")
gen major_ind = ~inlist(n3,"324","323","337","322","327","336","331","321") * major_sec
drop naic2 n3


// Handle duplicates
duplicates tag hqduns year, gen(dup_tag)
tab dup_tag PR if year==1995
sort hqduns year

*browse if dup_tag==1

* Assign PR status to firms with PR indicator, take average employment
egen PR_PH1 = max(PR), by(hqduns95 year)
egen emp_PH1 = mean(emp_US), by(hqduns95 year)
egen est_PH1 = mean(num_est_US), by(hqduns95 year)

replace emp_US = emp_PH1
replace num_est_US = est_PH1
replace PR = PR_PH1
drop *_PH1

duplicates drop hqduns year, force


// emp growth 
gen temp = emp_US if year == 1995
bysort hqduns95 : egen base_emp = max(temp)
drop temp

gen emp_growth = (emp_US - base_emp)/base_emp
replace emp_growth = 0 if emp_growth == . 

// estab growth 
gen temp = num_est_US if year == 1995
bysort hqduns95 : egen base_est = max(temp)
drop temp

gen est_growth = (num_est_US - base_est)/base_est
replace est_growth = 0 if est_growth == . 


drop if base_emp ==0 
sum base_emp if year == 1995 & PR , d
gen wgt=base_emp/( r(mean)) if PR 

sum base_emp if year == 1995 & PR==0 , d
replace wgt=base_emp/( r(mean)) if PR==0 

winsor wgt, p(.01) g(w_wgt) 

** Make balanced panel by dropping firms that drop out
bys hqduns95 : egen min_emp = min(emp_US) 
drop if min_emp == 0 


** Size Controls **
xtile emp_bins20 = base_emp, n(20)
xtile emp_bins10 = base_emp, n(10)
xtile emp_bins3 = base_emp, n(3)

** Regression setup **
	xi i.year|PR, noomit 
	drop _IyeaXP*1995

set matsize 4000


tostring m_naic3, gen(naic2)
replace naic2 = substr(naic2, 1,2)
replace naic2 = substr(naic2,1,1) if substr(naic2,1,1)=="3"
destring naic2, replace



* Look at distribution
tab emp_bins20 PR if year==1995, sum(base_emp)



save "$data/Replication Data/interim/NETS_emp.dta", replace
