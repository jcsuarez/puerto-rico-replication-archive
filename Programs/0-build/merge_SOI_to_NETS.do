// This file estimates semi-elasticities with SOI-estimated tax changes
// includes industry-level variation
// Author: Lysle Boller
// Date: April 25, 2020

clear all
set more off
snapshot erase _all
set seed 12345

* clean xwalk data to match wit NETS_emp
use "$output/NAICS_crosswalk_SOI", clear
ren naics_ind n3
gen n2 = substr(n3, 1, 2)
tempfile xwalk
save `xwalk', replace

**********************************************************
********* Merge with NETS Data: Set Sample First *********
**********************************************************
use "$data/Replication Data/interim/NETS_emp.dta", clear

*** Now merge to SOI data to get tax_impute var ***
tostring m_naic3, gen(n3)
* Encode NAICS2 as a string
tostring naic2, gen(n2string)
drop naic2
rename n2string n2
* Merge in the SOI industry definitions
merge m:1 n3 using `xwalk'
rename _merge _merge_n3
frame create naics2_crosswalk
frame naics2_crosswalk: use `xwalk'
frame naics2_crosswalk: drop if strlen(n3) == 3
frame naics2_crosswalk: tab n2 // Check unique
frlink m:1 n2, frame(naics2_crosswalk)
frget variable_n2 = variable, from(naics2_crosswalk)
replace variable = variable_n2 if variable == ""
drop _merge*
* Merge in the imputed tax rates
merge m:1 year variable using "$data/Replication Data/interim/SOI_imputed_ETR.dta"

**********************************************************************************
* Fill missing ETRs with earliest and latest available imputed ETR for each firm *
**********************************************************************************

* Drop groups with no match in the SOI data
bys hqduns95: egen has_etr = total(p_data)
keep if has_etr > 0
drop has_etr

* Impute pre-period with earliest tax rate available
foreach var of var p_data p_new p_new_fixp {
gen byte missing_`var' = missing(`var')
bys hqduns95 (missing_`var' year) : gen first_`var' = `var'[1]
bys hqduns95 (year) : gen missing_pre_`var' = sum(1 - missing_`var') == 0
bys hqduns95 (year) : replace `var' = first_`var' if missing_pre_`var' == 1
gen ok = 1 - missing_`var'
bys hqduns95 (ok year) : gen last_`var' = `var'[_N]
bys hqduns95 (year) : replace `var' = last_`var' if `var' == .
drop ok
drop missing_`var'
drop missing_pre_`var'
}

/* * Looks like NAICS code 337 is missing year 1997 for some reason
frame put _all, into(temp)
frame temp: keep if p_data == .
frame temp: desc */

* Compute counterfactual rates
gen tax_impute = p_data
replace tax_impute = p_new if PR
replace tax_impute = tax_impute * 100


gen tax_impute2 = p_data
replace tax_impute2 = p_new_fixp if PR
replace tax_impute2 = tax_impute2 * 100


** below is already implemented in clean_compu_analyis.do - DFL_ppe weight construction
// ********************************************************************************
// ***************************** Export for analysis ******************************
// ********************************************************************************
// * some DFL weights first, then drop (for entropy balancing)
// destring n2, replace
// xtile ppe95_binDFL = ppent if year == 1995 , n(5) 
// logit PR i.n2##i.ppe95_binDFL if year == 1995
// drop phat min_phat w_phat
// predict phat, pr 
// *winsor phat, p(.01) g(w_phat) 
// *replace phat = w_phat
// bys gvkey: egen min_phat = min(phat) 
// * ATOT
// gen DFL2 = (PR+(1-PR)*min_phat/(1-min_phat))


tab year PR, sum(tax_impute)

tostring m_naic3, gen(naic2)
replace naic2 = substr(naic2, 1,2)
replace naic2 = substr(naic2,1,1) if substr(naic2,1,1)=="3"
destring naic2, replace


save "$data/Replication Data/interim/NETS_emp_SOI_imputed.dta", replace

** Now do DFL
xtile binDFL = base_emp if year == 1995 , n(20) 
logit PR i.naic2##i.binDFL if year == 1995
capture: drop phat min_phat w w_phat DFL?
predict phat, pr 
*winsor phat, p(.01) g(w_phat) 
*replace phat = w_phat
bys hqduns95: egen min_phat = min(phat) 
* ATOT
gen DFL2 = wgt*(PR+(1-PR)*min_phat/(1-min_phat))

tab binDFL PR if year==1995, sum(base_emp)

* pre/post1/post2 for Long Difference
local start_break_year = 2004
local end_break_year   = 2012
capture: drop pre post? ET_*
gen pre = (year<1995)
gen post1 = (year>1995)*(year<=`start_break_year')
gen post2 = (year>`start_break_year')*(year<=`end_break_year')
gen post3 = (year!=1995) * (pre==0) * (post1==0) * (post2==0)


foreach var in pre post1 post2 post3  {
	gen ET_`var' = (`var'==1)*PR
}

save "$data/Replication Data/interim/NETS_emp_SOI_DFL.dta", replace


