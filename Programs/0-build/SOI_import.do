clear
set more off

// importing some basic IRS SOI stats about US possessions tax credit 
// author: dan
// date: 11/29/2017

// keeping a few variables for each business category:
	* Number of Returns
	* Total Reciepts
	* Taxes paid
	* net income 
	* income subject to tax
	* income tax, total 
	* regular tax
	* personal holding compny tax
	* recapture taxes
	* alternative minimum tax
	* environmental tax
	* Foreign tax credit
	* Possessions tax credit (MAIN)
	* non-conventional source fuel credit
	* General business credit
	* prior year minimum tax credit
	* income tax after credits
ssc install sxpose
	
* establishing some locals for some loops (big changes at 1998, 2002, 2007)
local file1994 "94CO07MI.xlsx"
local file1995 "CRTAB07.XLS"
local file1996 "CRTB07.xlsx"
local file1997 "TABL7.xlsx"
local file1998 "98co07nr.xlsx"
local file1999 "99co07nr.xlsx"
local file2000 "00co07nr.xlsx"
local file2001 "01co07nr.xlsx"
local file2002 "02co07nr.xlsx"
local file2003 "03co07nr.xlsx"
local file2004 "04co07ccr.xlsx"
local file2005 "05co07ccr.xlsx"
local file2006 "06co07ccr.xlsx"
local file2007 "07co07ccr.xlsx"
local file2008 "08co07ccr.xlsx"
local file2009 "09co07ccr.xlsx"
local file2010 "10co07ccr.xlsx"

* Starting cell for each file
local s1994 "A13"	
local s1995 "A14"	
local s1996 "A13"	
local s1997 "A13"	
local s1998 "A13"	
local s1999 "A14"		
local s2000 "A14"		
local s2001 "A14"	
local s2002 "A14"	
local s2003 "A14"	
local s2004 "A15"	
local s2005 "A15"	
local s2006 "A15"	
local s2007 "A15"	
local s2008 "A14"	
local s2009 "A15"	
local s2010 "A14"

*drop range for each file 
local d1994 "19/32"
local d1995 "18/103"
local d1996 "18/103"
local d1997 "18/103"
local d1998 "18/103"
local d1999 "17/101"
local d2000 "17/101"
local d2001 "15/9022"
local d2002 "15/9021"
local d2003 "15/9023"
local d2004 "15/9022"
local d2005 "15/9022"
local d2006 "14/9021"
local d2007 "13/13"
local d2008 "13/13"
local d2009 "13/14"
local d2010 "13/9019"

* rows for each file: tot, ag, mining, const, manu, manusubs, trans, wholesale, ret, fin, service
local k1994 "A B C D J N-R T-AB AD-AK AM AR AW BH BQ"
local k1995 "A B C D J N-R T-AB AD-AK AM AR AW BH BQ"
local k1996 "A B C D J N-R T-AB AD-AK AM AR AW BH BQ"
local k1997 "A B C D J N-R T-AB AD-AK AM AR AW BH BQ"
local k1998 "A B C G H J N-R T-AA AC-AJ AM AP BF BM BS BY CC CD CE CI CJ CN CR CU"
local k1999 "A B C G H J N-R T-AA AC-AJ AM AP BF BM BS BY CC CD CE CI CJ CN CR CU"
local k2000 "A B C G H J N-R T-AA AC-AJ AM AP BF BM BS BY CC CD CE CI CJ CN CR CU"
local k2001 "A B C G H J N-R T-AA AC-AJ AM AP BF BM BS BY CC CD CE CI CJ CN CR CU"
local k2002 "A B C G H I N-S U-AB AD-AJ AM AQ BG BO BX CC CH CI CJ CM CO CS CW CZ"
local k2003 "A B C G H I N-S U-AB AD-AJ AM AQ BG BO BX CC CH CI CJ CM CO CS CW CZ"
local k2004 "A B C G H I N-S U-AB AD-AJ AM AQ BG BO BX CC CH CI CJ CM CO CS CW CZ"
local k2005 "A B C G H I N-S U-AB AD-AJ AM AQ BG BO BX CC CH CI CJ CM CO CS CW CZ"
local k2006 "A B C G H I N-S U-AB AD-AJ AM AQ BG BO BX CC CH CI CJ CM CO CS CW CZ"
local k2007 "A B C G H I M-AG AI AM BA BH BO BT BX BY BZ CC CD CH CK CN "
local k2008 "A B C G H I M-AG AI AM BA BH BO BT BX BY BZ CC CD CH CK CN "
local k2009 "A B C G H I M-AG AI AM BA BH BO BT BX BY BZ CC CD CH CK CN "
local k2010 "A B C G H I M-AG AI AM BA BH BO BT BX BY BZ CC CD CH CK CN "



** Works for 1994-1997
foreach year of numlist 1994/1997 {
import excel "$SOI/`file`year''", clear cellra(`s`year'')
drop in 2/32
drop in 3/22
drop in 4/16
drop in `d`year''
form A %25s
destring B-BQ, replace ignore("*-," "[]")

keep `k`year''
gen year = `year'

tempfile int_`year'
save "`int_`year''", replace
}
*
use "`int_1994'", clear
append using "`int_1995'", force
append using "`int_1996'", force
append using "`int_1997'", force

recast str50 A, force
* RENAME VARS TO MATCH ACROSS YEARS
rename A category
rename B tot
rename C ag_t
rename D mining_t
*rename H utilities_t
rename J construction_t
rename N manufact_t
rename O man_food_bev
rename P man_tobacco
rename Q man_textile
rename R man_apparel
rename T man_wood
rename U man_furniture
rename V man_paper 
rename W man_printing
rename X man_chemicals
rename Y man_petrol
rename Z man_plastics
rename AA man_leather
rename AB man_minerals
rename AD man_primary_metals
rename AE man_fab_metal
rename AF man_machines
rename AG man_elec_components
rename AH man_motor_vehicles
rename AI man_trans
rename AJ man_instruments
rename AK man_misc
rename AM transport_t
rename AR wholesale_t
rename AW retail_t
rename BH fin_real_estate_management_t
rename BQ all_services_t

tempfile int1
save "`int1'", replace

* extra locals

local ea1998 "33"
local eb1998 "22"
local ec1998 "16"

local ea1999 "33"
local eb1999 "22"
local ec1999 "16"

local ea2000 "35"
local eb2000 "23"
local ec2000 "17"
  
local ea2001 "34"
local eb2001 "22"
local ec2001 "17" 
 
** Works for 1998-2001
foreach year of numlist 1998/2001 {
import excel "$SOI/`file`year''", clear cellra(`s`year'')
drop in 2/`ea`year''
drop in 3/`eb`year''
drop in 4/`ec`year''
drop in `d`year''
form A %25s
destring B-CY, replace ignore("*-," "[]")

keep `k`year''
gen year = `year'

tempfile int_`year'
save "`int_`year''", replace
}
*
use "`int_1998'", clear
append using "`int_1999'", force
append using "`int_2000'", force
append using "`int_2001'", force

recast str50 A, force
* RENAME VARS TO MATCH ACROSS YEARS
rename A category
rename B tot
rename C ag_t
rename G mining_t
rename H utilities_t
rename J construction_t
rename N manufact_t
rename O man_food
rename P man_beverage
rename Q man_textile
rename R man_apparel
rename T man_leather
rename U man_wood
rename V man_paper 
rename W man_printing
rename X man_petrol
rename Y man_chemicals
rename Z man_plastics
rename AA man_minerals
rename AC man_primary_metals
rename AD man_fab_metal
rename AE man_machines
rename AF man_comps
rename AG man_elec_components
rename AH man_trans
rename AI man_furniture
rename AJ man_misc
rename AM wholesale_t
rename AP retail_t
rename BF transport_t
rename BM information_t
rename BS finance_t
rename BY real_estate_t
rename CC prof_serv_t
rename CD management_t
rename CE support_t
rename CI education_t
rename CJ health_t
rename CN arts_t
rename CR accomodation_t
rename CU other_t

append using "`int1'", force
save "`int1'", replace


**** extra locals
local ea2002 "34"
local eb2002 "23"
local ec2002 "17" 

local ea2003 "34"
local eb2003 "23"
local ec2003 "17" 

local ea2004 "34"
local eb2004 "23"
local ec2004 "16" 

local ea2005 "34"
local eb2005 "23"
local ec2005 "17" 

local ea2006 "34"
local eb2006 "23"
local ec2006 "17" 

** Works for 2002-2006
foreach year of numlist 2002/2006 {
import excel "$SOI/`file`year''", clear cellra(`s`year'')
drop in 2/`ea`year''
drop in 3/`eb`year''
drop in 4/`ec`year''
drop in `d`year''
form A %25s
destring B-CZ, replace ignore("*-," "[]")

keep `k`year''
gen year = `year'

tempfile int_`year'
save "`int_`year''", replace
}
*
use "`int_2002'", clear
append using "`int_2003'", force
append using "`int_2004'", force
append using "`int_2005'", force
append using "`int_2006'", force

recast str50 A, force
* RENAME VARS TO MATCH ACROSS YEARS
rename A category
rename B tot
rename C ag_t
rename G mining_t
rename H utilities_t
rename I construction_t
rename N manufact_t
rename O man_food
rename P man_beverage
rename Q man_textile
rename R man_apparel
rename S man_leather
rename U man_wood
rename V man_paper 
rename W man_printing
rename X man_petrol
rename Y man_chemicals
rename Z man_plastics
rename AA man_minerals
rename AB man_primary_metals
rename AD man_fab_metal
rename AE man_machines
rename AF man_comps
rename AG man_elec_components
rename AH man_trans
rename AI man_furniture
rename AJ man_misc
rename AM wholesale_t
rename AQ retail_t
rename BG transport_t
rename BO information_t
rename BX finance_t
rename CC real_estate_t
rename CH prof_serv_t
rename CI management_t
rename CJ support_t
rename CM education_t
rename CO health_t
rename CS arts_t
rename CW accomodation_t
rename CZ other_t

append using "`int1'", force
save "`int1'", replace


**** extra locals
local ea2007 "32"
local eb2007 "21"
local ec2007 "16" 

local ea2008 "32"
local eb2008 "20"
local ec2008 "16" 

local ea2009 "32"
local eb2009 "20"
local ec2009 "16" 

local ea2010 "32"
local eb2010 "20"
local ec2010 "16" 

** Works for 2007-2010
foreach year of numlist 2007/2010 {
import excel "$SOI/`file`year''", clear cellra(`s`year'')
drop in 2/`ea`year''
drop in 3/`eb`year''
drop in 4/`ec`year''
drop in `d`year''
form A %25s
destring B-CR, replace ignore("*-," "[]")

keep `k`year''
gen year = `year'

tempfile int_`year'
save "`int_`year''", replace
}
*
use "`int_2007'", clear
append using "`int_2008'", force
append using "`int_2009'", force
append using "`int_2010'", force

recast str50 A, force
* RENAME VARS TO MATCH ACROSS YEARS
rename A category
rename B tot
rename C ag_t
rename G mining_t
rename H utilities_t
rename I construction_t
rename M manufact_t
rename N man_food
rename O man_beverage
rename P man_textile
rename Q man_apparel
rename R man_leather
rename S man_wood
rename T man_paper 
rename U man_printing
rename V man_petrol
rename W man_chemicals
rename X man_plastics
rename Y man_minerals
rename Z man_primary_metals
rename AA man_fab_metal
rename AB man_machines
rename AC man_comps
rename AD man_elec_components
rename AE man_trans
rename AF man_furniture
rename AG man_misc
rename AI wholesale_t
rename AM retail_t
rename BA transport_t
rename BH information_t
rename BO finance_t
rename BT real_estate_t
rename BX prof_serv_t
rename BY management_t
rename BZ support_t
rename CC education_t
rename CD health_t
rename CH arts_t
rename CK accomodation_t
rename CN other_t

append using `int1', force

******* Sorting by year and saving
gen id = _n
sort year id
drop id

*cleaning up category names 
replace category = upper(category)
replace category = subinstr(category, ".", "",.)
replace category = subinstr(category, "[2]", "",.)
replace category = subinstr(category, "[3]", "",.)
replace category = subinstr(category, "[5]", "",.)
replace category = subinstr(category, "[7]", "",.)
replace category = subinstr(category, "[25]", "",.)
replace category = subinstr(category, "RECAPTURE TAXES ", "RECAPTURE TAXES",.)
replace category = subinstr(category, ", TOTAL ", "",.)
replace category = subinstr(category, ", TOTAL", "",.)
replace category = subinstr(category, " TOTAL", "",.)
replace category = subinstr(category, "TOTAL ", "",.)
replace category = ltrim(category)
replace category = rtrim(category)
replace category = subinstr(category, " ", "_",.)
replace category = subinstr(category, "_SOURCE", "",.)
replace category = subinstr(category, "_ELECTRIC", "_ELE",.)

save "$SOI/aggregate_SOI_raw", replace

**** Cleaning up and aggregating industries
gen man_foodbevtob = 0 
replace man_foodbevtob = man_food if man_food != .
replace man_foodbevtob = man_foodbevtob + man_beverage if man_beverage != .
replace man_foodbevtob = man_foodbevtob + man_food_bev if man_food_bev != .
replace man_foodbevtob = man_foodbevtob + man_tobacco if man_tobacco != .
drop man_food man_beverage man_food_bev man_tobacco

gen man_comps_elec = 0
replace man_comps_elec = man_comps if man_comps != .
replace man_comps_elec = man_comps_elec + man_elec_components if man_elec_components != .
drop man_elec_components man_comps

replace man_trans = man_motor_vehicles + man_trans if man_motor_vehicle != .
replace man_trans = man_motor_vehicles if man_trans == .
drop man_motor_vehicles

* instruments go into other, machinery, and fabricate metals. Putting them in other for now
replace man_misc = man_instruments + man_misc if man_instruments != .
replace man_misc = man_instruments if man_misc == .
drop man_instruments

* aggregate fab metals, machinery, and misc
 gen man_mach_et_al = 0
 replace man_mach_et_al = man_misc if man_misc != .
 replace man_mach_et_al = man_mach_et_al + man_machines if man_machines != .
 replace man_mach_et_al = man_mach_et_al + man_fab_metal if man_fab_metal != .
 drop man_misc man_machines man_fab_metal

 ** aggregating other total industries (finance, real estate, insurance, and management)
 gen fin_real_t = 0
 replace fin_real_t = finance_t if finance_t != .
 replace fin_real_t = fin_real_t + real_estate_t if real_estate_t != .
 replace fin_real_t = fin_real_t + management_t if management_t != .
 replace fin_real_t = fin_real_t + fin_real_estate_management_t if fin_real_estate_management_t != .
 drop finance_t real_estate_t management_t fin_real_estate_management_t
 * all services
 gen service_t = 0
 replace service_t = all_services_t if all_services_t != .
 replace service_t = service_t + prof_serv_t if prof_serv_t != .
 replace service_t = service_t + information_t if information_t != .
 replace service_t = service_t + support_t if support_t != .
 replace service_t = service_t + education_t if education_t != .
 replace service_t = service_t + health_t if health_t != .
 replace service_t = service_t + arts_t if arts_t != .
 replace service_t = service_t + accomodation_t if accomodation_t != .
 replace service_t = service_t + other_t if other_t != .
 drop all_services_t prof_serv_t information_t support_t education_t health_t arts_t accomodation_t other_t
* transportation and public utilities
gen trans_ut_t = 0
replace trans_ut_t = utilities_t if utilities_t != .
replace trans_ut_t = trans_ut_t + transport_t if transport_t != .
drop transport_t utilities_t

save "$SOI/aggregate_SOI_long", replace

**** Reshape all 
use "$SOI/aggregate_SOI_long", clear 

order year cate 
global list "" 
foreach var of varlist tot-trans_ut_t { 
	rename `var' `var'_
	global list "$list `var'_"
} 
di "$list"
egen group = group(cat)
levelsof cat, local(cat_levels)

drop cat 
reshape wide tot-trans_ut_t , i(year) j(group)

foreach stub in $list { 
	*di "`stub'"
	forval i = 1/21 { 
	*di "`i'"
	capture:	rename `stub'`i' cat_`i'_`stub'
	}
} 

global list2 ""
	forval i = 1/21 { 
		global list2 "$list2 cat_`i'_"
	}
	
reshape long $list2 , i(year) j(variable) string 

forval i=1/21 {
	local var_n `: word `i' of `cat_levels''
	rename cat_`i'_ `var_n'
}

rename ALTERNATIVE_MINIMUM_TAX-US_POSSESSIONS_TAX_CREDIT, proper
replace Recapture_Taxes = Recapture_Of_Investment_Credit if year == 1994
* dropping redundant variables (regular tax is the same as Income_Tax with  
	// slightly different deductions and credits in 1994 and 1995
drop Recapture_Of_Investment_Credit Regular_Tax

label variable variable "Industry group or Total"
label variable Alternative_Minimum_Tax "AMT Taxes Paid Credit (1,000 USD)"
label variable Environmental_Tax "Environmental Tax Paid Credit (1,000 USD)"
label variable Foreign_Tax_Credit "Credit for Foreign Taxes Paid (1,000 USD)"
label variable General_Business_Credit "General Business Credit 12 Categories (1,000 USD)"
label variable Income_Subject_To_Tax "Net Income Subject to Income Tax (1,000 USD)"
label variable Income_Tax "Income Tax Liability Before Credits (1,000 USD)"
label variable Income_Tax_After_Credits "Income Tax Liability After Credits (1,000 USD)"
label variable Income_Tax_Before_Credits "Income Tax Liability Before Credits (includes related taxes, 1,000 USD)"
label variable Net_Income "Net Income (1,000 USD)"
label variable Nonconventional_Fuel_Credit "Nonconventional Fuel Credit (1,000 USD)"
label variable Number_Of_Returns "Number of Returns"
label variable Orphan_Drug_Credit "Orphan Drug Credit for Treating Rare Diseases (1,000 USD)"
label variable Personal_Holding_Company_Tax "Personal Holding Company Tax (1,000 USD)"
label variable Prior_Year_Minimum_Tax_Credit "Credit for Prior Year AMT (1,000 USD)"
label variable Qualified_Ele_Vehicle_Credit "Electric Vehicle Credit (1,000 USD)"
label variable Recapture_Taxes "Recapture of Investment Credit (1,000 USD)"
label variable Receipts "Gross Receipts (1,000 USD)"
label variable Taxes_Paid "All Other Taxes Paid (1,000 USD)"
label variable Us_Possessions_Tax_Credit "US Possessions Tax Credit (1,000 USD)"

* adding 3d NAICS codes
gen NAICS02 = " "
replace NAICS02 = "11" if variable == "ag_t_"
replace NAICS02 = "23" if variable == "construction_t_"
replace NAICS02 = "52, 53, 55" if variable == "fin_real_t_"
replace NAICS02 = "315" if variable == "man_apparel_"
replace NAICS02 = "325" if variable == "man_chemicals_"
replace NAICS02 = "334" if variable == "man_comps_elec_"
replace NAICS02 = "312-312" if variable == "man_foodbevtob_"
replace NAICS02 = "337" if variable == "man_furniture_"
replace NAICS02 = "316" if variable == "man_leather_"
replace NAICS02 = "332, 333, 334, 335, 339" if variable == "man_mach_et_al_"
replace NAICS02 = "327" if variable == "man_minerals_"
replace NAICS02 = "322" if variable == "man_paper_"
replace NAICS02 = "324" if variable == "man_petrol_"
replace NAICS02 = "326" if variable == "man_plastics_"
replace NAICS02 = "331" if variable == "man_primary_metals_"
replace NAICS02 = "323" if variable == "man_printing_"
replace NAICS02 = "313-314" if variable == "man_textile_"
replace NAICS02 = "336" if variable == "man_trans_"
replace NAICS02 = "321" if variable == "man_wood_"
replace NAICS02 = "31-33" if variable == "manufact_t_"
replace NAICS02 = "21" if variable == "mining_t_"
replace NAICS02 = "44-45" if variable == "retail_t_"
replace NAICS02 = "51, 54, 56, 61, 62, 71, 72, 81" if variable == "service_t_"
replace NAICS02 = " " if variable == "tot_"
replace NAICS02 = "22, 48-49" if variable == "trans_ut_t_"
replace NAICS02 = "42" if variable == "wholesale_t_"


save "$SOI/aggregate_SOI", replace
      

**** Make Simple Graph of Industries 
use "$SOI/aggregate_SOI", clear
replace variable = subinstr(variable,"_t_","",.)
replace variable = substr(variable,1,length(variable)-1) if substr(variable,length(variable)-2,length(variable)) == "_"


keep if year <=2006

*** Find largest ones 
preserve 
collapse (sum) Us , by(variable) 
sort Us
restore

twoway (scatter Us_Possessions_Tax_Credit year if variable == "tot_", connect(line) ) ///
	   (scatter Us_Possessions_Tax_Credit year if variable == "man_chemicals_", connect(line) ) ///
	   (scatter Us_Possessions_Tax_Credit year if variable == "man_instruments_ ", connect(line) ) ///	   
	   (scatter Us_Possessions_Tax_Credit year if variable == "man_food_bev_", connect(line) ) ///	   
	   (scatter Us_Possessions_Tax_Credit year if variable == "man_misc_", connect(line) ) ///	   	   
	   (scatter Us_Possessions_Tax_Credit year if variable == "man_beverage_", connect(line) ) ///	   	   
	   , xlab(1994(2)2006) ///
	   	graphregion(fcolor(white)) bgcolor(white) ///
		xtitle("Year") ///
		ytitle("Dollars", margin(medsmall)) ///
		legend(order(1 2 3 4 5 6) label(1 "Total")  label(2 "Chemical") ///
		label(3 "Instruments") label(4 "Food") label(5 "Misc.") label(6 "Beverage") rows(2) ) 
graph export "$output/Graphs/SOI_simple.pdf" , replace 

**** Make Graph of Sectors
use "$SOI/aggregate_SOI", clear
replace variable = subinstr(variable,"_t_","",.)
replace variable = substr(variable,1,length(variable)-1) if substr(variable,length(variable)-2,length(variable)) == "_"


keep if year <=2006
replace variable = "all_services" if variable == "service"
replace variable = "fin_real_estate_mgt" if variable == "fin_real"
replace variable = "transport" if variable == "trans_ut"
replace variable = "manufact" if variable == "manufact"
keep if inlist(variable,"wholesale","all_services","retail","fin_real_estate_mgt","transport","manufact","tot_","other")
replace Us = . if variable == "other"
keep if year <=2006
bys year: gen total = Us if variable == "tot_"
bys year: egen total2 = max(total)

* Do it once to get other
sort year  Us
bys year: gen graph_sum = sum(Us)
replace Us_Possessions_Tax_Credit = total2-(graph_sum-total2) if variable == "other"
drop total graph_sum total2

* Do it a second time to get  to get other
gen industry = 1 if variable == "manufact"
replace industry = 2 if variable == "transport"
replace industry = 3 if variable == "fin_real_estate_mgt"
replace industry = 4 if variable == "retail"
replace industry = 5 if variable == "all_services"
replace industry = 6 if variable == "wholesale"
replace industry = 7 if variable == "other"

drop if variable == "tot_"
gsort year industry
bys year: gen graph_sum = sum(Us)
bys year: egen total2 = max(graph_sum)
gen graph_sum_perc = 100*graph_sum/total2

twoway (area graph_sum_perc year if variable == "other")  ///
(area graph_sum_perc year if variable == "wholesale") /// 
(area graph_sum_perc year if variable == "all_services") ///
(area graph_sum_perc year if variable == "retail") ///
(area graph_sum_perc year if variable == "fin_real_estate_mgt") ///
(area graph_sum_perc year if variable == "transport") ///
(area graph_sum_perc year if variable == "manufact") ///
, xlab(1994(2)2006) ///
	   	graphregion(fcolor(white)) bgcolor(white) ///
		xtitle("Year") ///
		ytitle("Percent of Total", margin(medsmall)) ///
		legend(order(7 6 5 4 3 2 1) label(1 "Other")  label(2 "Wholesale") ///
		label(3 "Services")  label(4 "Retail") label(5 "Fin/Real Estate/Mgmt")  label(6 "Transport") ///
		label(7 "Manufacturing") rows(3) ) 
graph export "$output/Graphs/SOI_sector_perc.pdf" , replace 

twoway (area graph_sum year if variable == "other")  ///
(area graph_sum year if variable == "wholesale") /// 
(area graph_sum year if variable == "all_services") ///
(area graph_sum year if variable == "retail") ///
(area graph_sum year if variable == "fin_real_estate_mgt") ///
(area graph_sum year if variable == "transport") ///
(area graph_sum year if variable == "manufact")  ///
, xlab(1994(2)2006) ///
	   	graphregion(fcolor(white)) bgcolor(white) ///
		xtitle("Year") ///
		ytitle("Dollars", margin(medsmall)) ///
		legend(order(7 6 5 4 3 2 1) label(1 "Other")  label(2 "Wholesale") ///
		label(3 "Services")  label(4 "Retail") label(5 "Fin/Real Estate/Mgmt")  label(6 "Transport") ///
		label(7 "Manufacturing") rows(3) ) 
graph export "$output/Graphs/SOI_sector.pdf" , replace 		
		
**** Make Graph of Industries 
use "$SOI/aggregate_SOI", clear
replace variable = subinstr(variable,"_t_","",.)
replace variable = substr(variable,1,length(variable)-1) if substr(variable,length(variable)-2,length(variable)) == "_"


keep if year <=2006

drop if variable == "manufact"
drop if variable == "tot_"
replace variable = "man_trans_" if inlist(variable,"man_motor_vehicles_")
replace variable = "man_paper_" if inlist(variable,"man_printing_")
replace variable = "man_food_bev_" if inlist(variable,"man_food_","man_beverage_","man_tobacco_")
replace variable = "man_equipment_" if inlist(variable,"man_instruments_","man_elec_components_","man_machines_","man_comps_")
replace variable = "man_textile_" if inlist(variable,"man_textile_","man_leather_","man_apparel_")
replace variable = "other" if substr(variable,1,4) != "man_" 

collapse (sum) Us , by(year variable) 

*** Find largest ones 
preserve 
collapse (sum) Us , by(variable) 
gsort -Us
gen variable2 = variable
replace variable2 = "man_misc_" if _n >=10
gen group = _n 
replace group = 4 if group >= 10 
drop Us 
tempfile group 
save "`group'"
restore

merge m:1 variable using "`group'"
drop _merge 

replace variable2 = "man_equipment_" if variable2 == "man_mach_et_al_"
replace variable2 = "man_food_bev_" if variable2 == "man_foodbevtob_"
replace variable2 = "man_misc_" if variable2 == "man_comps_elec_"

collapse (sum) Us , by(year variable2 group)  //(first) variable2
sort year group

bys year (group): gen graph_sum = sum(Us)
bys year (group): egen total2 = max(graph_sum)
gen graph_sum_perc = 100*graph_sum/total2
drop if year <1995

twoway (area graph_sum_perc year if variable == "man_paper_") ///
(area graph_sum_perc year if variable == "man_plastics_") ///
(area graph_sum_perc year if variable == "man_trans_") ///
(area graph_sum_perc year if variable == "man_textile_") ///
(area graph_sum_perc year if variable == "other") ///
(area graph_sum_perc year if variable == "man_misc_") ///
(area graph_sum_perc year if variable == "man_food_bev_") ///
(area graph_sum_perc year if variable == "man_equipment_") /// 
(area graph_sum_perc year if variable == "man_chemicals_")  ///
, xlab(1995(2)2006) ///
	   	graphregion(fcolor(white)) bgcolor(white) ///
		xtitle("Year") ///
		ytitle("Percent of Total", margin(medsmall)) ///
		legend(order(9 8 7 6 5 4 3 2 1) label(9 "Chemical Man.")  label(8 "Equipment Man.") ///
		label(7 "Food/Bev Man.")  label(6 "Other Man.") label(5 "Other Non-Man.")  label(4 "Textile Man.") ///
		label(3 "Transport Man.") label(2 "Plastics Man.") label(1 "Paper Man.")  rows(3) ) 
graph export "$output/Graphs/SOI_industry_perc.pdf" , replace 

replace graph_sum = graph_sum/1000000
		
/* adjusting for inflation to put everything in 2017 dollars:
*/		
replace graph_sum = graph_sum * 245.1 / 152.4 if year == 1995
replace graph_sum = graph_sum * 245.1 / 156.9 if year == 1996
replace graph_sum = graph_sum * 245.1 / 160.5 if year == 1997
replace graph_sum = graph_sum * 245.1 / 163.0 if year == 1998
replace graph_sum = graph_sum * 245.1 / 166.6 if year == 1999
replace graph_sum = graph_sum * 245.1 / 172.2 if year == 2000
replace graph_sum = graph_sum * 245.1 / 177.1 if year == 2001
replace graph_sum = graph_sum * 245.1 / 179.9 if year == 2002
replace graph_sum = graph_sum * 245.1 / 184.0 if year == 2003
replace graph_sum = graph_sum * 245.1 / 188.9 if year == 2004
replace graph_sum = graph_sum * 245.1 / 195.3 if year == 2005
replace graph_sum = graph_sum * 245.1 / 201.6 if year == 2005

		
twoway (area graph_sum year if variable == "man_paper_") ///
(area graph_sum year if variable == "man_plastics_") ///
(area graph_sum year if variable == "man_trans_") ///
(area graph_sum year if variable == "man_textile_") ///
(area graph_sum year if variable == "other") ///
(area graph_sum year if variable == "man_misc_") ///
(area graph_sum year if variable == "man_food_bev_") ///
(area graph_sum year if variable == "man_equipment_") /// 
(area graph_sum year if variable == "man_chemicals_")  ///
, xlab(1995(2)2006) ///
	   	graphregion(fcolor(white)) bgcolor(white) ///
		xtitle("Year") ///
		ytitle("Billion of 2017 Dollars", margin(medsmall)) ///
		legend(order(9 8 7 6 5 4 3 2 1) label(9 "Chemical Man.")  label(8 "Equipment Man.") ///
		label(7 "Food/Bev Man.")  label(6 "Other Man.") label(5 "Other Non-Man.")  label(4 "Textile Man.") ///
		label(3 "Transport Man.") label(2 "Plastics Man.") label(1 "Paper Man.")  rows(3) )  ylab(0(1)5)
graph export "$output/Graphs/SOI_industry.pdf" , replace 


sum graph_sum_perc if year == 1995 & variable == "man_chemicals_"
local chem = round(r(mean))

sum graph_sum_perc if year == 1995 & variable == "man_food_bev_"
local fbt = round(r(mean)) - `chem'

local other = 100 - `fbt' - `chem'

sum graph_sum if year == 1995 & variable == "man_paper_"
local agg = round(r(mean),.1)

do "$analysis/jc_stat_out.ado"

jc_stat_out,  number(`chem') name("RESULTV") replace(1) deci(0) figs(2)
jc_stat_out,  number(`fbt') name("RESULTVI") replace(0) deci(0) figs(2)
jc_stat_out,  number(`other') name("RESULTVII") replace(0) deci(0) figs(2)
jc_stat_out,  number(`agg') name("RESULTVa") replace(0) deci(1) figs(3)

		
/*
twoway (line graph_sum year if variable == "man_paper_") ///
(line graph_sum year if variable == "man_plastics_") ///
(line graph_sum year if variable == "man_trans_") ///
(line graph_sum year if variable == "man_textile_") ///
(line graph_sum year if variable == "other") ///
(line graph_sum year if variable == "man_misc_") ///
(line graph_sum year if variable == "man_food_bev_") ///
(line graph_sum year if variable == "man_equipment_") /// 
(line graph_sum year if variable == "man_chemicals_")  ///
, xlab(1994(2)2006) ///
	   	graphregion(fcolor(white)) ///
		xtitle("Year") ///
		ytitle("Percent of Total") ///
		legend(order(9 8 7 6 5 4 3 2 1) label(9 "Chemical Man.")  label(8 "Equipment Man.") ///
		label(7 "Food/Bev Man.")  label(6 "Other Man.") label(5 "Other Non-Man.")  label(4 "Textile Man.") ///
		label(3 "Transpot Man.") label(2 "Transpot Man.") label(1 "Paper Man.")  rows(3) ) 
		*/
