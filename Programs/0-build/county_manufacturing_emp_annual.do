clear
set more off

// This file assembles annual employment in naics 31-33 at the pid level
// goal is two have a single dataset with 3 vars: pid, year, and manufacturing employment

clear all
set more off
*cd "/Users/mattpanhans/Dropbox (JC's Data Emporium)/personal_income_employment_data/QCEW"
*cd "/Volumes/Seagate Backup Plus/JC RA Work/QCEW/PR" 

local i = 1
tempfile inter
foreach year of numlist 1990/2012 {
di `year'

import delimited "$data/QCEW/raw_annual/`year'.annual.singlefile.csv", clear rowrange(1) colrange (1:12)

keep if industry_code == "31-33"

destring(area_fips), replace force

gen fips_state = floor(area_fips/1000)
gen fips_county = area_fips - fips_state*1000
drop if area_fips == .

drop disclosure_code

if `i' == 1 save "`inter'", replace
	
if `i' > 1 {
	append using "`inter'"
	save  "`inter'", replace
}
	
	local i = `i'+1

}

order fips_state fips_county year industry_code
drop if fips_county == 0 & fips_state != 72
drop if fips_state == 72 & fips_county != 0
keep if own_code == 5		// 0-total covered; 1 Federal; 2 State; 3 Local; 4 Intl Govt; 5 Private.
keep fips_state fips_county year industry_code annual_avg_estabs annual_avg_emplvl total_annual_wages
drop if fips_state == 78

gen pid = 1000*fips_state+fips_county

sort pid year industry_code

// Collapsing to place-year
collapse (sum) annual_avg_emplvl, by(pid year)
rename annual_avg_emplvl manu_emp


sort pid year 

save "$additional/county population estimates/county_manu_emp_ests.dta", replace
