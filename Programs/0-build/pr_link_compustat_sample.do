clear
set more off
snapshot erase _all

// this file makes a new pr_link that only uses firms from compustat
// author: dan (using code from matt and juan carlos)
// date: 3-12-2018

**** 3- Output descriptives and save dataset
use "$output_NETS/pr_extract_est.dta", clear

gen naic4 = floor(naics95/100)
replace naic4 = floor(naics95/10) if naic4 <1111
replace naic4 = floor(naics95) if naic4 <1111 
gen naic3 = floor(naics95/1000)
replace naic3 = floor(naics95/100) if naic3 <111
replace naic3 = floor(naics95/10) if naic3 <111
replace naic3 = floor(naics95) if naic3 <111
gen naic2 = floor(naics95/10000)
replace naic2 = floor(naics95/1000) if naic2 <11
replace naic2 = floor(naics95/100) if naic2 <11
replace naic2 = floor(naics95/10) if naic2 <11
replace naic2 = floor(naics95) if naic2 <11

drop if naic2 == 92 | naic3 == 813
* dropping USPS and federal government
drop if hqduns == 3261245 
* this is navy, drop
drop if hqduns == 161906193

rename hqduns95 hqduns

merge m:1 hqduns using "$WRDS/compustat_firm_hqduns.dta"
replace pr_link = 0 if _merge != 3 & pr_based != .  // getting rid of all links that aren't from compustat firms
drop _merge

rename hqduns hqduns95

gen industry_cd = .
replace industry_cd = 1 if naic3 == 311
replace industry_cd = 2 if naic3 == 314
replace industry_cd = 3 if naic3 == 315
replace industry_cd = 4 if naic3 == 325
replace industry_cd = 5 if naic4 == 3254
replace industry_cd = 6 if naic3 == 326
replace industry_cd = 7 if naic3 == 316
replace industry_cd = 8 if naic3 == 332
replace industry_cd = 9 if naic3 == 333
replace industry_cd = 10 if naic3 == 335
*replace industry_cd = 11 if naic3 == 
replace industry_cd = 12 if industry_cd == . & (naic2 == 31 | naic2 == 32 | naic2 == 33)
replace industry_cd = 13 if naic2 == 52 | naic2 == 53
replace industry_cd = 14 if naic2 == 54 | naic2 == 55 | naic2 == 61 | naic2 == 62 | naic2 == 71 | naic2 == 72 | naic2 == 81
replace industry_cd = 15 if naic2 == 42 | naic2 == 44 | naic2 == 45
replace industry_cd = 16 if industry_cd == .


label define l_naic3 1 "Food Mfg" 2 "Textile mill products" 3 "Apparel" 4 "Chemicals" 5 "Pharmaceuticals" ///
 6 "Rubber and Plastic" 7 "Leather" 8 "Fabricated metal" 9 "Machinery" 10 "Electrical equip" ///
 11 "Instruments" 12 "Other mfg" 13 "Finance, insurance, real estate" 14 "Services" ///
 15 "Wholesale and retail" 16 "Other non-mfg", replace

label values industry_cd l_naic3
 
gen total = 1

preserve
collapse (mean) pr_link (sum) total, by(fips95)
save "$output_NETS/pr_link_est_county_compu.dta", replace
restore

preserve
collapse (mean) pr_link (sum) total, by(fips95 industry_cd)
tsset fips95 industry_cd
tsfill, full
replace pr_link = 0 if pr_link == .
replace total = 0 if total == .
save "$output_NETS/pr_link_est_countyXindustry_compu.dta", replace
restore 

rename fips95 cty_fips
merge m:1 cty_fips using "$xwalk/cw_cty_czone.dta"

drop if _merge == 2
drop _merge

rename cty_fips fips95
rename czone czone95

preserve
collapse (mean) pr_link (sum) total, by(czone95)
save "$output_NETS/pr_link_est_czone_compu.dta", replace
restore

preserve
collapse (mean) pr_link (sum) total, by(czone95 industry_cd)
tsset czone95 industry_cd
tsfill, full
replace pr_link = 0 if pr_link == .
replace total = 0 if total == .
save "$output_NETS/pr_link_est_czoneXindustry_compu.dta", replace
restore 

gen total_hq = 0
replace total_hq = 1 if dunsnumber == hqduns95

preserve
drop if total_hq == 0
rename pr_link pr_linkhq
collapse (mean) pr_linkhq (sum) total_hq, by(fips95)
save "$output_NETS/pr_link_hq_county_compu.dta", replace
restore

preserve
drop if total_hq == 0
rename pr_link pr_linkhq
collapse (mean) pr_linkhq (sum) total_hq, by(fips95 industry_cd)
save "$output_NETS/pr_link_hq_countyXindustry_compu.dta", replace
restore 

// testing what percent of NETS employment in 3254 is PR linked

keep if industry_cd == 5
do "$analysis/jc_stat_out.ado"

sum emp95 if pr_link == 1, d
scalar pr_emp = r(mean)* r(N)

sum emp95, d
scalar tot_pharma_emp = r(mean)* r(N)

local percent_linked = pr_emp * 100 / tot_pharma_emp
jc_stat_out,  number(`percent_linked') name("RESULTpharmaI") replace(0) deci(1) figs(4)
