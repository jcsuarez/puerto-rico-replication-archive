***************************************************************
//Filename: pr_treat_taxhaven_clean.do
//Author: eddie yu
//Date: 2023-09-20
//Task: With compu_NETS_ETR_sizecutoff.dta, we construct PR treatment
//		variables data and tax haven usage data
***************************************************************

clear all
set more off
snapshot erase _all 


***************************************************************
***************************************************************
********** Same as cleaning part of figure5_clean.do **********
***************************************************************
***************************************************************

use "$data/Replication Data/interim/compu_NETS_ETR_sizecutoff.dta", replace

***************************************************************
***************************************************************
******************** PR selection variables *******************
***************************************************************
***************************************************************

// doing a cross section of active firms with positive assets
keep if at > 0 & at != .

// using 4d naics controls
gen n4 = substr(naicsh,1,4)

gen pharma = (n4 == "3254")

replace ads = 0 if ads == .
replace rd  = 0 if rd  == .
replace dltt= 0 if dltt== . 

// winsorizing rd/assets and dltt/assets
replace rd = rd / at
qui sum rd, d
replace rd = r(p99) if rd >r(p99)
replace ads = ads / at
qui sum ads, d
replace ads = r(p99) if ads >r(p99)
replace dltt = dltt / at
qui sum dltt, d
replace dltt = r(p99) if dltt >r(p99)

gen any_ads = ads > 0
gen any_rd  = rd  > 0

gen gpop = gp / (at)
gen PRexp = emp_PR / (emp_PR + emp_US)

gen prof = (ni > 0) * (ni != .)

* standardize variables
foreach i of var etr-nol btd etr_change gpop PRexp at logAT dltt ni {
qui sum `i', d
gen mean`i' = r(mean)
gen sd`i' = r(sd)
replace `i' = (`i' - mean`i') / sd`i'
drop mean`i' sd`i'
}


* PR selection variables
keep gvkey capex capex_base year n2 PR fs_capx cfor_capx revts at gp intan logAT ///
ppent ppe tot_ias capxs capex capx emps nis n3 PR_post post IK idbflag fic ftax ftax_1 pi ///
pharma etr-nol any_* btd etr_change gpop PRexp at dltt ni exp_sec exp_ind ppe95* ppe*

save "$data/Replication Data/interim/pr_treatment", replace


***************************************************************
***************************************************************
*************** Dyreng Tax haven usage variables **************
***************************************************************
***************************************************************

***************************************************************
* dyreng data
***************************************************************
use  "$additional/Dyreng_Tax_Havens/country31may2015.dta", replace
rename ciknumber cik
gen year = year(datadate)

gen pr_country = (country == "Puerto Rico") * countrycount
gen ir_country = (country == "Ireland") * countrycount
gen ba_country = (country == "Barbados") * countrycount
gen hk_country = (country == "Hong Kong") * countrycount
gen sp_country = (country == "Singapore") * countrycount
gen ci_country = (country == "Cayman Islands") * countrycount
gen sw_country = (country == "Switzerland") * countrycount

*** generating set of new countries of operation and new "taxhaven" mentions
bys cik country (year): gen new_country = _n
replace new_country = 0 if new_country > 1

gen largehaven = 0
replace largehaven = (taxhaven == 1) if inlist(country,"Ireland","Barbados","Hong Kong","Singapore","Cayman Islands","Switzerland")
bys cik country (year): gen new_haven = _n
replace new_haven = 0 if new_haven > 1 | taxhaven == 0
bys cik country (year): gen new_lhaven = _n
replace new_haven = 0 if new_lhaven > 1 | largehaven == 0

collapse (mean) totalcount (sum) *_country taxhaven new_haven new_lhaven (mean) ncountries, by(year cik)
tempfile dyreng_taxhaven
save "`dyreng_taxhaven'", replace


***************************************************************
* merge to ETR_compustat_Segments - all sample (not narrow comp)
***************************************************************
use "$data/Replication Data/interim/compu_NETS_ETR.dta", replace

destring cik, replace
drop if cik == .
drop if gvkey == 15403

merge 1:1 cik year using  "`dyreng_taxhaven'"

*** looking at firms in PR as of 1995
replace emp_PR = 0 if emp_PR == .

drop if _merge == 2 // drop unmatched dyreng variable data

foreach Q of var pr_country ir_country ba_country hk_country sp_country ci_country sw_country {
replace `Q' = 0 if `Q' == .
}
gen pr_mention = pr_country > 0
gen ir_mention = ir_country > 0
gen ba_mention = ba_country > 0
gen hk_mention = hk_country > 0
gen sp_mention = sp_country > 0
gen ci_mention = ci_country > 0
gen sw_mention = sw_country > 0

replace totalcount = 0 if totalcount == .
replace ncountries = 0 if ncountries == .
replace taxhaven = 0 if taxhaven == .
gen th_mention = taxhaven > 0 

gen any_new_haven = (new_haven > 0) * (new_haven != .)
gen any_new_lhaven = (new_lhaven > 0) * (new_lhaven != .)
gen any_new_country = (new_country > 0) * (new_country != .)

// bys cik: egen min_year = min(year)
// bys cik: egen max_year = max(year)
// * Run only on data until 2012 
// keep if year < 2013
// *keep if year > 1993
// * Run only on firms wth obs before and after 1996 
// keep if min_year < 1996 & max_year > 1996




// // getting to data needed to PNP version:
// keep gvkey capex capex_base year n2 PR fs_capx cfor_capx revts at gp intan logAT ///
// ppent ppe tot_ias capxs capex capx emps nis n3 PR_post post IK gp idbflag fic ftax ftax_1 pi ///
// ncountries *_mention taxhaven any_* new_*  exp_sec exp_ind ppe95* ppe* PR post


** variable for mentioning any haven
gen anyhaven = taxhaven > 0
replace anyhaven = 0 if taxhaven == .


save "$data/Replication Data/interim/tax_haven", replace

