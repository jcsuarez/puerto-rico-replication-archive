* Import additional county-level data from various sources

* Matt Panhans, May 2017

cd "$additional"

******************************************************************
* County-level demographics from 1990 Census (via Social Explorer)
******************************************************************

/* 
* TODO: 1. Place the .txt data file and the dictionary file you downloaded in the work folder, or enter the full path to these files!
*       2. You may have to increase memory using the 'set mem' statement. It is commented out in the code bellow.
*
* If you have any questions or need assistance contact info@socialexplorer.com.
*/

// Downloaded from Social Explorer by MP, May 2017.
// Data is from 1990 Census.

///set mem 512m
clear

set more off
infile using "county_level_demographics.dct", using("county_level_demographics_raw.txt")

format NAME %24s
format QName %24s
format FIPS %14s

destring(FIPS), gen(fips_county)

rename T012_001 population

gen pct_white = T012_002/population
gen pct_black = T012_003/population
label var pct_white "% white"
label var pct_black "% black"

gen pct_college = T022_005/T022_001
label var pct_college "% with Bachelor's degree"

gen pct_less_HS = T022_002/T022_001 
label var pct_less_HS "% with less than H.S. degree"

gen pct_unemp = T025_007/T025_003
label var pct_unemp "% Unemployed"

gen labor_participation_rate = T025_003/T025_001 
label var labor_participation_rate "Labor force participation rate"

gen pct_manufacturing_nondurable = T038_005/T038_001
label var pct_manufacturing_nondurable "% of employment in Manufacturing, nondurable goods"
gen pct_manufacturing_durable = T038_006/T038_001
label var pct_manufacturing_durable "% of employment in Manufacturing, durable goods"
gen pct_construction = T038_004/T038_001
label var pct_construction "% of employment in Construction"
gen pct_agriculture = T038_002/T038_001
label var pct_agriculture "% of employment in Agriculture"
gen pct_wholesale = T038_009/T038_001
label var pct_wholesale "% of employment in Wholesale trade"
gen pct_retail = T038_010/T038_001
label var pct_retail "% of employment in Retail trade"

keep NAME QName fips_county STATE COUNTY population pct_white pct_black pct_college ///
	pct_less_HS pct_unemp labor_participation_rate pct_manufacturing_nondurable ///
	pct_manufacturing_durable pct_construction pct_agriculture pct_wholesale ///
	pct_retail
 
save county_level_demographics.dta, replace



******************************************************************
* Impact of Imports on Local Labor Markets: Autor, Dorn, Hanson China Shock
******************************************************************

* Import China shock variable from Autor, Dorn, and Hanson.

* Input files: workfile_china_preperiod.dta

clear

use daviddorn/Autor-Dorn-Hanson-ChinaSyndrome-FileArchive/dta/workfile_china_preperiod.dta, clear

keep d_tradeusch_pw d_tradeotch_pw_lag timepwt48 czone statefip yr

keep if yr == 1990
* NOTE: ADH paper regressions use [aw=timepwt48]. For example:
* ivregress 2sls d_sh_empl_mfg (d_tradeusch_pw=d_tradeotch_pw_lag) [aw=timepwt48] if yr==1990, cluster(statefip)


label var d_tradeusch_pw "Import exposure Per Worker"
label var d_tradeotch_pw_lag "Import exposure Per Worker to non-US (IV)"
label var timepwt48 "Weight"


** Crosswalk from czone to county
merge 1:m czone using daviddorn/cw_cty_czone.dta

keep if _merge == 3
drop _merge

rename cty_fips fips_county
save import_dorn_chinashock.dta, replace

******************************************************************
* Share routine labor: Autor and Dorn (2013)
******************************************************************
* Import routine labor share from Autor and Dorn (2013)
* Input files: workfile2012.dta
clear

use daviddorn/Autor-Dorn-LowSkillServices-FileArchive/dta/workfile2012.dta, clear

keep l_sh_routine33a d_rpc czone statefip yr
keep if yr == 1990

label var l_sh_routine33a "Share Routine Labor, 1990"
label var d_rpc "Adjusted PCs per Employee, 1990"

** Crosswalk from czone to county
merge 1:m czone using daviddorn/cw_cty_czone.dta

keep if _merge == 3
drop _merge

rename cty_fips fips_county
save import_dorn_routinelabor.dta, replace

******************************************************************
* State-level minimum wage data from Meer and West
******************************************************************

use jonmeer/data_code/qcew.dta, clear
 
keep if industry == 10 // All ages in all industries in all sectors and ownership
keep if year == 1990 & quarter == 1

keep statefips realmw lnmin 
 
save import_meer_qcew.dta, replace
 
 
******************************************************************
* State-level union laws from NBER
****************************************************************** 
use publaw.dta, clear

keep if year == 84		// 1984 is most recent complete year

keep if group == 1		// state employees group

keep state rgtowork

rename state num

preserve
use FIPStoStatecrosswalk.dta, clear
drop if state == "DC" | state == "PR"
replace num = num - 1 if num > 9
tempfile fips_cw
save `fips_cw'
restore

merge 1:1 num using `fips_cw'
drop _merge
drop num

rename fipsstate statefips

save imported_publaw.dta, replace


******************************************************************
* Total Capital Stock (BEA)
******************************************************************
cd "$qcewdata/raw_annual"
* Load 1990 QCEW data
import delimited 1990.annual.singlefile.csv, clear rowrange(1) colrange (1:12)

keep if own_code == 5		// keep private only (drops local, state, federal govt)

destring(area_fips), replace force
gen fips_state = floor(area_fips/1000)

gen county = area_fips - fips_state*1000	// drop state level observations
drop if county == 0
drop county

rename area_fips fips_county

drop if fips_county == .
drop disclosure_code

keep if industry_code=="11" | industry_code=="23" | industry_code=="31-33" ///
 | industry_code=="42" | industry_code=="44-45" ///
 | industry_code=="48-49" | industry_code=="51" | industry_code=="52" ///
 | industry_code=="53" | industry_code=="54" | industry_code=="55" | industry_code=="56" ///
 | industry_code=="61" | industry_code=="62" | industry_code=="71" | industry_code=="72" ///
 | industry_code=="81"

rename industry_code naic2
keep fips_county naic2 annual_avg_emplvl

tempfile qcew_temp
save "`qcew_temp'", replace


* Load 1990 BEA National Capital Stock data.
cd "$additional"
import delimited "BEA_Table_3-1ES.csv", clear rowrange(5:83) varnames(5)
rename v3 capital_stock_ind
label var capital_stock_ind "Dollars (Billions)"
rename v2 industry

keep if inlist(line,1,2,59,10,11,12,24,33,34,35,44,49,55,58,62,63,66,67,72,75,78)

gen naic2 = ""
replace naic2 = "11" if line == 2	// Agriculture
replace naic2 = "23" if line == 10	// Construction
replace naic2 = "31-33" if line == 11	// Manufacturing
replace naic2 = "42" if line == 33	// Wholesale
replace naic2 = "44-45" if line == 34	// Retail
replace naic2 = "48-49" if line == 35	// Transportation
replace naic2 = "51" if line == 44	// Information
replace naic2 = "52" if line == 49	// Finance and Insurance
replace naic2 = "53" if line == 55	// Real Estate
replace naic2 = "54" if line == 58	// Professional services
replace naic2 = "55" if line == 62	// Management
replace naic2 = "56" if line == 63	// Waste Management
replace naic2 = "61" if line == 66	// Educational services
replace naic2 = "62" if line == 67	// Health care
replace naic2 = "71" if line == 72	// Arts, recreation
replace naic2 = "72" if line == 75	// Accommodation and food services
replace naic2 = "81" if line == 78	// Other, except govt

drop if naic2 == ""

merge 1:m naic2 using "`qcew_temp'"
keep if _merge == 3
drop _merge

bysort industry: egen total_ind_emp = sum(annual_avg_emplvl)
gen county_share_ind_emp = annual_avg_emplvl/total_ind_emp
gen county_ind_capital_stock = capital_stock_ind*county_share_ind_emp

collapse (sum) county_ind_capital_stock, by(fips_county)

rename county_ind_capital_stock capital_stock
label var capital_stock "Dollars (Billions)"

save county_capital_stock, replace


******************************************************************
* Merge together all the datasets
****************************************************************** 

use county_level_demographics.dta, clear

merge 1:1 fips_county using import_dorn_chinashock.dta
drop _merge

rename statefip statefips

merge 1:1 fips_county using import_dorn_routinelabor.dta
drop _merge
drop statefip

merge 1:1 fips_county using county_capital_stock.dta
drop _merge

merge m:1 statefips using import_meer_qcew.dta
drop _merge

merge m:1 statefips using imported_publaw.dta
drop _merge


drop if fips_county == .

save county_level_data_all.dta, replace
