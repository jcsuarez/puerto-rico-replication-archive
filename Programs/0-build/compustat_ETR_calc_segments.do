***************************************************************
//Filename: compustat_ETR_calc_segments.do
//Author: eddie yu
//Date: 2023-09-20
//Task: With newly pulled Compustat Historical Segments data pulled 2023-07-25
//		(WRDS/compu_segments.dta) and conducting compustat_merge_manual.do,
//		we construct the following dataset in $WRDS/ : 
//        - ETR_compustat_Segments.dta
//		  - ETR_compustat_1992.dta
//		  - All_compu_cusip.txt
//		  - PR_compu_cusip
***************************************************************
clear
capture log close
set more off
capture close
snapshot erase _all

*********************************
* starting with the manual compustat nets merge
*********************************
use "$WRDS/dyreng_compustat_1990-2012.dta", replace // DOWNLOADED 1-4-2018
rename fyear year 
destring gvkey, replace
tempfile dyreng
save "`dyreng'"

*use "$WRDS/compustat_full_base.dta", replace
use "$WRDS/compustat_merge_manual.dta", clear // INCLUDES HISTORICAL SEGMENTS
*IF YOU DON'T WANT TO DO EVENT REGRESSIONS: keep prePR company gvkey year
*drop cik
tostring cik, replace
merge 1:1 gvkey year using "`dyreng'"

* Making sure prePR is constant within GVKEY (note that no "1s" are added)
drop prePR
rename PR prePR
replace prePR = 0 if prePR == .
bysort gvkey: egen PR = max(prePR) 

**** Defining variables
xtset gvkey year
	* Effective tax rate
	gen etr = txpd / pi
	replace etr = 1 if etr > 1
	replace etr = 0 if etr < 0
	label var etr "Cash ETR"
	
	* Multinational indicator
	gen MNE = (pifo > 0) + (txfo > 0)
	replace MNE = 1 if MNE > 1
	label var MNE "Multinational"
	
	*log assets
	gen logAT = ln(at)
	label var logAT "Ln(Assets)"
	
	* r&d / sales, advertising / sales
	replace xrd = 0 if xrd == .
	replace xad = 0 if xad == .
	gen rd = xrd / sale
	gen ads = xad / sale
	label var rd "Research"
	label var ads "Advertising"
	
	* plants, property and equipment %, intangibles %, leverage
	gen ppe = ppent / at
	gen intang = intan / at
	replace dltt = 0 if dltt == .
	replace dlc = 0 if dlc == .
	gen lev = (dltt + dlc) / at
	label var ppe "Property Plants and Equipment"
	label var intang "Intangibles"
	label var lev "Leverage"
	
	* percent of capital investment
	gen capex = capx / ppent
	label var capex "Capital Expenditures"
	
	* special items as a percent of assets
	gen spec = spi / at
	label var spec "Special Items"
	
	* net operating loss at beginning of year indicator
	gen nol = tlcf > 0
	gen dnol = (tlcf - l.tlcf) / at
	label var nol "Net Operating Loss"

* sample cleaning from Dyreng et al. **********
// assets <10mill
// nonmissing TXPD and PI 
// nonnegative PI
// (NOT CURRENTLY USED) must have 5 years of data

//keep if at > 10
//keep if pi != . & txpd != .
//keep if pi > 0
//drop if logAT == . | rd == . | ads == . | ppe == . | intang == . | capex ==. | spec == . | nol == .

	* generating the book tax difference from desai and dharmapala
	gen btd = (txfed - pidom * .35) / at // Note that this is only for domestic taxes
	label var btd "Book Tax Difference"

	drop _merge
	
* Saving the data with ETR variables

bysort gvkey: egen first_y = min(year)
gen keeper = (first_y == year) * (year > 1992) 
replace keeper = 1 if year == 1992

gen change = (first_y == year) * (year > 1997) 
replace change = 1 if year == 1997
gen etr_1 = etr * keeper
gen etr_2 = etr * change
bysort gvkey: egen etr1 = max(etr_1)
bysort gvkey: egen etr2 = max(etr_2)
gen etr_change = etr2 - etr1

save "$WRDS/ETR_compustat_Segments.dta", replace


* Saving the data with ETR variables (just 1992 for event study)
* 	1992 is missing a huge number of firms that have stock returns, so I 
* 	save the minimum year > 1992 for firms without data in 1992
	* also taking a change in ETR from keeper to 1997 or later


keep if keeper == 1

	*making cusips match crsp data (uses first 8 digits)
	replace cusip = substr(cusip,1,8) // BETTER TO MATCH ON NCUSIP
	
duplicates drop cusip, force

save "$WRDS/ETR_compustat_1992.dta", replace
	
export delimited cusip using "$WRDS/All_compu_cusip.txt" ,  delimiter(" ") replace novar
export delimited cusip using "$WRDS/PR_compu_cusip.txt" if prePR == 1,  delimiter(" ") replace novar
