// Cleaning raw data imported as txt from word
// data come from pdf of Mian and Sufi
// date: 2-27-2018

clear
snapshot erase _all

import delimited "$additional/tradable definition/raw_trade_cats_4dNAICS.csv", clear

replace v1 = subinstr(v1, "`=char(9)'", "",.)
replace v1 = ltrim(v1)
replace v1 = rtrim(v1)
gen naics =  substr(v1,1,3)

gen nontrade = strpos(v1, "non-trad")
gen trade = strpos(v1, "tradable")
gen other = strpos(v1, "other")
gen cons = strpos(v1, "construction")

gen tradability = 1
replace tradability = 2 if nontrade > 0
replace tradability = 3 if other > nontrade & other > trade & other > cons
replace tradability = 4 if cons > nontrade & cons > other & cons > trade

keep naics tradability

label define tradability 1 yes 2 no 3 other 4 construction

gen has_yes = tradability == 1
gen has_no = tradability == 2
gen has_other = tradability == 3
gen has_const = tradability == 4

collapse (mean) has_* , by(naics)

save "$additional/tradable definition/trade_cats_3dNAICS", replace
